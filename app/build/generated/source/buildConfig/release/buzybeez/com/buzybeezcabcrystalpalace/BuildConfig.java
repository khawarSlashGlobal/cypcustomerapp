/**
 * Automatically generated file. DO NOT MODIFY
 */
package buzybeez.com.buzybeezcabcrystalpalace;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "buzybeez.com.buzybeezcabcrystalpalace";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 5;
  public static final String VERSION_NAME = "1.4";
}
