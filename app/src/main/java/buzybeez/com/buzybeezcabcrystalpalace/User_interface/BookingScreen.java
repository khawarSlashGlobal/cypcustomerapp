package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccountCollection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import buzybeez.com.buzybeezcabcrystalpalace.Helpers.CenterZoomLayoutManager;
import buzybeez.com.buzybeezcabcrystalpalace.DataCenter.DataHolder;
import buzybeez.com.buzybeezcabcrystalpalace.FireBase.CustomerVerfication;
import buzybeez.com.buzybeezcabcrystalpalace.Helpers.CarSelectionAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Helpers.CarlistAdaptor;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.GetBookingListener;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.DirectionFinder;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.DirectionFinderListener;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.Distance;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.Duration;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.Route;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.TimeToDespatcInterface;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Booking;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Car;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CfgCustApp;
import buzybeez.com.buzybeezcabcrystalpalace.Models.DriverLoc;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.socket.client.Ack;

import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.APP_OFFICE_NAME;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.FIXED_OFFICE_KEY;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.SHARE_RIDE_TEXT;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.fareList;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.fromtoviaList;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.isAirportAtDest;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.isAirportAtOri;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.originlat;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.originlon;

public class BookingScreen extends BaseActivity implements
        ConnectionListener, OnMapReadyCallback, DirectionFinderListener, TimeToDespatcInterface {
    String  timetoDispatch;
    //boolean bookedForFuture;
    //for date picker
    private static final String TAG = "BookinScreen";
    public static String selectedPickup, selectedDropoff;
    public static String officeAddress;
    public static boolean flag;
    public static boolean ASC = true;
    public static boolean DESC = false;
    public static String jobID = "";
    public static String customerRoom = "";
    public static String jobRef = "";
    //total duration and dsitance
    public static double totalDuration = 0;
    //dictionary to store fixed fare values
    //public static Map<String, List<Double>> fareList;
    // create list to store multiple values against one key of fixed fare

    public static double totalDistance = 0;
    static boolean status = false;
    public TextView time_estimation;
    public TextView journey_miles;
    public TextView flight_nmberId;
    public EditText instruction;
    public Button cancelButton;
    public String cflight = "";
    public String curentDate;
    public String date;
    public String address;
    public ConstraintLayout selectedCarLayout, RecyclerViewCL;
    public TextView selectedCarName, selectedPassen, selectedSuitcase, journeyMiles, journeyDuration;
    public RecyclerView mRecyclerViewCars;
    public RecyclerView.Adapter mAdapterCars;
    Route route;
    //for car fare selection
    double[] fareCalc;
    double[] mpvfareCalc;
    double fareSaloon;
    //for flight and child seat
    SwitchCompat switch1;
    Button moreOptionDoneId;
    String comment;
    double[] officekey;
    //carlist view variable
    //ListView list;
    String[] itemname = {
            "Saloon",
            "Estate",
            "MPV",
            "Executive",
            "8 Passenger",
    };
    Integer[] imgid = {
            R.drawable.saloonimage,
            R.drawable.estateimage,
            R.drawable.mpvimage,
            R.drawable.saloonimage,
            R.drawable.eightpassenger,

    };
    Context context;
    LatLngBounds bound;
    Handler handler;
    PolylineOptions polylineOptions;
    CarlistAdaptor adapter;
    ImageView selectedCarIV;
    ArrayList<Car> carlist = new ArrayList<>();
    ArrayList<DriverLoc> allNearDrivers = new ArrayList<>();
    private GetBookingListener ref;
    //booking screen variables
    private ImageView more_option;
    private TextView journeyFare;
    private Button completebooking;
    private ImageButton myLocationButton;
    //map variables
    private GoogleMap mMap = null;
    //map routes variable
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    //for date time selection
    private TextView booking_date;
    private TextView timeSelection;
    private Button scheduleDone;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private double log;
    private double lat;
    private ConstraintLayout layout;
    private ProgressDialog progressDialog;
    TextView timeAvailability;
    Gson gson;
    String json;
    ExternalAccountCollection customer;
    ArrayList<String> cardCheckId;
    private ImageView imagePayment;
    private ImageView imageViewArrow;
    private TextView paymentMethod;


    public static boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;

        } else {
            return Settings.System.getInt(c.getContentResolver(), Settings.System.TIME_12_24, 0) == 1;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_screen);
        Log.e("fromtovia",String.valueOf(fromtoviaList.size()));
        carlist.add(new Car("Saloon", "4", "1", R.drawable.saloonimage, true));
        carlist.add(new Car("Estate", "4", "2", R.drawable.estateimage, false));
        carlist.add(new Car("MPV", "5", "3", R.drawable.mpvimage, false));
        carlist.add(new Car("Executive", "3", "1", R.drawable.saloonimage, false));
        carlist.add(new Car("8 Passenger", "8", "4", R.drawable.eightpassenger, false));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        intializebooking_ids();

        targetlocationonClick();

        carList();

        sendbooking();

        nowBookingDateTime();

        snackbarForConnectivityStatus();

        checkInternetDisconnect();

        context = getApplicationContext();
        //  getAllAvalibleDrivers("S", originlat, originlon);
    }

    //skh: this method takes originm, lat lon and fetches all the clear drivers from the
    //server.
    public void getAllAvalibleDrivers(final String carsymbol, final double lat, final double lng) {

        if (SocketEvent.sharedinstance.socket == null) {
            SocketEvent.sharedinstance.initializeSocket();
        }
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(getApplicationContext())) {

                    timer.cancel();

//                    let table = Config.sharedInstance.table_driverCheckVerify
//
//                    let filter:[String:Any] = ["drvuid":drvuid]
//
//                    let projection:[String:Any] = ["graceperiod":1,"_id":0]
//
//                    let sort:[String:Any] = ["datetime":-1]
//
//
//                    let query:[Any] = [table,filter,projection,sort]

//                    'isvirtual': false,
//                            'vtype': vquery,
//                            'lstate': 'CLR',
//                            'loc': {
//                        $near: {
//                            $geometry: {
//                                'type': 'Point',
//                                        'coordinates': [fromLon, fromLat]
//                            },
//                            $maxDistance: distance
//                        }
//                    },
//                    'state': 'Clear'
                    List<Double> fromLatLng = new ArrayList<>();
                    fromLatLng.add(lat);
                    fromLatLng.add(lng);
                    JSONArray query = new JSONArray();
                    JSONObject filter = new JSONObject();
                    JSONObject projection = new JSONObject();
                    JSONObject sort = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    JSONObject loc = new JSONObject();

//                    Loc loccc = new Loc();
//                    loccc.setType("Point");
//                    loccc.setCoordinates(fromLatLng);
                    query.put("Driverloc");
                    try {
                        String data = "{$near: {$geometry: {type: \"Point\"  ,coordinates: [" + lat + "," + lng + "]},$maxDistance: 3218}";

                        jsonArr.put(lat);
                        jsonArr.put(lng);
                        loc.put("type", "Point");
                        loc.put("coordinates", jsonArr);
                        filter.put("isvirtual", false);
                        filter.put("vtype", carsymbol);
                        filter.put("lstate", "CLR");
                        // JSONObject kkk = new JSONObject(data);
                        filter.put("loc", data);
                        filter.put("state", "Clear");
                        query.put(filter);
                        query.put(projection);
                        query.put(sort);

                        SocketEvent.sharedinstance.socket.emit("getSpecificData", query, new Ack() {

                            @Override
                            public void call(final Object... args) {
                                if (args[0] != null && !args[0].toString().equals("0")) {
                                    try {
                                        JSONArray jsonArray = new JSONArray(args[0].toString());
//                                        for (int i = 0; i < jsonArray.length(); i++) {
//                                            JSONObject booking = jsonArray.getJSONObject(i);
//                                            JsonNode jsonNode = JSONParse.convertJsonFormat(booking);
//                                            ObjectMapper mapper = new ObjectMapper();
//                                            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//                                            Airports.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), Airports.class);
//                                            Airports a = Airports.sharedinstance;

                                        // }
                                    } catch (Exception e) {
                                        e.getMessage();
                                    }
                                }
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

        }, 0, 1000);


    }

    public void intializebooking_ids() {

        selectedCarLayout = (ConstraintLayout) findViewById(R.id.selectedCarCL);
        RecyclerViewCL = (ConstraintLayout) findViewById(R.id.RecyclerViewCL);
        selectedCarName = findViewById(R.id.selectedCarName);
        selectedPassen = findViewById(R.id.selectedPassengerNo);
        selectedSuitcase = findViewById(R.id.selectedSuitcaseNo);
        selectedCarIV = findViewById(R.id.selectedCarIV);
        journeyMiles = findViewById(R.id.distanceID);
        journeyDuration = findViewById(R.id.durationID);
        paymentMethod = findViewById(R.id.textViewPayment);
        imagePayment = findViewById(R.id.imageViewPayment);
        imageViewArrow = findViewById(R.id.imageViewArrow);
        more_option = (ImageView) findViewById(R.id.moreOptions);
        journeyFare = (TextView) findViewById(R.id.fare);
        completebooking = (Button) findViewById(R.id.gobooking);
        layout = findViewById(R.id.linearLayout);
        timeAvailability = findViewById(R.id.timeavailability);
        cardCheckId = new ArrayList<>();
    }

    public void targetlocationonClick() {

        myLocationButton = (ImageButton) findViewById(R.id.targetlocation);

        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bound != null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bound, 100));
                }
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        storeLatLong();

        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        LatLng currentLocation = new LatLng(latitude, longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));

        float f = 15;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, f));

        sendRequest();
    }

    private void sendRequest() {

        flag = true;

        selectedPickup = getIntent().getStringExtra("pickupValue");
        Log.e("pickupValue", selectedPickup);
        selectedDropoff = getIntent().getStringExtra("dropoffValue");


        if (selectedPickup == null) {
            Toast.makeText(this, "Please enter origin address!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (selectedDropoff == null) {
            Toast.makeText(this, "Please enter destination address!", Toast.LENGTH_SHORT).show();
            return;
        }

//Here
//		wayPoints.add("FK11 7ES,UK");
//		wayPoints.add("FK9 4LA, Stirling, UK");

        //wayPoints.add("Lexington,MA");
        try {
            new DirectionFinder(this, selectedPickup, fromtoviaList, selectedDropoff, flag).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDirectionFinderStart() {
//        progressDialog = ProgressDialog.show(this, "Please wait.",
//                "Wait..!", true);
        progressDialog = new ProgressDialog(BookingScreen.this, R.style.MyDialogTheme);
        progressDialog.setTitle("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setMessage("finding routes ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();


        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }


    }



    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {

        // progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();
        Polyline p1=null;

        for (final Route route : routes) {
            //   mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation.get(0), 16));

            //calculate total duration & time
            for (Duration d : route.duration) {
                totalDuration = totalDuration + d.value;
            }

            //converting sec to min, 1 sec = 0.0166667 min
            totalDuration = totalDuration * 0.0166667;
            totalDuration = round(totalDuration, 1);


            for (Distance d : route.distance) {
                //Note that regardless of what unit system is displayed as text, the distance.value
                // field always contains a value expressed in meters.

                totalDistance = totalDistance + d.value;
            }
            //Converting from meter to miles ,1 meter = 0.000621371 miles
            totalDistance = totalDistance * 0.000621371;
            totalDistance = round(totalDistance, 1);

            (time_estimation = (TextView) findViewById(R.id.timeestimationID)).setText("Total distance is " + totalDistance + " miles which take " + totalDuration + "min");
            journeyMiles.setText(totalDistance + " miles");
            journeyDuration.setText(totalDuration + " mins");


            //setting the origin pointer
            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.origin_pointer))
                    .anchor(0.5f, 0.5f)
                    .flat(true)
                    .title("ORIGIN")
                    .visible(true)
                    .snippet(route.startAddress.get(0))
                    .position(route.startLocation.get(0))));

            //setting waypoints marker
            if (fromtoviaList.size() != 0) {
                for (int j = 1; j <= fromtoviaList.size(); j++) {
                    originMarkers.add(mMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.waypoint_pointer))
                            .anchor(0.5f, 0.5f)
                            .flat(true)
                            .title(route.startAddress.get(j))
                            .position(route.startLocation.get(j))));
                }
            }

            //setting the dest pointer
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.destination_pointer))
                    .snippet(route.endAddress.get(fromtoviaList.size()))
                    .anchor(0.5f, 0.5f)
                    .title("DESTINATION")
                    .flat(true)
                    .position(route.endLocation.get(fromtoviaList.size()))));

            //Setting plyline
            polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(context.getResources().getColor(R.color.application_color_accent)).
                    width(10);


            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            final Polyline p = mMap.addPolyline(polylineOptions);
            polylinePaths.add(p);
            //moveToBounds(p);

            nearestOfficeLatLng(FIXED_OFFICE_KEY);
            latlongconvertAdress();

            ConstraintLayout fareTimeDistanceCL = findViewById(R.id.fareTimeDistance);
            ConstraintLayout timeduraCL = findViewById(R.id.timeduraCL);
            fareTimeDistanceCL.setVisibility(View.VISIBLE);
            // timeduraCL.setVisibility(View.VISIBLE);

            carsforFareCalculation("Saloon");
            updateViewForSelectedCar(carlist.get(0));
            p1=p;

        }
        sendRequestforDespatchTime();

        moveToBounds(p1);
    }

    //    setting the camera bounds to fit route in single screen
    private void moveToBounds(Polyline p) {
        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (int i = 0; i < p.getPoints().size(); i++) {
                builder.include(p.getPoints().get(i));
            }

            LatLngBounds bounds = builder.build();
            int padding = 200; // offset from edges of the map in pixels

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mMap.animateCamera(cu);
            bound = bounds;

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public void carList() {

        selectedCarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //selectedCarLayout.setVisibility(View.GONE);
                if (RecyclerViewCL.getVisibility() == View.GONE) {
                    RecyclerViewCL.setVisibility(View.VISIBLE);
                } else {
                    RecyclerViewCL.setVisibility(View.GONE);
                }
            }
        });

        // Create the recyclerview.
        mRecyclerViewCars = findViewById(R.id.carRecyclerView);

        // Create the horizontal LayoutManager
        //final GridLayoutManager layoutManager = new GridLayoutManager(this,1);
        //layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        CenterZoomLayoutManager layoutManager = new CenterZoomLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        // Set layout manager.
        mRecyclerViewCars.setLayoutManager(layoutManager);
        mAdapterCars = new CarSelectionAdapter(carlist, new CarSelectionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Car selectedCarData, View v) {
                carsforFareCalculation(selectedCarData.getCarName());
                updateViewForSelectedCar(selectedCarData);
                getClearDriver(selectedCarData.getCarName());
            }
        });
        mRecyclerViewCars.setAdapter(mAdapterCars);
        mAdapterCars.notifyDataSetChanged();
        final SnapHelper snapHelper = new LinearSnapHelper() {
            @Override
            public View findSnapView(RecyclerView.LayoutManager layoutManager) {
                return super.findSnapView(layoutManager);
            }
        };
        snapHelper.attachToRecyclerView(mRecyclerViewCars);
        mRecyclerViewCars.scrollToPosition(0);


    }

    public void updateViewForSelectedCar(Car selectedCarData) {
        completebooking.setText("Confirm " + selectedCarData.getCarName());
        selectedCarName.setText(selectedCarData.getCarName());
        selectedPassen.setText(selectedCarData.getCarPassengerCapacity());
        selectedSuitcase.setText(selectedCarData.getCarLuggageCapacity());
        selectedCarIV.setImageResource(selectedCarData.getCarImage());
        selectedCarData.setSleceted(true);
        for (Car c : carlist) {
            if (c.getCarName().equals(selectedCarData.getCarName())) {
            } else {
                c.setSleceted(false);
            }
        }
        mAdapterCars.notifyDataSetChanged();
        //RecyclerViewCL.setVisibility(View.GONE);
    }

    public void selectBookingDate() {

        booking_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        BookingScreen.this,
                        android.R.style.Theme_Holo_NoActionBar_TranslucentDecor,
                        mDateSetListener,
                        year, month, day);

                dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateset:date " + month + "/" + day + "/" + year);

                date = day + "-" + month + "-" + year;
                booking_date.setText(date);
            }
        };

    }

    //if user not select date and time during booking
    public void nowBookingDateTime() {
//        Date c = Calendar.getInstance().getTime();
//
//        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy");
//        String formattedDate = df.format(c);
//
//
//        Calendar cal = Calendar.getInstance();
//        int year = cal.get(Calendar.YEAR);
//        int month = cal.get(Calendar.MONTH);
//        int day = cal.get(Calendar.DAY_OF_MONTH);
//        month = month + 1;
//
//        date = day + "-" + month + "-" + year;
//        DataHolder.getInstance().bookingdate = formattedDate;
//
//        //for time
//        SimpleDateFormat time = new SimpleDateFormat("HH:MM");
//        String currentTime = time.format(c);
//        /*final Calendar mcurrentTime = Calendar.getInstance();
//        final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//        final int minute = mcurrentTime.get(Calendar.MINUTE);
//
//        String currentTime = (hour + ":" + minute);
//*/
//
//        DataHolder.getInstance().bookingtime = currentTime;
//
//        //unix current date and time for booking value "datentime"
//        DataHolder.getInstance().unixdateNtime = (System.currentTimeMillis() + DateTimeZone.forID("Europe/London").getOffset(new Instant())) / 1000L;

        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy");
        String formattedDate = df.format(c);

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        month = month + 1;

        date = day + "-" + month + "-" + year;
        DataHolder.getInstance().bookingdate = formattedDate;

        //for time
        final int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);

        StringBuffer timestamp = new StringBuffer();
        if (hour >= 10)
            timestamp.append(hour);
        else
            timestamp.append("0" + hour);

        timestamp.append(":");

        if (minute >= 10)
            timestamp.append(minute);
        else
            timestamp.append("0" + minute);

        DataHolder.getInstance().bookingtime = String.valueOf(timestamp);

        //unix current date and time for booking value "datentime"
        // DataHolder.getInstance().unixdateNtime = (System.currentTimeMillis() + DateTimeZone.forID("Europe/London").getOffset(new Instant())) / 1000L;
        int offset = TimeZone.getDefault().getRawOffset() + TimeZone.getDefault().getDSTSavings();
        long now = System.currentTimeMillis() + offset;
        DataHolder.getInstance().unixdateNtime = (now / 1000L);
    }

    public void bookingTime() {

        timeSelection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final Calendar mcurrentTime = Calendar.getInstance();
                final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                final int minute = mcurrentTime.get(Calendar.MINUTE);

                int year = mcurrentTime.get(Calendar.YEAR);
                int month = mcurrentTime.get(Calendar.MONTH);
                int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);

                month = month + 1;

                final String date2 = day + "-" + month + "-" + year;

                TimePickerDialog mTimePicker;

                mTimePicker = new TimePickerDialog(BookingScreen.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        if (date != null) {

                            if (date.equals(date2)) {

                                if (selectedHour <= hour && selectedMinute < minute || selectedHour < hour && selectedMinute >= minute) {

                                    Toast.makeText(BookingScreen.this, "This is previous time", Toast.LENGTH_SHORT).show();
                                    timeSelection.setText(hour + ":" + minute);

                                } else {

                                    timeSelection.setText(selectedHour + ":" + selectedMinute);

                                }

                            } else {

                                timeSelection.setText(selectedHour + ":" + selectedMinute);

                            }

                        } else {
                            Toast.makeText(BookingScreen.this, "First Select your Ride Date", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, hour, minute, false);//Yes 24 hour time

                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

    }

    //user selected time in unix00000000
    public void userselectedTimeUnix() {

        String toParse = date + " " + timeSelection.getText(); // Results in "2-5-2012 20:43"
        SimpleDateFormat formatter = new SimpleDateFormat("d-M-yyyy HH:mm"); // I assume d-M, you may refer to M-d for month-day instead.
        Date date = null; // You will need try/catch around this

        try {
            date = formatter.parse(toParse);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = 0;
        if (date != null) {
            int offset = TimeZone.getDefault().getRawOffset() + TimeZone.getDefault().getDSTSavings();
            //  millis = (date.getTime())/(1000L);
            millis = (date.getTime()) + (offset);


        }
        long now = millis / 1000L;
        DataHolder.getInstance().unixdateNtime = now;
        Calendar cal = Calendar.getInstance();

        TimeZone timeZone =  cal.getTimeZone();




        long unixTimeStamp = Network.sharedInstance.getBST();
        if(now-unixTimeStamp>1800)
        {
            timeAvailability.setVisibility(View.GONE);
        }
        else{
            timeAvailability.setVisibility(View.VISIBLE);
        }
    }

    public void moreOptions(View v) {
        final Dialog moreOptionDialog = new Dialog(this);
        moreOptionDialog.setContentView(R.layout.activity_moreoption);
        Window window = moreOptionDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        switch1 = (SwitchCompat) moreOptionDialog.findViewById(R.id.switch1);
        flight_nmberId = (TextView) moreOptionDialog.findViewById(R.id.flightNumberId);
        moreOptionDoneId = (Button) moreOptionDialog.findViewById(R.id.moreOptionDoneId);
        instruction = (EditText) moreOptionDialog.findViewById(R.id.instruction);
        cancelButton = (Button) moreOptionDialog.findViewById(R.id.cancel);


        moreOptionDialog.show();
        moreOptionDialog.setCancelable(false);

        if (!flight_nmberId.getText().equals("")) {

            String flightNO = DataHolder.getInstance().flightholder;

            flight_nmberId.setText(flightNO);
        }

        if (!instruction.getText().equals("")) {

            String additionalInstruction = DataHolder.getInstance().additionalInstruction;

            instruction.setText(additionalInstruction);
        }

        if (comment != null && !comment.equals("")) {

            switch1.setChecked(true);

        }

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // switchColor(isChecked);
                moreOptionMethod();
            }
        });


        moreOptionDoneId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataHolder.getInstance().flightholder = String.valueOf(flight_nmberId.getText());
                DataHolder.getInstance().additionalInstruction = String.valueOf(instruction.getText());
                moreOptionDialog.dismiss();

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moreOptionDialog.dismiss();
            }
        });

    }

    public void moreOptionMethod() {

        Boolean switchState = switch1.isChecked();

        if (switchState) {


            comment = "customer need a child seat. ";
            DataHolder.getInstance().takechildSeat = comment;
            Toast.makeText(this, "child seat selected", Toast.LENGTH_SHORT).show();

        } else {

            comment = "";
            DataHolder.getInstance().takechildSeat = comment;
            Toast.makeText(this, "child seat off", Toast.LENGTH_SHORT).show();

        }
    }

    //date and time popup activity calling
    public void bookingDateandTime(View v) {
        final Dialog scheduleDialog = new Dialog(this);
        scheduleDialog.setContentView(R.layout.datetimepopup);
        Window window = scheduleDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        booking_date = (TextView) scheduleDialog.findViewById(R.id.setScheduleBooking);
        timeSelection = (TextView) scheduleDialog.findViewById(R.id.timepicker);
        scheduleDone = (Button) scheduleDialog.findViewById(R.id.scheduleDoneId);
        booking_date.setText(DataHolder.getInstance().bookingdate);
        timeSelection.setText(DataHolder.getInstance().bookingtime);

        selectBookingDate();
        bookingTime();
        scheduleDialog.show();

        scheduleDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataHolder.getInstance().bookingdate = date;
                DataHolder.getInstance().bookingtime = timeSelection.getText().toString();
                userselectedTimeUnix();
                scheduleDialog.dismiss();
            }
        });
    }

//    //fare calculation only for saloon
//    private void calculateFareSaloon(Route route) {
//        FareCalculation fareCalculation = new FareCalculation(getApplicationContext());
//        fareCalc = fareCalculation.calculateFare("S", totalDistance);
//        carSymbol = "S";
//    }

    //fare getting with respect to cars
    public double[] carsforFareCalculation(String vehicleType) {
        FareCalculation fareCalculation = new FareCalculation(getApplicationContext());
        switch (vehicleType) {
            case "Saloon":
                carSymbol = "S";
                if ((isAirportAtOri || isAirportAtDest) && fromtoviaList.size()==0) {
                    double cusFare = (getFixedFareFromServer(totalDistance, fareList, "S"));
                    fareCalc = new double[]{cusFare, cusFare};
                } else {
                    fareCalc = fareCalculation.calculateFare("S", totalDistance);
                }
                break;
            case "Estate":
                carSymbol = "E";
                if ((isAirportAtOri || isAirportAtDest) && fromtoviaList.size()==0) {
                    double cusFare = (getFixedFareFromServer(totalDistance, fareList, "E"));
                    fareCalc = new double[]{cusFare, cusFare};
                } else {
                    fareCalc = fareCalculation.calculateFare("E", totalDistance);
                }
                break;
            case "MPV":
                carSymbol = "6";
                if ((isAirportAtOri || isAirportAtDest) && fromtoviaList.size()==0) {
                    double cusFare = (getFixedFareFromServer(totalDistance, fareList, "6"));
                    fareCalc = new double[]{cusFare, cusFare};
                } else {
                    fareCalc = fareCalculation.calculateFare("6", totalDistance);
                }
                break;
            case "Executive":
                carSymbol = "X";
                if ((isAirportAtOri || isAirportAtDest) && fromtoviaList.size()==0) {
                    double cusFare = (getFixedFareFromServer(totalDistance, fareList, "X"));
                    fareCalc = new double[]{cusFare, cusFare};
                } else {
                    fareCalc = fareCalculation.calculateFare("X", totalDistance);
                }
                break;
            case "8 Passenger":
                carSymbol = "8";
                if ((isAirportAtOri || isAirportAtDest) && fromtoviaList.size()==0) {
                    double cusFare = (getFixedFareFromServer(totalDistance, fareList, "8"));
                    fareCalc = new double[]{cusFare, cusFare};
                } else {
                    fareCalc = fareCalculation.calculateFare("8", totalDistance);
                }
                break;
        }
        DataHolder.getInstance().cusfare = fareCalc[0];
        fareCalculation.setDrvfare(fareCalc[0]);
        journeyFare.setText(String.format(" %.2f", fareCalc[0]));
        return fareCalc;
    }

//    private void saloonFare() {
//        try {
//            FareCalculation fareCalculation = new FareCalculation(getApplicationContext());
//            double fixedFare;
//            if (isAirportAtOri) {
//                fareSaloon = fareCalculation.getFixedFare(carSymbol);
//                DataHolder.getInstance().cusfare = fareSaloon;
//                journeyFare.setText(String.format(" %.2f", fareSaloon));
//            } else if (isAirportAtDest) {
//                fareSaloon = fareCalculation.getFixedFare(carSymbol);
//                DataHolder.getInstance().cusfare = fareSaloon;
//                journeyFare.setText(String.format(" %.2f", fareSaloon));
//            } else {
//                fareSaloon = fareCalc[1];
//                DataHolder.getInstance().cusfare = fareSaloon;
//                journeyFare.setText(String.format(" %.2f", fareSaloon));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    public void bookingData() {
        try {
            String payMethod;
            SharedPreferences preferences = getSharedPreferences("creditCardCheck", MODE_PRIVATE);
            final String getCard= preferences.getString("checkCard", "");

            if(getCard.equals("yes"))
            {
                payMethod="CREDIT CARD";
            }

            else {
                payMethod="CASH";
            }
            final String token = FirebaseInstanceId.getInstance().getToken();

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(getApplicationContext())) {

                //calculateDistance();  skh:this was commented by musadiq bhai

                final FareCalculation fareCalculation = new FareCalculation(BookingScreen.this);

                //	final double getMilesToInsertInBooking = Double.parseDouble(Distance.mMilesDistanceFormated);

                final double getMilesToInsertInBooking = totalDistance;
                final double getDrvFareToInsertInBooking = fareCalculation.getDrvfare();

                //    sendRequestforDespatchTime();

//                new Thread() {
//                    @Override
//                    public void run() {

//                        try {
//                            Thread.sleep(3000L);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
                try {

                    JSONArray query = new JSONArray();
                    query.put("Booking");

                    JSONArray returnfromtoVias = setFromToVia();

                    final String returnJobRef = getJobRef(getSharedPrefPhone());

                    final JSONArray returnLogC = setLogC();

                    JSONObject values = new JSONObject();

                    try {
                        values.put("from", selectedPickup);
                        values.put("from_info", "");
                        values.put("from_outcode", DataHolder.getInstance().originData.get(1));
                        values.put("fromtovia", returnfromtoVias);
                        values.put("logc", returnLogC);
                        //values.put("office", DataHolder.getInstance().namesOfNearOffice.get(0));
                        values.put("office", FIXED_OFFICE_KEY);
                        values.put("telephone", getSharedPrefPhone());
                        values.put("userid", getSharedPrefPhone());
                        values.put("custname", getSharedPrefName());
                        values.put("time", DataHolder.getInstance().bookingtime);
                        values.put("date", DataHolder.getInstance().bookingdate);
                        values.put("to", selectedDropoff);
                        values.put("to_info", "");
                        values.put("to_outcode", DataHolder.getInstance().destinationData.get(1));
                        values.put("fare", Math.round(DataHolder.getInstance().cusfare));
                        values.put("drvfare", Math.round(getDrvFareToInsertInBooking));
                        values.put("jobmileage", Math.round(getMilesToInsertInBooking));
                        values.put("jobref", returnJobRef);
                        values.put("mstate", CfgCustApp.sharedinstance.getLegacyserverkey() + "|@|" + token);
                        values.put("timetodespatch", DataHolder.getInstance().timetodespatch);
                        values.put("datentime", DataHolder.getInstance().unixdateNtime);
                        values.put("changed", false);
                        values.put("account", payMethod);
                        values.put("accuser", "");
                        values.put("bookedby", "CustomerOnline");
                        values.put("comment", DataHolder.getInstance().takechildSeat + "" + DataHolder.getInstance().additionalInstruction);
                        values.put("creditcard", getSharedPrefEmail());
                        values.put("cstate", "booked");
                        values.put("despatchtime", 0.0);
                        values.put("driverrate", "CASH");
                        values.put("drvrcallsign", "");
                        values.put("drvreqdname", "");
                        values.put("drvrname", "");
                        values.put("drvrreqcallsign", "");
                        values.put("dstate", "");
                        values.put("flag", 0);
                        values.put("flightno", DataHolder.getInstance().flightholder);
                        values.put("hold", true);
                        values.put("isdirty", false);
                        values.put("jobtype", "normal");
                        values.put("jstate", "unallocated");
                        values.put("leadtime", 0.0);
                        values.put("logd", JSONObject.NULL);
                        values.put("numofvia", fromtoviaList.size());
                        values.put("oldfare", 0.0);
                        values.put("olddrvfare", 0.0);
                        values.put("orderno", "");
                        values.put("tag", "1 of 1");
                        values.put("vehicletype", carSymbol);
                        values.put("pin", "");
                        values.put("callerid", "");

                        query.put(values);

                        if (polylinePaths.size() != 0) {
                            SocketEvent.sharedinstance.socket.emit("createdata", query, new Ack() {
                                @Override
                                public void call(Object... args) {

                                    if (!args[0].toString().equals("0") && !args[0].equals(null)) {
                                        jobID = args[0].toString();
                                        jobRef = returnJobRef;
                                        //saving history data in SQLite db
//                                    db.insertBookingForHistory(DataHolder.getInstance().bookingdate,
//                                            DataHolder.getInstance().bookingtime, BookingScreen.selectedPickup,
//                                            BookingScreen.selectedDropoff, Math.round(DataHolder.getInstance().cusfare),
//                                            "Booked", jobID, jobRef, DataHolder.getInstance().unixdateNtime);

                                        DataHolder.getInstance().vehType = carSymbol;
                                        DataHolder.getInstance().originLng = DataHolder.getInstance().originDataLatLng.get(1);
                                        DataHolder.getInstance().originLat = DataHolder.getInstance().originDataLatLng.get(0);
                                        DataHolder.getInstance().origin = selectedPickup;
                                        DataHolder.getInstance().destination = selectedDropoff;
                                        DataHolder.getInstance()._fromOutcode = DataHolder.getInstance().originData.get(1);
                                        DataHolder.getInstance().jobID = args[0].toString();
                                        DataHolder.getInstance().jobReference = returnJobRef;
                                        DataHolder.getInstance().telephone = getSharedPrefPhone();
                                        DataHolder.getInstance().jsonArrayLogC = returnLogC;


                                        long differ = DataHolder.getInstance().unixdateNtime - Network.sharedInstance.getBST();
                                        if (differ >= 1800) {
                                            //  startAlarmBroadcastReceiver(BookingScreen.this,(differ-900)*1000);
                                            //bookingData();
                                            Intent bookingIntent = new Intent(BookingScreen.this, OriginAndDestination.class);
                                            bookingIntent.putExtra("Booking Status", true);
                                            startActivity(bookingIntent);
                                            finish();
                                            db.insertBookingForHistory(DataHolder.getInstance().bookingdate,
                                                    DataHolder.getInstance().bookingtime, BookingScreen.selectedPickup,
                                                    BookingScreen.selectedDropoff, Math.round(DataHolder.getInstance().cusfare),
                                                    "Booked", jobID, jobRef, DataHolder.getInstance().unixdateNtime);
                                            ThreadSendMail("<html>\n" +
                                                    "   <head>\n" +
                                                    "      <style>\n" +
                                                    "         .banner-color {\n" +
                                                    "         background-color: #eb681f;\n" +
                                                    "         }\n" +
                                                    "         .title-color {\n" +
                                                    "         color: #0066cc;\n" +
                                                    "         }\n" +
                                                    "         .button-color {\n" +
                                                    "         background-color: #0066cc;\n" +
                                                    "         }\n" +
                                                    "         @media screen and (min-width: 500px) {\n" +
                                                    "         .banner-color {\n" +
                                                    "         background-color: #0066cc;\n" +
                                                    "         }\n" +
                                                    "         .title-color {\n" +
                                                    "         color: #eb681f;\n" +
                                                    "         }\n" +
                                                    "         .button-color {\n" +
                                                    "         background-color: #eb681f;\n" +
                                                    "         }\n" +
                                                    "         }\n" +
                                                    "      </style>\n" +
                                                    "   </head>\n" +
                                                    "   <body>\n" +
                                                    "      <div style=\"background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important\">\n" +
                                                    "         <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                                                    "            <tbody>\n" +
                                                    "               <tr>\n" +
                                                    "                  <td align=\"center\">\n" +
                                                    "                     <center style=\"width:100%\">\n" +
                                                    "                        <table bgcolor=\"#FFFFFF\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif\" width=\"512\">\n" +
                                                    "                           <tbody>\n" +
                                                    "                              <tr>\n" +
                                                    "                                 <td bgcolor=\"#F3F3F3\" width=\"100%\" style=\"background-color:#f3f3f3;padding:12px;border-bottom:1px solid #ececec\">\n" +
                                                    "                                    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;width:100%!important;font-family:Helvetica,Arial,sans-serif;min-width:100%!important\" width=\"100%\">\n" +
                                                    "                                       <tbody>\n" +
                                                    "                                          <tr>\n" +
                                                    "                                             <td align=\"left\" valign=\"middle\" width=\"50%\"><span style=\"margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px\">"+SHARE_RIDE_TEXT+"</span></td>\n" +
                                                    "                                             <td valign=\"middle\" width=\"50%\" align=\"right\" style=\"padding:0 0 0 10px\"><span style=\"margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px\">"+ Calendar.getInstance().getTime()+"</span></td>\n" +
                                                    "                                             <td width=\"1\">&nbsp;</td>\n" +
                                                    "                                          </tr>\n" +
                                                    "                                       </tbody>\n" +
                                                    "                                    </table>\n" +
                                                    "                                 </td>\n" +
                                                    "                              </tr>\n" +
                                                    "                              <tr>\n" +
                                                    "                                 <td align=\"left\">\n" +
                                                    "                                    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                                                    "                                       <tbody>\n" +
                                                    "                                          <tr>\n" +
                                                    "                                             <td width=\"100%\">\n" +
                                                    "                                                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                                                    "                                                   <tbody>\n" +
                                                    "                                                      <tr>\n" +
                                                    "                                                         <td align=\"center\" bgcolor=\""+context.getResources().getColor(R.color.application_color_accent)+"\" style=\"padding:20px 48px;color:#ffffff\" class=\"banner-color\">\n" +
                                                    "                                                            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                                                    "                                                               <tbody>\n" +
                                                    "                                                                  <tr>\n" +
                                                    "                                                                     <td align=\"center\" width=\"100%\">\n" +
                                                    "                                                                        <h1 style=\"padding:0;margin:0;color:#ffffff;font-weight:500;font-size:20px;line-height:24px\">Station Cars "+APP_OFFICE_NAME+"</h1>\n" +
                                                    "                                                                     </td>\n" +
                                                    "                                                                  </tr>\n" +
                                                    "                                                               </tbody>\n" +
                                                    "                                                            </table>\n" +
                                                    "                                                         </td>\n" +
                                                    "                                                      </tr>\n" +
                                                    "                                                      <tr>\n" +
                                                    "                                                         <td align=\"center\" style=\"padding:20px 0 10px 0\">\n" +
                                                    "                                                            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                                                    "                                                               <tbody>\n" +
                                                    "                                                                  <tr>\n" +
                                                    "                                                                     <td align=\"center\" width=\"100%\" style=\"padding: 0 15px;text-align: justify;color: rgb(76, 76, 76);font-size: 12px;line-height: 18px;\">\n" +
                                                    "                                                                        <h3 style=\"font-weight: 600; padding: 0px; margin: 0px; font-size: 16px; line-height: 24px; text-align: center;\" class=\"title-color\">Hey "+getSharedPrefName()+",</h3>\n" +
                                                    "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\">You booked the ride.<b> Ride Info:</b></p>\n" +
                                                    "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> From: </b>"+ DataHolder.getInstance().origin+"</p>\n" +
                                                    "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> To: </b>"+DataHolder.getInstance().destination+"</p>\n" +
                                                    "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> Booking Number: </b>" +DataHolder.getInstance().jobReference+"</p>\n" +
                                                    "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> Fare: </b>" +DataHolder.getInstance().cusfare+"</p>\n" +
                                                    "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> Journey Date: </b>" +DataHolder.getInstance().bookingdate+"</p>\n" +
                                                    "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> Journey time: </b>" +DataHolder.getInstance().bookingtime+"</p>\n" +


                                                    "                                                                        <div style=\"font-weight: 200; text-align: center; margin: 25px;\"><a style=\"padding:0.6em 1em;border-radius:600px;color:#ffffff;font-size:14px;text-decoration:none;font-weight:bold\" class=\"button-color\">Thank You for using "+SHARE_RIDE_TEXT+"</a></div>\n" +
                                                    "                                                                     </td>\n" +
                                                    "                                                                  </tr>\n" +
                                                    "                                                               </tbody>\n" +
                                                    "                                                            </table>\n" +
                                                    "                                                         </td>\n" +
                                                    "                                                      </tr>\n" +
                                                    "                                                      <tr>\n" +
                                                    "                                                      </tr>\n" +
                                                    "                                                      <tr>\n" +
                                                    "                                                      </tr>\n" +
                                                    "                                                   </tbody>\n" +
                                                    "                                                </table>\n" +
                                                    "                                             </td>\n" +
                                                    "                                          </tr>\n" +
                                                    "                                       </tbody>\n" +
                                                    "                                    </table>\n" +
                                                    "                                 </td>\n" +
                                                    "                              </tr>\n" +
                                                    "                              <tr>\n" +
                                                    "                                 <td align=\"left\">\n" +
                                                    "                                    <table bgcolor=\"#FFFFFF\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                                                    "                                       <tbody>\n" +
                                                    "                                          <tr>\n" +
                                                    "                                             <td align=\"center\" width=\"100%\">\n" +
                                                    "                                                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                                                    "                                                   <tbody>\n" +
                                                    "                                                      <tr>\n" +
                                                    "                                                         <td align=\"center\" valign=\"middle\" width=\"100%\" style=\"border-top:1px solid #d9d9d9;padding:12px 0px 20px 0px;text-align:center;color:#4c4c4c;font-weight:200;font-size:12px;line-height:18px\">Regards,\n" +
                                                    "                                                            <br><b>"+SHARE_RIDE_TEXT+" Customer App Team</b>\n" +
                                                    "                                                         </td>\n" +
                                                    "                                                      </tr>\n" +
                                                    "                                                   </tbody>\n" +
                                                    "                                                </table>\n" +
                                                    "                                             </td>\n" +
                                                    "                                          </tr>\n" +
                                                    "                                          <tr>\n" +
                                                    "                                             <td align=\"center\" width=\"100%\">\n" +
                                                    "                                                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                                                    "                                                   <tbody>\n" +
                                                    "                                                      <tr>\n" +
                                                    "                                                         <td align=\"center\" style=\"padding:0 0 8px 0\" width=\"100%\"></td>\n" +
                                                    "                                                      </tr>\n" +
                                                    "                                                   </tbody>\n" +
                                                    "                                                </table>\n" +
                                                    "                                             </td>\n" +
                                                    "                                          </tr>\n" +
                                                    "                                       </tbody>\n" +
                                                    "                                    </table>\n" +
                                                    "                                 </td>\n" +
                                                    "                              </tr>\n" +
                                                    "                           </tbody>\n" +
                                                    "                        </table>\n" +
                                                    "                     </center>\n" +
                                                    "                  </td>\n" +
                                                    "               </tr>\n" +
                                                    "            </tbody>\n" +
                                                    "         </table>\n" +
                                                    "      </div>\n" +
                                                    "   </body>\n" +
                                                    "</html>");
                                        } else {
                                            fareCalculation.setDrvfare(0.0);
                                            startActivity(new Intent(BookingScreen.this, DriverSearch.class));
                                            finish();
                                        }


                                    } else {
                                        //error make ur booking again
                                        startActivity(new Intent(BookingScreen.this, OriginAndDestination.class));
                                        finish();
                                    }
                                }
                            });

                        } else
                            autoDismissDialogue("Error", "Please select your address again..");

                    } catch (JSONException e) {
                        e.printStackTrace();


                    }
                } catch (Exception e) {
                    Log.e("Booking_error",e.toString());
                    Log.e("Booking_errormsg",e.getMessage());
                    Log.e("Booking_errorlocal",e.getLocalizedMessage());
                }

//                    }
//                }.start();

            }

        } catch (Exception e) {
            e.printStackTrace();
//           Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public JSONArray setFromToVia() throws JSONException {
        Log.e("fromtoviai",String.valueOf(fromtoviaList.size()));
        JSONArray jArray = new JSONArray();
        JSONObject origin = new JSONObject();
        origin.put("info", "");
        origin.put("address", DataHolder.getInstance().originData.get(0));
        origin.put("lat", DataHolder.getInstance().originDataLatLng.get(0));
        origin.put("lon", DataHolder.getInstance().originDataLatLng.get(1));
        origin.put("postcode", DataHolder.getInstance().originData.get(2));
        jArray.put(origin);
        Log.e("fromtoviaii",String.valueOf(fromtoviaList.size()));
        JSONObject destination = new JSONObject();

        destination.put("info", "");
        destination.put("address", DataHolder.getInstance().destinationData.get(0));
        destination.put("lat", DataHolder.getInstance().destinationDataLatLng.get(0));
        destination.put("lon", DataHolder.getInstance().destinationDataLatLng.get(1));
        destination.put("postcode", DataHolder.getInstance().destinationData.get(2));

        jArray.put(destination);
        Log.e("fromtoviaiii",String.valueOf(fromtoviaList.size()));
        for (FromToVia ftv : fromtoviaList) {
            //Address a  = getLocationFromAddress(this,wp);
            JSONObject obj = new JSONObject();
            obj.put("info", "");
            obj.put("address", ftv.getAddress() + "");
            obj.put("lat", ftv.getLat());
            obj.put("lon", ftv.getLon());
            obj.put("postcode", ftv.getPostcode());
            jArray.put(obj);
        }
        Log.e("fromtovia",jArray.toString());
        for (int i = 0; i < 7 - (fromtoviaList.size()); i++) {
            JSONObject obj = new JSONObject();
            obj.put("info", JSONObject.NULL);
            obj.put("address", JSONObject.NULL);
            obj.put("lat", 0.0);
            obj.put("lon", 0.0);
            obj.put("postcode", JSONObject.NULL);
            jArray.put(obj);
        }
        Log.e("fromtovia",jArray.toString());
        return jArray;
    }

    public Address getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;
        Address location = null;
        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            location = address.get(0);
            //p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return location;
    }

    private JSONArray setLogC() throws JSONException {

        String returnDeviceID = getDeviceID();
        JSONArray insertInLogCArray = new JSONArray();
        insertInLogCArray.put(Network.sharedInstance.getBST());
        insertInLogCArray.put("booked");
        insertInLogCArray.put(getSharedPrefName());
        insertInLogCArray.put(returnDeviceID);
        JSONArray LogC = new JSONArray();
        LogC.put(insertInLogCArray);
        return LogC;
    }

    public void sendbooking() {
        completebooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                   completebooking.setEnabled(false);
                if (Network.isNetworkAvailable(context) && polylinePaths.size() != 0) {
                    try {
                        if (getSharedPrefPhone().equals("") || getSharedPrefPhone() == null) {
                            fromtoviaList.clear();
                             completebooking.setEnabled(true);
                            Intent intent = new Intent(BookingScreen.this, CustomerVerfication.class);
                            startActivity(intent);
                        } else {
                            bookingData();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    snackbarForConnectivityStatus();
                }

            }

        });
    }

/*
	private static Map<String, Double> sortByComparator(Map<String, Double> unsortMap, final boolean order) {

		List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(unsortMap.entrySet());

		// Sorting the list based on values
		Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
			public int compare(Map.Entry<String, Double> o1,
							   Map.Entry<String, Double> o2) {
				if (order) {
					return o1.getValue().compareTo(o2.getValue());
				} else {
					return o2.getValue().compareTo(o1.getValue());
				}
			}
		});

		// Maintaining insertion order with the help of LinkedList
		Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
		for (Map.Entry<String, Double> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
			// DataHolder.getInstance().namesOfNearOffice.add(entry.getKey());
			DataHolder.getInstance().namesOfNearOffice.add("XYZ");
		}

		return sortedMap;
	}

	private double distance(double lat1, double lon1, double lat2, double lon2) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1))
				* Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1))
				* Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		return (dist);
	}

	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}

	private void calculateDistance() {

		ArrayList<Double> calculatedDistance = new ArrayList<>();

		Double d1 = distance(DataHolder.getInstance().originDataLatLng.get(0), DataHolder.getInstance().originDataLatLng.get(1),
				DataHolder.getInstance().nearestOfficeLatLng.get(0), DataHolder.getInstance().nearestOfficeLatLng.get(1));

		Double d2 = distance(DataHolder.getInstance().originDataLatLng.get(0), DataHolder.getInstance().originDataLatLng.get(1),
				DataHolder.getInstance().nearestOfficeLatLng.get(2), DataHolder.getInstance().nearestOfficeLatLng.get(3));


		Double d3 = distance(DataHolder.getInstance().originDataLatLng.get(0), DataHolder.getInstance().originDataLatLng.get(1),
				DataHolder.getInstance().nearestOfficeLatLng.get(4), DataHolder.getInstance().nearestOfficeLatLng.get(5));


		Double d4 = distance(DataHolder.getInstance().originDataLatLng.get(0), DataHolder.getInstance().originDataLatLng.get(1),
				DataHolder.getInstance().nearestOfficeLatLng.get(6), DataHolder.getInstance().nearestOfficeLatLng.get(7));


		Double d5 = distance(DataHolder.getInstance().originDataLatLng.get(0), DataHolder.getInstance().originDataLatLng.get(1),
				DataHolder.getInstance().nearestOfficeLatLng.get(8), DataHolder.getInstance().nearestOfficeLatLng.get(9));


		Double d6 = distance(DataHolder.getInstance().originDataLatLng.get(0), DataHolder.getInstance().originDataLatLng.get(1),
				DataHolder.getInstance().nearestOfficeLatLng.get(10), DataHolder.getInstance().nearestOfficeLatLng.get(11));


		calculatedDistance.add(0, d1);
		calculatedDistance.add(1, d2);
		calculatedDistance.add(2, d3);
		calculatedDistance.add(3, d4);
		calculatedDistance.add(4, d5);
		calculatedDistance.add(5, d6);

		DataHolder.getInstance().nearestOfficeData.put("CYP", calculatedDistance.get(0));
		DataHolder.getInstance().nearestOfficeData.put("WCP", calculatedDistance.get(1));
		DataHolder.getInstance().nearestOfficeData.put("NBT", calculatedDistance.get(2));
		DataHolder.getInstance().nearestOfficeData.put("SUR", calculatedDistance.get(3));
		DataHolder.getInstance().nearestOfficeData.put("NBA", calculatedDistance.get(4));
		DataHolder.getInstance().nearestOfficeData.put("XYZ", calculatedDistance.get(5));

		sortByComparator(DataHolder.getInstance().nearestOfficeData, ASC);

		//for all offices
		// nearestOfficeLatLng(DataHolder.getInstance().namesOfNearOffice.get(0));


		//for local office only for testing
		nearestOfficeLatLng(FIXED_OFFICE_KEY);


		//only for SURBIITON office
		//nearestOfficeLatLng("SUR");

		latlongconvertAdress();

	}*/

    public void latlongconvertAdress() {
        flag = false;

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, log, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            Log.wtf("officeaddress:",String.valueOf(lat)+"@"+String.valueOf(log));
        } catch (IOException e) {
            e.printStackTrace();
        }

        address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();
        double lat = addresses.get(0).getLatitude();
        double lng = addresses.get(0).getLongitude();// Only if available else return NULL
    }

    private void sendRequestforDespatchTime() {
        Log.e("officedirection","");
        selectedPickup = getIntent().getStringExtra("pickupValue");

        officeAddress = address;
//Here
        try {
            new DirectionFinder(this, selectedPickup, fromtoviaList, officeAddress, flag).execute();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        }
    }

    @Override
    public void start() {
        //    if (Network.isNetworkAvailable(context)) {
//            progressDialog = ProgressDialog.show(this, "Please wait.",
//                    "Wait..!", true);
//            progressDialog = new ProgressDialog(this, R.style.MyDialogTheme);
//            progressDialog.setTitle("Please Wait");
//            progressDialog.setCancelable(false);
//            progressDialog.setMessage("finding routes ...");
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //     progressDialog.show();

        //  }

    }

    @Override
    public void finish(String time) {


        timetoDispatch = time;

        String[] splitTime = timetoDispatch.split(" ");
        String getdespatchtime = splitTime[0];

        long timeDistance = (long) Double.parseDouble(getdespatchtime);

        long convertToSecond = (timeDistance * 60L);


        long differ = DataHolder.getInstance().unixdateNtime - Network.sharedInstance.getBST();

        if (differ >= 1800) {
            //this is for the future time booking , we  will subtract the
            // time taken by driver to reach the pickup in the current time
            //because driver will leave for the pickup early
            //bookedForFuture = true;
            long timetodesp = DataHolder.getInstance().unixdateNtime - convertToSecond;
            //put values for booking
            DataHolder.getInstance().timetodespatch = timetodesp;
        } else {
            ///bookedForFuture = false;
            //this is for the current time booking , we  will add the
            // time taken by driver to reach the pickup in the current time
            long timetodesp = DataHolder.getInstance().unixdateNtime + convertToSecond;

            //put values for booking
            DataHolder.getInstance().timetodespatch = timetodesp;
        }
        getClearDriver("Saloon");
        progressDialog.dismiss();
    }

    public double[] nearestOfficeLatLng(String Key) {

        switch (Key) {

            case "CYP":
                lat = 51.41798;
                log = -0.07307;
                break;

            case "WCP":
                lat = 51.3813;
                log = -0.24526;
                break;

            case "NBT":
                lat = 51.41245;
                log = -0.28409;
                break;

            case "SUR":
                lat = 51.3922;
                log = -0.30329;
                break;

            case "NBA":
                lat = 51.648589999;
                log = -0.17336;
                break;

            case "XYZ":
                lat = 56.129918;
                log = -3.792925;
                break;

        }

        return officekey;

    }

    public void checkInternetDisconnect() {

//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                handler.postDelayed(this, 3000);
        if (!isNetworkAvailable()) {
            progressDialog.dismiss();
            status = true;
        }
        if (isNetworkAvailable() && polylinePaths.size() == 0 && status == true) {
            sendRequest();

        }

        if (isNetworkAvailable() && polylinePaths.size() > 0) {
            handler.removeCallbacksAndMessages(null);

        }
//            }
//
//        }, 3000);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void switchColor(boolean checked) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            switch1.getThumbDrawable().setColorFilter(checked ? Color.GREEN : Color.RED, PorterDuff.Mode.MULTIPLY);
            switch1.getTrackDrawable().setColorFilter(!checked ? Color.GRAY : Color.BLACK, PorterDuff.Mode.MULTIPLY);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    double calculateFixedFareSaloon(double miles, ArrayList<Double> milesUnit, ArrayList<Double> costUnit) {
        double totalFare = 0;
        double tmiles = 0;
        for (int i = 0; i < costUnit.size(); i++) {
            // System.out.println("######CALCULATING FARE FOR THE RATE = " + costUnit.get(i) + " TILL NEXT " + milesUnit.get(i) + " miles ########");
            tmiles = tmiles + milesUnit.get(i);
            int x = 0;
            double j = 0;
            while (x <= tmiles && miles > 0) {
                if (x < milesUnit.get(i)) {
                    miles--;
                    x++;
                    totalFare = totalFare + (1 * costUnit.get(i));
                    //System.out.println("iteration#" + x + ", fare=" + totalFare);
                    //  j = j + (1 * costUnit.get(i));
                } else {
                    break;
                }
            }
            // System.out.println("######FARE = " + j + " miles " + "At RATE = " + costUnit.get(i) + " FOR " + milesUnit.get(i) + " miles ########\n\n");
        }
        // System.out.println("######*****TOTAL FARE = " + totalFare + " miles *****########");
        BigDecimal bd = new BigDecimal(Double.toString(totalFare));
        bd = bd.setScale(0, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    public double getFixedFareFromServer(double miles, Map<String, List<Double>> fareList, String carType) {

        double fareS = 0;
        double fare = 0;
        double fareE = 0;
        double fare6 = 0;
        double fare8 = 0;
        double fareX = 0;
        CfgCustApp cfg = CfgCustApp.sharedinstance;
        ArrayList<Map<String, Double>> All_vehicle_fare = cfg.getAll_vehicle_fare();


        if (fareList.containsKey("S")) {
            fareS = (fareList.get("S")).get(0);
        } else {
            if (miles < 12) {
                fareS = 20.0;
            } else {
                fareS = calculateFixedFareSaloon(miles, cfg.getMilesUnit(), cfg.getCostUnit());
            }
        }
        switch (carType) {
            case "S"://saloon
                fare = fareS;
                break;

            case "E"://estate
                if (fareList.containsKey("E")) {
                    fareE = (fareList.get("E")).get(0);
                } else {
                    double addedFarePercent = (All_vehicle_fare.get(0)).get("E");
                    fareE = fareS + fareS * (addedFarePercent / 100);
                }
                fare = fareE;
                break;

            case "6"://MPV
                if (fareList.containsKey("6")) {
                    fare6 = (fareList.get("6")).get(0);
                } else {
                    double addedFarePercent = (All_vehicle_fare.get(0)).get("6");
                    fare6 = fareS + fareS * (addedFarePercent / 100);
                }
                fare = fare6;
                break;

            case "8"://8 Passenger
                if (fareList.containsKey("8")) {
                    fare8 = (fareList.get("8")).get(0);
                } else {

                    double addedFarePercent = (All_vehicle_fare.get(0)).get("8");
                    fare8 = fareS + fareS * (addedFarePercent / 100);
                }
                fare = fare8;
                break;

            case "X":// Executive
                if (fareList.containsKey("X")) {
                    fareX = (fareList.get("X")).get(0);
                } else {
                    double addedFarePercent = (All_vehicle_fare.get(0)).get("X");
                    fareX = fareS + fareS * (addedFarePercent / 100);
                }
                fare = fareX;
                break;
        }
        BigDecimal bd = new BigDecimal(Double.toString(fare));
        bd = bd.setScale(0, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    //skh
    @Override
    public void onDirectionFinderFail(String error_message) {
        progressDialog.dismiss();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog);
        alertDialogBuilder.setTitle("Opps! an error eccurred");
        alertDialogBuilder.setIcon(R.drawable.ic_error_black_24dp);
        alertDialogBuilder.setMessage(error_message);
        alertDialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

    }
    public void getClearDriver(final String vehicleType){
        try {
            String selectedCar = "";
            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this) && DataHolder.getInstance().timetodespatch!=0) {
                JSONArray query = new JSONArray();
                query.put("Driverloc");
                if(vehicleType.equals("Saloon")){
                    selectedCar = "S";
                }
                else if (vehicleType.equals("Estate")){
                    selectedCar = "E";
                }
                else if (vehicleType.equals("MPV")){
                    selectedCar = "6";
                }
                else if (vehicleType.equals("Executive")){
                    selectedCar = "X";
                }
                else if (vehicleType.equals("8 Passenger")){
                    selectedCar = "8";
                }

                try {
                    //String data = "{'isvirtual': false, 'vtype': S,'lstate': 'CLR','loc': {$near: {$geometry: {'type': 'Point','coordinates': [67.0626351, 24.8891354]},$maxDistance: 3218.69 },'state': 'Clear' }";
//             String data = "{\"isvirtual\":" + false +
//                   "," +
//                   " \"vtype\": \"S\"," +
//                   "\"lstate\": \"CLR\"," +
//                   "\"loc\": " +
//                   "{$near:" +
//                   " {$geometry:" +
//                   "{" +
//                   "\"type\": \"Point\"," +
//                   "\"coordinates\":" +
//                   "[" +DataHolder.getInstance().originDataLatLng.get(1)+
//                   "," + DataHolder.getInstance().originDataLatLng.get(0) +
//                   "]}" +
//                   ",$maxDistance:3218.69}}," +
//                   "\"state\":\"Clear\"}";




                    JSONObject obj = new JSONObject().put("isvirtual", false)
//                            .put("office_id",CfgCustApp.sharedinstance.getJobInOffice())
                            .put("vtype", selectedCar).put("lstate", "CLR").put("state","Clear")
                            .put("loc", new JSONObject().put("$near",new JSONObject().put("$maxDistance",3218.69)
                                    .put("$geometry",new JSONObject().put("type","Point")
                                            .put("coordinates",new JSONArray().
                                                    put(DataHolder.getInstance().originDataLatLng.get(1))
                                                    .put(DataHolder.getInstance().originDataLatLng.get(0))))));


                    query.put(obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                SocketEvent.sharedinstance.socket.emit("getdata", query,
                        new Ack() {
                            @Override
                            public void call(Object... args) {
                                if (args[0] != null && !args[0].toString().equals("0")) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(args[0].toString());
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(jsonObject);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        DriverLoc.sharedInstance = mapper.readValue(new TreeTraversingParser(jsonNode), DriverLoc.class);
                                        final double timeforAvailability = calculateDistancebetweenDriverandCustomer(
                                                DataHolder.getInstance().originDataLatLng.get(0),
                                                DataHolder.getInstance().originDataLatLng.get(1),
                                                DriverLoc.sharedInstance.getLat(),
                                                DriverLoc.sharedInstance.getLong());
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                timeAvailability.setText("Driver will be Available in "+ Math.round(timeforAvailability )+" mins");
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }else {

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            timeAvailability.setText("Driver will be Available in "+timetoDispatch+"s");

                                        }
                                    });
                                }
                            }
                        });
            } else{
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public double calculateDistancebetweenDriverandCustomer(double originlat,double originlng,double driverlat,double driverlng)
    {
        float[] drvorigindistance = new float[1];


        Location.distanceBetween(originlat,originlng,driverlat,driverlng,drvorigindistance);
        Double meters = (double) drvorigindistance[0];
        //double kilometer = (meters * .001);
        double seconds = (meters*0.000621371/0.00555556);
        //    double seconds = (meters/20);
        return (seconds  / 60) ;
// Location startLatLng = new Location("");
// st artLatLng.setLatitude(originlat); ;
// startLatLng.setLongitude(originlng);
//        Location driverLatLng = new Location("");
//      driverLatLng.setLatitude(driverlat); ;
//        startLatLng.setLongitude(driverlng);
//        double time =(( startLatLng.distanceTo(driverLatLng))/20/60);
//        return time;

    }


    public void getCard()
    {
        SharedPreferences pref = getSharedPreferences("creditCardCheck", MODE_PRIVATE);

        final String card_Id = pref.getString("cardId","");
        //final String cust_Id = pref.getString("custId","");




        if(!card_Id.equals(""))
        {
            Stripe.apiKey = "sk_test_0XDT5RHd7IXtuxJ0vhYGRGsh";
            gson = new GsonBuilder().setPrettyPrinting().create();
            final Map<String, Object> cardParams = new HashMap<String, Object>();
            cardParams.put("object", "card");

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {

                        SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);
                        String custEmail = preferences.getString("cust_email", "");


                        final Map<String, Object> options = new HashMap<>();
                        options.put("email", custEmail);


                        List<Customer> customers = Customer.list(options).getData();

                        //String cust_Id = customers.get(0).getId();
                        if(customers.size()>0)
                        {String cust_Id = customers.get(0).getId();
                            customer = Customer.retrieve(cust_Id).getSources().list(cardParams);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    json = gson.toJson(customer);
                                    // Log.d("sources",card1.toString());
                                    try {
                                        JSONObject obj = new JSONObject(json);
                                        JSONArray userArray = obj.getJSONArray("data");
                                        if(userArray.equals(""))
                                        {
                                            SharedPreferences sharedPreferences = getSharedPreferences("creditCardCheck", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.putString("checkCard","" );
                                            editor.putString("cardId","");
                                            editor.putString("custId","");
                                            editor.putString("cardNumber","");
                                            editor.putString("brand","");
                                            editor.apply();
                                        }
                                        for (int i = 0; i < userArray.length(); i++) {
                                            JSONObject cardDetail = userArray.getJSONObject(i);

                                            String id = cardDetail.getString("id");


                                            if(id.equals(card_Id))
                                            {
                                                cardCheckId.add(id);
                                            }




                                        }


                                        if(cardCheckId.isEmpty())
                                        {


                                            SharedPreferences sharedPreferences = getSharedPreferences("creditCardCheck", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.putString("checkCard","" );
                                            editor.putString("cardId","");
                                            editor.putString("custId","");
                                            editor.putString("cardNumber","");
                                            editor.putString("brand","");
                                            editor.apply();
                                            setPaymentMethod();

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }





                        else {


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    SharedPreferences sharedPreferences = getSharedPreferences("creditCardCheck", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("checkCard","" );
                                    editor.putString("cardId","");
                                    editor.putString("custId","");
                                    editor.putString("cardNumber","");
                                    editor.putString("brand","");
                                    editor.apply();
                                    setPaymentMethod();
                                }
                            });




                        }











                    } catch (AuthenticationException e) {
                        e.printStackTrace();
                    } catch (InvalidRequestException e) {
                        e.printStackTrace();
                    } catch (APIConnectionException e) {
                        e.printStackTrace();
                    } catch (CardException e) {
                        e.printStackTrace();
                    } catch (APIException e) {
                        e.printStackTrace();
                    }
                }
            }).start();


        }




    }

    @Override
    protected void onResume() {
        super.onResume();
        setPaymentMethod();
    }

    public void setPaymentMethod()
    {

        SharedPreferences preferences = getSharedPreferences("creditCardCheck", MODE_PRIVATE);
        final String getCard= preferences.getString("checkCard", "");
        final String cardNumber = preferences.getString("cardNumber","");
        final String brand = preferences.getString("brand","");
        final String card="..."+cardNumber;
        final String cardId = preferences.getString("cardId","");
        final String custId = preferences.getString("custId","");



        if(getCard.equals("yes"))
        {


            paymentMethod.setText(card);


            if(brand.equals("Visa"))
            {
                imagePayment.setImageResource(R.drawable.cio_ic_visa);
            }
            else if(brand.equals("MasterCard"))
            {
                imagePayment.setImageResource(R.drawable.mastercard);
            }

            else if(brand.equals("American Express"))
            {
                imagePayment.setImageResource(R.drawable.amex);
            }
            else {
                imagePayment.setImageResource(R.drawable.cio_ic_visa);
            }

        }
        else {
            paymentMethod.setText("Cash");
            imagePayment.setImageResource(R.drawable.cash_pay);
        }
    }


    public void selectPaymentMethod(View view) {
        // this code will be uncomment at the time of payment live api integeration



//        SharedPreferences pref = getSharedPreferences("", MODE_PRIVATE);
//        String email = pref.getString("cust_email", "");
//        if(!email.equals(""))
//        {
//            Intent intent = new Intent(BookingScreen.this,PaymentMethodActivity.class);
//            startActivity(intent);
//
//        }
//
//        else {
//
//            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BookingScreen.this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
//            alertDialogBuilder.setTitle(BookingScreen.this.getString(R.string.app_name));
//            alertDialogBuilder.setMessage("Please Verify Your Account");
//            alertDialogBuilder.setPositiveButton("Verify", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    Intent intent = new Intent(BookingScreen.this, CustomerVerfication.class);
//                    startActivity(intent);
//                    dialog.cancel();
//                }
//            });
//            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                    dialog.dismiss();
//                }
//            });
//            alertDialogBuilder.show();
//
//
//
//        }



    }
}