package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.SwitchCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import buzybeez.com.buzybeezcabcrystalpalace.R;


import buzybeez.com.buzybeezcabcrystalpalace.DataCenter.DataHolder;
import buzybeez.com.buzybeezcabcrystalpalace.DataHandler.SQLiteAirportList;
import buzybeez.com.buzybeezcabcrystalpalace.Fragments.BookingHistoryActivity;
import buzybeez.com.buzybeezcabcrystalpalace.Helpers.CarSelectionAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Helpers.CenterZoomLayoutManager;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.UpdateFareandMile;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.DirectionFinder;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.DirectionFinderListener;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.Distance;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.Route;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.TimeToDespatcInterface;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Airports;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Booking;
import buzybeez.com.buzybeezcabcrystalpalace.Models.BookingHistory;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Car;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CfgCustApp;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CustomerConfig;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.Models.SaveDesDetail;
import buzybeez.com.buzybeezcabcrystalpalace.Models.SaveOriginDetail;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.socket.client.Ack;

import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.FIXED_OFFICE_KEY;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.airportLocationDest;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.bookedBookingHistoryOfUser;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.canceledBookingHistoryOfUser;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.fromtoviaList;

public class EditBookingActivity extends  BaseActivity implements
        ConnectionListener ,	ViasRecyclerViewAdapter.ItemClickListener , UpdateFareandMile ,
        DirectionFinderListener, TimeToDespatcInterface
{

    ArrayList<Car> carlist = new ArrayList<>();
    Booking booking = Booking.getInstance();
    public ConstraintLayout selectedCarLayout, RecyclerViewCL;
    public TextView selectedCarName, selectedPassen,
            selectedSuitcase, journeyMiles,journeyFare
            , editBookingDate ,editBookingTime
            , editBookingTo ,editBookingFrom , additionalInfo;
    public RecyclerView mRecyclerViewCars;
    public RecyclerView.Adapter mAdapterCars;
    ImageView selectedCarIV;
    public   ViasRecyclerViewAdapter ViasAdapter;
    public   RecyclerView editBookingVias;
    public List<FromToVia> fromtoviaList = new ArrayList<>();
    SwitchCompat switch1;
    Button moreOptionDoneId;
    String comment;
    public TextView flight_nmberId;
    public EditText instruction;
    public Button cancelButton;
    Dialog moreOptionDialog;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    long timeToDespatch =0;
    long dateandtimeunix=0;
    double[] fareCalc;
    SQLiteAirportList sqLiteAirportList;
    public Map<String, List<Double>> fareList;
    public  List<Double> valuesOfFixedFare;
    boolean isAirportOrigin , isAirportDestination = false;
    public ImageView editBookingAddVias;
    ArrayList<SaveDesDetail> saveDesDetails = new ArrayList<>();
    ArrayList<SaveOriginDetail> saveOriginDetail = new ArrayList<>();
    private UpdateFareandMile updateFareandMile;
    private ProgressDialog progressDialog;
    public  double totalDistance = 0;
    double[] officekey;
    private double log;
    private double lat;
    private String officeaddress;
    Button UpdateBookingbtn;
    String getdespatchtime;
    long despatchtimeconvertToSecond = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_booking);


        initializeIDS();

        initializeCars();

        carList();

        isAirportSelected();

        snackbarForConnectivityStatus();

        getSelectedCarFromDB();

        selectBookingDate();

        bookingTime();

        changePickup();

        changeDropOff();

        addVias();

        updateBooking();

    }

    public void initializeCars(){
        carlist.add(new Car("Saloon", "4", "2", R.drawable.saloonimage, true));
        carlist.add(new Car("Estate", "4", "3", R.drawable.estateimage, false));
        carlist.add(new Car("MPV", "6", "3", R.drawable.mpvimage, false));
        carlist.add(new Car("Executive", "4", "2", R.drawable.saloonimage, false));
        carlist.add(new Car("8 Passenger", "8", "4", R.drawable.eightpassenger, false));

//		if (totalDistance==0){
        journeyMiles.setText(booking.getJobmileage() + " miles");
//
//		}else {
        //journeyMiles.setText(totalDistance + " miles");

        //}
        dateandtimeunix = (long) booking.getDatentime();
        journeyFare.setText(Math.round(booking.getFare())+".00");
        editBookingDate.setText(String.valueOf(booking.getDate()));
        editBookingTime.setText(String.valueOf(booking.getTime()));
        editBookingTo.setText(String.valueOf(booking.getTo()));
        editBookingFrom.setText(String.valueOf(booking.getfrom()));
        editBookingVias.setLayoutManager(new LinearLayoutManager(this));
        for (int i = 2; i <= booking.getFromtovia().size() - 1; i++) {
            if (booking.getFromtovia().get(i).getAddress() != null) {
                fromtoviaList.add(booking.getFromtovia().get(i));
            }
        }
        ViasAdapter = new ViasRecyclerViewAdapter(this, fromtoviaList);
        ViasAdapter.setClickListener(this);
        editBookingVias.setAdapter(ViasAdapter);

        additionalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moreOptions();
            }
        });

        saveOriginDetail.add( new SaveOriginDetail(booking.getFromtovia().get(0).getAddress().toString(),booking.getFromtovia().get(0).getLat(),
                booking.getFromtovia().get(0).getLon(),booking.getFromtovia().get(0).getPostcode().toString(),booking.getFromOutcode()));
        saveDesDetails.add( new SaveDesDetail(booking.getFromtovia().get(1).getAddress().toString(),booking.getFromtovia().get(1).getLat(),
                booking.getFromtovia().get(1).getLon(),booking.getFromtovia().get(1).getPostcode().toString(),booking.getto_outcode()));
    }

    public void updateViewForSelectedCar(Car selectedCarData) {
        //completebooking.setText("Confirm " + selectedCarData.getCarName());
        selectedCarName.setText(selectedCarData.getCarName());
        selectedPassen.setText(selectedCarData.getCarPassengerCapacity());
        selectedSuitcase.setText(selectedCarData.getCarLuggageCapacity());
        selectedCarIV.setImageResource(selectedCarData.getCarImage());
        selectedCarData.setSleceted(true);
        for (Car c : carlist) {
            if (c.getCarName().equals(selectedCarData.getCarName())) {
            } else {
                c.setSleceted(false);
            }
        }
        mAdapterCars.notifyDataSetChanged();
        //RecyclerViewCL.setVisibility(View.GONE);
    }

    public void  getSelectedCarFromDB(){
        if (booking.getVehicletype().equals("S")) {

            updateViewForSelectedCar(carlist.get(0));


        } else if (booking.getVehicletype().equals("E")) {

            updateViewForSelectedCar(carlist.get(1));

        } else if (booking.getVehicletype().equals("X")) {

            updateViewForSelectedCar(carlist.get(3));

        } else if (booking.getVehicletype().equals("8")) {

            updateViewForSelectedCar(carlist.get(4));

        } else if (booking.getVehicletype().equals("6")) {

            updateViewForSelectedCar(carlist.get(2));
        }


//		journeyMiles.setText(b.getJobmileage() + " miles");
//		journeyDuration.setText(b.getJobmileage() + " mins");
//		journeyFare.setText(String.valueOf(b.getFare()));

    }

    public void carList() {

        selectedCarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //selectedCarLayout.setVisibility(View.GONE);
                if (RecyclerViewCL.getVisibility() == View.GONE) {
                    RecyclerViewCL.setVisibility(View.VISIBLE);
                } else {
                    RecyclerViewCL.setVisibility(View.GONE);
                }
            }
        });

        mRecyclerViewCars = findViewById(R.id.carRecyclerView);



        CenterZoomLayoutManager layoutManager = new CenterZoomLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        mRecyclerViewCars.setLayoutManager(layoutManager);
        mAdapterCars = new CarSelectionAdapter(carlist, new CarSelectionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Car selectedCarData, View v) {
                if (totalDistance==0){
                    carsforFareCalculation(selectedCarData.getCarName(),booking.getJobmileage());
                }else {
                    carsforFareCalculation(selectedCarData.getCarName(),totalDistance);
                }

                //getClearDriver(selectedCarData.getCarName());
                updateViewForSelectedCar(selectedCarData);

            }
        });
        mRecyclerViewCars.setAdapter(mAdapterCars);
        mAdapterCars.notifyDataSetChanged();
        final SnapHelper snapHelper =  new LinearSnapHelper() {
            @Override
            public View findSnapView(RecyclerView.LayoutManager layoutManager) {
                return super.findSnapView(layoutManager);
            }
        };
        snapHelper.attachToRecyclerView(mRecyclerViewCars);
        mRecyclerViewCars.scrollToPosition(0);


    }

    public void initializeIDS(){
        selectedCarLayout = (ConstraintLayout) findViewById(R.id.selectedCarCL);
        RecyclerViewCL = (ConstraintLayout) findViewById(R.id.RecyclerViewCL);
        selectedCarName = findViewById(R.id.selectedCarName);
        selectedPassen = findViewById(R.id.selectedPassengerNo);
        selectedSuitcase = findViewById(R.id.selectedSuitcaseNo);
        selectedCarIV = findViewById(R.id.selectedCarIV);
        journeyMiles = findViewById(R.id.distanceID);
        journeyFare = (TextView) findViewById(R.id.fareLabel);
        editBookingDate = (TextView) findViewById(R.id.editBookingDate);
        editBookingTime = (TextView) findViewById(R.id.editBookingTime);
        editBookingTo = (TextView) findViewById(R.id.editBookingto);
        editBookingFrom = (TextView) findViewById(R.id.editBookingfrom);
        editBookingVias = (RecyclerView) findViewById(R.id.editBookingVias);
        additionalInfo = (TextView) findViewById(R.id.additionalInfo);
        moreOptionDialog = new Dialog(this);
        moreOptionDialog.setContentView(R.layout.activity_moreoption);
        Window window = moreOptionDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        switch1 = (SwitchCompat) moreOptionDialog.findViewById(R.id.switch1);
        flight_nmberId = (TextView) moreOptionDialog.findViewById(R.id.flightNumberId);
        moreOptionDoneId = (Button) moreOptionDialog.findViewById(R.id.moreOptionDoneId);
        instruction = (EditText) moreOptionDialog.findViewById(R.id.instruction);
        cancelButton = (Button) moreOptionDialog.findViewById(R.id.cancel);
        sqLiteAirportList = new SQLiteAirportList(this);
        editBookingAddVias = (ImageView) findViewById(R.id.editBookingAddVias);
        updateFareandMile = (UpdateFareandMile) BaseActivity.context;
        UpdateBookingbtn = (Button) findViewById(R.id.updateBooking);
        timeToDespatch = (long) booking.getTimetodespatch();
    }

    @Override
    public void onItemClick(View view, int position) {
        fromtoviaList.remove(position);
        ViasAdapter.notifyDataSetChanged();
        updateFareandMile.RefreshView();
    }

    public void moreOptions() {

        moreOptionDialog.show();
        moreOptionDialog.setCancelable(false);
        flight_nmberId.setText(booking.getFlightno());

        if (booking.getComment().equals("customer need a child seat. ")){
            switch1.setChecked(true);
        }

        if (booking.getComment().contains("customer need a child seat. ")){
            switch1.setChecked(true);
            String[] separated = booking.getComment().split("customer need a child seat. ");

            if (separated.length>0){
                instruction.setText(separated[1]);
            }

        }

        if (!booking.getComment().contains("customer need a child seat. ")){
            instruction.setText(booking.getComment());
        }





        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                moreOptionMethod();
            }
        });


        moreOptionDoneId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                booking.setFlightno(flight_nmberId.getText().toString());
                moreOptionMethod();
                booking.setComment(comment+""+instruction.getText().toString());
                moreOptionDialog.dismiss();

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                booking.setFlightno("");
                booking.setComment("");
                switch1.setChecked(false);
                moreOptionDialog.dismiss();
            }
        });



    }

    public void moreOptionMethod() {

        Boolean switchState = switch1.isChecked();

        if (switchState) {


            comment = "customer need a child seat. ";
            Toast.makeText(this, "child seat selected", Toast.LENGTH_SHORT).show();

        } else {

            comment = "";
            Toast.makeText(this, "child seat off", Toast.LENGTH_SHORT).show();

        }
    }

    public void selectBookingDate() {

        editBookingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//				String[] separated = b.getDate().split("-");
//				int year = Integer.parseInt(separated[0]);
//				int month = Integer.parseInt(separated[1]);
//				int day = Integer.parseInt(separated[2]);

                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        EditBookingActivity.this,
                        android.R.style.Theme_Holo_NoActionBar_TranslucentDecor,
                        mDateSetListener,
                        year, month, day);
//				long now = (long) (b.getDatentime()*1000);
//				String myDate = b.getDate();
//				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//				Date date = null;
//				try {
//					date = sdf.parse(myDate);
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
//				long millis = date.getTime();
                dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                //dialog.getDatePicker().setMaxDate(now+(1000*60*60*24*24));
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                editBookingDate.setText(day + "-" + month + "-" + year);
                booking.setDate(day + "-" + month + "-" + year);
                userselectedTimeUnix();
            }
        };

    }

    public void bookingTime() {

        editBookingTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final Calendar mcurrentTime = Calendar.getInstance();
                final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                final int minute = mcurrentTime.get(Calendar.MINUTE);

                int year = mcurrentTime.get(Calendar.YEAR);
                int month = mcurrentTime.get(Calendar.MONTH);
                int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);

                month = month + 1;

                final String date2 = day + "-" + month + "-" + year;

                TimePickerDialog mTimePicker;

                mTimePicker = new TimePickerDialog(EditBookingActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        if (booking.getDate() != null) {

                            if (booking.getDate().equals(date2)) {

                                if (selectedHour <= hour && selectedMinute < minute || selectedHour < hour && selectedMinute >= minute) {

                                    Toast.makeText(EditBookingActivity.this, "This is previous time", Toast.LENGTH_SHORT).show();
                                    editBookingTime.setText(hour + ":" + minute);
                                    booking.setTime(hour + ":" + minute);

                                } else {

                                    editBookingTime.setText(selectedHour + ":" + selectedMinute);
                                    booking.setTime(selectedHour + ":" + selectedMinute);

                                }

                            } else {

                                editBookingTime.setText(selectedHour + ":" + selectedMinute);
                                booking.setTime(selectedHour + ":" + selectedMinute);


                            }
                            userselectedTimeUnix();

                        } else {
                            Toast.makeText(EditBookingActivity.this, "First Select your Ride Date", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

    }

    public void userselectedTimeUnix() {

        String toParse = editBookingDate.getText().toString() + " " + editBookingTime.getText().toString(); // Results in "2-5-2012 20:43"
        SimpleDateFormat formatter = new SimpleDateFormat("d-M-yyyy hh:mm"); // I assume d-M, you may refer to M-d for month-day instead.
        Date date = null; // You will need try/catch around this

        try {
            date = formatter.parse(toParse);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = 0;
        if (date != null) {
            int offset = TimeZone.getDefault().getRawOffset() + TimeZone.getDefault().getDSTSavings();
            //  millis = (date.getTime())/(1000L);
            millis = (date.getTime()) + (offset);


        }
        long now = millis / 1000L;
        dateandtimeunix = now;


        long differ = dateandtimeunix - Network.sharedInstance.getBST();
        long timetodesp;
        if (differ >= 1800) {
            if (despatchtimeconvertToSecond==0){
                long reg = (long) (booking.getDatentime() - (long) booking.getTimetodespatch());
                timeToDespatch = dateandtimeunix - reg ;
            }else {
                timetodesp = dateandtimeunix - despatchtimeconvertToSecond;
                timeToDespatch = timetodesp;
            }

        }
//		b.setTimetodespatch(timeToDespatch);


    }

    public ArrayList<SaveOriginDetail> getOriginAdressDetail(String data) {
        String[] retrivepostCode = new String[0];
        Geocoder mGeocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        saveOriginDetail.clear();
        try {
            addresses = mGeocoder.getFromLocationName(data, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses.get(0).getPostalCode()!=null){
            retrivepostCode = addresses.get(0).getPostalCode().split(" ");
            saveOriginDetail.add(new SaveOriginDetail(data,
                    addresses.get(0).getLatitude(),addresses.get(0).getLongitude(),addresses.get(0).getPostalCode(),
                    retrivepostCode[0]));
        }else {

            String postCode , outCode;
            String[] tempOutCode = addresses.toString().split(",");
            String[] splitoutCode = tempOutCode[0].split(" ");
            postCode = tempOutCode[0];
            if (splitoutCode.length == 2) {
                outCode = splitoutCode[1];
            } else {
                outCode = "NPC";
            }
            saveOriginDetail.add(new SaveOriginDetail(data,
                    addresses.get(0).getLatitude(),addresses.get(0).getLongitude(),postCode,
                    outCode));
        }



        return saveOriginDetail;
    }


    public double[] carsforFareCalculation(String vehicleType , Double mileage) {
        FareCalculation fareCalculation = new FareCalculation(getApplicationContext());
        switch (vehicleType) {
            case "Saloon":
                carSymbol = "S";
                if ((isAirportOrigin && fromtoviaList.size()==0) || (isAirportDestination && fromtoviaList.size()==0)) {
                    double cusFare = (getFixedFareFromServer(mileage, fareList, "S"));
                    fareCalc = new double[]{cusFare, cusFare};
                } else {
                    fareCalc = fareCalculation.calculateFare("S",mileage);
                }
                break;
            case "Estate":
                carSymbol = "E";
                if ((isAirportOrigin && fromtoviaList.size()==0) || (isAirportDestination && fromtoviaList.size()==0)){
                    double cusFare = (getFixedFareFromServer(mileage, fareList, "E"));
                    fareCalc = new double[]{cusFare, cusFare};
                } else {
                    fareCalc = fareCalculation.calculateFare("E", mileage);
                }
                break;
            case "MPV":
                carSymbol = "6";
                if ((isAirportOrigin && fromtoviaList.size()==0) || (isAirportDestination && fromtoviaList.size()==0)){
                    double cusFare = (getFixedFareFromServer(mileage, fareList, "6"));
                    fareCalc = new double[]{cusFare, cusFare};
                } else {
                    fareCalc = fareCalculation.calculateFare("6", mileage);
                }
                break;
            case "Executive":
                carSymbol = "X";
                if ((isAirportOrigin && fromtoviaList.size()==0) || (isAirportDestination && fromtoviaList.size()==0)){
                    double cusFare = (getFixedFareFromServer(mileage, fareList, "X"));
                    fareCalc = new double[]{cusFare, cusFare};
                } else {
                    fareCalc = fareCalculation.calculateFare("X", mileage);
                }
                break;
            case "8 Passenger":
                carSymbol = "8";
                if ((isAirportOrigin && fromtoviaList.size()==0) || (isAirportDestination && fromtoviaList.size()==0)){
                    double cusFare = (getFixedFareFromServer(mileage, fareList, "8"));
                    fareCalc = new double[]{cusFare, cusFare};
                } else {
                    fareCalc = fareCalculation.calculateFare("8", mileage);
                }
                break;
        }

        journeyFare.setText(""+Math.round(fareCalc[0])+".00");
        return fareCalc;
    }

    public double getFixedFareFromServer(double miles, Map<String, List<Double>> fareList, String carType) {

        double fareS = 0;
        double fare = 0;
        double fareE = 0;
        double fare6 = 0;
        double fare8 = 0;
        double fareX = 0;
        CfgCustApp cfg = CfgCustApp.sharedinstance;
        ArrayList<Map<String, Double>> All_vehicle_fare = cfg.getAll_vehicle_fare();


        if (fareList.containsKey("S")) {
            fareS = (fareList.get("S")).get(0);
        } else {
            if (miles < cfg.getMilesUnit().get(0)) {
                fareS = cfg.getAirport_min_price();
            } else {
                fareS = calculateFixedFareSaloon(miles, cfg.getMilesUnit(), cfg.getCostUnit());
            }
        }
        switch (carType) {
            case "S"://saloon
                fare = fareS;
                break;

            case "E"://estate
                if (fareList.containsKey("E")) {
                    fareE = (fareList.get("E")).get(0);
                } else {
                    double addedFarePercent = (All_vehicle_fare.get(0)).get("E");
                    fareE = fareS + fareS * (addedFarePercent / 100);
                }
                fare = fareE;
                break;

            case "6"://MPV
                if (fareList.containsKey("6")) {
                    fare6 = (fareList.get("6")).get(0);
                } else {
                    double addedFarePercent = (All_vehicle_fare.get(0)).get("6");
                    fare6 = fareS + fareS * (addedFarePercent / 100);
                }
                fare = fare6;
                break;

            case "8"://8 Passenger
                if (fareList.containsKey("8")) {
                    fare8 = (fareList.get("8")).get(0);
                } else {

                    double addedFarePercent = (All_vehicle_fare.get(0)).get("8");
                    fare8 = fareS + fareS * (addedFarePercent / 100);
                }
                fare = fare8;
                break;

            case "X":// Executive
                if (fareList.containsKey("X")) {
                    fareX = (fareList.get("X")).get(0);
                } else {
                    double addedFarePercent = (All_vehicle_fare.get(0)).get("X");
                    fareX = fareS + fareS * (addedFarePercent / 100);
                }
                fare = fareX;
                break;
        }
        BigDecimal bd = new BigDecimal(Double.toString(fare));
        bd = bd.setScale(0, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    double calculateFixedFareSaloon(double miles, ArrayList<Double> milesUnit, ArrayList<Double> costUnit) {
        double totalFare = 0;
        double tmiles = 0;
        for (int i = 0; i < costUnit.size(); i++) {
            tmiles = tmiles + milesUnit.get(i);
            int x = 0;
            while (x <= tmiles && miles > 0) {
                if (x < milesUnit.get(i)) {
                    miles--;
                    x++;
                    totalFare = totalFare + (1 * costUnit.get(i));

                } else {
                    break;
                }
            }
        }
        BigDecimal bd = new BigDecimal(Double.toString(totalFare));
        bd = bd.setScale(0, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void placeToPlot(String pickup , String dropoff ) {
        try {
            JSONArray data = new JSONArray();
            data.put("AirportFixedPrices");

            JSONObject placeToPlotData = new JSONObject();
            placeToPlotData.put("FromPlace", pickup);
            placeToPlotData.put("ToPlot", dropoff);
            placeToPlotData.put("AccRef", "CASH");
            placeToPlotData.put("PlaceToPlotChk", 1);

            data.put(placeToPlotData);

            SocketEvent.sharedinstance.socket.emit("getmultipledata", data, new Ack() {
                @Override
                public void call(Object... args) {
                    try {

                        JSONArray jsonArray = new JSONArray(args[0].toString());

                        fareList = new HashMap<>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject plotsData = jsonArray.getJSONObject(i);
                            double accVal = plotsData.getDouble("AccVal");
                            String vehicleType = plotsData.getString("VehicleType");
                            double drvVal = plotsData.getDouble("DrvVal");

                            valuesOfFixedFare = new ArrayList<>();

                            valuesOfFixedFare.add(accVal);
                            valuesOfFixedFare.add(drvVal);

                            if (vehicleType.equals("Saloon")) {

                                fareList.put("S", valuesOfFixedFare);


                            } else if (vehicleType.equals("Estate")) {

                                fareList.put("E", valuesOfFixedFare);

                            } else if (vehicleType.equals("Executive")) {

                                fareList.put("X", valuesOfFixedFare);

                            } else if (vehicleType.equals("8 Passenger")) {

                                fareList.put("8", valuesOfFixedFare);

                            } else if (vehicleType.equals("6 Passenger")) {

                                fareList.put("6", valuesOfFixedFare);
                            }


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void plotToPlace(String pickup , String dropoff) {
        try {
            JSONArray data = new JSONArray();
            data.put("AirportFixedPrices");

            JSONObject plotToPlaceData = new JSONObject();
            plotToPlaceData.put("FromPlot", pickup);
            plotToPlaceData.put("ToPlace", dropoff);
            plotToPlaceData.put("AccRef", "CASH");
            plotToPlaceData.put("PlotToPlaceChk", 1);

            data.put(plotToPlaceData);

            SocketEvent.sharedinstance.socket.emit("getmultipledata", data, new Ack() {
                @Override
                public void call(Object... args) {

                    try {
                        JSONArray jsonArray = new JSONArray(args[0].toString());

                        fareList = new HashMap<>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject plotsData = jsonArray.getJSONObject(i);
                            double accVal = plotsData.getDouble("AccVal");
                            String vehicleType = plotsData.getString("VehicleType");
                            double drvVal = plotsData.getDouble("DrvVal");

                            valuesOfFixedFare = new ArrayList<>();

                            valuesOfFixedFare.add(accVal);
                            valuesOfFixedFare.add(drvVal);

                            if (vehicleType.equals("Saloon")) {

                                fareList.put("S", valuesOfFixedFare);

                            } else if (vehicleType.equals("Estate")) {

                                fareList.put("E", valuesOfFixedFare);

                            } else if (vehicleType.equals("Executive")) {

                                fareList.put("X", valuesOfFixedFare);

                            } else if (vehicleType.equals("8 Passenger")) {

                                fareList.put("8", valuesOfFixedFare);

                            } else if (vehicleType.equals("6 Passenger")) {

                                fareList.put("6", valuesOfFixedFare);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void isAirportSelected(){

        for (Airports a: sqLiteAirportList.getAllAirports()) {
            if (a.getName().equals(editBookingTo.getText().toString()) && fromtoviaList.size()==0){
                plotToPlace(booking.getFromOutcode(),booking.getTo());
                isAirportDestination = true;
                return;
            }else if (a.getName().equals(editBookingFrom.getText().toString()) && fromtoviaList.size()==0){
                placeToPlot(booking.getfrom(),booking.getto_outcode());
                isAirportOrigin = true;
                return;
            } }
    }


    public void changePickup(){
        editBookingFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditBookingActivity.this, AutocompletePlacePickerActivity.class);
                intent.putExtra("action", "editpickup");
                startActivityForResult(intent, 201);
            }
        });
    }

    public void changeDropOff(){

        editBookingTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditBookingActivity.this, AutocompletePlacePickerActivity.class);
                intent.putExtra("action", "editdropoffloc");
                startActivityForResult(intent, 201);
            }
        });

    }

    public void addVias(){

        editBookingAddVias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fromtoviaList.size()>=7){
                    Toast.makeText(getApplicationContext(), "Limit Exceed", Toast.LENGTH_SHORT).show();
                }
                else  {
                    Intent intent = new Intent(EditBookingActivity.this, AutocompletePlacePickerActivity.class);
                    intent.putExtra("action", "editvias");
                    startActivityForResult(intent, 202);
                }

            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //isAirportOrigin = false;
        //isAirportDestination = false;


        if (requestCode == 202) {
            if (resultCode == RESULT_OK) {
                String intentAction = data.getStringExtra("action");
                if (intentAction.equals("editvias")) {
                    if (fromtoviaList.size()!=0){
                        for (int i = 0; i< fromtoviaList.size() ; i++){
                            if (fromtoviaList.get(i).getAddress().equals(data.getStringExtra("viasAddress"))){
                                Toast.makeText(EditBookingActivity.this,"You already add this adress",Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                    }
                    fromtoviaList.add(CreateFrom2ViasOBJ(data.getStringExtra("viasAddress")));
                    ViasAdapter.notifyDataSetChanged();
                    int pos = fromtoviaList.size() - 1;
                    editBookingVias.scrollToPosition(pos);

                    updateFareandMile.RefreshView();




                }}
        }




        if (requestCode == 201) {
            if (resultCode == RESULT_OK) {
                String intentAction = data.getStringExtra("action");
                if (intentAction.equals("editpickup")) {

                    getOriginAdressDetail(data.getStringExtra("address"));

                    editBookingFrom.setText(data.getStringExtra("address"));



                    if (data.getExtras().getBoolean("selectedAirportOri")){
                        updateFareandMile.isPlacetoPlot();
                        isAirportOrigin=true;
                        return;
                    }else {
                        isAirportOrigin = false;
                    }
                    if (isAirportDestination){
                        updateFareandMile.isPlottoPlace();
                        return;
                    }

                }
                if (intentAction.equals("editdropoffloc")) {

                    getDesAdressDetail(data.getStringExtra("address"));

                    editBookingTo.setText(data.getStringExtra("address"));


                    if (data.getExtras().getBoolean("selectedAirportDes")){
                        updateFareandMile.isPlottoPlace();
                        isAirportDestination = true;
                        return;
                    }else {
                        isAirportDestination=false;
                    }

                    if (isAirportOrigin){
                        updateFareandMile.isPlacetoPlot();
                        return;
                    }


                }


                updateFareandMile.RefreshView();



            }
        }
    }


    public JSONArray setFromToVia() throws JSONException {

        JSONArray jArray = new JSONArray();
        JSONObject origin = new JSONObject();
        origin.put("info", "");
        origin.put("address",saveOriginDetail.get(0).getAddress());
        origin.put("lat", saveOriginDetail.get(0).getLat());
        origin.put("lon", saveOriginDetail.get(0).getLon());
        origin.put("postcode", saveOriginDetail.get(0).getPostcode());
        jArray.put(origin);

        JSONObject destination = new JSONObject();

        destination.put("info", "");
        destination.put("address", saveDesDetails.get(0).getAddress());
        destination.put("lat", saveDesDetails.get(0).getLat());
        destination.put("lon", saveDesDetails.get(0).getLon());
        destination.put("postcode", saveDesDetails.get(0).getPostcode());

        jArray.put(destination);

        for (FromToVia ftv : fromtoviaList) {
            //Address a  = getLocationFromAddress(this,wp);
            JSONObject obj = new JSONObject();
            obj.put("info", JSONObject.NULL);
            obj.put("address", ftv.getAddress() + "");
            obj.put("lat", ftv.getLat());
            obj.put("lon", ftv.getLon());
            obj.put("postcode", ftv.getPostcode());
            jArray.put(obj);
        }

        for (int i = 0; i < 7 - (fromtoviaList.size()); i++) {
            FromToVia f = new FromToVia();
            JSONObject obj = new JSONObject();
            obj.put("info", JSONObject.NULL);
            obj.put("address", JSONObject.NULL);
            obj.put("lat",0.00000d );
            obj.put("lon", 0.00000d);
            obj.put("postcode", JSONObject.NULL);
            jArray.put(obj);
        }


        return jArray;
    }

    public FromToVia CreateFrom2ViasOBJ(String data) {
        Geocoder mGeocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = mGeocoder.getFromLocationName(data, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        double wlat = addresses.get(0).getLatitude();
        double wlon = addresses.get(0).getLongitude();
        String postcode = addresses.get(0).getPostalCode();
        String wpAddress = addresses.get(0).getAddressLine(0);
        String winfo = null;
        if (postcode == null || postcode.equals("")){
            postcode = "NPC";
        }
        return new FromToVia(winfo, data, wlat, wlon, postcode);
    }

    public ArrayList<SaveDesDetail> getDesAdressDetail(String data) {
        Geocoder mGeocoder = new Geocoder(this, Locale.getDefault());
        String[] retrivepostCode = new String[0];
        List<Address> addresses = null;
        saveDesDetails.clear();
        try {
            addresses = mGeocoder.getFromLocationName(data, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.get(0).getPostalCode()!=null){
            retrivepostCode = addresses.get(0).getPostalCode().split(" ");
            saveDesDetails.add(new SaveDesDetail(data,
                    addresses.get(0).getLatitude(),addresses.get(0).getLongitude(),addresses.get(0).getPostalCode(),
                    retrivepostCode[0]));

        }else {
            String postCode , outCode;
            String[] tempOutCode = addresses.toString().split(",");
            String[] splitoutCode = tempOutCode[0].split(" ");
            postCode = tempOutCode[0];
            if (splitoutCode.length == 2) {
                outCode = splitoutCode[1];
            } else {
                outCode = "NPC";
            }
            saveDesDetails.add(new SaveDesDetail(data,
                    addresses.get(0).getLatitude(),addresses.get(0).getLongitude(),postCode,
                    outCode));
        }



        return saveDesDetails;
    }

    @Override
    public void RefreshView() {
        try {
            new DirectionFinder(this, editBookingFrom.getText().toString(),fromtoviaList ,editBookingTo.getText().toString(), true).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void isPlacetoPlot() {
        placeToPlot(editBookingFrom.getText().toString(),saveDesDetails.get(0).getOutcode());
        updateFareandMile.RefreshView();

    }

    @Override
    public void isPlottoPlace() {
        plotToPlace(saveOriginDetail.get(0).getOutcode(),editBookingTo.getText().toString());
        updateFareandMile.RefreshView();
    }

    @Override
    public void CalculateFare() {
        carsforFareCalculation("Saloon",totalDistance);
        updateViewForSelectedCar(carlist.get(0));

    }


    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Calculating Fare ..!", true);
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        for (final Route route : routes) {
            for (Distance d : route.distance) {
                totalDistance = totalDistance + d.value;
            }
            totalDistance = totalDistance * 0.000621371;
            totalDistance = Math.round(totalDistance);
        }

        updateFareandMile.CalculateFare();
        journeyMiles.setText(totalDistance + " miles");
    }

    @Override
    public void onDirectionFinderFail(String error_message) {

    }

    @Override
    public void start() {

    }

    @Override
    public void finish(String time) {
        String timetoDispatch = time;

        String[] splitTime = timetoDispatch.split(" ");
        getdespatchtime = splitTime[0];
        long timeDistance = (long) Double.parseDouble(getdespatchtime);
        despatchtimeconvertToSecond = (timeDistance * 60L);
        //timeToDespatch = convertToSecond;


        long differ = dateandtimeunix - Network.sharedInstance.getBST();
        if (differ >= 1800) {
            long timetodesp = dateandtimeunix - despatchtimeconvertToSecond;
            timeToDespatch = timetodesp;
        } else {
            long timetodesp = dateandtimeunix + despatchtimeconvertToSecond;
            timeToDespatch = timetodesp;
        }

    }






    private void sendRequestforDespatchTime() {

        try {
            new DirectionFinder(this, saveOriginDetail.get(0).getAddress(), officeaddress , 1).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }











    public  void  updateBooking(){

        UpdateBookingbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!editBookingFrom.getText().toString().equals(editBookingTo.getText().toString())){
                    if (timeToDespatch == 0){
                        timeToDespatch = (long) booking.getTimetodespatch();
                    }
//				if (dateandtimeunix == 0){
//					dateandtimeunix = (long) booking.getDatentime();
//				}

                    if (fareCalc==null){
                        fareCalc = new double[]{booking.getFare(), booking.getFare()};
                        fareCalc[0] = booking.getFare();
                    }

                    if (totalDistance == 0){
                        totalDistance = booking.getJobmileage();
                    }

                    bookingData();

                }
                else{
                    Toast.makeText(EditBookingActivity.this, "Both addresses can't same", Toast.LENGTH_SHORT).show();

                }}
        });


    }


    public void bookingData() {
        try {
            String office;

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(getApplicationContext())) {

                FareCalculation fareCalculation = new FareCalculation(this);
                final double getDrvFareToInsertInBooking = fareCalculation.getDrvfare();
                if (DataHolder.getInstance().namesOfNearOffice.size()!=0){
                    office = DataHolder.getInstance().namesOfNearOffice.get(0);
                    DataHolder.getInstance().namesOfNearOffice.clear();
                }else {
                    office = booking.getOffice();

                }


                try {

                    JSONArray query = new JSONArray();
                    query.put("Booking");

                    JSONObject filter = new JSONObject();
                    try {
                        filter.put("_id", booking.getId());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    query.put(filter);

                    JSONArray returnfromtoVias = setFromToVia();

                    JSONObject values = new JSONObject();

                    try {
                        values.put("from", saveOriginDetail.get(0).getAddress());
                        values.put("from_info", "");
                        values.put("from_outcode", saveOriginDetail.get(0).getOutcode());
                        values.put("fromtovia", returnfromtoVias);
                        values.put("office", FIXED_OFFICE_KEY);
                        values.put("time", editBookingTime.getText().toString().trim());
                        values.put("date", editBookingDate.getText().toString().trim());
                        values.put("to", saveDesDetails.get(0).getAddress());
                        values.put("to_info", "");
                        values.put("to_outcode", saveDesDetails.get(0).getOutcode());
                        values.put("fare", fareCalc[0]);
                        values.put("drvfare", Math.round(getDrvFareToInsertInBooking));
                        values.put("jobmileage", Math.round(totalDistance));
                        values.put("timetodespatch", timeToDespatch);
                        values.put("datentime",dateandtimeunix);
                        values.put("comment", booking.getComment());
                        values.put("flightno",booking.getFlightno());
                        values.put("numofvia", fromtoviaList.size());
                        values.put("vehicletype", carSymbol);

                        query.put(values);

                        SocketEvent.sharedinstance.socket.emit("updatedata", query, new Ack() {
                            @Override
                            public void call(Object... args) {
                                bookedBookingHistoryOfUser.clear();
                                // startActivity(new Intent(getApplicationContext(), BookingHistoryActivity.class));
                                finish();
                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}



