package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import buzybeez.com.buzybeezcabcrystalpalace.Models.BookingHistory;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CustomerConfig;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Driver;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Rating;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.ratingavg;
import de.hdodenhof.circleimageview.CircleImageView;

import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.SHARE_SUBJECT;

public class RatingScreen extends BaseActivity {
    Button btnDone;
    SharedPreferences setPrefrence;


    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setPrefrence = getSharedPreferences(Rating.prefratingname, Context.MODE_PRIVATE);
        edit = setPrefrence.edit();
        setContentView(R.layout.activity_rating_screen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // String ip = CustomerConfig.sharedinstance.getImageIP();
        final RatingBar ratingBar = (RatingBar) findViewById(R.id.rating);
        final TextView officetxt = (TextView) findViewById(R.id.thanksoffice);
        officetxt.setText("Thanks For Riding With "+SHARE_SUBJECT);
        TextView CusFare = (TextView) findViewById(R.id.fare);
        final CircleImageView driver = (CircleImageView) findViewById(R.id.imgDrvPic);
        if (BookingHistory.sharedinstance.getFare() != null) {
            edit.putString(Rating.prefdriverfare, String.valueOf(BookingHistory.sharedinstance.getFare()));
            edit.apply();
        }
//        if (Driver.sharedInstance.getDrvUID() != null) {
//            edit.putString(Rating.prefdriverid, Driver.sharedInstance.getDrvUID());
//            edit.apply();
//        }
        String[] uidsplit=  setPrefrence.getString(Rating.prefdriverid, "").split("@");
        CusFare.setText(setPrefrence.getString(Rating.prefdriverfare, ""));
        String image_url =  setPrefrence.getString(Rating.img_ip,"") + "/" + uidsplit[1] + "/"
                + uidsplit[0] + "/DriverSnapshot.png";


        Log.i("rate_id:",image_url);
        Glide.with(getApplicationContext())
                .load(image_url)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        driver.setImageBitmap(resource);
                    }
                });


        btnDone = findViewById(R.id.done);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDone.setEnabled(false);
                //checks if user given rating in not 0.0 then gives average of user rating and database rating of driver
                if (ratingBar.getRating() > 0.0) {
                    ratingavg ratingavg1 = new ratingavg(setPrefrence.getString(Rating.prefdriverid, ""), ratingBar.getRating());
                }
                edit.putString(Rating.prefdriverfare, "");
                edit.putString(Rating.prefdriverid, "");
                edit.putString(Rating.img_ip,"");
                edit.apply();
                Log.wtf("Driveridafterlogin:", "E:" + setPrefrence.getString(Rating.prefdriverid, ""));
                Intent intent = new Intent(RatingScreen.this, OriginAndDestination.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}