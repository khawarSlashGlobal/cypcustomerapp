package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.ChargeCollection;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccountCollection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview.CardAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CardInfo;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CardNumber;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;

public class PaymentMethodActivity extends BaseActivity implements ConnectionListener {
    private static final String TAG = "PhoneAuth";
    private String phoneVerificationId;
    private PhoneAuthProvider.ForceResendingToken resendToken;
    private FirebaseAuth fbAuth;
    TextView txt_debit_credit, card, availableCard, txtVerify;
    ImageView cardImage;
    ListView cardList;
    List<CardNumber> mCardNumber;
    CardAdapter cardAdapter;
    Gson gson;
    Button button;
    String json;
    ExternalAccountCollection customer;
    List<CardInfo> cardInfo;
    Switch cardSwitch;
    List<Customer> customers;
    ProgressBar progressBar;
    String custId = "";
    boolean refresh = false;
    String number;
    AlertDialog alertDialog;
    ProgressBar progressBarCustom;
    //private PhoneAuthProvider.OnVerificationStateChangedCallbacks
    //verificationCallbacks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);


        fbAuth = FirebaseAuth.getInstance();
        refresh = false;
        initialize();
        if
        (Network.isNetworkAvailable(PaymentMethodActivity.this)) {
            getCardId();

        } else if (!Network.isNetworkAvailable(PaymentMethodActivity.this)) {
            progressBar.setVisibility(View.GONE);
        }

        snackbarForConnectivityStatus();
    }


    public void paymentList()
    {
        cardList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String id = cardInfo.get(i).getId();

            }
        });


    }


    public void initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Add payment method");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mCardNumber = new ArrayList<>();
        cardInfo = new ArrayList<>();
        txtVerify = findViewById(R.id.textViewVerify);
        txt_debit_credit = findViewById(R.id.txt_credit_debit);
        cardList = findViewById(R.id.listViewCard);
        availableCard = findViewById(R.id.textViewCardDetails);
        button = findViewById(R.id.buttons);
        progressBar = findViewById(R.id.progressBarCard);
        cardSwitch = findViewById(R.id.switchCard);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCards();
            }
        });

        cardSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (cardSwitch.isChecked()) {
                    // Toast.makeText(getApplicationContext(),"switch on",Toast.LENGTH_LONG).show();
                    SharedPreferences sharedPreferences = getSharedPreferences("creditCardCheck", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("checkCard", "yes");
                    editor.apply();
                } else {

                    // Toast.makeText(getApplicationContext(),"switch off",Toast.LENGTH_LONG).show();

                    SharedPreferences sharedPreferences = getSharedPreferences("creditCardCheck", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("checkCard", "");
                    editor.apply();
                }
            }
        });


    }
    public void getCardId() {
        progressBar.setVisibility(View.VISIBLE);

        SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);
        String custEmail = preferences.getString("cust_email", "");
        String phone = preferences.getString("cust_phone", "");
        getDataFromEmail(custEmail, phone);

    }

    public void getDataFromEmail(final String customerEmail, final String phone) {
        Stripe.apiKey = "sk_test_0XDT5RHd7IXtuxJ0vhYGRGsh";
        gson = new GsonBuilder().setPrettyPrinting().create();
        final Map<String, Object> options = new HashMap<>();
        //options.put("description",phone);
        options.put("email", customerEmail);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    customers = Customer.list(options).getData();




                } catch (AuthenticationException e) {
                    e.printStackTrace();
                } catch (InvalidRequestException e) {
                    e.printStackTrace();
                } catch (APIConnectionException e) {
                    e.printStackTrace();
                } catch (CardException e) {
                    e.printStackTrace();
                } catch (APIException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (customers.size() > 0) {
                            Log.d("cust: ", customers.toString());
//                            email =customers.get(0).getEmail();
                            custId = customers.get(0).getId();
                            number = customers.get(0).getShipping().getPhone();
                            Log.d("phone: ", phone);
                            SharedPreferences preferences = getSharedPreferences("Check Customer", MODE_PRIVATE);
                            String verify = preferences.getString("verify", "");

                            if (verify.equals("yes")) {
                                selectAllCard();
                            } else {

                                progressBar.setVisibility(View.GONE);

                                availableCard.setText("Verify Your Account for add card into your app");
                                txtVerify.setVisibility(View.VISIBLE);
                                txtVerify.setText("click here for Verify");


                                txtVerify.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        verifyCustomer();
                                    }
                                });

                            }

                        } else {
                            progressBar.setVisibility(View.GONE);
                            availableCard.setVisibility(View.VISIBLE);
                            availableCard.setText("Account is not registerd");

                        }

                    }
                });

            }
        })
                .start();


    }


    public void verifyCustomer() {

        progressBar.setVisibility(View.VISIBLE);
        sendVerificationCode(number);


    }

    private void sendVerificationCode(String mobile) {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                30,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                verificationCallbacks);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            verificationCallbacks =
            new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                @Override
                public void onVerificationCompleted(
                        PhoneAuthCredential credential) {
                    // Toast.makeText(CustomerVerfication.this, "Code send", Toast.LENGTH_SHORT).show();

                    signInWithPhoneAuthCredential(credential);

                }

                @Override
                public void onVerificationFailed(FirebaseException e) {

                    if (e instanceof FirebaseAuthInvalidCredentialsException) {
                        // Invalid request
                        //confirmCodeBtn.setEnabled(false);
                        //Toast.makeText(CustomerVerfication.this, "Wrong Code", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Invalid credential: "
                                + e.getLocalizedMessage());

                        Toast.makeText(PaymentMethodActivity.this, "Please enter Valid Number", Toast.LENGTH_SHORT).show();
                    } else if (e instanceof FirebaseTooManyRequestsException) {
                        // SMS quota exceeded
                        Log.d(TAG, "SMS Quota exceeded.");
                        Toast.makeText(PaymentMethodActivity.this, "SMS Quota exceeded.", Toast.LENGTH_SHORT).show();

                    }


                }

                @Override
                public void onCodeAutoRetrievalTimeOut(String s) {

                    super.onCodeAutoRetrievalTimeOut(s);

                }


                @Override
                public void onCodeSent(String verificationId,
                                       PhoneAuthProvider.ForceResendingToken token) {

                    Toast.makeText(PaymentMethodActivity.this, "code send", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);

                    phoneVerificationId = verificationId;
                    resendToken = token;

                    Log.d("", "" + phoneVerificationId);

                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(PaymentMethodActivity.this);
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View dialogView = inflater.inflate(R.layout.verified_customer_dialog, null);
                    dialogBuilder.setView(dialogView);
                    alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    TextView textViewNumber = dialogView.findViewById(R.id.textViewVerification);
                    final EditText edtCode = dialogView.findViewById(R.id.editTextCode);
                    Button btnSend = dialogView.findViewById(R.id.buttonSend);

                    progressBarCustom = dialogView.findViewById(R.id.progressBarCustom);
                    textViewNumber.setText("Code sent to: " + number);
                    btnSend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final String code = edtCode.getText().toString();
                            if (code.equals("")) {
                                Toast.makeText(getApplicationContext(), "Please Enter Code", Toast.LENGTH_LONG).show();
                            } else {
                                verifyVerificationCode(code);

                            }
                        }
                    });

                }
            };


    //}


    private void verifyVerificationCode(String code) {
        progressBarCustom.setVisibility(View.VISIBLE);
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(phoneVerificationId, code);

        fbAuth.signInWithCredential(credential)
                .addOnCompleteListener(PaymentMethodActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            progressBarCustom.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "Sucessfull", Toast.LENGTH_LONG).show();
                            alertDialog.dismiss();
                            SharedPreferences sharedPreferences = getSharedPreferences("Check Customer", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("verify", "yes");
                            editor.apply();
                            getCardId();


                        } else {

                            //verification unsuccessful.. display an error message
                            progressBarCustom.setVisibility(View.GONE);
                            String message = "Somthing is wrong, we will fix it soon...";
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }


                        }
                    }
                });

    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        fbAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {


                            fbAuth.signOut();
                            //finish();


                        } else {
                            if (task.getException() instanceof
                                    FirebaseAuthInvalidCredentialsException) {

                                // The verification code entered was invalid
                                Toast.makeText(PaymentMethodActivity.this, "Please enter a valid code", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                });


    }


    public void selectAllCard() {

        Stripe.apiKey = "sk_test_0XDT5RHd7IXtuxJ0vhYGRGsh";
        gson = new GsonBuilder().setPrettyPrinting().create();


        if (!custId.equals("")) {
            final Map<String, Object> cardParams = new HashMap<String, Object>();
            cardParams.put("object", "card");
            cardParams.put("limit", 3);

            new Thread(new Runnable() {
                @Override
                public void run() {


                    try {
                        customer = Customer.retrieve(custId).getSources().list(cardParams);

                    } catch (AuthenticationException e) {
                        Log.d("auth exception: ", e.toString());
                        e.printStackTrace();
                    } catch (InvalidRequestException e) {
                        Log.d("invalid", e.toString());
                        e.printStackTrace();
                    } catch (APIConnectionException e) {
                        Log.d("api conn: ", e.toString());
                        e.printStackTrace();
                    } catch (CardException e) {
                        Log.d("card exception: ", e.toString());
                        e.printStackTrace();
                    } catch (APIException e) {
                        Log.d("api exception: ", e.toString());
                        e.printStackTrace();
                    }




//
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Log.d("customer", customer.toString());
                            json = gson.toJson(customer);
                            Log.d("selectAllCard: ", json);

                            try {
                                JSONObject obj = new JSONObject(json);
                                JSONArray userArray = obj.getJSONArray("data");
                                for (int i = 0; i < userArray.length(); i++) {
                                    JSONObject cardDetail = userArray.getJSONObject(i);
                                    String brand = cardDetail.getString("brand");
                                    String country = cardDetail.getString("country");
                                    String expMonth = String.valueOf(Integer.parseInt(cardDetail.getString("expMonth")));
                                    String expYear = String.valueOf(Integer.parseInt(cardDetail.getString("expYear")));
                                    String cvcCheck = cardDetail.getString("cvcCheck");
                                    String last4 = cardDetail.getString("last4");
                                    String id = cardDetail.getString("id");
                                    String fingerprint = cardDetail.getString("fingerprint");
                                    cardInfo.add(new CardInfo(brand, country, expMonth, expYear, cvcCheck, last4, id, custId, fingerprint));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//
                            showCards();
                        }
                    });
                }
            }).start();
        }
    }




    public void credit(View view) {

        if
        (Network.isNetworkAvailable(PaymentMethodActivity.this)) {
            Intent intent = new Intent(PaymentMethodActivity.this, AddCardsActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Please connect your internet first", Toast.LENGTH_LONG).show();
        }


        //finish();
    }

    @Override
    protected void onResume() {
        super.onResume();


        if (refresh == true) {


            Intent refresh = new Intent(this, PaymentMethodActivity.class);
            startActivity(refresh);
            finish();

        }


    }


    @Override
    protected void onStop() {
        super.onStop();


        refresh = true;


    }

    public void showCards() {
        if (!cardInfo.isEmpty()) {
            availableCard.setVisibility(View.VISIBLE);
            cardAdapter = new CardAdapter(PaymentMethodActivity.this, cardInfo, availableCard);
            cardList.setAdapter(cardAdapter);
            cardAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.GONE);
            availableCard.setText("Your Available cards");
            txtVerify.setVisibility(View.GONE);
        } else {
            availableCard.setVisibility(View.VISIBLE);
            availableCard.setText("No Card Found");
            txtVerify.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);

        }


    }


}
