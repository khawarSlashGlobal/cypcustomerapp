package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import buzybeez.com.buzybeezcabcrystalpalace.CustomLocation.adapters.PlacesAutoCompleteAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.CustomLocation.listeners.RecyclerItemClickListener;
import buzybeez.com.buzybeezcabcrystalpalace.CustomLocation.utility.Constants;
import buzybeez.com.buzybeezcabcrystalpalace.DataHandler.SQLiteAirportList;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Airports;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Helpers.ScrollDisabledListView;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;

import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.dropoffloc;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.fromtoviaList;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.isAirportAtDest;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.isAirportAtOri;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.pickuploc;

public class AutocompletePlacePickerActivity extends AppCompatActivity {
    ///////////////////
    String intentAction;
    ArrayList<Airports> airportDataList = new ArrayList<>();
    SQLiteAirportList airportDbReff;
    LinearLayout cantsearchtxt;
    public String[] airports;
    ScrollDisabledListView list;
    ArrayAdapter adapter;
    AutoCompleteTextView mAutocompleteView;
    protected GoogleApiClient mGoogleApiClient;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private static final LatLngBounds BOUNDS = new LatLngBounds(
            new LatLng(-0, 0), new LatLng(0, 0));

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        Log.i(Constants.PlacesTag, "GoogleApiClient for places Connected");

                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.e(Constants.PlacesTag, "GoogleApiClient for places suspended");

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.e(Constants.PlacesTag, "GoogleApiClient for places Failed");
                    }
                })
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }
//////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autocomplete_place_picker);
        intentAction = getIntent().getStringExtra("action");
        Toolbar toolbar = (Toolbar) findViewById(R.id.AutoComptoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        airportDbReff = new SQLiteAirportList(this);
        airportDataList = airportDbReff.getAllAirports();
        airports = new String[airportDataList.size()];
        int i = 0;
        for (Airports a:airportDataList
        ) { airports[i]=a.getName();
            i++;
        }


        /////////////////

        list = (ScrollDisabledListView) findViewById(R.id.discountedList);
        adapter = new ArrayAdapter(this, R.layout.listairports, airports);
        list.setAdapter(adapter);
        buildGoogleApiClient();
        mAutocompleteView = findViewById(R.id.autoCompleteTextView);
        mAutocompleteView .requestFocus();
        cantsearchtxt=findViewById(R.id.cantsearchtext);
        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(this, R.layout.listairports,
                mGoogleApiClient, BOUNDS, null);


        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewOD);
        //mLinearLayoutManager = new LinearLayoutManager(this);

        mLinearLayoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAutoCompleteAdapter);

        mRecyclerView.setVisibility(View.GONE);
        list.setVisibility(View.GONE);

        mAutocompleteView.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(Network.isNetworkAvailable(AutocompletePlacePickerActivity.this)){
                    cantsearchtxt.setVisibility(View.GONE);
                    if (!s.toString().equals("") && mGoogleApiClient.isConnected()) {
                        mAutoCompleteAdapter.getFilter().filter(s.toString());
                    } else if (!mGoogleApiClient.isConnected()) {
                        //Toast.makeText(getApplicationContext(), Constants.API_NOT_CONNECTED, Toast.LENGTH_SHORT).show();
                        Log.e(Constants.PlacesTag, Constants.API_NOT_CONNECTED);
                    }
                    String text = mAutocompleteView.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.getFilter().filter(text);

                    if(count==0){
                        mRecyclerView.setVisibility(View.GONE);
                        list.setVisibility(View.GONE);
                    }else{
                        mRecyclerView.setVisibility(View.VISIBLE);
                        list.setVisibility(View.VISIBLE);
                    }

                }
                else
                {
                    cantsearchtxt.setVisibility(View.VISIBLE);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(final View view, int position) {
                        //selection is made from google
                        setSelectionInTextviews(view,"google");

                    }
                })

        );

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //selection is made from our Predefined List

                setSelectionInTextviews(view,"airports");
            }
        });
//////////////////
    }

    private void setSelectionInTextviews(View view,String ListType) {
        if(intentAction.equals("pickup")){

            if(ListType.equals("airports")){
                isAirportAtOri = true;
            }else{
                isAirportAtOri = false;
            }
            TextView selectedItemText  = view.findViewById(R.id.address);
            pickuploc.setText(selectedItemText.getText().toString().trim());
            Intent returnIntent = new Intent();
            returnIntent.putExtra("action",intentAction);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }
        if(intentAction.equals("dropoffloc")){
            if(ListType.equals("airports")){
                isAirportAtDest = true;
            }else{
                isAirportAtDest = false;
            }
            TextView selectedItemText  = view.findViewById(R.id.address);
            dropoffloc.setText(selectedItemText.getText().toString().trim());
            Intent returnIntent = new Intent();
            returnIntent.putExtra("action",intentAction);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }

        if(intentAction.equals("waypoints")){
//            if(ListType.equals("airports")){
//                isAirportAtDest = true;
//            }else{
            //  isAirportAtDest = false;
            //   }
            TextView selectedItemText  = view.findViewById(R.id.address);
            String wpAddress = selectedItemText.getText().toString().trim();
            if(!(fromtoviaList.size()<7) || wpAddress.equals(pickuploc.getText().toString()) || wpAddress.equals(dropoffloc.getText().toString()) )
            {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("action","");
                setResult(Activity.RESULT_CANCELED,returnIntent);

                finish();

                return;
            }
            for(int i=0;i<fromtoviaList.size();i++)
            {
                if(fromtoviaList.get(i).getAddress().equals(wpAddress)){
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("action","");
                    setResult(Activity.RESULT_CANCELED,returnIntent);
                    finish();
                    return;
                }
            }


            fromtoviaList.add(createFromToViaObjwithlatlng(view));
            Intent returnIntent = new Intent();
            returnIntent.putExtra("action",intentAction);
            setResult(Activity.RESULT_OK,returnIntent);

            finish();
        }
        if(intentAction.equals("editpickup")){

            boolean isAirportSelectOri=false;
            if(ListType.equals("airports")){
                isAirportSelectOri = true;
            }else{
                isAirportSelectOri = false;
            }
            TextView selectedItemText  = view.findViewById(R.id.address);
//            pickuploc.setText(selectedItemText.getText().toString().trim());
//            if (pickup1 !=null) {
//                pickup1.setText(selectedItemText.getText().toString().trim());
//            }
            Intent returnIntent = new Intent();
            returnIntent.putExtra("action",intentAction);
            returnIntent.putExtra("selectedAirportOri",isAirportSelectOri);
            returnIntent.putExtra("address",selectedItemText.getText().toString().trim());
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }
        if(intentAction.equals("editdropoffloc")){
            boolean isAirportSelectDes=false;
            if(ListType.equals("airports")){
                isAirportSelectDes = true;
            }else{
                isAirportSelectDes = false;
            }
            TextView selectedItemText  = view.findViewById(R.id.address);
//            dropoffloc.setText(selectedItemText.getText().toString().trim());
//            if (destination1 != null) {
//                destination1.setText(selectedItemText.getText().toString().trim());
//            }
            Intent returnIntent = new Intent();
            returnIntent.putExtra("action",intentAction);
            returnIntent.putExtra("selectedAirportDes",isAirportSelectDes);
            returnIntent.putExtra("address",selectedItemText.getText().toString().trim());
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }

        if(intentAction.equals("editvias")){
            Intent returnIntent = new Intent();
            returnIntent.putExtra("action",intentAction);
            TextView selectedItemText  = view.findViewById(R.id.address);
            returnIntent.putExtra("viasAddress",selectedItemText.getText().toString().trim());
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }
    }

    public FromToVia createFromToViaObjwithlatlng(View view) {
        // Place place = PlacePicker.getPlace(data, this);
        String winfo = "";
        TextView selectedItemText  = view.findViewById(R.id.address);
        String wpAddress = selectedItemText.getText().toString().trim();
        Double wlat = 0.0;
        Double wlon =0.0;
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng p1 = null;
        String wpPostalCode="";
        Address location = null;
        try {
            // May throw an IOException
            address = coder.getFromLocationName(wpAddress, 1);

            location = address.get(0);
            wpPostalCode = location.getPostalCode();
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );
            wlat= p1.latitude;
            wlon= p1.longitude;
        } catch (IOException ex) {

            ex.printStackTrace();
        }
        FromToVia ftv = new FromToVia(winfo, wpAddress, wlat,wlon , wpPostalCode);
        return ftv;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
