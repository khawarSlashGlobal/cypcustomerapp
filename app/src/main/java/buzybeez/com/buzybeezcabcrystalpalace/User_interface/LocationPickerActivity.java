package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.GPStracker;

import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.AddWaypoints.adapter;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.fromtoviaList;

public class LocationPickerActivity extends BaseActivity implements
        OnMapReadyCallback, ConnectionListener
        , GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraIdleListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static double lat;
    public static double lng;
    public static String postcode;
    public static String outcode;
    public static boolean flag;
    static AutoCompleteTextView locationAutocomplete;
    Button addLocation;
    ImageView marker_imagehome;
    List<Address> addresses;
    GPStracker gpStracker;
    LatLng latLng;
    String yourAddress;
    TextView marker_label;
    int PLACE_PICKER_REQUEST = 1;
    ProgressBar progressBar;
    String action;
    private GoogleMap mMap;
    private ImageButton myLocationButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_location_picker);
            gpStracker = new GPStracker(this);

            //intent.putExtra("action","home");
            action = getIntent().getStringExtra("action");


            if (getLocationMode(this) == 1) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            } else {
                snackbarForConnectivityStatus();
                screenObjects_id();
                MylocationBtnClick();
                setLocationAutocompleteClickListner();
            }

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        addLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AsyncTask execute = new Asynctask();
                ((Asynctask) execute).onPreExecute();
                // progressBar.setVisibility(View.VISIBLE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            String homelocation = locationAutocomplete.getText().toString();
                            Geocoder geocoder = new Geocoder(LocationPickerActivity.this, Locale.getDefault());
                            String homepostalcode = geocoder.getFromLocationName(homelocation, 1).get(0).getPostalCode();
                            if (homepostalcode != null) {
                                String[] retrivepostCode = homepostalcode.split(" ,");
                                outcode = retrivepostCode[0];
                                postcode = homepostalcode;
                            }
                            lat = geocoder.getFromLocationName(homelocation, 1).get(0).getLatitude();
                            lng = geocoder.getFromLocationName(homelocation, 1).get(0).getLongitude();
                            if (action.equals("home")) {
                                sendhomelocSharedPrefence(locationAutocomplete.getText().toString());
                                Toast.makeText(LocationPickerActivity.this, "Saved Home Location", Toast.LENGTH_SHORT).show();
                            }
                            if (action.equals("office")) {
                                sendOfficelocSharedPrefence(locationAutocomplete.getText().toString());
                                Toast.makeText(LocationPickerActivity.this, "Saved Office Location", Toast.LENGTH_SHORT).show();
                            }
                            if (action.equals("vias")) {
                                addWaypoint(locationAutocomplete.getText().toString());
                                Toast.makeText(LocationPickerActivity.this, "Saved Vais Location", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                });

                finish();

            }
        });
    }

    public void screenObjects_id() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addLocation = (Button) findViewById(R.id.donebooking);
        marker_imagehome = (ImageView) findViewById(R.id.marker_imagehome);
        locationAutocomplete = (AutoCompleteTextView) findViewById(R.id.SearchAutoCompLocPicker);
        marker_label = (TextView) findViewById(R.id.marker_label);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (action.equals("home")) {
            addLocation.setText("Save Home Location");
            getSupportActionBar().setTitle("Add Home Location");
        }
        if (action.equals("office")) {
            addLocation.setText("Save Office Location");
            getSupportActionBar().setTitle("Add office Location");
        }
        if (action.equals("vias")) {
            addLocation.setText("Save Vias Location");
            getSupportActionBar().setTitle("Add Vias Location");
        }


    }

    public int getLocationMode(Context context) throws Settings.SettingNotFoundException {
        return Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        marker_label.setVisibility(View.GONE);
        mMap = googleMap;
        mMap.clear();
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        try {
            Geocoder gcd = new Geocoder(this, Locale.getDefault());
            addresses = gcd.getFromLocation(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude, 1);


            if (addresses.size() > 0 && addresses != null) {
                yourAddress = addresses.get(0).getAddressLine(0);
                LatLng lng = new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);
                if (lng.latitude != 0.0) {
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                    mMap.moveCamera(cameraUpdate);
                }

            }


        } catch (IOException e) {
            e.printStackTrace();
        }


        if (addresses.size() == 0) {
            moveCameraToCurrLoc();
        }

    }


    @Override
    public void onCameraIdle() {
        gpStracker = new GPStracker(this);
        marker_label.setVisibility(View.GONE);
        // marker_imagehome.setVisibility(View.GONE);
        try {
            Geocoder gcd = new Geocoder(this, Locale.getDefault());
            addresses = gcd.getFromLocation(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude, 1);
            if (addresses.size() > 0 && addresses != null) {
                yourAddress = addresses.get(0).getAddressLine(0);


            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        //  mMap.addMarker(new MarkerOptions().position(mMap.getCameraPosition().target).anchor(0.5f, .05f).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
        // if(gpStracker.getLatitude()!=0.0){
        locationAutocomplete.setText(yourAddress);
        //}
    }

    @Override
    public void onCameraMove() {
        marker_imagehome.setVisibility(View.VISIBLE);
        marker_label.setVisibility(View.VISIBLE);
        marker_label.setText("Searching........");
        mMap.clear();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void moveCameraToCurrLoc() {
        //storeLatLong();
        gpStracker = new GPStracker(this);
        latLng = new LatLng(gpStracker.getLatitude(), gpStracker.getLongitude());
        if (latLng.latitude != 0.0) {
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(17.0f).build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            mMap.moveCamera(cameraUpdate);
        }
    }

    public void MylocationBtnClick() {
        myLocationButton = (ImageButton) findViewById(R.id.targetlocation);
        gpStracker = new GPStracker(LocationPickerActivity.this);
        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double lati = gpStracker.getLatitude();
                Double longi = gpStracker.getLongitude();

                if (lati == 0.0) {
                    if (checkLocationPermission()) {
                        EnableGPSAutoMatically();
                        gotoCurrentLocWhenAvalible();
                    } else {
                        requestPermission();
                    }
                } else {
                    latLng = new LatLng(lati, longi);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16.0f);
                    mMap.animateCamera(cameraUpdate);
                }

            }
        });

    }


    public void gotoCurrentLocWhenAvalible() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                GPStracker gpStracker = new GPStracker(LocationPickerActivity.this);
                if (gpStracker.getLocation() != null) {
                    latLng = new LatLng(gpStracker.getLatitude(), gpStracker.getLongitude());
                    if (latLng.latitude != 0.0) {
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(17.0f).build();
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        mMap.moveCamera(cameraUpdate);
                    }
                    handler.removeCallbacksAndMessages(null);
                } else {
                    handler.postDelayed(this, 100);
                }
            }
        }, 100);
    }


    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(LocationPickerActivity.this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            break;

                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            try {
                                status.startResolutionForResult(LocationPickerActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                            }
                            break;

                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Toast.makeText(LocationPickerActivity.this, "Setting change not allowed", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


    }

    public void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ALLPERMISSIONS_CODE);

        } else {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ALLPERMISSIONS_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case REQUEST_ALLPERMISSIONS_CODE: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    EnableGPSAutoMatically();
                    gotoCurrentLocWhenAvalible();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void setLocationAutocompleteClickListner() {

        locationAutocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = true;

                AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                        .setTypeFilter(Place.TYPE_COUNTRY)
                        .setCountry("UK")
                        .build();
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                Intent intent;

                //restrict user to search places only in UK

                try {
                    intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .setFilter(autocompleteFilter).build(LocationPickerActivity.this);

//                    intent = builder.build((Activity) OriginAndDestination.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST) {

            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String address = String.format(String.valueOf(place.getAddress()));
                if (flag == true) {
                    locationAutocomplete.setText(address);
                    extractAddressFromOrigin(place);

                }
            }
        }
    }


    public void extractAddressFromOrigin(Place place) {

        Geocoder mGeocoder = new Geocoder(LocationPickerActivity.this, Locale.getDefault());

        List<Address> addresses = null;
        try {
            addresses = mGeocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            addresses.get(0).getLocality();
            if (addresses.get(0).getPostalCode() == null) {
                latitude = addresses.get(0).getLatitude();
                longitude = addresses.get(0).getLongitude();
            } else {
                latitude = addresses.get(0).getLatitude();
                longitude = addresses.get(0).getLongitude();
                LatLng lng = new LatLng(latitude, longitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(lng, 17.0f);
                mMap.moveCamera(cameraUpdate);
            }

        }
    }


    public void sendhomelocSharedPrefence(String home_Loc) {
        SharedPreferences.Editor editor = getSharedPreferences("home_address", MODE_PRIVATE).edit();
        editor.putString("name", home_Loc);
        editor.putString("homepostcode", postcode);
        editor.putString("homeoutcode", outcode);
        editor.putString("homelat", (String.valueOf(lat)));
        editor.putString("homelng", String.valueOf(lng));
        editor.apply();
    }

    public void sendOfficelocSharedPrefence(String address) {
        SharedPreferences.Editor editor = getSharedPreferences("office_address", MODE_PRIVATE).edit();
        editor.putString("office", address);
        editor.putString("officepostcode", postcode);
        editor.putString("officeoutcode", outcode);
        editor.putString("officelat", (String.valueOf(lat)));
        editor.putString("officelng", String.valueOf(lng));
        editor.apply();
    }

    public void addWaypoint(String address) {
        FromToVia ftv = new FromToVia("", address, lat, lng, postcode);
        fromtoviaList.add(ftv);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public class Asynctask extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            // progressBar.setVisibility(View.GONE);
        }
    }


}