package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;
import com.stripe.model.ChargeCollection;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview.TransactionAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Transaction;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;

public class ChargesDetailActivty extends BaseActivity implements ConnectionListener {

    ImageView cardImage ;
    TextView  textCardNumber;
    TextView transactionData;
    ListView listViewPayment;
    String cardId;
    String custId;
    String cardNumber;
    String brand;
    String fingerprints;
    List<Transaction> transactionsInfo;
    TransactionAdapter transactionAdapter;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charges_detail_activty);
        intialize();
        if
        (Network.isNetworkAvailable(ChargesDetailActivty.this)) {
            getPaymentData();

        } else if (!Network.isNetworkAvailable(ChargesDetailActivty.this)) {

        }
        snackbarForConnectivityStatus();

    }
    public void getPaymentData()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ChargesDetailActivty.this);
        progressDialog.setTitle("Load Transaction");
        progressDialog.setMessage("Loading please wait...");
        progressDialog.show();

        Stripe.apiKey = "sk_test_0XDT5RHd7IXtuxJ0vhYGRGsh";

//        final Map<String, Object> card = new HashMap<String, Object>();
//        card.put("id","card_1EKO8XLj2nfEGuW9wMKvJV2d");

        final Map<String, Object> source = new HashMap<String, Object>();
        //source.put("limit","3");
        source.put("customer", custId);
        // card.put("object",card);



        new Thread(){
            @Override
            public void run() {
                try {



                    final List<Charge> charge =Charge.list(source).getData();

                    Log.d("charge", charge.toString());

                    // int a = charge.size();


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for(int i = 0; i<charge.size();i++)
                            {
                                DecimalFormat decimalFormat = new DecimalFormat("#.00");

                                double amounts = charge.get(i).getAmount()/100;

                                String amount = String.valueOf(decimalFormat.format(amounts));
                                String currency = String.valueOf(charge.get(i).getCurrency());
                                Long unixSeconds = charge.get(i).getCreated();
//                                String fingerprint = charge.get(0).getCard().getFingerprint();
                                Date date = new Date(unixSeconds*1000L);
                                String time = String.valueOf(date.getTime());
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss");


                                //sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
                                String formattedDate = sdf.format(date);
//
                                if(charge.get(i).getSource().getId().equals(cardId))
                                {
                                    transactionsInfo.add(new Transaction(cardId,amount,currency,formattedDate,time));
                                }

//                                if(charge.get(i).g)
//                                {
//                                    transactionsInfo.add(new Transaction(cardId,amount,currency,formattedDate,time));
//                                }



                            }
                            progressDialog.dismiss();
                            if(transactionsInfo.isEmpty())
                            {
                                transactionData.setText("No Transaction Found from this card");
                            }
                            transactionAdapter = new TransactionAdapter(ChargesDetailActivty.this,transactionsInfo);
                            listViewPayment.setAdapter(transactionAdapter);
                            transactionAdapter.notifyDataSetChanged();
                        }
                    });




                } catch (AuthenticationException e) {
                    e.printStackTrace();
                } catch (InvalidRequestException e) {
                    e.printStackTrace();
                } catch (APIConnectionException e) {
                    e.printStackTrace();
                } catch (CardException e) {
                    e.printStackTrace();
                } catch (APIException e) {
                    e.printStackTrace();
                }
            }
        }.start();


    }

    public void intialize()
    {  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Transaction List");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        cardImage = findViewById(R.id.imageViewCreditCard);
        textCardNumber = findViewById(R.id.textViewCardNumber);
        listViewPayment = findViewById(R.id.list_view_payment);
        transactionData = findViewById(R.id.transactionData);
        Bundle extras = getIntent().getExtras();
        cardId= extras.getString("card_id");
        custId=extras.getString("cust_id");
        cardNumber = extras.getString("number");
        brand=extras.getString("brand");
        fingerprints=extras.getString("fingerprint");
        transactionsInfo= new ArrayList<>();
        setValue();
        //Toast.makeText(getApplicationContext(),cardId,Toast.LENGTH_LONG).show();

    }

    public void setValue()
    {
        textCardNumber.setText("..."+cardNumber);

        if(brand.equals("Visa"))
        {
            cardImage.setImageResource(R.drawable.cio_ic_visa);
        }
        else if(brand.equals("MasterCard"))
        {
            cardImage.setImageResource(R.drawable.mastercard);
        }

        else if(brand.equals("American Express"))
        {
            cardImage.setImageResource(R.drawable.amex);
        }
        else {
            cardImage.setImageResource(R.drawable.cio_ic_visa);
        }

    }

}
