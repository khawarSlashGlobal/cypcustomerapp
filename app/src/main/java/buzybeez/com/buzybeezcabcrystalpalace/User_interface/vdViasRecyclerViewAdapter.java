package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;

import java.util.List;

import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.R;

public class vdViasRecyclerViewAdapter extends RecyclerView.Adapter<vdViasRecyclerViewAdapter.ViewHolder> {

    private List<FromToVia> mfromtovia;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    vdViasRecyclerViewAdapter(Context context, List<FromToVia> fromtovia) {
        this.mInflater = LayoutInflater.from(context);
        this.mfromtovia = fromtovia;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.vdwaypoint, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String addWaypoint =(String) mfromtovia.get(position).getAddress();
        holder.myTextView.setText(addWaypoint);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mfromtovia.size();
    }

    // convenience method for getting data at click position
    FromToVia getItem(int id) {
        return mfromtovia.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;

    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AutoCompleteTextView myTextView;
        ImageView cancelbtn;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.select_location1);
            cancelbtn = itemView.findViewById(R.id.cancel_wp);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
}