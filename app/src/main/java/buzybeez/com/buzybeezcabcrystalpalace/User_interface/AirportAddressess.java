package buzybeez.com.buzybeezcabcrystalpalace.User_interface;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.Locale;

import buzybeez.com.buzybeezcabcrystalpalace.R;

import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.selection;


//This is not for use

public class AirportAddressess extends BaseActivity {
    public String[] airports = {"LONDON GATWICK AIRPORT NORTH TERMINAL RH6 0PJ",
            "LONDON GATWICK AIRPORT SOUTH TERMINAL RH6 0NP",
            "LONDON HEATHROW AIRPORT TERMINAL 1 TW6 1AP",
            "LONDON HEATHROW AIRPORT TERMINAL 2 TW6 1EW",
            "LONDON HEATHROW AIRPORT TERMINAL 3 TW6 1QG",
            "LONDON HEATHROW AIRPORT TERMINAL 4 TW6 3XA",
            "LONDON HEATHROW AIRPORT TERMINAL 4, LONGFORD, HOUNSLOW, UK TW14 8SF",
            "LONDON HEATHROW AIRPORT TERMINAL 5 TW6 2GA",
            "TERMINAL 5, LONGFORD, HOUNSLOW, UK TW6 2GA",
            "LONDON STANSTED AIRPORT CM24 1RW",
            "LONDON LUTON AIRPORT LU2 9QT",
            "LONDON CITY AIRPORT E16 2PX"};
    ImageView imgCancelSearch;
    AutoCompleteTextView tvSearchAirports;
    ArrayAdapter adapter;
    ListView list;
    String selectedAirport = "";
    int switchScreens = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airport_addressess);

        imgCancelSearch = findViewById(R.id.cancel_search);
        tvSearchAirports = findViewById(R.id.searchAirports);
        list = findViewById(R.id.lvAirports);


        addAirportsInList();
        searchAirport();
        removeText();
        airportSelection();

    }

    private void searchAirport() {
        tvSearchAirports.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String text = tvSearchAirports.getText().toString().toLowerCase(Locale.getDefault());
                adapter.getFilter().filter(text);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void addAirportsInList() {
        adapter = new ArrayAdapter(this, R.layout.listairports, airports);

        list.setAdapter(adapter);
    }

    private void removeText() {
        imgCancelSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!tvSearchAirports.equals(""))
                    tvSearchAirports.setText("");
                else
                    return;
            }
        });
    }

    private void airportSelection() {
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedAirport = list.getItemAtPosition(i).toString();
                alertAirportSelection();

            }
        });
    }

    public void alertAirportSelection() {

//        if (!AirportAddressess.this.isFinishing()) {

        try {
            AirportAddressess.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new android.app.AlertDialog.Builder(AirportAddressess.this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                            .setIcon(R.drawable.ic_audiotrack_light)
                            .setTitle("Airport selection")
                            .setCancelable(false)
                            .setPositiveButton("Origin",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switchScreens = 1;
                                            Intent intent = new Intent(AirportAddressess.this, OriginAndDestination.class);
                                            intent.putExtra("airportOrigin", selectedAirport);
                                            intent.putExtra("flag", switchScreens);
                                            startActivity(intent);
                                            finish();
                                            selection = 1;
                                        }
                                    })
                            .setNeutralButton("Destination", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switchScreens = 2;
                                    Intent intent = new Intent(AirportAddressess.this, OriginAndDestination.class);
                                    intent.putExtra("airportDest", selectedAirport);
                                    intent.putExtra("flag", switchScreens);
                                    startActivity(intent);
                                    finish();
                                    selection = 2;
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).create().show();
                }
            });

        } catch (Exception e) {
            e.getMessage();
        }

//        }
    }


}
