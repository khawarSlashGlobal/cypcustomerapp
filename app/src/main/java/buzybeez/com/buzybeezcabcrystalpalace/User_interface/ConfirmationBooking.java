package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import buzybeez.com.buzybeezcabcrystalpalace.R;
//this is not used
public class ConfirmationBooking extends AppCompatActivity {

    public View bottomSheet;
    private BottomSheetBehavior mBottomSheetBehavior1;
    private Button mButton1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_booking);

        // bottomSheet = findViewById(R.id.bottom_sheet1);
        //mBottomSheetBehavior1 = BottomSheetBehavior.from(bottomSheet);

        mButton1 = (Button) findViewById(R.id.button_1);
        mButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBottomSheetBehavior1.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_EXPANDED);
                    mButton1.setText("this date");
                } else {
                    mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    mButton1.setText("this is time");
                }
            }
        });
    }
}
