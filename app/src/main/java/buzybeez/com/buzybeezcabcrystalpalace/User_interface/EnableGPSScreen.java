package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.Timer;
import java.util.TimerTask;

import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ListenerGPS;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.GPStracker;

public class EnableGPSScreen extends BaseActivity implements ListenerGPS, ConnectionListener {
    public static final String LOCAL_BROADCAST_ACTION = "com.dev2qa.example.broadcast.activity.LOCAL_BROADCAST";

    public static final String LOCAL_BROADCAST_SOURCE = "LOCAL_BROADCAST_SOURCE";
    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();
    LocationManager locationManager;
    boolean gpsStatus;
    Button btn;
    Timer timer;
    TimerTask timerTask;
    GPStracker gps;
    private RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enable_gpsscreen);
        layout = findViewById(R.id.relativeLayout);
        btn = findViewById(R.id.btnNext);

        checkGpsStatus();
        btnClickToEnableGPS();
        gps = new GPStracker(EnableGPSScreen.this);

        snackbarForConnectivityStatus();
    }

    private void checkGpsStatus() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    }

    private void btnClickToEnableGPS() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                stoptimertask();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //onResume we start our timer so it can start when the app comes from the background
        startTimer();
    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 10000, 10000); //
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {


        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void run() {
                        //get the current timeStamp
                        checkGpsStatus();
                        if (gpsStatus == true) {
                            //Go to next page i.e, start the next activity.
                            storeLatLong();
                            startActivity(new Intent(EnableGPSScreen.this, OriginAndDestination.class));
//                            finish();
                            //storeLatLong();
                            stoptimertask();
                        } else
                            return;
                    }
                });
            }
        };
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (gpsStatus == true)
            storeLatLong();
    }


    @Override
    public void listen(String msg) {
        if (msg.equals("gpsoff")) {
//            alertOk("GPS OFF", "Switch on GPS");
        } else {
            Intent i = new Intent(EnableGPSScreen.this, OriginAndDestination.class);
            startActivity(i);
        }
    }


}
