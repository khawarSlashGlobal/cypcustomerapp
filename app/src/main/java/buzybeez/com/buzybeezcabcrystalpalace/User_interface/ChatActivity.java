package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview.ChatAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Chat;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CustomerConfig;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Driver;
import buzybeez.com.buzybeezcabcrystalpalace.Models.DriverLoc;
import buzybeez.com.buzybeezcabcrystalpalace.Models.LoadMsg;
import buzybeez.com.buzybeezcabcrystalpalace.Models.SecureData;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;

public class ChatActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSION_CALL = 111;
    static ListView listViewMsg;
    EditText txt_send;
    public ImageButton btn_sent, btn_call;
    Intent intent;
    public static List<Chat> mChat;
    public List<Chat> mChats;
    public static ChatAdapter chatAdapter;
    String msg;
    static Activity activity;
    static Context context = null;
    SocketEvent socketEvent;
    public static boolean active = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        listViewMsg = findViewById(R.id.list_view_msg);
        txt_send = findViewById(R.id.text_edit);
        btn_sent = findViewById(R.id.btn_sent);
        btn_call = findViewById(R.id.driverCall);
        mChat = new ArrayList<>();
        mChats = new ArrayList<>();
        intent = getIntent();
        final String driverId = Driver.sharedInstance.getDrvUID();
        final String custId = CustomerConfig.sharedinstance.get_id();
        activity = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Messages");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (!SecureData.arrayList.isEmpty()) {
            secure();
            createAdapter();
            SecureData.arrayList.clear();
        }


        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isPermissionGranted()) {
                    Drivercall_action();
                }
            }
        });


        btn_sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txt_send.getText().toString().equals("")) {
                    Toast.makeText(ChatActivity.this, "Please enter message", Toast.LENGTH_LONG).show();
                } else {
                    send();
                }
            }
        });
    }

    //this function is for send messages
    public void send() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String now = formatter.format(date);
        String msg = txt_send.getText().toString();
        SocketEvent.sharedinstance.sendMessage(msg);
        mChat.add(new Chat(msg, now, false));
        txt_send.setText("");
        txt_send.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        createAdapter();
    }

    public boolean isPermissionGranted() {

        if (ContextCompat.checkSelfPermission(getBaseContext(),
                android.Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ChatActivity.this,
                    android.Manifest.permission.CALL_PHONE)) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE},
                        REQUEST_PERMISSION_CALL);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE},
                        REQUEST_PERMISSION_CALL);
            }
        }
        return true;
    }


    public void Drivercall_action() {

        try {
            Uri number = Uri.parse("tel:" + DriverLoc.sharedInstance.gettelephone());
            Intent callIntent = new Intent(Intent.ACTION_CALL, number);
            if (android.support.v4.app.ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(callIntent);
        } catch (android.content.ActivityNotFoundException ex) {
        }

    }

    //this function update listview when new message receive
    public static void getDatas(String message) {
        if (!message.equals("")) {
            secure();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            String now = formatter.format(date);
            ChatActivity.mChat.add(new Chat(message, now, true));
            SecureData.arrayList.clear();
            createAdapter();
        }

    }

    //this function is for created adapter
    public static void createAdapter() {
        chatAdapter = new ChatAdapter(activity, mChat, true);
        listViewMsg.setAdapter(chatAdapter);
        chatAdapter.notifyDataSetChanged();
        listViewMsg.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
    }

    //this function will be secure old data and though this we get old message and show next time
    public static void secure() {
        mChat.addAll(SecureData.arrayList);
    }

    //this function is for get messages when activty is create first time and message is availabe in array lis
    public void getMessageResume() {
        if(DriverSearch.checKNotification==1)
        {
            DriverSearch.manager.cancelAll();
            DriverSearch.checKNotification=0;
        }
        SocketEvent.isActivityOpen = 1;
        SocketEvent.counter = 0;
        mChat.addAll(LoadMsg.msg);
        createAdapter();
        LoadMsg.msg.clear();

        SecureData.arrayList.clear();
        txt_send.requestFocus();
    }

    //this function is for save message when activity will resume again old messages will be display
    //and varible a is for activty and b is for counter when activty is stop so no need for badge
    public void storeMessage() {
        SocketEvent.isActivityOpen = 0;
        SocketEvent.b = 1;
        //SocketEvent.counter = 0;
        SecureData.arrayList.addAll(mChat);
        SecureData.arrayList.size();
    }


    @Override
    protected void onStop() {
        super.onStop();
        storeMessage();

    }


    @Override
    protected void onResume() {
        super.onResume();

        getMessageResume();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SocketEvent.b = 0;
        SocketEvent.isActivityOpen = 0;

    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;

    }


}
