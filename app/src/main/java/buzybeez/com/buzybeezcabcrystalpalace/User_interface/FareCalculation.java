package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.content.Context;
import android.os.Looper;

import java.util.List;

import buzybeez.com.buzybeezcabcrystalpalace.DataHandler.SQLiteDatabaseHandler;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FareListMapping;

import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.fareList;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.isAirportAtDest;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.isAirportAtOri;

/**
 * Created by hv on 3/20/18.
 */

public class FareCalculation {

	public static double drvFareToInsertInBooking;
	Context mContext;
	private SQLiteDatabaseHandler db;

	public FareCalculation(Context context) {

		mContext = context;

	}

	public double[] calculateFare(String vehicleSymbol, double distanceInMiles) {

		double dis = distanceInMiles;
		double numOfVia = 0.0;
		double Standard_VAT = 0.0;
		double driverfare = 0.0;
		double customerfare = 0.0;

		try {

			db = new SQLiteDatabaseHandler(mContext);
			FareListMapping f = new FareListMapping();
			db.getCarSymbol(vehicleSymbol, f);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//   MARK:- Checking For Acc_Round

		FareListMapping rates = FareListMapping.sharedInstanceFareList;

		if (rates.getAcc_Round() == "None" && (rates.getAcc_RTypePriceRb() == "Distance")) {

		} else {
			double acc = Double.parseDouble(rates.getAcc_ToNearest());

			if (rates.getAcc_Round() == "Up" && rates.getAcc_RTypePriceRb() == "Distance") {

				if (acc <= 1.0) {

					dis = Math.ceil(dis);

				} else {

					if (acc > 1.0) {

						dis = Math.ceil(dis);

						dis = dis + (acc - 1);

					}
				}

			} else {

				if (rates.getAcc_Round() == "Down" && rates.getAcc_RTypePriceRb() == "Distance") {

					if (acc <= 1.0) {

						dis = Math.floor(dis);

					} else {

						if (acc > 1.0) {

							dis = Math.floor(dis);

							dis = dis - acc - 1;

						}

					}

				}

			}

		}
		// MARK:- Calculate Fare

		double fare = 0.0;

		int m = 0;

		int n = m;

		double swap = 0.0;

		for (int i = 0; i < 8; i++) {

			List<String> accUnit = rates.getAcc_Unit();

			double accUnitValue = Double.parseDouble(accUnit.get(i));

			if ((dis > swap && dis <= (swap + accUnitValue))) {

				m = i;
				// swap = swap + acc_unit[j];
				break;
			}
			swap = swap + accUnitValue;
		}
		n = m;


		for (int j = 0; j <= m; j++) {

			if (j == 0) {

				List<String> accCost = rates.getAcc_Cost();

				double accCostValue = Double.parseDouble(accCost.get(m));

				fare = dis * accCostValue;

				n = n - 1;

			} else {

				List<String> accUnit = rates.getAcc_Unit();

				List<String> accCost = rates.getAcc_Cost();

				//changes on these lines
				double accCostValue_n = Double.parseDouble(accCost.get(n));
				double accCostValue_m = Double.parseDouble(accCost.get(m));
				double accUnitValue_n = Double.parseDouble(accUnit.get(n));

				fare = fare + (accUnitValue_n * (accCostValue_n - accCostValue_m));

				n = n - 1;


			}
		}

		//MARK:- Scheme4


		//MARK:- Checking If Fare is greater then Minimum prize other set minimum_price

		double acc_min_price = Double.parseDouble(rates.getAcc_MinPrice());

		if (fare > acc_min_price) {

			if (rates.getAcc_Round() == "None" && rates.getAcc_RTypePriceRb() == "Price") {

			} else {
				double acc_nearest = Double.parseDouble(rates.getAcc_ToNearest());
				if (rates.getAcc_Round() == "Up" && rates.getAcc_RTypePriceRb() == "Price") {


					if (acc_nearest <= 1.0) {

						fare = Math.ceil(fare);

					} else {

						if (acc_nearest > 1.0) {

							fare = Math.ceil(fare);

							fare = fare + (acc_nearest - 1);

						}

					}

				} else {

					if (rates.getAcc_Round() == "Down" && rates.getAcc_RTypePriceRb() == "Price") {

						if (acc_nearest <= 1) {

							fare = Math.floor(fare);

						} else {

							if (acc_nearest > 1) {

								fare = Math.floor(fare);

								fare = fare - acc_nearest - 1;

							}

						}

					}

				}

			}

		} else {

			if (fare <= acc_min_price) {

				fare = acc_min_price;
			}

		}


		//MARK:- Add Extra_drop_value in Fare
		double accExtraDrop = Double.parseDouble(rates.getAcc_ExtDrop());
		fare = fare + (numOfVia * accExtraDrop);

		//MARK:- Calculate Standard Rate and add in Fare

		//vATRate is Acc_VATRAte

		if (rates.getAcc_VatRate() == "Exempt" || rates.getAcc_VatRate() == "Zero Rated") {

		} else {

			if (rates.getAcc_VatRate() == "Standard Rate") {

				Standard_VAT = 20 * 0.01 * fare;

				fare = Standard_VAT + fare;

			}

		}

		//Calculating Driver Fare

		//Checking For Driver Round

		if (rates.getDrv_Round() == "None" && rates.getDrv_RTypePriceRb() == "Distance") {


		} else {
			double drv_nearest = Double.parseDouble(rates.getDrv_ToNearest());
			if (rates.getDrv_Round() == "Up" && rates.getDrv_RTypePriceRb() == "Distance") {

				if (drv_nearest <= 1.0) {

					dis = Math.ceil(dis);

				} else {

					if (drv_nearest > 1.0) {

						dis = Math.ceil(dis);

						dis = dis + (drv_nearest - 1);
					}

				}

			} else {

				if (rates.getDrv_Round() == "Down" && rates.getDrv_RTypePriceRb() == "Distance") {

					if (drv_nearest <= 1.0) {

						dis = Math.floor(dis);

					} else {

						if (drv_nearest > 1.0) {

							dis = Math.floor(dis);


							dis = dis - (drv_nearest - 1.0);

						}

					}

				}

			}

		}

		double drvfare = 0.0;

		int drvm = 0;

		int drvn = drvm;

		double drvswap = 0;

		for (int j = 0; j < 8; j++) {

			List<String> driverUnit = rates.getDrv_Unit();
			double drv_UnitsValue = Double.parseDouble(driverUnit.get(j));

			if ((dis > swap) && (dis <= (swap + drv_UnitsValue))) {

				drvm = j;

				drvswap = drvswap + drv_UnitsValue;

				break;

			}

		}

		drvn = drvm;

		// for var i: `var`! = 0; i <= drvm; i++
		for (int i = 0; i <= drvm; i++) {

//        for i in 0...drvm {

			if (i == 0) {

				List<String> driverCost = rates.getDrv_Cost();
				double drv_CostValue = Double.parseDouble(driverCost.get(drvm));

				drvfare = dis * drv_CostValue;

				drvn = drvn - 1;

			} else {

				List<String> driverUnit = rates.getDrv_Unit();
				List<String> driverCost = rates.getDrv_Cost();

				double drv_UnitsValue_n = Double.parseDouble(driverUnit.get(drvn));
				double drv_CostValue_n = Double.parseDouble(driverCost.get(drvn));
				double drv_CostValue_m = Double.parseDouble(driverCost.get(drvm));


				drvfare = drvfare + (drv_UnitsValue_n * (drv_CostValue_n - drv_CostValue_m));

				drvn = drvn - 1;

			}

		}

		// scheme4

		if (drvfare > acc_min_price) {

			if (rates.getDrv_Round() == "None" && rates.getDrv_RTypePriceRb() == "Price") {

			} else {

				double drv_nearest = Double.parseDouble(rates.getDrv_ToNearest());
				if (rates.getDrv_Round() == "Up" && rates.getDrv_RTypePriceRb() == "Price") {

					if (drv_nearest <= 1.0) {
						drvfare = Math.ceil(drvfare);

					} else {

						if (drv_nearest > 1.0) {

							drvfare = Math.ceil(drvfare);

							drvfare = drvfare + (drv_nearest - 1);
						}

					}

				} else {

					if (rates.getDrv_Round() == "Down" && rates.getDrv_RTypePriceRb() == "Price") {

						if (drv_nearest <= 1.0) {
							drvfare = Math.floor(drvfare);

						} else {

							if (drv_nearest > 1.0) {

								drvfare = Math.floor(drvfare);

								drvfare = drvfare - drv_nearest - 1;

							}

						}

					}

				}

			}

		} else {

			if (drvfare <= acc_min_price) {

				drvfare = acc_min_price;

			}

		}

		double drv_ExtraDropValue = Double.parseDouble(rates.getDrv_ExtDrop());
		drvfare = drvfare + (numOfVia * drv_ExtraDropValue);

		if (rates.getDrv_VATRate() == "Exempt" || rates.getDrv_VATRate() == "Zero Rated") {

		} else {

			if (rates.getDrv_VATRate() == "Standard Rate") {

				Standard_VAT = 20 * 0.01 * drvfare;

				drvfare = Standard_VAT + drvfare;
			}

		}

		double drv_Percentrage = Double.parseDouble(rates.getDrv_Perc());

		drvfare = drvfare * drv_Percentrage * 0.01;

		double t_driverfare = Math.round(drvfare);

		driverfare = t_driverfare;

		customerfare = fare;
		drvFareToInsertInBooking = fare;

		if (driverfare > customerfare) {

			customerfare = driverfare;
			drvFareToInsertInBooking = driverfare;
		} else {
			driverfare = customerfare;
			drvFareToInsertInBooking = customerfare;
		}
		double[] faresArray = {driverfare, customerfare};

		return faresArray;
	}

	public double getDrvfare() {
		return drvFareToInsertInBooking;
	}
	public void setDrvfare(double cusfare) {
		drvFareToInsertInBooking =cusfare ;
	}

	public double getFixedFare(String vehicleSymbol) {

		Looper.getMainLooper();

		if (isAirportAtOri == true || isAirportAtDest == true) {

			List<Double> list = fareList.get(vehicleSymbol);

			if (list == null) {
				return drvFareToInsertInBooking;

			} else {
				double fixedFare = list.get(0);
				return fixedFare;
			}
		} else
			return drvFareToInsertInBooking;
	}

}
