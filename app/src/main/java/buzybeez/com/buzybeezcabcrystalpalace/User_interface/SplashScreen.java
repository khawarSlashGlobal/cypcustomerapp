package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;

import buzybeez.com.buzybeezcabcrystalpalace.DataCenter.DataHolder;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CfgCustApp;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CustomerConfig;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Driver;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.fabric.sdk.android.Fabric;

import buzybeez.com.buzybeezcabcrystalpalace.R;

import static android.provider.Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;

/**
 * Created by hv on 1/22/18.
 */


public class SplashScreen extends BaseActivity {
    private static int SPLASH_TIME_OUT = 3000;
    LocationManager locationManager;
    int locationMode;
    Animation fromtop;
    ImageView img;
    // Splash screen timer
    boolean gpsStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.splashactivity);
        fromtop= AnimationUtils.loadAnimation(this,R.anim.fromtop);
        img=findViewById(R.id.imageView3);
        img.setAnimation(fromtop);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        //	Log.i("token",  FirebaseInstanceId.getInstance().getToken());

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {


                checkGpsStatus();
                // This method will be executed once the timer is over
                // Start your app main activity
                SharedPreferences extras = getSharedPreferences("localnoti", Context.MODE_PRIVATE);
                SharedPreferences.Editor editorlocal = extras.edit();
                if( getIntent().getExtras()!=null)
                { if (SocketEvent.sharedinstance.socket == null && Network.isNetworkAvailable(SplashScreen.this)) {
                    SocketEvent.sharedinstance.socketConnectAfterDisconnect();
                }
                    Bundle bundle= getIntent().getExtras();
                    for(String key :bundle.keySet()) {
                        if(key.equals("state") && bundle.get(key).equals("despatch")) {

                            SharedPreferences preferences = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
                            editor= preferences.edit();
                            editor.putString("jobid", bundle.getString("_id"));
                            String Driveruid=bundle.getString("drvrcallsign");
                            String[] split = Driveruid.split("@");
                            editor.putString("drvcallsign",split[0] );
                            editor.putString("officeId", split[1]);
                            //editor.putString("drvImageIp", "");
                            //editor.putString("jobRef", bundle.getString("jobRef"));
                            editor.apply();
                            Driver.sharedInstance.setDrvUID(Driveruid);
                            Intent intent = new Intent(SplashScreen.this, DriverSearch.class);
                            startActivity(intent);
                            finish();
                            return;
                        }}}
                if (checkSharedPrefeces()) {
                    if (SocketEvent.sharedinstance.socket == null && Network.isNetworkAvailable(SplashScreen.this)) {
                        SocketEvent.sharedinstance.socketConnectAfterDisconnect();
                    }
                    Intent intent = new Intent(SplashScreen.this, DriverSearch.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    finish();
                }



                else if (extras.getString("futurebooking","").equals("1")) {
                    if (SocketEvent.sharedinstance.socket == null && Network.isNetworkAvailable(SplashScreen.this)) {
                        SocketEvent.sharedinstance.socketConnectAfterDisconnect();
                    }
                    DataHolder.getInstance().vehType = extras.getString("vehType","");
                    DataHolder.getInstance().originLng = Double.parseDouble(extras.getString("originLng",""));
                    DataHolder.getInstance().originLat = Double.parseDouble(extras.getString("originLat",""));
                    DataHolder.getInstance().origin = extras.getString("origin","");
                    DataHolder.getInstance().destination = extras.getString("destination","");
                    DataHolder.getInstance()._fromOutcode = extras.getString("_fromOutcode","");
                    DataHolder.getInstance().jobID = extras.getString("jobID","");
                    DriverSearch.JOBID=DataHolder.getInstance().jobID;
                    DataHolder.getInstance().jobReference = extras.getString("jobReference","");
                    DataHolder.getInstance().telephone = extras.getString("telephone","");
                    CfgCustApp.sharedinstance.setJobInOffice(extras.getString("jobinoffice",""));
                    CustomerConfig.sharedinstance.setImageIP(extras.getString("drvImageIp",""));
                    editorlocal.putString("futurebooking","0");
                    editorlocal.apply();
                    Intent intent = new Intent(SplashScreen.this, DriverSearch.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    finish();
                }


                else {

                    try {
                        locationMode = android.provider.Settings.Secure.getInt(context.getContentResolver(), android.provider.Settings.Secure.LOCATION_MODE);
                    } catch (Settings.SettingNotFoundException e) {
                        e.printStackTrace();
                    }
                    if (gpsStatus == true && locationMode != LOCATION_MODE_HIGH_ACCURACY) {
                        storeLatLong();
                    }
                    Intent i = new Intent(SplashScreen.this, OriginAndDestination.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    public void checkGpsStatus() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    }


    public boolean checkSharedPrefeces() {
        SharedPreferences preferences = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
        String jobid = preferences.getString("jobid", "");
        String drvcallsign = preferences.getString("drvcallsign", "");
        String officeId = preferences.getString("officeId", "");
        String drvImageIp = preferences.getString("drvImageIp", "");
        String jobRef = preferences.getString("jobRef", "");
        if (!jobid.equals("") && !drvcallsign.equals("") && !officeId.equals("") && !drvImageIp.equals("") && !jobRef.equals("")) {
            return true;
        } else {
            return false;
        }
    }
}