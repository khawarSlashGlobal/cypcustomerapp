package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.GPStracker;

import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.AddWaypoints.adapter;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.fromtoviaList;
//this is not used
public class WaypointMaps extends BaseActivity implements
        OnMapReadyCallback, ConnectionListener
        , GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraIdleListener {

    public  boolean flag;
    AutoCompleteTextView waypointAddressTV;
    public Button confirmWaypoint;
    ImageView marker_imagehome;
    List<Address> addresses;
    GPStracker gpStracker;
    LatLng latLng;
    String yourAddress;
    TextView marker_label;
    int PLACE_PICKER_REQUEST = 1;
    private GoogleMap mMap;
    private ImageButton myLocationButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_location_picker);


            if (getLocationMode(this) == 1) {
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            } else {

                snackbarForConnectivityStatus();
                screenObjects_id();
                targetlocationonClick();
                setpickuploc();

            }

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        confirmWaypoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AsyncTask execute = new Asynctask();
                ((Asynctask) execute).onPreExecute();
                // progressBar.setVisibility(View.VISIBLE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String winfo="",wpAddress="",wpPostalCode = "";
                        double wlat=0,wlon=0;
                        try {
                            Geocoder geocoder = new Geocoder(WaypointMaps.this, Locale.getDefault());
                            winfo = "";
                            wpAddress = waypointAddressTV.getText().toString();
                            wlat =geocoder.getFromLocationName(wpAddress, 1).get(0).getLatitude();
                            wlon =geocoder.getFromLocationName(wpAddress, 1).get(0).getLongitude();
                            wpPostalCode = geocoder.getFromLocationName(wpAddress, 1).get(0).getPostalCode();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        FromToVia ftv = new FromToVia(winfo, wpAddress, wlat, wlon, wpPostalCode);
                        fromtoviaList.add(ftv);
                        adapter.notifyDataSetChanged();
                    }
                });
                // Toast.makeText(WaypointMaps.this, "Waypoint Saved Home Location", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }


    public void screenObjects_id() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        confirmWaypoint = (Button) findViewById(R.id.donebooking);
        marker_imagehome = (ImageView) findViewById(R.id.marker_imagehome);
        waypointAddressTV = (AutoCompleteTextView) findViewById(R.id.origin);
        marker_label = (TextView) findViewById(R.id.marker_label);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    public int getLocationMode(Context context) throws android.provider.Settings.SettingNotFoundException {
        return android.provider.Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        marker_label.setVisibility(View.GONE);
        mMap = googleMap;
        mMap.clear();


        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        try {
            Geocoder gcd = new Geocoder(this, Locale.getDefault());
            addresses = gcd.getFromLocation(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude, 1);


            if (addresses.size() > 0 && addresses != null) {
                yourAddress = addresses.get(0).getAddressLine(0);
                LatLng lng = new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                mMap.moveCamera(cameraUpdate);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (addresses.size() == 0) {
                getLocationOfOrigin();
            }

        } catch (IOException e) {
            e.printStackTrace();

        }
    }


    @Override
    public void onCameraIdle() {
        marker_label.setVisibility(View.GONE);
        // marker_imagehome.setVisibility(View.GONE);
        try {
            Geocoder gcd = new Geocoder(this, Locale.getDefault());
            addresses = gcd.getFromLocation(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude, 1);
            if (addresses.size() > 0 && addresses != null) {
                yourAddress = addresses.get(0).getAddressLine(0);


            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        //  mMap.addMarker(new MarkerOptions().position(mMap.getCameraPosition().target).anchor(0.5f, .05f).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
        waypointAddressTV.setText(yourAddress);
    }

    @Override
    public void onCameraMove() {
        marker_imagehome.setVisibility(View.VISIBLE);
        marker_label.setVisibility(View.VISIBLE);
        marker_label.setText("Searching........");
        mMap.clear();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public void getLocationOfOrigin() throws IOException {
        //storeLatLong();
        gpStracker = new GPStracker(this);
        latLng = new LatLng(gpStracker.getLatitude(), gpStracker.getLongitude());
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(17.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.moveCamera(cameraUpdate);
    }

    public void targetlocationonClick() {
        myLocationButton = (ImageButton) findViewById(R.id.targetlocation);
        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GPStracker gpStracker = new GPStracker(getApplicationContext());
                Double lati = gpStracker.getLatitude();
                Double longi = gpStracker.getLongitude();
                latLng = new LatLng(lati, longi);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17.0f);
                mMap.animateCamera(cameraUpdate);
            }
        });

    }

    public void setpickuploc() {
        waypointAddressTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = true;
//                AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
//                        .setTypeFilter(Place.TYPE_COUNTRY)
//                        .setCountry("UK")
//                        .build();
//                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//                Intent intent;
//                //restrict user to search places only in UK
//                try {
//                    intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
//                            .setFilter(autocompleteFilter).build(WaypointMaps.this);
////                    intent = builder.build((Activity) OriginAndDestination.this);
//                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
//                } catch (GooglePlayServicesRepairableException e) {
//                    e.printStackTrace();
//                } catch (GooglePlayServicesNotAvailableException e) {
//                    e.printStackTrace();
//                }
                Intent intent = new Intent(WaypointMaps.this, AutocompletePlacePickerActivity.class);
                intent.putExtra("action", "waypoints");
                startActivityForResult(intent, 1);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String address = String.format(String.valueOf(place.getAddress()));
                if (flag == true) {
                    waypointAddressTV.setText(address);
                    extractAddressFromOrigin(place);
                }
            }
        }
    }


    public void extractAddressFromOrigin(Place place) {

        Geocoder mGeocoder = new Geocoder(WaypointMaps.this, Locale.getDefault());

        List<Address> addresses = null;
        try {
            addresses = mGeocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            addresses.get(0).getLocality();
            if (addresses.get(0).getPostalCode() == null) {
                latitude = addresses.get(0).getLatitude();
                longitude = addresses.get(0).getLongitude();
            } else {
                String postalCode = addresses.get(0).getPostalCode();
                latitude = addresses.get(0).getLatitude();
                longitude = addresses.get(0).getLongitude();
                LatLng lng = new LatLng(latitude, longitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                mMap.moveCamera(cameraUpdate);
            }

        }
    }


    public class Asynctask extends AsyncTask {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            // progressBar.setVisibility(View.GONE);
        }
    }


}
