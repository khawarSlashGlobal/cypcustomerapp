package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import buzybeez.com.buzybeezcabcrystalpalace.DataCenter.DataHolder;
import buzybeez.com.buzybeezcabcrystalpalace.DataHandler.SQLiteAirportList;
import buzybeez.com.buzybeezcabcrystalpalace.DataHandler.SQLiteCustomerProfile;
import buzybeez.com.buzybeezcabcrystalpalace.FireBase.CustomerVerfication;
import buzybeez.com.buzybeezcabcrystalpalace.Fragments.BookingHistoryActivity;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.WSCallerVersionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Airports;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Booking;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CfgCustApp;
import buzybeez.com.buzybeezcabcrystalpalace.Models.DriverLoc;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.Models.GooglePlayStoreAppVersionNameLoader;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Rating;
import buzybeez.com.buzybeezcabcrystalpalace.Models.User;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Connect;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.GPStracker;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.socket.client.Ack;

import static android.provider.Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.FIXED_OFFICE_KEY;


import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.SHARE_RIDE_TEXT;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.SHARE_SUBJECT;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.SHARE_TEXT;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.BookingScreen.totalDistance;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.BookingScreen.totalDuration;
import static com.sun.activation.registries.LogSupport.log;

public class OriginAndDestination extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback,
        ConnectionListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraIdleListener,
        WSCallerVersionListener,
        GPStracker.LocationProviderStatusListner {

    boolean tiltcheck =false;
    byte[] bytearray;

    public static boolean flag;

    public static String address = null;







    public static int selection = 0;
    // public static int flagOfActivities = 0;
    //google place picker of pickup and destination
    public static AutoCompleteTextView pickuploc, dropoffloc;
    //  public static ArrayList<String> waypointAddress = new ArrayList<>();

    public static String airportLocationOrigin = "";
    public static String airportpostcodeOrigin = "";
    public static String airportLocationDest = "";
    public static String airportpostcodeDest = "";
    //skh :
    public static ArrayList<Booking> AllBookingHistoryOfUser = new ArrayList<>();
    public static ArrayList<Booking> bookedBookingHistoryOfUser = new ArrayList<Booking>();
    public static ArrayList<Booking> completedBookingHistoryOfUser = new ArrayList<Booking>();
    public static ArrayList<Booking> canceledBookingHistoryOfUser = new ArrayList<Booking>();
    public static boolean isProfileDetailAvalible = true;


    static String _address = "";
    static String airportOutCodeOrigin = "";
    static String airportOutCodeDest = "";
    final Handler handler = new Handler();
    private final int PICK_IMAGE_REQUEST = 71;
    public ArrayList<Airports> airportDataList = new ArrayList<>();
    Context c = this;
    ConstraintLayout pickupdestinationlayout;
    int PLACE_PICKER_REQUEST = 333;
    ImageView cancelPickupLocation, cancelDropLocation, imgAeroplane, swapicon,tiltmap;
    ImageView markerImageView;
    List<Address> addresses;
    ConstraintLayout NoGPSMSG;
    String yourAddress;
    buzybeez.com.buzybeezcabcrystalpalace.User_interface.EnableGPSScreen enable = new buzybeez.com.buzybeezcabcrystalpalace.User_interface.EnableGPSScreen();
    GPStracker gps;
    LatLng latLng;
    Timer timer;
    TimerTask timerTask;
    LocationManager locationManager;
    LocationListener locationListener;
    String home_Location, office_Location;
    ImageView imageView;
    String downloadUrlStr;
    String uid;
    String airportOrigin = null;
    String airportDestination = null;
    String addrss = "";
    boolean setPickUpLocation = false;
    boolean setDestination = false;
    Marker previouMarker, newMarker;
    String addr, address11;
    //for movable place picker
    TextView markerTextView, addwaypointTV;
    double airportLatOrigin = 0.0;
    double airportLngOrigin = 0.0;
    double airportLatDest = 0.0;
    double airportLngDest = 0.0;

    SQLiteAirportList db;
    SharedPreferences setPrefrence;
    static   boolean startingchecknetwork = false;
    WifiManager wifiManager;
    //for current location triger
//    private  SensorManager mSensorManager;
//    private  Sensor mLightSensor;
//    private float mLightQuantity;
    private ImageButton myLocationButton, homeLocationButton, officeLocationButton;
    private Button nextBtn;
    private DrawerLayout layout;
    private Uri filePath;
//    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
//    private FirebaseDatabase mDb = FirebaseDatabase.getInstance();

    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);
        return stream.toByteArray();

    }


    //this is oncreate
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_origin_and_destination);

        if(isNetworkAvailable()){     setPrefrence = getSharedPreferences(Rating.prefratingname, Context.MODE_PRIVATE);
            if (!setPrefrence.getString(Rating.prefdriverid, "").equals("") && !setPrefrence.getString(Rating.prefdriverfare, "").equals("")) {
                Intent intent = new Intent(OriginAndDestination.this, RatingScreen.class);
                startActivity(intent);

            }}
        int loc=0;
        try { loc=getLocationMode(this) ;} catch (Exception e) {
            e.printStackTrace();
            //return;
        }
        db = new SQLiteAirportList(this);

        if
        (Network.isNetworkAvailable(OriginAndDestination.this)) {
            if (SocketEvent.sharedinstance.socket == null) {
                SocketEvent.sharedinstance.initializeSocket();
            }
        }

        if (loc == 1) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
        // else {
//        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
//        mLightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
//
//        // Implement a listener to receive updates
//        SensorEventListener listener = new SensorEventListener() {
//            @Override
//            public void onSensorChanged(SensorEvent event) {
//                mLightQuantity = event.values[0];
//                Log.e("style",String.valueOf(mLightQuantity));
//                if(mLightQuantity<10.0 && mMap!=null){
//                  mMap.setMapStyle(
//                            MapStyleOptions.loadRawResourceStyle(
//                                    OriginAndDestination.this, R.raw.night_style_json));
//                }
//else if( mMap!=null){
//
//                   mMap.setMapStyle(
//                            MapStyleOptions.loadRawResourceStyle(
//                                    OriginAndDestination.this, R.raw.style_json));
//                }
//            }
//
//            @Override
//            public void onAccuracyChanged(Sensor sensor, int accuracy) {
//
//            }
//        };
//
//        // Register the listener with the light sensor -- choosing
//        // one of the SensorManager.SENSOR_DELAY_* constants.
//        mSensorManager.registerListener(
//                listener, mLightSensor, SensorManager.SENSOR_DELAY_UI);

        appDrawer();

        screenObjects_id();

        snackbarForConnectivityStatus();

        initializeApplication();

        targetlocationonClick();

        setpickuploc();

        setdropAddress();
        if
        (Network.isNetworkAvailable(OriginAndDestination.this)) {
            RatesAccMileage();
            getAllAirportList();
            new GooglePlayStoreAppVersionNameLoader(getApplicationContext(), this).execute();
            //   showlivedriver();
        }
        CustDetailNavDrawer();

        enable_home_office_button();

        detectDrawerClick();

        homebuttonclick();

        officebuttonclick();
        // getAllBookedBookingHistory();


//
//               choseprofile();
//
//                uploadFile(filePath);
//
//                downloadImage();
        try{
            boolean bookingstatus = (Objects.requireNonNull((getIntent().getExtras()).getBoolean("Booking Status")));

            if (bookingstatus) {
                BookingStatusAlert();
            }
        }
        catch(Exception e){}

//                airportOrigin = getIntent().getStringExtra("airportOrigin");
//                airportDestination = getIntent().getStringExtra("airportDest");
//                flagOfActivities = getIntent().getIntExtra("flag", 0);

        // addrss = _address;

//                if (flagOfActivities == 0) {
//                    return;
//                }
//
//                if (flagOfActivities == 1) {
//                    pickuploc.setText(airportOrigin);
//
//                } else {
//                    pickuploc.setText(addrss);
//
//                }
//                if (flagOfActivities == 2) {
//                    dropoffloc.setText(airportDestination);
//
//                } else {
//                    dropoffloc.setText(addrss);
//                }


        ShowLocationDialog();


        getCfgCustApp(this);

        nearstOfficeLocation();


        //    }





        pickuploc.setVisibility(View.VISIBLE);
        dropoffloc.setVisibility(View.VISIBLE);
        nextBtn.setVisibility(View.VISIBLE);
        myLocationButton.setVisibility(View.VISIBLE);
        imgAeroplane.setVisibility(View.VISIBLE);



    }




    void showlivedriver(){
        getClearDriveronce();

        drvshandler = new Handler(Looper.getMainLooper());

        final int delay = 5000; //milliseconds

        drvshandler.postDelayed(new Runnable() {

            public void run() {

                getClearDriver();

                drvshandler.postDelayed(this,delay);
            }
        }, delay);
    }





    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 10000, 10000); //
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void run() {
                        //get the current timeStamp

                        if (mLocation == null) {
                            //initializeGAPIClient();
                            //storeLatLong();
                        } else
                            stoptimertask();
                    }
                });
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    public int getLocationMode(Context context) throws Settings.SettingNotFoundException {
        return Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
    }
    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(end.latitude - begin.latitude);
        double lng = Math.abs(end.longitude - begin.longitude);

        if (end.latitude < begin.latitude && end.longitude < begin.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (end.latitude >= begin.latitude && end.longitude < begin.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (end.latitude >= begin.latitude && end.longitude >= begin.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (end.latitude < begin.latitude && end.longitude >= begin.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }
    ValueAnimator valueAnimator[];
    private int index[], next[];
    Handler  drvshandler;
    LatLng start[], end[];
    LatLng[] driverlatlng;
    Marker[] drvCarMarker;
    float v[];
    Double lat[], lng[];
    LatLng newPos[];
    float[] bearing;
    List<LatLng>[] newlist;
    int lengthconst;
    public void getClearDriveronce(){


        JSONArray query = new JSONArray();
        query.put("Driverloc");

        JSONObject obj= new JSONObject();
        try {
            //  obj.put("office_id", FIXED_OFFICE_KEY);
        }
        catch(Exception e)
        {
        }
        query.put(obj);


        SocketEvent.sharedinstance.socket.emit("getmultipledata", query,
                new Ack() {
                    @Override
                    public void call(Object... args) {
                        if (args[0] != null && !args[0].toString().equals("0")) {

                            try {
                                JSONArray jsonArray = new JSONArray(args[0].toString());
                                newlist = new ArrayList[jsonArray.length()];
                                index= new int[jsonArray.length()];
                                start= new LatLng[jsonArray.length()];
                                end=new LatLng[jsonArray.length()];
                                next=new  int[jsonArray.length()];
                                valueAnimator= new ValueAnimator[jsonArray.length()];
                                lengthconst=jsonArray.length();
                                v=new float[jsonArray.length()];
                                lat= new Double[jsonArray.length()];
                                newPos= new LatLng[jsonArray.length()];
                                bearing =new float[jsonArray.length()];
                                lng= new Double[jsonArray.length()];
                                for (int i = 0; i <jsonArray.length() ; i++) {
                                    newlist[i] = new ArrayList();
                                    index[i] = 0;
                                    next[i] = 0;
                                    valueAnimator[i] = ValueAnimator.ofFloat(0, 1);
                                    valueAnimator[i].setDuration(5000);
                                    valueAnimator[i].setInterpolator(new LinearInterpolator());
                                    bearing[i]= new Random().nextInt(360);
                                }
                                driverlatlng= new LatLng[jsonArray.length()];
                                drvCarMarker= new Marker[jsonArray.length()];
                                for(int i=0; i<jsonArray.length();i++)
                                {
                                    JsonNode jsonNode = JSONParse.convertJsonFormat(jsonArray.getJSONObject(i));
                                    ObjectMapper mapper = new ObjectMapper();
                                    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                    DriverLoc.sharedInstance = mapper.readValue(new TreeTraversingParser(jsonNode), DriverLoc.class);
                                    driverlatlng[i] = new LatLng(DriverLoc.sharedInstance.getLat(), DriverLoc.sharedInstance.getLong());
                                    newlist[i].add(driverlatlng[i]);
                                    final int j= i;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            drvCarMarker[j] = mMap.addMarker(new MarkerOptions().position(driverlatlng[j]).flat(true)
                                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car)));
                                            drvCarMarker[j].setRotation(bearing[j]);
                                        }
                                    });

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }}});}
    public void getClearDriver(){


        JSONArray query = new JSONArray();
        query.put("Driverloc");

        JSONObject obj= new JSONObject();
        try {
            //    obj.put("office_id", FIXED_OFFICE_KEY);
        }
        catch(Exception e){}
        query.put(obj);


        SocketEvent.sharedinstance.socket.emit("getmultipledata", query,
                new Ack() {
                    @Override
                    public void call(Object... args) {
                        if (args[0] != null && !args[0].toString().equals("0")) {
                            JSONArray jsonArray= new JSONArray();
                            try {
                                jsonArray = new JSONArray(args[0].toString());}
                            catch(Exception e){}
                            if(jsonArray.length()==lengthconst){
                                for(int k=0;k<jsonArray.length() ;k++){
                                    try{
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(jsonArray.getJSONObject(k));
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        DriverLoc.sharedInstance = mapper.readValue(new TreeTraversingParser(jsonNode), DriverLoc.class);


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    driverlatlng[k] = new LatLng(DriverLoc.sharedInstance.getLat(), DriverLoc.sharedInstance.getLong());

                                    newlist[k].add(driverlatlng[k]);

                                    if (index[k] < newlist[k].size() + 1) {

                                        start[k] = newlist[k].get(index[k]);
                                        end[k] = newlist[k].get(next[k]);

                                        index[k]++;
                                        next[k] = index[k] + 1;


                                    }
                                    final int kcont=k;
                                    valueAnimator[kcont].addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                        @Override
                                        public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                                            if(kcont<v.length) {
                                                v[kcont] = valueAnimator.getAnimatedFraction();
                                            }
                                            else{
                                                return;
                                            }try {
                                                lng[kcont] = v[kcont] * end[kcont].longitude + (1 - v[kcont])
                                                        * start[kcont].longitude;
                                                lat[kcont] = v[kcont] * end[kcont].latitude + (1 - v[kcont])
                                                        * start[kcont].latitude;
                                            } catch (Exception e) {
                                                return;
                                            }
                                            newPos[kcont] = new LatLng(lat[kcont], lng[kcont]);
                                            //   if (start.latitude != end.latitude) {
                                            //   if (start.latitude != end.latitude) {
                                            //     try {

                                            drvCarMarker[kcont].setPosition(newPos[kcont]);
//                                                } catch (Exception e) {
//                                                    return;
//                                                }
                                            try {
                                                drvCarMarker[kcont].setAnchor(0.5f, 0.5f);

                                                drvCarMarker[kcont].setRotation(getBearing(end[kcont], newPos[kcont]));
                                            }
                                            catch(Exception e){return;}
                                            //      }

                                        }
                                    });
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            float result[]= new float[1];
                                            Location.distanceBetween(start[kcont].latitude, start[kcont].longitude,end[kcont].latitude,end[kcont].longitude,result);
                                            if(result[0]>0.01f) {
                                                valueAnimator[kcont].start();
                                            }
                                            else
                                            {
                                                drvCarMarker[kcont].remove();
                                                drvCarMarker[kcont] = mMap.addMarker(new MarkerOptions().position(driverlatlng[kcont]).flat(true)
                                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car)));
                                                drvCarMarker[kcont].setRotation(bearing[kcont]);


                                            }
                                        }
                                    });
                                }}
                            else{
                                for(int i=0;i<lengthconst;i++){
                                    final int iconstant=i;

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try{
                                                drvCarMarker[iconstant].remove();
                                            }
                                            catch(Exception e){}}
                                    });
                                }
                                getClearDriveronce();
                            }}}});}


    @Override
    protected void onResume() {
        super.onResume();

//        if (!startingchecknetwork) {
//
//            if
//            (!Network.isNetworkAvailable(OriginAndDestination.this)) {
//                Connect con = new Connect(OriginAndDestination.this);
//                snackbarForConnectivityStatus();
//                try {
//                    Thread.sleep(2000);
//                } catch (Exception e) {
//                }
//                if
//                (!Network.isNetworkAvailable(OriginAndDestination.this)) {
//                    return;
//                }
//            }
//            startingchecknetwork = true;}

//        super.onResume();
//        enable.stoptimertask();
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.originSelectionMap);
//        mapFragment.getMapAsync(this);

        ShowLocationDialog();
//        pickuploc.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (pickuploc.getText().toString().equals(airportOrigin)) {
//                    flagOfActivities = 0;
//                }
//                if (dropoffloc.getText().toString().equals(airportDestination)) {
//                    flagOfActivities = 2;
//                } else {
//                    flagOfActivities = 0;
//                }
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//        dropoffloc.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                if (dropoffloc.getText().toString().equals(airportDestination)) {
//                    flagOfActivities = 0;
//                }
//                if (pickuploc.getText().toString().equals(airportOrigin)) {
//                    flagOfActivities = 1;
//                } else {
//                    flagOfActivities = 0;
//                }
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.originSelectionMap);
        mapFragment.getMapAsync(this);
        //   getAllBookedBookingHistory();
        if(Network.isNetworkAvailable(OriginAndDestination.this)) {
            getAllAirportList();
        }
        try {
            int locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            if (checkLocationPermission() && locationMode == LOCATION_MODE_HIGH_ACCURACY) {
                NoGPSMSG.setVisibility(View.GONE);
                if (markerTextView.getVisibility() == View.GONE)
                    markerTextView.setVisibility(View.VISIBLE);
            } else {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        gps = new GPStracker(OriginAndDestination.this, OriginAndDestination.this);


                        if (gps.getLocation() != null) {
                            try {
                                getLocationOfOrigin();
                                handler.removeCallbacksAndMessages(null);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            handler.postDelayed(this, 100);
                        }
                    }
                }, 100);

                NoGPSMSG.setVisibility(View.VISIBLE);
                if (markerTextView.getVisibility() == View.VISIBLE)
                    markerTextView.setVisibility(View.GONE);


            }

        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        isPermissionGranted();
        if(getSharedPrefPhone()!="" && getSharedPrefPhone()!=null) {
            final String android_id = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            JSONArray query = new JSONArray();
            JSONObject filter = new JSONObject();

            query.put("CustomerReg");
            try{
                filter.put("cust_phone", getSharedPrefPhone());
                query.put(filter);

                SocketEvent.sharedinstance.socket.emit("getdata", query, new Ack() {

                    @Override
                    public void call(final Object... args) {

                        if (args[0] != null && !args[0].toString().equals("0")) {
                            JSONObject data = (JSONObject) args[0];
                            try{
                                if(   !data.getString("cust_uid").toString().equals(android_id))
                                {
                                    Log.e("shareprefclear","true");
                                    setNumberSharedPreference("", "", "");

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OriginAndDestination.this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                                            alertDialogBuilder.setTitle(SHARE_RIDE_TEXT);
                                            alertDialogBuilder.setMessage("Please Verify Your Account");
                                            alertDialogBuilder.setPositiveButton("Verify", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    Intent intent = new Intent(OriginAndDestination.this, CustomerVerfication.class);
                                                    startActivity(intent);
                                                    dialog.cancel();
                                                }
                                            });
                                            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    dialog.dismiss();
                                                }
                                            });
                                            alertDialogBuilder.show();

                                        }
                                    });

                                }
                            }
                            catch(Exception e){}


                        }
                    }
                });}
            catch(Exception e){}

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //Toast.makeText(OriginAndDestination.this, "1", Toast.LENGTH_SHORT).show();
        }

        if (item.getItemId() == R.id.action_loc_history) {
            //fetch phone number
            SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);
            final String cust_phone1 = preferences.getString("cust_phone", "");
            if (cust_phone1.equals("")) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder1.setIcon(R.drawable.ic_error_black_24dp);
                builder1.setTitle("History not avalible");
                builder1.setMessage("History is not avalible, please add your profile information.");
                builder1.setCancelable(false);
                builder1.setPositiveButton("Add Profile Info",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(OriginAndDestination.this, AccountInfo.class);
                                startActivity(intent);
                                dialog.cancel();
                            }
                        });
                builder1.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            } else {

                getAllBookedBookingHistory();


            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void screenObjects_id() {
        addwaypointTV = findViewById(R.id.addwaypointTV);
        NoGPSMSG = findViewById(R.id.noCurrentLocMsgCL);
        pickupdestinationlayout = (ConstraintLayout) findViewById(R.id.pdestlayout);
        imgAeroplane = findViewById(R.id.aeroplane);
        tiltmap=findViewById(R.id.tiltmap);
        imageView = (ImageView) findViewById(R.id.profile);
        myLocationButton = (ImageButton) findViewById(R.id.targetlocation);
        homeLocationButton = (ImageButton) findViewById(R.id.home);
        officeLocationButton = (ImageButton) findViewById(R.id.office);
        cancelPickupLocation = (ImageView) findViewById(R.id.cancel_pickup1);
        cancelDropLocation = (ImageView) findViewById(R.id.cancel_drop);
        nextBtn = (Button) findViewById(R.id.donebooking);
        swapicon=(ImageView) findViewById(R.id.swap_icon);
        progressDialog = new ProgressDialog(OriginAndDestination.this, R.style.MyDialogTheme);
        progressDialog.setTitle("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        //nextBtn.setEnabled(false);
        layout = findViewById(R.id.drawer_layout);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.originSelectionMap);
        mapFragment.getMapAsync(this);

        pickuploc = findViewById(R.id.select_location1);
        dropoffloc = findViewById(R.id.select_location2);

        markerTextView = (TextView) findViewById(R.id.marker_label);
        markerImageView = (ImageView) findViewById(R.id.marker_imagehome);
        //markerImageView.setImageResource(R.drawable.marker);
        SharedPreferences preference = getSharedPreferences("home_address", MODE_PRIVATE);
        home_Location = preference.getString("name", "");

        //When user enter location cancel button will appear in pickup location
        pickuploc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                cancelPickupLocation.setVisibility(View.VISIBLE);
            }


        });
        //When user enter location cancel button will appear in drop location
        dropoffloc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                cancelDropLocation.setVisibility(View.VISIBLE);
            }
        });


        cancelPickupLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickuploc.setText("");
                addr = "";
                _address = "";
                cancelPickupLocation.setVisibility(View.GONE);
            }
        });
        swapicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String swaptemp;
                boolean isairporttemp;
                isairporttemp=isAirportAtOri;
                isAirportAtOri= isAirportAtDest;
                isAirportAtDest= isairporttemp;
                swaptemp= pickuploc.getText().toString();
                pickuploc.setText( dropoffloc.getText().toString());
                dropoffloc.setText(swaptemp);
                if(pickuploc.getText().toString().equals(""))
                {
                    cancelPickupLocation.setVisibility(View.GONE);
                }
                else
                {
                    extractAddressFromOrigin(pickuploc.getText().toString(),OriginAndDestination.this);

                }
                if(dropoffloc.getText().toString().equals(""))
                {
                    cancelDropLocation.setVisibility(View.GONE);
                }   else
                {
                    extractAddressFromDestination(dropoffloc.getText().toString(),OriginAndDestination.this);

                }


            }
        });

        cancelDropLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dropoffloc.setText("");
                addr = "";
                _address = "";
                cancelDropLocation.setVisibility(View.GONE);
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnclicktobookingscreen(pickuploc,dropoffloc,OriginAndDestination.this);
            }
        });
        tiltmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tiltcheck) {
                    CameraPosition cp = new CameraPosition.Builder()
                            .bearing(mMap.getCameraPosition().bearing)
                            .tilt(90)
                            .target(new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude))
                            .zoom(17)
                            .build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cp);
                    mMap.animateCamera(cameraUpdate);


                    tiltcheck=true;
                }
                else{
                    CameraPosition cp = new CameraPosition.Builder().target(new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude))
                            .zoom(16)
                            .bearing(mMap.getCameraPosition().bearing)
                            .build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cp);
                    mMap.animateCamera(cameraUpdate);
                    tiltcheck=false;
                }
            }});
        imgAeroplane.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);

                if (!Network.isNetworkAvailable(OriginAndDestination.this)) {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(OriginAndDestination.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    builder1.setIcon(R.drawable.ic_error_black_24dp);
                    builder1.setTitle("Airport not avalible");
                    builder1.setMessage("Airport is not avalible, please enable internet.");
                    builder1.setCancelable(false);

                    builder1.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {

                    getAllAirportList();

                    Intent intent = new Intent(OriginAndDestination.this, AirportPickerActivity.class);
                    startActivityForResult(intent, 2);


                }


            }
        });
    }

    public void targetlocationonClick() {

        myLocationButton = (ImageButton) findViewById(R.id.targetlocation);

        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gps = new GPStracker(getApplicationContext(), OriginAndDestination.this);
                Double lati = gps.getLatitude();
                Double longi = gps.getLongitude();

                if (lati == 0.0) {
                    if (checkLocationPermission()) {
                        EnableGPSAutoMatically();
                    } else {
                        requestPermission();
                    }
                } else {

                    latLng = new LatLng(lati, longi);
//                    CameraPosition cp = new CameraPosition.Builder()
//                            .bearing(0)
//                            .tilt(90)
//                            .target(latLng)
//                            .zoom(20)
//                            .build();
//                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cp);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16.0f);
                    mMap.animateCamera(cameraUpdate);
                }
            }
        });

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                final Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);

                final byte[] size = getBytes(bitmap);
                long fileSizeInKB = size.length / 1024;

                if (bitmap != null || fileSizeInKB <= 2000) {
                    imageView.setImageBitmap(bitmap);
                    uploadFile(filePath);

                    if (getSharedPrefPhone()!= null && !getSharedPrefPhone().equals("")) {
                        SQLiteCustomerProfile sqLiteCustomerProfile = new SQLiteCustomerProfile(getApplicationContext());
                        sqLiteCustomerProfile.addprofile(getBytes(bitmap));
//                        JSONArray jsonArray = new JSONArray();
//                        jsonArray.put("CustomerReg");
//                        JSONObject jsonObject= new JSONObject();
//                        try {
//                            jsonObject.put("cust_phone", getSharedPrefPhone());
//                            jsonArray.put(jsonObject);
//                        }
//                        catch(Exception e){}
//                        SocketEvent.sharedinstance.socket.emit("getdata", jsonArray, new Ack() {
//
//                            @Override
//                            public void call(final Object... args) {
//
//                                if (args[0] != null && !args[0].toString().equals("0"))
//                                {
                        JSONArray jsonArray= new JSONArray();
                        jsonArray.put("CustomerReg");
                        JSONObject  jsonObject= new JSONObject();
                        try{
                            jsonObject.put("cust_phone",getSharedPrefPhone() );
                            jsonArray.put(jsonObject);
                            jsonObject= new JSONObject();
                            jsonObject.put("cust_image",size);}
                        catch(Exception e){}
                        jsonArray.put(jsonObject);

                        SocketEvent.sharedinstance.socket.emit("updatedata", jsonArray, new Ack() {
                            @Override
                            public void call(Object... args) {

                            }
                        });
                    }
//                                else
//                                {
//                                    JSONArray jsonArray= new JSONArray();
//                                    jsonArray.put("Check");
//                                    JSONObject  jsonObject= new JSONObject();
//                                    try{
//                                        jsonObject.put("phone",getSharedPrefPhone().toString() );
//                                        jsonObject.put("image",   size);
//                                        jsonArray.put(jsonObject);}
//                                    catch(Exception e){
//                                        e.printStackTrace();
//                                    }
//
//
//                                    SocketEvent.sharedinstance.socket.emit("createdata", jsonArray, new Ack() {
//                                        @Override
//                                        public void call(Object... args) {
//
//                                        }
//                                    });
//
//                                }
                    //     }});
                    //}
                } else {
                    Toast.makeText(this, "Please Select Other Image", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String intentAction = data.getStringExtra("action");
                if (intentAction.equals("pickup")) {

                    extractAddressFromOrigin(String.valueOf(pickuploc.getText().toString().trim()),this);
                    LatLng lng = new LatLng(latitude, longitude);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                    mMap.moveCamera(cameraUpdate);
                }
                if (intentAction.equals("dropoffloc")) {

                    nextBtn.setEnabled(true);
                    extractAddressFromDestination(String.valueOf(dropoffloc.getText().toString().trim()),this);
                    LatLng lng = new LatLng(latitude, longitude);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                    mMap.moveCamera(cameraUpdate);
                }
            }
        }


        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                String intentAction = data.getStringExtra("action");
                if (intentAction.equals("pickup")) {

                    extractAddressFromOrigin(String.valueOf(pickuploc.getText().toString().trim()),this);
                }
                if (intentAction.equals("dropoffloc")) {

                    nextBtn.setEnabled(true);
                    extractAddressFromDestination(String.valueOf(dropoffloc.getText().toString().trim()),this);
                }
            }
        }
        if(requestCode==9){
            getAllBookedBookingHistory();
        }
    }

    private void checkIfPlaceIsAirport(String address, String oriOrDes) {
        // int flagofac = 0;
        Geocoder mGeocoder = new Geocoder(OriginAndDestination.this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = mGeocoder.getFromLocationName(address, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            if (addresses.get(0).getPostalCode() != null) {
                String postalAddress = addresses.get(0).getPostalCode();
                String airportPlace = "";
                for (Airports a : airportDataList) {
                    if (postalAddress.equals(a.getPostcode())) {
                        airportPlace = a.getName();
                    }
                }
                if ((airportPlace.equals("")) == false) {
                    if (oriOrDes.equals("Ori")) {
                        isAirportAtOri = true;
                        pickuploc.setText(airportPlace);
                        Toast.makeText(this, "Airport at ori", Toast.LENGTH_SHORT).show();
                    } else {
                        isAirportAtDest = true;
                        dropoffloc.setText(airportPlace);
                        Toast.makeText(this, "Airport at des", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
        //flagOfActivities = flagofac;
    }

    public void setdropAddress() {
        dropoffloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = false;
                Intent intent = new Intent(OriginAndDestination.this, AutocompletePlacePickerActivity.class);
                intent.putExtra("action", "dropoffloc");
                startActivityForResult(intent, 1);
            }
        });
    }

    public void setpickuploc() {
        pickuploc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = true;
                Intent intent = new Intent(OriginAndDestination.this, AutocompletePlacePickerActivity.class);
                intent.putExtra("action", "pickup");
                startActivityForResult(intent, 1);

            }
        });
    }

    public void appDrawer() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        drawer.addDrawerListener(toggle);

        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            this.finishAffinity();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.origin_and_destination, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;

        if (id == R.id.history) {
            //fetch phone number
            SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);
            final String cust_phone1 = preferences.getString("cust_phone", "");
            if (cust_phone1.equals("")) {
                drawer.closeDrawer(GravityCompat.START);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                builder1.setIcon(R.drawable.ic_error_black_24dp);
                builder1.setTitle("History not avalible");
                builder1.setMessage("History is not avalible, please add your profile information.");
                builder1.setCancelable(false);
                builder1.setPositiveButton("Add Profile Info",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(OriginAndDestination.this, AccountInfo.class);
                                startActivity(intent);
                                dialog.cancel();
                            }
                        });
                builder1.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            } else {

                getAllBookedBookingHistory();


            }


            return true;


        } else if (id == R.id.setting) {
            Intent intent = new Intent(OriginAndDestination.this, buzybeez.com.buzybeezcabcrystalpalace.User_interface.Settings.class);
            startActivity(intent);

        } else if (id == R.id.about) {
            String version = "";
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OriginAndDestination.this, R.style.Theme_AppCompat_Light_Dialog);
            try {
                PackageInfo pInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
                version = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            alertDialogBuilder.setIcon(R.drawable.about)
                    .setTitle("Cabor")
                    .setMessage("App Version:" + version + "                           ");
            alertDialogBuilder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                        }
                    });


            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        } else if (id == R.id.nav_share) {
            shareButton();
        }

        else if(id == R.id.payment)
        {
            Intent intent = new Intent(OriginAndDestination.this,Payment.class);
            startActivity(intent);
            //select
        }

        else if(id == R.id.callOffc)
        {
            getOfficeNumberbyEmit(FIXED_OFFICE_KEY);
        }

        else if(id == R.id.becomedriver)
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.stationcars&hl=en_US" )));

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try{
            boolean success=false;
//    Calendar cal = Calendar.getInstance();
//    int hour = cal.get(Calendar.HOUR_OF_DAY);
//    if(hour < 6 || hour > 18){
//
//        success = mMap.setMapStyle(
//                MapStyleOptions.loadRawResourceStyle(
//                        this, R.raw.night_style_json));
//       // isNight = true;
//    } else {
//        //isNight = false;
//
//
            success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));
//    }
            if (!success) {
                Log.e("styled", "Style parsing failed.");
            }
        } catch(Resources.NotFoundException e) {
            Log.e("styled", "Can't find style. Error: ", e);
        }
        // Customise the styling of the base map using a JSON object defined
        // in a raw resource file.




        //these are commented because lolipop phone
        pickuploc.setVisibility(View.VISIBLE);
        dropoffloc.setVisibility(View.VISIBLE);
        nextBtn.setVisibility(View.VISIBLE);
        myLocationButton.setVisibility(View.VISIBLE);
        imgAeroplane.setVisibility(View.VISIBLE);


        if (pickuploc.getText().toString().equals("") && dropoffloc.getText().toString().equals("")) {
            cancelDropLocation.setVisibility(View.GONE);
            cancelPickupLocation.setVisibility(View.GONE);

        } else if (pickuploc.getText().toString().equals("") && dropoffloc.getText().toString() != null) {
            cancelPickupLocation.setVisibility(View.GONE);
            cancelDropLocation.setVisibility(View.VISIBLE);

        } else if (pickuploc.getText().toString() != null && dropoffloc.getText().toString().equals("")) {
            cancelPickupLocation.setVisibility(View.VISIBLE);
            cancelDropLocation.setVisibility(View.GONE);
        } else {
            cancelDropLocation.setVisibility(View.VISIBLE);
            cancelPickupLocation.setVisibility(View.VISIBLE);
        }

//            marker_imagehome.setVisibility(View.GONE);
        //    mMap.clear();


        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        try {
            Geocoder gcd = new Geocoder(this, Locale.getDefault());
            addresses = gcd.getFromLocation(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude, 1);


            if (addresses.size() > 0 && addresses != null) {
                yourAddress = addresses.get(0).getAddressLine(0);
                markerTextView.setText(yourAddress);
                LatLng lng = new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(lng, 15.0f);
                mMap.moveCamera(cameraUpdate);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (addresses.size() == 0) {
                getLocationOfOrigin();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        // This textview shows the location of user it will appear on top of marker
        markerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markerTextView.setEnabled(false);
                alertLocationSelection();

            }
        });

        markerTextView.setText(yourAddress);
//        if (selection == 1 || selection == 2) {
//            extractAddressfromflight();
//        }
    }

    public void getLocationOfOrigin() throws IOException {
        //storeLatLong();
        gps = new GPStracker(this, OriginAndDestination.this);
        if ((gps.getLatitude() == 0.0) == false) {
            latLng = new LatLng(gps.getLatitude(), gps.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15.0f).build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            mMap.moveCamera(cameraUpdate);
            markerTextView.setText(yourAddress);
        }
    }

    public void extractAddressFromOrigin(Place place) {

        Geocoder mGeocoder = new Geocoder(OriginAndDestination.this, Locale.getDefault());
        DataHolder.getInstance().originData.clear();
        DataHolder.getInstance().originDataLatLng.clear();
        List<Address> addresses = null;
        try {
            addresses = mGeocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            addresses.get(0).getLocality();
            if (addresses.get(0).getPostalCode() == null) {
                address = String.valueOf(place.getAddress());
                String[] tempOutCode = address.split(",");
                String[] splitoutCode = tempOutCode[0].split(" ");
                mpostcode = tempOutCode[0];
                if (splitoutCode.length == 2) {
                    mOutcode = splitoutCode[1];
                } else {
                    mOutcode = "NPC";
                }

                latitude = addresses.get(0).getLatitude();
                originlat = latitude;

                longitude = addresses.get(0).getLongitude();
                originlon = longitude;

                DataHolder.getInstance().originData.add(0, address);
                DataHolder.getInstance().originData.add(1, mOutcode);
                DataHolder.getInstance().originData.add(2, mpostcode);

                DataHolder.getInstance().originDataLatLng.add(0, originlat);
                DataHolder.getInstance().originDataLatLng.add(1, originlon);

            } else {
                String postalCode = addresses.get(0).getPostalCode();
                address = String.valueOf(place.getAddress());
                String[] retrivepostCode = postalCode.split(" ");
                mOutcode = retrivepostCode[0];
//                mpostcode = retrivepostCode[1];
                mpostcode = postalCode;

                latitude = addresses.get(0).getLatitude();
                originlat = latitude;

                longitude = addresses.get(0).getLongitude();
                originlon = longitude;

                DataHolder.getInstance().originData.add(0, address);
                DataHolder.getInstance().originData.add(1, mOutcode);
                DataHolder.getInstance().originData.add(2, mpostcode);


                DataHolder.getInstance().originDataLatLng.add(0, originlat);
                DataHolder.getInstance().originDataLatLng.add(1, originlon);

            }
            if (latitude != 0.0 && longitude != 0.0) {
                LatLng lng = new LatLng(latitude, longitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                mMap.moveCamera(cameraUpdate);
            } else {

            }
        }
    }



    public void extractAddressFromDestination(Place place) {
        Geocoder mGeocoder = new Geocoder(OriginAndDestination.this, Locale.getDefault());
        DataHolder.getInstance().destinationData.clear();
        DataHolder.getInstance().destinationDataLatLng.clear();
        List<Address> addresses = null;
        try {
            addresses = mGeocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            addresses.get(0).getLocality();
            if (addresses.get(0).getPostalCode() == null) {
                address = String.valueOf(place.getAddress());
                String[] tempOutCode = address.split(",");
                String[] splitoutCode = tempOutCode[0].split(" ");
                mDespostcode = tempOutCode[0];

                if (splitoutCode.length == 2) {
                    mDesOutcode = splitoutCode[1];
                } else {
                    mDesOutcode = "NPC";
                }

                latitude = addresses.get(0).getLatitude();
                deslat = latitude;

                longitude = addresses.get(0).getLongitude();
                deslon = longitude;

                DataHolder.getInstance().destinationData.add(0, mDesaddress);
                DataHolder.getInstance().destinationData.add(1, mDesOutcode);
                DataHolder.getInstance().destinationData.add(2, mDespostcode);

                DataHolder.getInstance().destinationDataLatLng.add(0, deslat);
                DataHolder.getInstance().destinationDataLatLng.add(1, deslon);

            } else {
                String postalCode = addresses.get(0).getPostalCode();
                mDesaddress = String.valueOf(place.getAddress());
                String[] retrivepostCode = postalCode.split(" ");
                mDesOutcode = retrivepostCode[0];
                // mDespostcode = retrivepostCode[1];
                mDespostcode = postalCode;

                latitude = addresses.get(0).getLatitude();
                deslat = latitude;

                longitude = addresses.get(0).getLongitude();
                deslon = longitude;

                DataHolder.getInstance().destinationData.add(0, mDesaddress);
                DataHolder.getInstance().destinationData.add(1, mDesOutcode);
                DataHolder.getInstance().destinationData.add(2, mDespostcode);

                DataHolder.getInstance().destinationDataLatLng.add(0, deslat);
                DataHolder.getInstance().destinationDataLatLng.add(1, deslon);

            }
            if (latitude != 0.0 && longitude != 0.0) {
                LatLng lng = new LatLng(latitude, longitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                mMap.moveCamera(cameraUpdate);
            }
        }
    }



    public void alertLocationSelection() {

        if (!((Activity) OriginAndDestination.this).isFinishing()) {

            try {

                OriginAndDestination.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new AlertDialog.Builder(OriginAndDestination.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                                .setIcon(R.drawable.round_not_listed_location_black_18dp)
                                .setTitle("Location Selection")
                                .setMessage("Please specify that you want to set this location as your destination or origin?")
                                .setCancelable(false)
                                .setNegativeButton("Origin",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                if (addresses != null && addresses.size() > 0) {
                                                    addresses.get(0).getLocality();
                                                    if (addresses.get(0).getPostalCode() == null) {
                                                        mOutcode = "NPC";
                                                        String[] tempOutCode = yourAddress.split(",");
                                                        mpostcode = tempOutCode[0];

                                                        latitude = addresses.get(0).getLatitude();
                                                        originlat = latitude;

                                                        longitude = addresses.get(0).getLongitude();
                                                        originlon = longitude;

                                                        DataHolder.getInstance().originData.add(0, yourAddress);
                                                        DataHolder.getInstance().originData.add(1, mOutcode);
                                                        DataHolder.getInstance().originData.add(2, mpostcode);

                                                        DataHolder.getInstance().originDataLatLng.add(0, originlat);
                                                        DataHolder.getInstance().originDataLatLng.add(1, originlon);

                                                    } else {
                                                        String postalCode = addresses.get(0).getPostalCode();
                                                        String[] retrivepostCode = postalCode.split(" ");
                                                        mOutcode = retrivepostCode[0];
//                mpostcode = retrivepostCode[1];
                                                        mpostcode = postalCode;

                                                        latitude = addresses.get(0).getLatitude();
                                                        originlat = latitude;

                                                        longitude = addresses.get(0).getLongitude();
                                                        originlon = longitude;

                                                        DataHolder.getInstance().originData.add(0, yourAddress);
                                                        DataHolder.getInstance().originData.add(1, mOutcode);
                                                        DataHolder.getInstance().originData.add(2, mpostcode);


                                                        DataHolder.getInstance().originDataLatLng.add(0, originlat);
                                                        DataHolder.getInstance().originDataLatLng.add(1, originlon);

                                                    }

                                                }

                                                pickuploc.setText(yourAddress);
                                                markerTextView.setEnabled(true);
                                            }
                                        })
                                .setPositiveButton("Destination", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (addresses != null && addresses.size() > 0) {
                                            addresses.get(0).getLocality();
                                            if (addresses.get(0).getPostalCode() == null) {
                                                mDesOutcode = "NPC";

                                                String[] tempOutCode = yourAddress.split(",");
                                                mDespostcode = tempOutCode[0];

                                                latitude = addresses.get(0).getLatitude();
                                                deslat = latitude;

                                                longitude = addresses.get(0).getLongitude();
                                                deslon = longitude;

                                                DataHolder.getInstance().destinationData.add(0, yourAddress);
                                                DataHolder.getInstance().destinationData.add(1, mDesOutcode);
                                                DataHolder.getInstance().destinationData.add(2, mDespostcode);

                                                DataHolder.getInstance().destinationDataLatLng.add(0, deslat);
                                                DataHolder.getInstance().destinationDataLatLng.add(1, deslon);

                                            } else {
                                                String postalCode = addresses.get(0).getPostalCode();
                                                String[] retrivepostCode = postalCode.split(" ");
                                                mDesOutcode = retrivepostCode[0];
                                                // mDespostcode = retrivepostCode[1];
                                                mDespostcode = postalCode;

                                                latitude = addresses.get(0).getLatitude();
                                                deslat = latitude;

                                                longitude = addresses.get(0).getLongitude();
                                                deslon = longitude;


                                                DataHolder.getInstance().destinationData.add(0, yourAddress);
                                                DataHolder.getInstance().destinationData.add(1, mDesOutcode);
                                                DataHolder.getInstance().destinationData.add(2, mDespostcode);

                                                DataHolder.getInstance().destinationDataLatLng.add(0, deslat);
                                                DataHolder.getInstance().destinationDataLatLng.add(1, deslon);

                                            }
                                        }

                                        dropoffloc.setText(yourAddress);
                                        markerTextView.setEnabled(true);
                                    }
                                })
                                .setNeutralButton("cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        markerTextView.setEnabled(true);
                                        dialog.cancel();
                                    }
                                }).create().show();

                    }
                });

            } catch (Exception e) {
                e.getMessage();
            }


        }
    }

    @Override
    public void onCameraIdle() {

        //addwaypointTV.setVisibility(View.VISIBLE);
        //markerImageView.setVisibility(View.INVISIBLE);
        pickuploc.setVisibility(View.VISIBLE);
        dropoffloc.setVisibility(View.VISIBLE);
        nextBtn.setVisibility(View.VISIBLE);
        myLocationButton.setVisibility(View.VISIBLE);
        imgAeroplane.setVisibility(View.VISIBLE);
        pickupdestinationlayout.setVisibility(View.VISIBLE);
        enable_home_office_button();


        try {
            Geocoder gcd = new Geocoder(this, Locale.getDefault());
            addresses = gcd.getFromLocation(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude, 1);
            if (addresses.size() > 0 && addresses != null) {
                yourAddress = addresses.get(0).getAddressLine(0);
                markerTextView.setText(yourAddress);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        // mMap.addMarker(new MarkerOptions().position(mMap.getCameraPosition().target).anchor(0.5f, .05f).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
        markerTextView.setText(yourAddress);


        if (pickuploc.getText().toString().equals("") && dropoffloc.getText().toString().equals("")) {
            cancelDropLocation.setVisibility(View.GONE);
            cancelPickupLocation.setVisibility(View.GONE);

        } else if (pickuploc.getText().toString().equals("") && dropoffloc.getText().toString() != null) {
            cancelPickupLocation.setVisibility(View.GONE);
            cancelDropLocation.setVisibility(View.VISIBLE);

        } else if (pickuploc.getText().toString() != null && dropoffloc.getText().toString().equals("")) {
            cancelPickupLocation.setVisibility(View.VISIBLE);
            cancelDropLocation.setVisibility(View.GONE);
        } else {
            cancelDropLocation.setVisibility(View.VISIBLE);
            cancelPickupLocation.setVisibility(View.VISIBLE);
        }
        if (!isNetworkAvailable()) {
            markerTextView.setText("No Internet");
        }

    }

    @Override
    public void onCameraMove() {

        //   mMap.clear();
        //addwaypointTV.setVisibility(View.INVISIBLE);
        imgAeroplane.setVisibility(View.GONE);
        pickuploc.setVisibility(View.GONE);
        dropoffloc.setVisibility(View.GONE);
        nextBtn.setVisibility(View.GONE);
        myLocationButton.setVisibility(View.GONE);
        cancelPickupLocation.setVisibility(View.GONE);
        cancelDropLocation.setVisibility(View.GONE);
        pickupdestinationlayout.setVisibility(View.GONE);
        markerImageView.setVisibility(View.VISIBLE);
        markerTextView.setText("Searching......");


        //  markerTextView.setText(yourAddress);
        if (home_Location.equals("") && office_Location.equals("")) {
            homeLocationButton.setVisibility(View.GONE);
            officeLocationButton.setVisibility(View.GONE);
        } else {
            homeLocationButton.setVisibility(View.GONE);
            officeLocationButton.setVisibility(View.GONE);
        }

    }

    private void shareButton() {


        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, SHARE_SUBJECT);
        shareIntent.putExtra(Intent.EXTRA_TEXT, SHARE_TEXT);
        startActivity(Intent.createChooser(shareIntent, "Share With Friends"));
    }

    private void CustDetailNavDrawer() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);
        String name = preferences.getString("cust_name", "");
        String email = preferences.getString("cust_email", "");

        TextView name_textview = (TextView) headerView.findViewById(R.id.customerName);
        TextView email_textview = (TextView) headerView.findViewById(R.id.customerEmail);

        if (name.equals("") && email.equals("")) {
//			name_textview.setText("XYZ");
//			email_textview.setText("Enter Your");
            name_textview.setVisibility(View.GONE);
            email_textview.setVisibility(View.GONE);


        } else {
            name_textview.setText(name);
            email_textview.setText(email);

        }
    }

    public void enable_home_office_button() {

        SharedPreferences preferences = getSharedPreferences("office_address", MODE_PRIVATE);
        office_Location = preferences.getString("office", "");

        SharedPreferences preference = getSharedPreferences("home_address", MODE_PRIVATE);
        home_Location = preference.getString("name", "");


        if (!home_Location.equals("")) {
            homeLocationButton.setVisibility(View.VISIBLE);
        }
        if (!office_Location.equals("")) {
            officeLocationButton.setVisibility(View.VISIBLE);
        }


    }

    public void detectDrawerClick() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                CustDetailNavDrawer();
                enable_home_office_button();
                choseprofile();
                downloadImage();

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                //enable_home_office_button();
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });
    }

    public void homebuttonclick() {
        final SharedPreferences preference = getSharedPreferences("home_address", MODE_PRIVATE);
        homeLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!((Activity) OriginAndDestination.this).isFinishing()) {

                    try {

                        OriginAndDestination.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AlertDialog.Builder(OriginAndDestination.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                                        .setIcon(R.drawable.round_not_listed_location_black_18dp)
                                        .setTitle("Location Selection")
                                        .setMessage("Please specify that you want to set this location as your destination or origin?")
                                        .setCancelable(false)
                                        .setNegativeButton("Origin",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        mpostcode = preference.getString("homepostcode", "");
                                                        originlat = Double.parseDouble(preference.getString("homelat", ""));
                                                        originlon = Double.parseDouble(preference.getString("homelng", ""));
                                                        mOutcode = preference.getString("homeoutcode", "");

                                                        LatLng lng = new LatLng(originlat, originlon);
                                                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                                                        mMap.moveCamera(cameraUpdate);

                                                        DataHolder.getInstance().originData.add(0, home_Location);
                                                        DataHolder.getInstance().originData.add(1, mOutcode);
                                                        DataHolder.getInstance().originData.add(2, mpostcode);
                                                        DataHolder.getInstance().originDataLatLng.add(0, originlat);
                                                        DataHolder.getInstance().originDataLatLng.add(1, originlon);


                                                        pickuploc.setText(home_Location);

                                                    }
                                                })
                                        .setPositiveButton("Destination", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                mDespostcode = preference.getString("homepostcode", "");
                                                deslat = Double.parseDouble(preference.getString("homelat", ""));
                                                deslon = Double.parseDouble(preference.getString("homelng", ""));
                                                mDesOutcode = preference.getString("homeoutcode", "");

                                                LatLng lng = new LatLng(deslat, deslon);
                                                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                                                mMap.moveCamera(cameraUpdate);

                                                DataHolder.getInstance().destinationData.add(0, home_Location);
                                                DataHolder.getInstance().destinationData.add(1, mDesOutcode);
                                                DataHolder.getInstance().destinationData.add(2, mDespostcode);

                                                DataHolder.getInstance().destinationDataLatLng.add(0, deslat);
                                                DataHolder.getInstance().destinationDataLatLng.add(1, deslon);


                                                dropoffloc.setText(home_Location);
                                            }
                                        })
                                        .setNeutralButton("cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).create().show();
                            }
                        });

                    } catch (Exception e) {
                        e.getMessage();
                    }

                }

            }
        });


    }

    public void officebuttonclick() {
        final SharedPreferences preference = getSharedPreferences("office_address", MODE_PRIVATE);


        officeLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!((Activity) OriginAndDestination.this).isFinishing()) {

                    try {

                        OriginAndDestination.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AlertDialog.Builder(OriginAndDestination.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                                        .setIcon(R.drawable.round_not_listed_location_black_18dp)
                                        .setTitle("Location Selection")
                                        .setMessage("Please specify that you want to set this location as your destination or origin?")
                                        .setCancelable(false)
                                        .setNegativeButton("Origin",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        mpostcode = preference.getString("officepostcode", "");
                                                        originlat = Double.parseDouble(preference.getString("officelat", ""));
                                                        originlon = Double.parseDouble(preference.getString("officelng", ""));
                                                        mOutcode = preference.getString("officeoutcode", "");

                                                        LatLng lng = new LatLng(originlat, originlon);
                                                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                                                        mMap.moveCamera(cameraUpdate);

                                                        DataHolder.getInstance().originData.add(0, office_Location);
                                                        DataHolder.getInstance().originData.add(1, mOutcode);
                                                        DataHolder.getInstance().originData.add(2, mpostcode);


                                                        DataHolder.getInstance().originDataLatLng.add(0, originlat);
                                                        DataHolder.getInstance().originDataLatLng.add(1, originlon);


                                                        pickuploc.setText(office_Location);
                                                    }
                                                })
                                        .setPositiveButton("Destination", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                mDespostcode = preference.getString("officepostcode", "");
                                                deslat = Double.parseDouble(preference.getString("officelat", ""));
                                                deslon = Double.parseDouble(preference.getString("officelng", ""));
                                                mDesOutcode = preference.getString("officeoutcode", "");

                                                LatLng lng = new LatLng(deslat, deslon);
                                                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                                                mMap.moveCamera(cameraUpdate);

                                                DataHolder.getInstance().destinationData.add(0, office_Location);
                                                DataHolder.getInstance().destinationData.add(1, mDesOutcode);
                                                DataHolder.getInstance().destinationData.add(2, mDespostcode);

                                                DataHolder.getInstance().destinationDataLatLng.add(0, deslat);
                                                DataHolder.getInstance().destinationDataLatLng.add(1, deslon);


                                                dropoffloc.setText(office_Location);
                                            }
                                        })
                                        .setNeutralButton("cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).create().show();
                            }
                        });

                    } catch (Exception e) {
                        e.getMessage();
                    }

                }
            }
        });


    }

    public void choseprofile() {
        imageView = (ImageView) findViewById(R.id.profile);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });


    }

    private void uploadFile(final Uri file) {
        if (getSharedPrefPhone()!= null && !getSharedPrefPhone().equals("")) {
//            uid = getSharedPrefPhone();
//            FirebaseStorage storage = FirebaseStorage.getInstance();
//            StorageReference mountainsRef = storage.getReference().child("users/" + uid);
//            final UploadTask uploadTask = mountainsRef.putFile(file);
//            uploadTask.addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception exception) {
//                    // Handle unsuccessful uploads
//                    Toast.makeText(OriginAndDestination.this, "Fail to upload " + exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//                }
//            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
//                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
//                    downloadUrlStr = downloadUrl.toString();
//
//
//                    mDb.getReference().child("users/" + uid).setValue(new User(downloadUrlStr));
//
//                }
//            });
        }


    }

    public void downloadImage() {

        final SQLiteCustomerProfile sqLiteCustomerProfile = new SQLiteCustomerProfile(this);

        if (sqLiteCustomerProfile.getprofile() != null) {
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(sqLiteCustomerProfile.getprofile(), 0, sqLiteCustomerProfile.getprofile().length));

        } else {

            if (getSharedPrefPhone()!=null && !getSharedPrefPhone().equals("")) {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put("CustomerReg");
                JSONObject jsonObject= new JSONObject();
                try {
                    jsonObject.put("cust_phone", getSharedPrefPhone());
                    jsonArray.put(jsonObject);
                }
                catch(Exception e){}
                SocketEvent.sharedinstance.socket.emit("getdata", jsonArray, new Ack() {

                    @Override
                    public void call(final Object... args) {

                        if (args[0] != null && !args[0].toString().equals("0"))
                        {

                            JSONObject data = (JSONObject) args[0];
                            try {
                                bytearray = data.getString("cust_image").toString().getBytes();

                            }
                            catch(Exception e){
                                e.printStackTrace();
                            }
                            try { runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Bitmap bitmap2 = BitmapFactory.decodeByteArray(android.util.Base64.decode(new String(bytearray), android.util.Base64.DEFAULT), 0, android.util.Base64.decode(new String(bytearray), android.util.Base64.DEFAULT).length);
                                        imageView.setImageBitmap(bitmap2);
                                        sqLiteCustomerProfile.addprofile(android.util.Base64.decode(new String(bytearray), android.util.Base64.DEFAULT));
                                    }
                                    catch(Exception e){}

                                }

                            });}
                            catch(Exception ee){
                                ee.printStackTrace();
                            }

                        }}});
//                uid = mAuth.getCurrentUser().getPhoneNumber();
//
//                mDb.getReference().child("users/" + uid).addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        String compare = String.valueOf(mDb.getReference("users/" + uid).getKey());
//                        String id = String.valueOf(mAuth.getCurrentUser().getPhoneNumber());
//                        if (dataSnapshot.getValue() != null) {
//
//                            if (compare.equals(id)) {
//                                buzybeez.com.buzybeezcabsurbiton.Models.User user = dataSnapshot.getValue(buzybeez.com.buzybeezcabsurbiton.Models.User.class);
//
//
//                                //  Glide.with(getApplicationContext()).load(user.getImage()).into(imageView);
//                                Glide.with(getApplicationContext())
//                                        .load(user.getImage())
//                                        .asBitmap()
//                                        .into(new SimpleTarget<Bitmap>() {
//                                            @Override
//                                            public void onResourceReady(final Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                                                final SQLiteCustomerProfile sqLiteCustomerProfile = new SQLiteCustomerProfile(getApplicationContext());
//                                                sqLiteCustomerProfile.addprofile(getBytes(resource));
//                                                //  imageView.setImageBitmap(BitmapFactory.decodeByteArray(sqLiteCustomerProfile.getprofile(), 0, sqLiteCustomerProfile.getprofile().length));
//
//                                                imageView.setImageBitmap(resource);
//
//
//                                                        }
//                                        });
//                            }
//                        } else {
//                            imageView.setImageResource(R.drawable.profile);
//                        }
//
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                });
//

            }
        }
    }

    public void BookingStatusAlert() {


        OriginAndDestination.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(OriginAndDestination.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                        .setIcon(R.drawable.ic_check_black_24dp)
                        .setTitle("Success")
                        .setMessage("Booking has been saved" + " We will contact you at pickup time")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).create().show();
            }
        });

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void openwpselectionscreen(View view) {
        //   if(Network.isNetworkAvailable(OriginAndDestination.this)) {
        startActivity(new Intent(OriginAndDestination.this, AddWaypoints.class));
        //  }
    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            //Toast.makeText(OriginAndDestination.this, "Location Api Connected", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            //Toast.makeText(OriginAndDestination.this, "Location Api suspended", Toast.LENGTH_SHORT).show();

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                            //Toast.makeText(OriginAndDestination.this, "Location Api failed", Toast.LENGTH_SHORT).show();
                        }
                    }).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            break;

                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            try {
                                status.startResolutionForResult(OriginAndDestination.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                            }
                            break;

                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            toast("Setting change not allowed");
                            break;
                    }
                }
            });
        }
    }

    private void toast(String message) {
        try {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            log("Window has been closed");
        }

    }

    public boolean checkLocationPermission() {
        int result = ContextCompat.checkSelfPermission(OriginAndDestination.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;

        } else {
            return false;
        }
    }

    public void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ALLPERMISSIONS_CODE);

        } else {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ALLPERMISSIONS_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case REQUEST_ALLPERMISSIONS_CODE: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    EnableGPSAutoMatically();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void ShowLocationDialog() {

        TextView enableLocation = (TextView) findViewById(R.id.enablelocation);
        enableLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkLocationPermission()) {
                    EnableGPSAutoMatically();
                } else {
                    requestPermission();
                }
            }
        });
    }

    //skh: this method takes phone number and fetch all the records and save them in the below array lists,
    // these array list are imported in BookingHistoryActivity and used as data in the booked ,
    // completed and canceled tab.
    public void getAllBookedBookingHistory() {
        // if
//    (!Network.isNetworkAvailable(OriginAndDestination.this)) {return;}
//        //fetch phone number
//
//        SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);
//        final String cust_phone1 = preferences.getString("cust_phone", "");
//        if (cust_phone1.equals("")) {
//
//        } else {
//            //if phone number is avalible, connect socket if not connected
//            if (SocketEvent.sharedinstance.socket == null) {
//                SocketEvent.sharedinstance.initializeSocket();
//            }
////            final Timer timer = new Timer();
////            timer.schedule(new TimerTask() {
////                @Override
////                public void run() {
//
//            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(getApplicationContext())) {
//
////                        timer.cancel();
//                JSONArray query = new JSONArray();
//                JSONObject filter = new JSONObject();
//                query.put("Booking");
//                try {
//                    //filter.put("telephone", cust_phone);
//                    filter.put("office", FIXED_OFFICE_KEY);
//                    filter.put("telephone", cust_phone1);
//                    query.put(filter);
//                    progressDialog.setMessage("Loading History...");
//                    progressDialog.show();
//                    SocketEvent.sharedinstance.socket.emit("getagentpayments", query, new Ack() {
//
//                        @Override
//                        public void call(final Object... args) {
//                            bookedBookingHistoryOfUser.clear();
//                            completedBookingHistoryOfUser.clear();
//                            canceledBookingHistoryOfUser.clear();
//                            if (args[0] != null && !args[0].toString().equals("0")) {
//                                try {
//                                    JSONArray jsonArray = new JSONArray(args[0].toString());
//                                    for (int i = 0; i < jsonArray.length(); i++) {
//                                        JSONObject booking = jsonArray.getJSONObject(i);
//                                        JsonNode jsonNode = JSONParse.convertJsonFormat(booking);
//                                        ObjectMapper mapper = new ObjectMapper();
//                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//                                        Booking.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
//                                        Booking b = Booking.sharedinstance;
//                                        //setup datarraylists for each recycler view
//                                        if (b.getCstate().equals("booked")) {
//                                         //   bookedBookingHistoryOfUser.add(b);
//                                        } else if (b.getCstate().equals("despatched")) {
//                                            completedBookingHistoryOfUser.add(b);
//                                        } else if (b.getCstate().equals("cancelled")) {
//                                            canceledBookingHistoryOfUser.add(b);
//                                        }
//                                    }
//                                    progressDialog.cancel();
        if(!bookedBookingHistoryOfUser.isEmpty() ||!completedBookingHistoryOfUser.isEmpty() ||!canceledBookingHistoryOfUser.isEmpty() ){
            bookedBookingHistoryOfUser.clear();
            canceledBookingHistoryOfUser.clear();
            completedBookingHistoryOfUser.clear();}
        Intent intent = new Intent(OriginAndDestination.this, BookingHistoryActivity.class);
        startActivity(intent);
//                                } catch (Exception e) {
//                                    progressDialog.cancel();
//                                    e.getMessage();
//                                }
//                            }
//                        }
//                    });
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            //   }
//
//            // }, 100, 1000);
//
//        }

    }


    //skh: this method takes phone number and fetch all the records and save them in the below array lists,
    // these array list are imported in BookingHistoryActivity and used as data in the booked ,
    // completed and canceled tab.
    public void getAllAirportList() {

        if (db.getAllAirports().size() == 0) {

            if (SocketEvent.sharedinstance.socket == null) {
                SocketEvent.sharedinstance.initializeSocket();
            }
//            final Timer timer = new Timer();
//            timer.schedule(new TimerTask() {
//                @Override
//                public void run() {

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(getApplicationContext())) {

//                        timer.cancel();
                JSONArray query = new JSONArray();
                JSONObject filter = new JSONObject();
                query.put("Airports");
                try {
                    filter.put("OfficeId", FIXED_OFFICE_KEY);
                    query.put(filter);
                    //getmultiplerecords
                    progressDialog.setMessage("Loading Airorts...");
                    progressDialog.show();
                    SocketEvent.sharedinstance.socket.emit("getagentpayments", query, new Ack() {

                        @Override
                        public void call(final Object... args) {
                            if (args[0] != null && !args[0].toString().equals("0")) {
                                try {
                                    JSONArray jsonArray = new JSONArray(args[0].toString());
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject booking = jsonArray.getJSONObject(i);
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(booking);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        Airports.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), Airports.class);
                                        Airports a = Airports.sharedinstance;
                                        airportDataList.add(a);
                                        db.addAirportToDb(a);

                                    }
                                    progressDialog.cancel();
                                } catch (Exception e) {
                                    progressDialog.cancel();
                                    e.getMessage();
                                }
                            }
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            /// insertAriports(AppDatabase.getAppDatabase(OriginAndDestination.this), airportDataList);
        }

        //   }, 0, 1000);
        // }

    }


    //skh:These methods are the impelmented from "interface LocationProviderStatusListner"
    // which is in GPStracker.java class
    @Override
    public void onDeviceLocationChanged(Location location) {
        // Toast.makeText( this, "Location Changed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocProviderDisabled(String provider) {
        if (NoGPSMSG.getVisibility() == View.GONE) {
            NoGPSMSG.setVisibility(View.VISIBLE);
        }

        if (markerTextView.getVisibility() == View.VISIBLE) {
            markerTextView.setVisibility(View.GONE);

        }
    }

    @Override
    public void onLocProviderEnabled(String provider) {
        if (NoGPSMSG.getVisibility() == View.VISIBLE) {
            NoGPSMSG.setVisibility(View.GONE);
        }
        if (markerTextView.getVisibility() == View.GONE) {
            markerTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onProviderStatusChanged(String provider, int status, Bundle extras) {

    }


    @Override
    public void onGetResponse(boolean isUpdateAvailable) {
        Log.e("ResultAPPMAIN", String.valueOf(isUpdateAvailable));
        if (isUpdateAvailable) {
            showUpdateDialog();
        }

    }

    private void showUpdateDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OriginAndDestination.this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        alertDialogBuilder.setTitle(OriginAndDestination.this.getString(R.string.app_name));
        alertDialogBuilder.setMessage("An Update is Available");
        alertDialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                OriginAndDestination.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                if (isForceUpdate) {
//                    finish();
//                }
                dialog.dismiss();
            }
        });
        alertDialogBuilder.show();



    }
}