package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants;
import buzybeez.com.buzybeezcabcrystalpalace.DataCenter.DataHolder;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.poblistener;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.DirectionFinder;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.DirectionFinderListener;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.Distance;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.Duration;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.Route;
import buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules.TimeToDespatcInterface;
import buzybeez.com.buzybeezcabcrystalpalace.Models.BookingHistory;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CfgCustApp;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CustomerConfig;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Driver;
import buzybeez.com.buzybeezcabcrystalpalace.Models.DriverLoc;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.Models.LoadMsg;
import buzybeez.com.buzybeezcabcrystalpalace.Models.OfficeDetail;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Rating;
import buzybeez.com.buzybeezcabcrystalpalace.Models.SecureData;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.GPStracker;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.socket.client.Ack;

//import static buzybeez.com.buzybeezcabsurbiton.Fragments.TabsForHistory.BookedJobs.removeData;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.APP_OFFICE_NAME;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.FIXED_OFFICE_KEY;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.SHARE_RIDE_TEXT;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.BookingScreen.customerRoom;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.BookingScreen.jobID;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.BookingScreen.jobRef;

public class DriverSearch extends BaseActivity implements ConnectionListener,
        OnMapReadyCallback, DirectionFinderListener, TimeToDespatcInterface, poblistener {
    double totalDuration = 0;
    double speed=0;
    double totalDistance = 0;
    int ReRouteCount=0;
    public static Context activity;
    // public static String selectedPickup, selectedDropoff;
    public static String getDriverIp = "";
    public static Handler directionhandler;
    public static String JOBID;
    public static Handler rejectEmitHandler;
    public GoogleMap mMap = null;
    LatLng latLng;
    boolean checkPOBemit = false;
    LottieAnimationView lottieSearching;
    Marker drvCarMarker;
    Marker custLatLngMarker;
    LinearLayout searchlottieLayout;
    int flag = 0;
    Double lat, lng;
    float v;
    int secondemit=15;
    LatLng newPos;
    boolean tiltcheck =false;
    boolean isMarkerRotating = false;
    Handler handler;
    String driverLatLng;
    LatLng start, end;
    List<LatLng> newlist = new ArrayList<>();
    LatLng driverlatlng;
    int timer = 0;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    ViewGroup actionBarLayout;
    Menu nav_Menu;
    CoordinatorLayout coordinatorLayout;
    ConstraintLayout iv_trigger;
    View bottomsheetdriver;
    ImageView circularImageView,vehicleImageView,tiltmap;
    ArrayList<String> ids;
    String jobid;
    int pos;
    BottomSheetBehavior behavior;
    SharedPreferences.Editor editor;
    String ip;
    String image_url,vehicleimg_url;
    LatLngBounds bound;
    Toolbar toolbar;
    String drvImageIp;
    String jobidSahredPrefence;
    int counter = 0;
    SharedPreferences preferences;
    TextView txtDriverName;
    TextView txtDriverRegNumber,car_speed,RouteDuration,RouteDistance;
    //	TextView txtDriverCarModel ;
    //TextView txtDriverCarColor;
    TextView txtDriverContact;
    ImageButton driverSMS;
    ImageButton driverCallIB, officeCallIB, cancelBookingIB;
    TextView CustTo;
    TextView CustFrom;
    TextView CustFare;
    TextView comment;
    TextView bookedvehicle, dateTimeTV, distanceTV, durationTV;
    TextView flightno;
    static TextView counterMsg;
    ConstraintLayout CommentLayout, infoCL;
    ConstraintLayout FlightLayout;
    LinearLayout BottomSheetLL;
    private List<LatLng> polyLineList;
    private ProgressDialog progressDialog;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private int index, next;
    private int emitcount = 0;
    private ImageButton myLocationButton;
    private Handler DatagetHanler;
    private int status = 0;
    private int Internetcounter = 0;
    static Context mContext;
    static Activity mActivty;
    static NotificationManager manager;
    static int checKNotification=0;
    int i;
    LatLng viaslatlng;
    ValueAnimator valueAnimator;
    long milliseconds;
    long unixTimeStamp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_search);

        //     if(getIntent().hasExtra("futurebooking")) {
        // Bundle extras=getIntent().getExtras();
//            SharedPreferences extras = getSharedPreferences("localnoti", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editorlocal = extras.edit();
//            if (extras.getString("futurebooking","").equals("1")) {
//                DataHolder.getInstance().vehType = extras.getString("vehType","");
//                DataHolder.getInstance().originLng = Double.parseDouble(extras.getString("originLng",""));
//                DataHolder.getInstance().originLat = Double.parseDouble(extras.getString("originLat",""));
//                DataHolder.getInstance().origin = extras.getString("origin","");
//                DataHolder.getInstance().destination = extras.getString("destination","");
//                DataHolder.getInstance()._fromOutcode = extras.getString("_fromOutcode","");
//                DataHolder.getInstance().jobID = extras.getString("jobID","");
//               JOBID=DataHolder.getInstance().jobID;
//                DataHolder.getInstance().jobReference = extras.getString("jobReference","");
//                DataHolder.getInstance().telephone = extras.getString("telephone","");
//                CfgCustApp.sharedinstance.setJobInOffice(extras.getString("jobinoffice",""));
//                CustomerConfig.sharedinstance.setImageIP(extras.getString("drvImageIp",""));
//                editorlocal.putString("futurebooking","0");
//                editorlocal.apply();
//            }
//            try{
//            Thread.sleep(5000);
//            }
//            catch(Exception e){}
        // }
        valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(5000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        SocketEvent.  poblisten=(poblistener) this;
        mActivty = this;
        mContext = this;
        activity = DriverSearch.context;
        // getDozeMode();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Driver Tracking");

        if (SocketEvent.sharedinstance.socket == null && Network.isNetworkAvailable(this)) {
            SocketEvent.sharedinstance.socketConnectAfterDisconnect();
        }

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);

        circularImageView = findViewById(R.id.imgDrvPic);
        vehicleImageView=findViewById(R.id.imagevehicle);
        tiltmap=findViewById(R.id.tiltmap);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        //Navigation

        mNavigationView = (NavigationView) findViewById(R.id.navMenu);
        nav_Menu = mNavigationView.getMenu();

        getsharedprefenceBookingData();

        hideHomeNav();

        navigationDrawerOptions();

        CheckRejectEmit();

        polyLineList = new ArrayList<>();

        lottieSearching = findViewById(R.id.searching);
        searchlottieLayout = findViewById(R.id.loteelayout);
        //frameLayout = findViewById(R.id.frame);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.driverSearchMap);
        mapFragment.getMapAsync(this);

        snackbarForConnectivityStatus();


        init_persistent_bottomsheet();

        //checkInternetDisconnect();

        targetlocationonClick();

        if (JOBID.equals("")) {
            getJobData();
            sendJobToDriver();
        }
        if(CustomerConfig.sharedinstance.getImageIP()==null)
        {
            nearstOfficeLocation();
        }
        if(CfgCustApp.sharedinstance.getGoogleDirectionKey()==null){
            getCfgCustApp(this);
        }


        InitializaScreen();


        AfterSwipe();
//        Calendar cal = Calendar.getInstance();
//
//        TimeZone timeZone =  cal.getTimeZone();
//
//
//        Date cals =    Calendar.getInstance(TimeZone.getDefault()).getTime();
//
//        milliseconds =   cals.getTime();
//
//        milliseconds = milliseconds + timeZone.getOffset(milliseconds);
//      unixTimeStamp = milliseconds / 1000L;
    }
    //for get counter
    public static void getCounter() {
        if (SocketEvent.counter > 0) {
            int count = SocketEvent.counter;
            counterMsg.setVisibility(View.VISIBLE);
            counterMsg.setText(String.valueOf(count));
        } else {
            counterMsg.setVisibility(View.GONE);
        }

    }
    //for notification
    public static void getAct() {
        String CHANNEL_ID = "01";

        Intent intent = new Intent(mActivty, ChatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Uri uriSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        PendingIntent contentIntent = PendingIntent.getActivity(mActivty, 0, intent,
                0);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(mActivty)
                        .setSmallIcon(R.drawable.appicon)
                        .setContentTitle("New Message")
                        .setContentText(SocketEvent.msg)
                        .setContentIntent(contentIntent)
                        .setSound(uriSound)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setChannelId(CHANNEL_ID);
        builder.setContentIntent(contentIntent);
        manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            assert manager!=null;
            builder.setChannelId(CHANNEL_ID);
            manager.createNotificationChannel(notificationChannel);
        }
        assert manager!=null;
        manager.notify(0, builder.build());
        checKNotification=1;

    }






    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void searchDriver() {
        while(DriverLoc.sharedInstance.getName()==null)
        {
            Log.wtf("noinstancedata","trying");
        }
        if (Driver.sharedInstance.getDrvUID() != null) {
            //show data of driver on offer
            loadDriverDataBottomSheet();


            handler = new Handler(Looper.getMainLooper());

            final int delay = 3000; //milliseconds

            handler.postDelayed(new Runnable() {

                public void run() {

                    if (flag == 0) {


                        sendRequest();

                        driverlatlng = new LatLng(DriverLoc.sharedInstance.getLat(), DriverLoc.sharedInstance.getLong());
                        newlist.add(driverlatlng);
                        index = 0;
                        next = 0;

                        searchlottieLayout.setVisibility(View.INVISIBLE);


                        handler.removeCallbacksAndMessages(null);
                    }

                }
            }, delay);
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        //mMap.getUiSettings().setAllGesturesEnabled(true);
        getLocationOfOrigin();

    }

    public void getLocationOfOrigin() {
        try {
            //storeLatLong();
            GPStracker gps = new GPStracker(this);

            latLng = new LatLng(gps.getLatitude(), gps.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(13.0f).build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            mMap.moveCamera(cameraUpdate);
            mMap.stopAnimation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //downloading image and show it in image view with details
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void loadDriverDataBottomSheet() {
        nav_Menu.findItem(R.id.ShareRideInfo).setVisible(true);
        infoCL.setVisibility(View.GONE);
        Toast.makeText(activity, "Driver Found", Toast.LENGTH_SHORT).show();
        BottomSheetLL.setVisibility(View.VISIBLE);
        txtDriverName.setText(DriverLoc.sharedInstance.getName());
        txtDriverRegNumber.setText(DriverLoc.sharedInstance.getReg());
        txtDriverRegNumber.setText(DriverLoc.sharedInstance.getVmake() + " ( " + DriverLoc.sharedInstance.getVcolour() + " ) - " + DriverLoc.sharedInstance.getReg());
        //txtDriverCarModel.setText(DriverLoc.sharedInstance.getVmake());
        //txtDriverCarColor.setText(DriverLoc.sharedInstance.getVcolour());
        txtDriverContact.setText(DriverLoc.sharedInstance.gettelephone());
        CustTo.setText(BookingHistory.sharedinstance.getFrom());
        CustFrom.setText(BookingHistory.sharedinstance.getTo());
        CustFare.setText(String.valueOf(BookingHistory.sharedinstance.getFare()));
//		date.setText(BookingHistory.sharedinstance.getDate());
        //	time.setText(BookingHistory.sharedinstance.getTime());
        //  bookedvehicle.setText(BookingHistory.sharedinstance.getVehicletype());
        driverSMS.setVisibility(View.VISIBLE);
        driverCallIB.setVisibility(View.VISIBLE);

        if (Objects.equals(BookingHistory.sharedinstance.getVehicletype(), "S")) {
            bookedvehicle.setText("Saloon");
        } else if (Objects.equals(BookingHistory.sharedinstance.getVehicletype(), "E")) {
            bookedvehicle.setText("Estate");
        } else if (Objects.equals(BookingHistory.sharedinstance.getVehicletype(), "5")) {
            bookedvehicle.setText("MPV");
        } else if (Objects.equals(BookingHistory.sharedinstance.getVehicletype(), "X")) {
            bookedvehicle.setText("Executive");
        } else if (Objects.equals(BookingHistory.sharedinstance.getVehicletype(), "8")) {
            bookedvehicle.setText("8 Passenger");
        }

        dateTimeTV.setText(BookingHistory.sharedinstance.getDate() + " / " + BookingHistory.sharedinstance.getTime());
        //distanceTV.setText(BookingHistory.sharedinstance.());
        //durationTV.setText(BookingHistory.sharedinstance.getDate());

        //
        sendDriverStateSharedPrefence();


        if (Objects.equals(BookingHistory.sharedinstance.getComment(), "")) {
            CommentLayout.setVisibility(View.GONE);
        } else {
            CommentLayout.setVisibility(View.VISIBLE);
            comment.setText(BookingHistory.sharedinstance.getComment());
        }

        if (Objects.equals(BookingHistory.sharedinstance.getFlightno(), "")) {
            FlightLayout.setVisibility(View.GONE);
        } else {
            FlightLayout.setVisibility(View.VISIBLE);
            flightno.setText(BookingHistory.sharedinstance.getFlightno());
        }

        driverCallIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPermissionGranted()) {
                    Drivercall_action();
                }
            }
        });
        driverSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(DriverSearch.this, ChatActivity.class);
//                startActivity(intent);
//                SocketEvent.isActivityOpen = 1;


                sendSMS();
            }
        });

        officeCallIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPermissionGranted()) {
                    getOfficeNumberbyEmit(FIXED_OFFICE_KEY);
                }
            }
        });
        cancelBookingIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelBookingItemClicked();
            }
        });
        if (!drvImageIp.equals("")) {

            ip = drvImageIp;
            image_url =   ip + "/" + officeId + "/" + drvCallsign + "/DriverSnapshot.png";

        } else {
            ip = CustomerConfig.sharedinstance.getImageIP();
            image_url =  ip + "/" + Driver.sharedInstance.getOfficeID() + "/" + Driver.sharedInstance.getCallsign() + "/DriverSnapshot.png";
        }

        vehicleimg_url=  ip  + "/" + DriverLoc.sharedInstance.getOfficeId() + "/" + DriverLoc.sharedInstance.getReg() + "/VehicleSnapshot.png";

        Glide.with(getApplicationContext())
                .load(image_url)


                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                        roundedBitmapDrawable.setCircular(true);
                        circularImageView.setImageDrawable(roundedBitmapDrawable);
                    }
                });

        Glide.with(getApplicationContext())
                .load(vehicleimg_url)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                        roundedBitmapDrawable.setCircular(true);
                        vehicleImageView.setImageDrawable(roundedBitmapDrawable);
                    }
                });


        String str = "";
        int i = 0;
        List<FromToVia> ftv1 = BookingHistory.sharedinstance.getFromtovia();
        if (!(ftv1.get(2).getAddress() == null)) {
            findViewById(R.id.viasLabel).setVisibility(View.VISIBLE);
            findViewById(R.id.tvVias).setVisibility(View.VISIBLE);
            findViewById(R.id.viasIV).setVisibility(View.VISIBLE);
            for (FromToVia ftv : ftv1) {
                if (!(ftv.getAddress() == null)) {

                    if ((ftv.getAddress().toString().equals(null)) == false) {
                        i++;
                        if (i > 2) {
                            str = str + ftv.getAddress() + "\n";
                        }
                    }
                }
                ((TextView) findViewById(R.id.tvVias)).setText(str);
            }
        } else {
            findViewById(R.id.viasLabel).setVisibility(View.GONE);
            findViewById(R.id.tvVias).setVisibility(View.GONE);
            findViewById(R.id.viasIV).setVisibility(View.GONE);

        }


    }
    @Override
    protected void onResume() {
        super.onResume();


              if (!SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this)) {

        SocketEvent.sharedinstance.socketConnectAfterDisconnect();


//        Calendar cal = Calendar.getInstance();
//
//        TimeZone timeZone =  cal.getTimeZone();
//
//
//        Date cals =    Calendar.getInstance(TimeZone.getDefault()).getTime();
//
//        milliseconds =   cals.getTime();
//
//        milliseconds = milliseconds + timeZone.getOffset(milliseconds);
//        if( (milliseconds / 1000L)-unixTimeStamp>600)
//        {
        checkInternetDisconnect();
        // unixTimeStamp=milliseconds / 1000L;
        //  }

           }


        if(checKNotification!=1)
        {
            counterMsg.setVisibility(View.GONE);
            LoadMsg.msg.clear();
            SocketEvent.counter=0;
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        //counterMsg.setVisibility(View.GONE);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(checKNotification==1)
        {
            manager.cancelAll();
        }
        SecureData.arrayList.clear();
        LoadMsg.msg.clear();
        SocketEvent.counter=0;
        //Toast.makeText(getApplicationContext(),"destroy",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDirectionFinderStart() {
        try {


            if (originMarkers != null) {
                for (Marker marker : originMarkers) {
                    marker.remove();
                }
            }

            if (destinationMarkers != null) {
                for (Marker marker : destinationMarkers) {
                    marker.remove();
                }
            }

            if (polylinePaths != null) {
                for (Polyline polyline : polylinePaths) {
                    polyline.remove();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

    }

    @Override
    public void onDirectionFinderSuccess(final List<Route> router) {

        try {
            if (Network.isNetworkAvailable(getApplicationContext())) {
                mMap.clear();

                //progressDialog.dismiss();
                polylinePaths = new ArrayList<>();
                originMarkers = new ArrayList<>();
                destinationMarkers = new ArrayList<>();

                for (final Route route : router)
                {
                    for (Duration d : route.duration) {
                        totalDuration = totalDuration + d.value;
                    }

                    //converting sec to min, 1 sec = 0.0166667 min
                    totalDuration = totalDuration * 0.0166667;
                    totalDuration = round(totalDuration, 1);


                    for (Distance d : route.distance) {
                        //Note that regardless of what unit system is displayed as text, the distance.value
                        // field always contains a value expressed in meters.

                        totalDistance = totalDistance + d.value;
                    }
                    //Converting from meter to miles ,1 meter = 0.000621371 miles
                    totalDistance = totalDistance * 0.000621371;
                    totalDistance = round(totalDistance, 1);

                    RouteDistance.setText(totalDistance + " miles");
                    RouteDuration.setText(totalDuration + " mins");

//					originMarkers.add(mMap.addMarker(new MarkerOptions()
//							.icon(BitmapDescriptorFactory.fromResource(R.drawable.originmarker))
//							.title(route.startAddress)
//							.position(route.startLocation)));
                    if(checkPOBemit==false){
                        custLatLngMarker = (mMap.addMarker(new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.originmarker))
                                .title("ORIGIN")
                                .snippet(route.startAddress.get(0))
                                .position(route.startLocation.get(0))));}
                    LatLng     deslatlng= new LatLng(BookingHistory.sharedinstance.getFromtovia().get(1).getLat(),BookingHistory.sharedinstance.getFromtovia().get(1).getLon());
                    mMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.destinationmarker))
                            .title("DESTINATION")
                            .snippet(BookingHistory.sharedinstance.getFromtovia().get(1).getAddress().toString())
                            .position(deslatlng)
                            .visible(true)
                            .anchor(0.5f,0.5f));
                    for(i--;i>1;i--){
                        viaslatlng= new LatLng(BookingHistory.sharedinstance.getFromtovia().get(i).getLat(),BookingHistory.sharedinstance.getFromtovia().get(i).getLon());

                        mMap.addMarker(new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.waypoint_pointer))
                                .title(BookingHistory.sharedinstance.getFromtovia().get(i).getAddress().toString())
                                .position(viaslatlng)
                                .visible(true)
                                .anchor(0.5f,0.5f));
                    }

                    if(tiltcheck){                    drvCarMarker = mMap.addMarker(new MarkerOptions().position(driverlatlng).flat(false)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.razacar1)));
                    }
                    else{                    drvCarMarker = mMap.addMarker(new MarkerOptions().position(driverlatlng).flat(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car)));
                    }

                    Log.e("directionfinder","called");
                    final PolylineOptions polylineOptions = new PolylineOptions().
                            geodesic(true).
                            color(Color.GRAY).
                            width(20);

                    for (int i = 0; i < route.points.size(); i++)
                        polylineOptions.add(route.points.get(i));

                    polylinePaths.add(mMap.addPolyline(polylineOptions));

                    moveToBounds(mMap.addPolyline(polylineOptions));

                    polyLineList = polylineOptions.getPoints();


                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (LatLng latLng : polyLineList) {
                        builder.include(latLng);
                    }

//                    directionhandler = new Handler();
//
//
//                    directionhandler.postDelayed(new Runnable() {
//
//
//                        @Override
//                        public void run() {
//                            //sendRequest();




                    //   directionhandler.postDelayed(this, 5000);
//                        }
//                    }, 5000);

                }


            } else {
                autoDismissDialogue("Error", "No internet please try again!");
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            autoDismissDialogue("Error", "Can't make directions please select destination again!");
            return;
        }

    }


    //    setting the camera bounds to fit route in single screen
    private void moveToBounds(Polyline p) {
        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (int i = 0; i < p.getPoints().size(); i++) {
                builder.include(p.getPoints().get(i));
            }

            LatLngBounds bounds = builder.build();
            int padding = 40; // offset from edges of the map in pixels

            CameraUpdate cu = CameraUpdateFactory.newLatLng(driverlatlng);
            if(tiltcheck){
                CameraPosition cp = new CameraPosition.Builder()
                        .target(driverlatlng)
                        .tilt(90)
                        .bearing(getBearing(end,newPos))
                        .zoom(20)
                        .build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cp);
                mMap.moveCamera(cameraUpdate);
            }
            else{
                mMap.animateCamera(cu);}
            bound = bounds;

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    private void sendRequest() {
        flag = 1;
//        selectedPickup = String.valueOf(DataHolder.getInstance().originDataLatLng);
//        GPStracker gps = new GPStracker(this);
//        latLng = new LatLng(gps.getLatitude(), gps.getLongitude());
//        String orign = String.valueOf(latLng);
//        String[] splitLatLng = orign.split("[\\(||\\)]");
//        String[] originLatLng = splitLatLng[1].split(",");
//        String originLat = originLatLng[0];
//        String originLng = originLatLng[1];
//
//        String o1 = (Location.convert(Double.parseDouble(originLat), Location.FORMAT_DEGREES));
//        String o2 = (Location.convert(Double.parseDouble(originLng), Location.FORMAT_DEGREES));
//
//        String customerLatLng = o1 + "," + o2;


        ;


        lat = DriverLoc.sharedInstance.getLat();
        lng = DriverLoc.sharedInstance.getLong();

        driverLatLng = lat + "," + lng;


        try {
            new DirectionFinder(this, BookingHistory.sharedinstance.getFrom(), driverLatLng, flag).execute();

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }


    @Override
    public void onBackPressed() {
        //  super.onBackPressed();
        originMarkers.clear();
        destinationMarkers.clear();
        polylinePaths.clear();

    }

    @Override
    public void start() {

    }

    @Override
    public void finish(String time) {

        //progressDialog.dismiss();
    }


    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(end.latitude - begin.latitude);
        double lng = Math.abs(end.longitude - begin.longitude);

        if (end.latitude < begin.latitude && end.longitude < begin.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (end.latitude >= begin.latitude && end.longitude < begin.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (end.latitude >= begin.latitude && end.longitude >= begin.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (end.latitude < begin.latitude && end.longitude >= begin.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private void hideHomeNav() {
        nav_Menu.findItem(R.id.home).setVisible(false);
    }

    private void showHomeNav() {
        nav_Menu.findItem(R.id.home).setVisible(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);

                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public void navigationDrawerOptions() {

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                switch (id) {
                    case R.id.home:

                        try {
                            ids = db.getids("Booked");
                            jobid = Objects.requireNonNull(getIntent().getExtras()).getString("jobid");
                            pos = Objects.requireNonNull(getIntent().getExtras()).getInt("pos");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        boolean retvl = (ids.contains(jobid));
                        if (!retvl) {

                            db.insertBookingForHistory(DataHolder.getInstance().bookingdate,
                                    DataHolder.getInstance().bookingtime, DataHolder.getInstance().origin,
                                    DataHolder.getInstance().destination, DataHolder.getInstance().cusfare,
                                    "Booked", DataHolder.getInstance().jobID, DataHolder.getInstance().jobReference, DataHolder.getInstance().unixdateNtime);

                        }


                        startActivity(new Intent(DriverSearch.this, OriginAndDestination.class));
                        mDrawerLayout.closeDrawers();
                        DataHolder.getInstance().clearAllDataHolders();
                        break;

                    case R.id.cancelBookng:

                        cancelBookingItemClicked();
                        break;

                    case R.id.callOffc:
                        //Toast.makeText(getApplicationContext(), "Office", Toast.LENGTH_SHORT).show();
                        mDrawerLayout.closeDrawers();
                        if (isPermissionGranted()) {
                            getOfficeNumberbyEmit(FIXED_OFFICE_KEY);
                        }
                        break;
                    case R.id.ShareRideInfo:
                        //Toast.makeText(getApplicationContext(), "Office", Toast.LENGTH_SHORT).show();

                        GPStracker gps = new GPStracker(DriverSearch.this);
                        //    LatLng currentlatLng = new LatLng(gps.getLatitude(), gps.getLongitude());

                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, AppConstants.SHARE_RIDE_TEXT);
                        shareIntent.putExtra(Intent.EXTRA_TEXT, "\nHey I'm taking ride using "+ AppConstants.SHARE_RIDE_TEXT+"\n\nRIDE INFO \nFrom:"+BookingHistory.sharedinstance.getFrom()+"\nTo:"+BookingHistory.sharedinstance.getTo()+"\nDriver Name:"+DriverLoc.sharedInstance.getName()+"\nVehicle Type:"+DriverLoc.sharedInstance.getVtype()+"\nVehicle Number:"+DriverLoc.sharedInstance.getReg()+"\nVehicle model:"+DriverLoc.sharedInstance.getVmodel().split("-")[0]+"Vehicle Maker:"+DriverLoc.sharedInstance.getVmake()+"\nVehicle color:"+DriverLoc.sharedInstance.getVcolour()+"\nBooking Date And Time:"+BookingHistory.sharedinstance.getDate() + " / " + BookingHistory.sharedinstance.getTime() +"\nCurrent Location: http://maps.google.com/maps?saddr=" +gps.getLatitude()+","+gps.getLongitude());
                        startActivity(Intent.createChooser(shareIntent, "Share With Family"));

                        break;
                }

                return true;
            }
        });
        View header = mNavigationView.getHeaderView(0);


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    public void cancelBookingItemClicked() {
        ThreadSendMail("<html>\n" +
                "   <head>\n" +
                "      <style>\n" +
                "         .banner-color {\n" +
                "         background-color: #eb681f;\n" +
                "         }\n" +
                "         .title-color {\n" +
                "         color: #0066cc;\n" +
                "         }\n" +
                "         .button-color {\n" +
                "         background-color: #0066cc;\n" +
                "         }\n" +
                "         @media screen and (min-width: 500px) {\n" +
                "         .banner-color {\n" +
                "         background-color: #0066cc;\n" +
                "         }\n" +
                "         .title-color {\n" +
                "         color: #eb681f;\n" +
                "         }\n" +
                "         .button-color {\n" +
                "         background-color: #eb681f;\n" +
                "         }\n" +
                "         }\n" +
                "      </style>\n" +
                "   </head>\n" +
                "   <body>\n" +
                "      <div style=\"background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important\">\n" +
                "         <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                "            <tbody>\n" +
                "               <tr>\n" +
                "                  <td align=\"center\">\n" +
                "                     <center style=\"width:100%\">\n" +
                "                        <table bgcolor=\"#FFFFFF\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif\" width=\"512\">\n" +
                "                           <tbody>\n" +
                "                              <tr>\n" +
                "                                 <td bgcolor=\"#F3F3F3\" width=\"100%\" style=\"background-color:#f3f3f3;padding:12px;border-bottom:1px solid #ececec\">\n" +
                "                                    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;width:100%!important;font-family:Helvetica,Arial,sans-serif;min-width:100%!important\" width=\"100%\">\n" +
                "                                       <tbody>\n" +
                "                                          <tr>\n" +
                "                                             <td align=\"left\" valign=\"middle\" width=\"50%\"><span style=\"margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px\">"+SHARE_RIDE_TEXT+"</span></td>\n" +
                "                                             <td valign=\"middle\" width=\"50%\" align=\"right\" style=\"padding:0 0 0 10px\"><span style=\"margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px\">"+ Calendar.getInstance().getTime()+"</span></td>\n" +
                "                                             <td width=\"1\">&nbsp;</td>\n" +
                "                                          </tr>\n" +
                "                                       </tbody>\n" +
                "                                    </table>\n" +
                "                                 </td>\n" +
                "                              </tr>\n" +
                "                              <tr>\n" +
                "                                 <td align=\"left\">\n" +
                "                                    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                "                                       <tbody>\n" +
                "                                          <tr>\n" +
                "                                             <td width=\"100%\">\n" +
                "                                                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                "                                                   <tbody>\n" +
                "                                                      <tr>\n" +
                "                                                         <td align=\"center\" bgcolor=\""+context.getResources().getColor(R.color.application_color_accent)+"\" style=\"padding:20px 48px;color:#ffffff\" class=\"banner-color\">\n" +
                "                                                            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                "                                                               <tbody>\n" +
                "                                                                  <tr>\n" +
                "                                                                     <td align=\"center\" width=\"100%\">\n" +
                "                                                                        <h1 style=\"padding:0;margin:0;color:#ffffff;font-weight:500;font-size:20px;line-height:24px\">Station Cars "+APP_OFFICE_NAME+"</h1>\n" +
                "                                                                     </td>\n" +
                "                                                                  </tr>\n" +
                "                                                               </tbody>\n" +
                "                                                            </table>\n" +
                "                                                         </td>\n" +
                "                                                      </tr>\n" +
                "                                                      <tr>\n" +
                "                                                         <td align=\"center\" style=\"padding:20px 0 10px 0\">\n" +
                "                                                            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                "                                                               <tbody>\n" +
                "                                                                  <tr>\n" +
                "                                                                     <td align=\"center\" width=\"100%\" style=\"padding: 0 15px;text-align: justify;color: rgb(76, 76, 76);font-size: 12px;line-height: 18px;\">\n" +
                "                                                                        <h3 style=\"font-weight: 600; padding: 0px; margin: 0px; font-size: 16px; line-height: 24px; text-align: center;\" class=\"title-color\">Hey "+getSharedPrefName()+",</h3>\n" +
                "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\">You cancelled the ride.<b> Ride Info:</b></p>\n" +
                "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> From: </b>"+BookingHistory.sharedinstance.getFrom()+"</p>\n" +
                "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> To: </b>"+BookingHistory.sharedinstance.getTo()+"</p>\n" +
                "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> Booking Number: </b>"+BookingHistory.sharedinstance.getJobref()+"</p>\n" +
                "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> Driver Name: </b>"+DriverLoc.sharedInstance.getName()+"</p>\n" +
                "                                                                        <p style=\"margin: 20px 0 30px 0;font-size: 15px;text-align: center;\"><b> Fare: </b>"+BookingHistory.sharedinstance.getFare()+"</p>\n" +

                "                                                                        <div style=\"font-weight: 200; text-align: center; margin: 25px;\"><a style=\"padding:0.6em 1em;border-radius:600px;color:#ffffff;font-size:14px;text-decoration:none;font-weight:bold\" class=\"button-color\">Thank You for using "+SHARE_RIDE_TEXT+"</a></div>\n" +
                "                                                                     </td>\n" +
                "                                                                  </tr>\n" +
                "                                                               </tbody>\n" +
                "                                                            </table>\n" +
                "                                                         </td>\n" +
                "                                                      </tr>\n" +
                "                                                      <tr>\n" +
                "                                                      </tr>\n" +
                "                                                      <tr>\n" +
                "                                                      </tr>\n" +
                "                                                   </tbody>\n" +
                "                                                </table>\n" +
                "                                             </td>\n" +
                "                                          </tr>\n" +
                "                                       </tbody>\n" +
                "                                    </table>\n" +
                "                                 </td>\n" +
                "                              </tr>\n" +
                "                              <tr>\n" +
                "                                 <td align=\"left\">\n" +
                "                                    <table bgcolor=\"#FFFFFF\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                "                                       <tbody>\n" +
                "                                          <tr>\n" +
                "                                             <td align=\"center\" width=\"100%\">\n" +
                "                                                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                "                                                   <tbody>\n" +
                "                                                      <tr>\n" +
                "                                                         <td align=\"center\" valign=\"middle\" width=\"100%\" style=\"border-top:1px solid #d9d9d9;padding:12px 0px 20px 0px;text-align:center;color:#4c4c4c;font-weight:200;font-size:12px;line-height:18px\">Regards,\n" +
                "                                                            <br><b>"+SHARE_RIDE_TEXT+" Customer App Team</b>\n" +
                "                                                         </td>\n" +
                "                                                      </tr>\n" +
                "                                                   </tbody>\n" +
                "                                                </table>\n" +
                "                                             </td>\n" +
                "                                          </tr>\n" +
                "                                          <tr>\n" +
                "                                             <td align=\"center\" width=\"100%\">\n" +
                "                                                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-weight:200;font-family:Helvetica,Arial,sans-serif\" width=\"100%\">\n" +
                "                                                   <tbody>\n" +
                "                                                      <tr>\n" +
                "                                                         <td align=\"center\" style=\"padding:0 0 8px 0\" width=\"100%\"></td>\n" +
                "                                                      </tr>\n" +
                "                                                   </tbody>\n" +
                "                                                </table>\n" +
                "                                             </td>\n" +
                "                                          </tr>\n" +
                "                                       </tbody>\n" +
                "                                    </table>\n" +
                "                                 </td>\n" +
                "                              </tr>\n" +
                "                           </tbody>\n" +
                "                        </table>\n" +
                "                     </center>\n" +
                "                  </td>\n" +
                "               </tr>\n" +
                "            </tbody>\n" +
                "         </table>\n" +
                "      </div>\n" +
                "   </body>\n" +
                "</html>");
        if (Network.isNetworkAvailable(getApplicationContext()) && BookingHistory.sharedinstance.getTo() != null) {
            cancelBooking();

            checkDRVreject = false;
            if (rejectEmitHandler != null) {
                rejectEmitHandler.removeCallbacksAndMessages(null);
            }

            if (driverlatlng != null) {
                if (directionhandler != null) {
                    directionhandler.removeCallbacksAndMessages(null);
                }
            }
            SharedPreferences setPrefrence = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
            editor = setPrefrence.edit();
            editor.putString("jobid", "");
            editor.putString("drvcallsign", "");
            editor.putString("officeId", "");
            editor.putString("drvImageIp", "");
            editor.putString("jobRef", "");
            editor.apply();

            setPrefrence = getSharedPreferences(Rating.prefratingname, Context.MODE_PRIVATE);
            editor = setPrefrence.edit();
            editor.putString(Rating.prefdriverid, "");

            editor.putString(Rating.img_ip, "");
            editor.apply();

            officeId = "";
            drvCallsign = "";
            JOBID = "";

            Driver.sharedInstance = new Driver();

            try {
                ids = db.getids("Booked");
                jobid = Objects.requireNonNull(getIntent().getExtras()).getString("jobid");
                pos = Objects.requireNonNull(getIntent().getExtras()).getInt("pos");
            } catch (Exception e) {
                e.printStackTrace();
            }

            boolean retval = (ids.contains(jobid));


            if (retval) {
                if (jobid != null && pos >= 0) {
                    //removeData(pos);
                }


                db.insertBookingForHistory(BookingHistory.sharedinstance.getDate(),
                        BookingHistory.sharedinstance.getTime(), BookingHistory.sharedinstance.getTo(),
                        BookingHistory.sharedinstance.getFrom(), BookingHistory.sharedinstance.getFare(),
                        "Cancelled", BookingHistory.sharedinstance.getId(), BookingHistory.sharedinstance.getJobref(), BookingHistory.sharedinstance.getDatentime());
            } else {

                db.insertBookingForHistory(BookingHistory.sharedinstance.getDate(),
                        BookingHistory.sharedinstance.getTime(), BookingHistory.sharedinstance.getTo(),
                        BookingHistory.sharedinstance.getFrom(),
                        BookingHistory.sharedinstance.getFare(),
                        "Cancelled", BookingHistory.sharedinstance.getId(),
                        BookingHistory.sharedinstance.getJobref(),
                        BookingHistory.sharedinstance.getDatentime());
            }

            autoDismissDialogue("Cancelled", "You cancelled your booking");
            startActivity(new Intent(DriverSearch.this, OriginAndDestination.class));
            DataHolder.getInstance().clearAllDataHolders();
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);

            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Wait... Job in Process", Toast.LENGTH_SHORT).show();
            //autoDismissDialogue("Error", "No Internet");

        }
        rejectEmitHandler.removeCallbacksAndMessages(null);
        BookingHistory.sharedinstance = new BookingHistory();
        Driver.sharedInstance = new Driver();
    }


    public void init_persistent_bottomsheet() {
        View bottomsheetdriver = coordinatorLayout.findViewById(R.id.bottom);
        iv_trigger = bottomsheetdriver.findViewById(R.id.toplayout);
        txtDriverName = (TextView) bottomsheetdriver.findViewById(R.id.drvName);
        car_speed= (TextView) bottomsheetdriver.findViewById(R.id.car_speed);
        RouteDuration= (TextView) bottomsheetdriver.findViewById(R.id.RouteDuration);
        RouteDistance= (TextView) bottomsheetdriver.findViewById(R.id.RouteDistance);
        txtDriverRegNumber = (TextView) bottomsheetdriver.findViewById(R.id.drvRegNumber);
//		txtDriverCarModel = (TextView)bottomsheetdriver.findViewById(R.id.drvCarModel);
//		txtDriverCarColor = (TextView)bottomsheetdriver.findViewById(R.id.drvVColor);
        txtDriverContact = (TextView) bottomsheetdriver.findViewById(R.id.drvcontactnum);
        driverSMS = (ImageButton) bottomsheetdriver.findViewById(R.id.driverMessage);
        driverCallIB = (ImageButton) bottomsheetdriver.findViewById(R.id.driverCall);
        counterMsg = bottomsheetdriver.findViewById(R.id.counter_msg);
        CustTo = (TextView) bottomsheetdriver.findViewById(R.id.to);
        CustFrom = (TextView) bottomsheetdriver.findViewById(R.id.from);
        CustFare = (TextView) bottomsheetdriver.findViewById(R.id.fare);
        comment = (TextView) bottomsheetdriver.findViewById(R.id.comment);
        bookedvehicle = (TextView) bottomsheetdriver.findViewById(R.id.selectedcar);
        dateTimeTV = (TextView) bottomsheetdriver.findViewById(R.id.dateTimeTV);
        flightno = (TextView) bottomsheetdriver.findViewById(R.id.flightno);
        distanceTV = findViewById(R.id.distanceID);
        //	durationTV = findViewById(R.id.durationID);
        //	date =(TextView) bottomsheetdriver.findViewById(R.id.date);
        //	time = (TextView)bottomsheetdriver.findViewById(R.id.time);
        CommentLayout = bottomsheetdriver.findViewById(R.id.commentLayout);
        FlightLayout = bottomsheetdriver.findViewById(R.id.flightLayout);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomsheetdriver);
        officeCallIB = findViewById(R.id.callOficeIB);
        cancelBookingIB = findViewById(R.id.cancelBookingIB);
        infoCL = findViewById(R.id.infoCL);
        BottomSheetLL = findViewById(R.id.BottomSheetLL);


        iv_trigger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        if (behavior != null)
            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    //showing the different states
                    switch (newState) {
                        case BottomSheetBehavior.STATE_HIDDEN:
                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:
                            break;
                        case BottomSheetBehavior.STATE_COLLAPSED:
                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:
                            break;
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    // React to dragging events

                }
            });


    }




    public void Drivercall_action() {

        try {
            Uri number = Uri.parse("tel:" + DriverLoc.sharedInstance.gettelephone());
            Intent callIntent = new Intent(Intent.ACTION_CALL, number);
            if (android.support.v4.app.ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(callIntent);
        } catch (android.content.ActivityNotFoundException ex) {
        }

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    getOfficeNumberbyEmit(FIXED_OFFICE_KEY);
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }


    }


    public void checkInternetDisconnect() {

        if (!preferences.getString("jobid", "").equals("")) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void run() {


                    if (SocketEvent.sharedinstance.socket == null && Network.isNetworkAvailable(DriverSearch.this)) {
                        SocketEvent.sharedinstance.socketConnectAfterDisconnect();
                    }
//					SocketEvent.sharedinstance.socket.emit("addCust", getCustomerRoom(), new Ack() {
//
//						@Override
//						public void call(Object... args) {
//						}
//					});
//					SocketEvent.sharedinstance.getDriverLocByEmit();
//					getJobData();
                    getJobDataforroom();

                    if (BookingHistory.sharedinstance.getJstate() != null && Internetcounter == 3) {
                        if (BookingHistory.sharedinstance.getJstate().equals("allocated") || BookingHistory.sharedinstance.getJstate().equals("Accepted")) {
                            sendRequest();
                            loadDriverDataBottomSheet();
                            if (searchlottieLayout != null) {
                                searchlottieLayout.setVisibility(View.GONE);
                            }
                            handler.removeCallbacksAndMessages(null);
                            Internetcounter = 0;
                            return;


                        } else if (BookingHistory.sharedinstance.getDstate().equals("POB")) {
                            POBDriverEmitReRoute();
                            loadDriverDataBottomSheet();
                            if (searchlottieLayout != null) {
                                searchlottieLayout.setVisibility(View.GONE);
                            }
                            handler.removeCallbacksAndMessages(null);
                            Internetcounter = 0;
                            return;
                        } else if (BookingHistory.sharedinstance.getJstate().equals("JobDone")) {
                            charge();
                            Intent intent = new Intent(DriverSearch.this, RatingScreen.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            SharedPreferences setPrefrence = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
                            editor = setPrefrence.edit();
                            editor.putString("jobid", "");
                            editor.putString("drvcallsign", "");
                            editor.putString("officeId", "");
                            editor.putString("drvImageIp", "");
                            editor.putString("jobRef", "");
                            editor.apply();
                            officeId = "";
                            drvCallsign = "";
                            JOBID = "";
                            handler.removeCallbacksAndMessages(null);
                            Internetcounter = 0;
                            return;
                        } else if (BookingHistory.sharedinstance.getDstate() != null && BookingHistory.sharedinstance.getDstate().equals("Rejected")) {
                            RejectNoDriverFound();
                            handler.removeCallbacksAndMessages(null);
                            return;
                        }
                    }

                    handler.postDelayed(this, 2000);
                    Internetcounter++;
                }
            }, 2000);

//				if (driversearchInternetstatus) {
//
//					if (SocketEvent.sharedinstance.socket == null) {
//						SocketEvent.sharedinstance.socketConnectAfterDisconnect();
//					}
//
//					SocketEvent.sharedinstance.socket.emit("addCust", getCustomerRoom(), new Ack() {
//
//						@Override
//						public void call(Object... args) {
//						}
//					});
//					if(BookingHistory.sharedinstance==null) {
//						getJobData();
//					}else if(BookingHistory.sharedinstance.getJstate().equals("Accepted")){
//						//getJobData();
//						sendRequest();
//						loadDriverImage();
//
//					}else if (BookingHistory.sharedinstance.getDstate().equals("POB")){
//						POBDriverEmitReRoute();
//
//					}
////					sendRequest();
////					loadDriverImage();
//
//				}
//
//
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void getJobData() {
        try {

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this)) {
                JSONArray query = new JSONArray();
                query.put("Booking");

                jobID = DataHolder.getInstance().jobID;

                JSONObject filter = new JSONObject();


                if (!jobidSahredPrefence.equals("")) {
                    filter.put("_id", jobidSahredPrefence);
                    //Testing

                    driverlatlng = new LatLng(DriverLoc.sharedInstance.getLat(), DriverLoc.sharedInstance.getLong());
                    newlist.add(driverlatlng);
                    index = 0;
                    next = 0;

                } else {
                    filter.put("_id", jobID);
                }
                query.put(filter);

                SocketEvent.sharedinstance.socket.emit("getdata", query,
                        new Ack() {
                            @Override
                            public void call(Object... args) {
                                if (args[0] != null && !args[0].toString().equals("0")) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(args[0].toString());
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(jsonObject);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        BookingHistory.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), BookingHistory.class);
                                       DataHolder.getInstance().jobReference=BookingHistory.sharedinstance.getJobref();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
            } else
                autoDismissDialogue("Wait", "Please Wait");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void getJobDataforroom() {
        try {

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this)) {
                JSONArray query = new JSONArray();
                query.put("Booking");

                jobID = DataHolder.getInstance().jobID;

                JSONObject filter = new JSONObject();


                if (!jobidSahredPrefence.equals("")) {
                    filter.put("_id", jobidSahredPrefence);
                    //Testing

                    driverlatlng = new LatLng(DriverLoc.sharedInstance.getLat(), DriverLoc.sharedInstance.getLong());
                    newlist.add(driverlatlng);
                    index = 0;
                    next = 0;

                } else {
                    filter.put("_id", jobID);
                }
                query.put(filter);

                SocketEvent.sharedinstance.socket.emit("getdata", query,
                        new Ack() {
                            @Override
                            public void call(Object... args) {
                                if (args[0] != null && !args[0].toString().equals("0")) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(args[0].toString());
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(jsonObject);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        BookingHistory.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), BookingHistory.class);
                                            DataHolder.getInstance().jobReference=BookingHistory.sharedinstance.getJobref();
                                        SocketEvent.sharedinstance.socket.emit("addCust", getCustomerRoom(), new Ack() {
                                            @Override
                                            public void call(Object... args) {
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
            } else
                autoDismissDialogue("Wait", "Please Wait");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void cancelBooking() {
        try {

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this)) {
                JSONArray table = new JSONArray();
                table.put("Booking");

                JSONArray mainLogC = new JSONArray();
                JSONArray logcData = new JSONArray();

                jobID = DataHolder.getInstance().jobID;

                JSONObject filter = new JSONObject();


                if (!jobidSahredPrefence.equals("")) {
                    filter.put("_id", jobidSahredPrefence);
                } else {
                    filter.put("_id", jobID);
                }


                table.put(filter);

                // double fare = Math.round(DataHolder.getInstance().cusfare);
                double fare = Math.round(BookingHistory.sharedinstance.getFare());

                //  Object[] logc = DataHolder.getInstance().objectLogC;
                Object[] logc = BookingHistory.sharedinstance.getLogc();


//                if (logc.length == 0) {
                //                   mainLogC = DataHolder.getInstance().jsonArrayLogC;
//
//
//
//                    logcData.put(Network.sharedInstance.getBST());
//                    logcData.put("Customer cancelled job");
//                    logcData.put(getSharedPrefEmail());
//                    logcData.put(getSharedPrefName());
//
//                    mainLogC.put(logcData);
//
//                } else
                //   {

                for (int i = 0; i < logc.length; i++) {

                    Object[] obj = (Object[]) logc[i];

                    JSONArray previousLogC = new JSONArray();

                    for (int j = 0; j < obj.length; j++) {

                        Object var = obj[j];

                        previousLogC.put(var);
                    }
                    mainLogC.put(previousLogC);
                }
                logcData = new JSONArray();

                logcData.put(Network.sharedInstance.getBST());
                logcData.put("Customer cancelled job");
                logcData.put(getSharedPrefEmail());
                logcData.put(getSharedPrefName());

                mainLogC.put(logcData);
                // }

                JSONObject parameters = new JSONObject();
                parameters.put("jstate", "allocated");
                parameters.put("cstate", "cancelled");
                parameters.put("dstate", "");
                parameters.put("flag", 1);
                parameters.put("fare", 0);
                parameters.put("drvfare", 0);
                parameters.put("drvfare", 0);
                parameters.put("oldfare", fare);
                parameters.put("olddrvfare", fare);
                parameters.put("logc", mainLogC);

                table.put(parameters);

                SocketEvent.sharedinstance.socket.emit("updatedata", table, new Ack() {
                    @Override
                    public void call(Object... args) {
                    }
                });

                String eventName = "CustCancelJob";

                String driverCallSign = DriverLoc.sharedInstance.getCallsign() + "@" + DriverLoc.sharedInstance.getOfficeId();

                JSONArray arr = new JSONArray();
                arr.put(getCustomerRoom());
                arr.put(driverCallSign);
                arr.put(eventName);
                // arr.put(jobID);
                arr.put(BookingHistory.sharedinstance.getId());
                arr.put("");

                String intraDC = "intraDC";

                SocketEvent.sharedinstance.socket.emit(intraDC, arr);


            } else
                autoDismissDialogue("Error", "No Internet");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void sendJobToDriver() {
        infoCL.setVisibility(View.VISIBLE);
        BottomSheetLL.setVisibility(View.GONE);
        try {
            SocketEvent.sharedinstance.socket.emit("addCust", getCustomerRoom(), new Ack() {
                @Override
                public void call(Object... args) {
                }
            });

            JSONArray sendJobArray = new JSONArray();
            sendJobArray.put(DataHolder.getInstance().vehType);
            sendJobArray.put(DataHolder.getInstance().originLng);
            sendJobArray.put(DataHolder.getInstance().originLat);
            sendJobArray.put(DataHolder.getInstance().jobID);
            sendJobArray.put(getCustomerRoom());
            sendJobArray.put(DataHolder.getInstance().origin);
            sendJobArray.put(DataHolder.getInstance().destination);
            sendJobArray.put(DataHolder.getInstance()._fromOutcode);
            sendJobArray.put(CfgCustApp.sharedinstance.getJobInOffice());
            String emit = "CsendjobD_surbiton";
            //      String emit = "CustReq2Desp";
            SocketEvent.sharedinstance.socket.emit(emit, sendJobArray);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sendJobToallDriver() {
        infoCL.setVisibility(View.VISIBLE);
        BottomSheetLL.setVisibility(View.GONE);
        try {
            SocketEvent.sharedinstance.socket.emit("addCust", getCustomerRoom(), new Ack() {
                @Override
                public void call(Object... args) {
                }
            });

            JSONArray sendJobArray = new JSONArray();
            sendJobArray.put(DataHolder.getInstance().vehType);
            sendJobArray.put(DataHolder.getInstance().originLng);
            sendJobArray.put(DataHolder.getInstance().originLat);
            sendJobArray.put(DataHolder.getInstance().jobID);
            sendJobArray.put(getCustomerRoom());
            sendJobArray.put(DataHolder.getInstance().origin);
            sendJobArray.put(DataHolder.getInstance().destination);
            sendJobArray.put(DataHolder.getInstance()._fromOutcode);
            // sendJobArray.put(CfgCustApp.sharedinstance.getJobInOffice());
            // String emit = "CsendjobD_surbiton";
            String emit = "CustReq2Desp";
            SocketEvent.sharedinstance.socket.emit(emit, sendJobArray);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getCustomerRoom() {
        customerRoom = "";
        String jobRefPhone = DataHolder.getInstance().jobReference;

        if (jobRefPhone.equals("")) {

jobRef=BookingHistory.sharedinstance.getJobref();
        } else {
            jobRef = jobRefPhone;
        }


        //	jobRef = jobRefPhone;
        if (jobRef.contains("@")) {
            String[] ind = jobRef.split("@");
            String[] index = ind[0].split("-");
            String room = index[1];
            customerRoom = room;
        }
        return customerRoom;
    }

    private void sendSMS() {
        try {

            Intent intent = new Intent(Intent.ACTION_SENDTO);
            String num = DriverLoc.sharedInstance.gettelephone();
            intent.setData(Uri.parse("sms:" + num));
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException anfe) {
            Log.d("Error", "Error");
        }
    }


    public void sendDriverStateSharedPrefence() {
        SharedPreferences setPrefrence = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
        editor = setPrefrence.edit();
        if(JOBID!="" && JOBID!=null) {
            jobID = JOBID;
        }
        if (!jobID.equals("") && !Driver.sharedInstance.getCallsign().equals("") && !Driver.sharedInstance.getOfficeID().equals("") && !CustomerConfig.sharedinstance.getImageIP().equals("") && !DataHolder.getInstance().jobReference.equals("")) {
            editor.putString("jobid", jobID);
            editor.putString("drvcallsign", Driver.sharedInstance.getCallsign());
            editor.putString("officeId", Driver.sharedInstance.getOfficeID());
            editor.putString("drvImageIp", CustomerConfig.sharedinstance.getImageIP());
            editor.putString("jobRef", DataHolder.getInstance().jobReference);
            editor.apply();


        }
        setPrefrence = getSharedPreferences(Rating.prefratingname, Context.MODE_PRIVATE);
        if(setPrefrence.getString(Rating.img_ip,"").equals("") ||setPrefrence.getString(Rating.prefdriverid,"").equals("") ){
            editor = setPrefrence.edit();
            editor.putString(Rating.prefdriverid, Driver.sharedInstance.getDrvUID());

            editor.putString(Rating.img_ip, CustomerConfig.sharedinstance.getImageIP());
            editor.apply();}
    }

    public void targetlocationonClick() {

        myLocationButton = (ImageButton) findViewById(R.id.targetlocation);

        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bound != null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bound, 100));
                }

            }
        });

        tiltmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drvCarMarker != null) {

                    drvCarMarker.remove();

                }
                if(!tiltcheck) {
                    CameraPosition cp = new CameraPosition.Builder()
                            .bearing(mMap.getCameraPosition().bearing)
                            .tilt(90)
                            .target(driverlatlng)
                            .zoom(20)
                            .build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cp);
                    mMap.animateCamera(cameraUpdate);



                    drvCarMarker = mMap.addMarker(new MarkerOptions().position(driverlatlng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.razacar1)));




                    tiltcheck=true;
                }
                else{
                    CameraPosition cp = new CameraPosition.Builder().target(new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude))
                            .zoom(16)
                            .bearing(mMap.getCameraPosition().bearing)
                            .build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cp);
                    mMap.animateCamera(cameraUpdate);
                    tiltcheck=false;
                    drvCarMarker = mMap.addMarker(new MarkerOptions().position(driverlatlng).flat(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car)));

                }
            }});
    }


    public void getsharedprefenceBookingData() {
        preferences = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
        JOBID = preferences.getString("jobid", "");
        drvImageIp = preferences.getString("drvImageIp", "");
        jobRef = preferences.getString("jobRef", "");
        jobidSahredPrefence = preferences.getString("jobid", "");
        getDriverIp = drvImageIp;
        Driver driver = Driver.sharedInstance;
        driver.setcallsign(drvCallsign);
        driver.setOfficeID(officeId);
    }

    private void POBDriverEmitReRoute() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                checkPOBemit=true;
                cancelBookingIB.setVisibility(View.GONE);
                findViewById(R.id.textView15).setVisibility(View.GONE);
                nav_Menu.findItem(R.id.cancelBookng).setVisible(false);

            }
        });

        flag = 1;
//        selectedPickup = String.valueOf(DataHolder.getInstance().originDataLatLng);
//        GPStracker gps = new GPStracker(this);
//
//        latLng = new LatLng(gps.getLatitude(), gps.getLongitude());
//        String orign = String.valueOf(latLng);
//        String[] splitLatLng = orign.split("[\\(||\\)]");
//        String[] originLatLng = splitLatLng[1].split(",");
//        String originLat = originLatLng[0];
//        String originLng = originLatLng[1];
//
//        String o1 = (Location.convert(Double.parseDouble(originLat), Location.FORMAT_DEGREES));
//        String o2 = (Location.convert(Double.parseDouble(originLng), Location.FORMAT_DEGREES));
//
//        String customerLatLng = o1 + "," + o2;
//
//
        try {
            lat = DriverLoc.sharedInstance.getLat();
            lng = DriverLoc.sharedInstance.getLong();

            driverLatLng = lat + "," + lng;
            try{
                for(i=2;BookingHistory.sharedinstance.getFromtovia().get(i).getAddress()!=null && i<9;i++){}
            }
            catch(Exception e){}
            new DirectionFinder(this, driverLatLng,BookingHistory.sharedinstance.getFromtovia().subList(2,i), BookingHistory.sharedinstance.getTo(), true).execute();

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }


    public void InitializaScreen() {

        if (JOBID.equals("")) {
            final Handler hndler = new Handler();
            hndler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    counter++;
                    if (Driver.sharedInstance.getDrvUID() != null) {
                        searchDriver();
                        hndler.removeCallbacksAndMessages(null);
                        return;
                    } else {
                        if(counter == secondemit)
                        {
                            sendJobToallDriver();
                        }
                        else if (counter == 60) {
                            NoDriverFound();
                            showHomeNav();
                            hndler.removeCallbacksAndMessages(null);
                            return;
                        }
                    }

                    hndler.postDelayed(this, 1000);
                }
            }, 1000);
        }

    }


    public void NoDriverFound() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DriverSearch.this, R.style.Theme_AppCompat_Light_Dialog);
        alertDialogBuilder.setTitle("Info");
        alertDialogBuilder.setIcon(R.drawable.ic_error_black_24dp);
        alertDialogBuilder.setMessage("No Driver available at the moment...");
        alertDialogBuilder.setPositiveButton("Call Us",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        if (isPermissionGranted()) {
                            getOfficeNumberbyEmit(FIXED_OFFICE_KEY);
                        }
                    }
                });

        alertDialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);
    }

    public void AfterSwipe() {

        if (!JOBID.equals("") && Network.isNetworkAvailable(DriverSearch.this)) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void run() {

                    handler.postDelayed(this, 2000);
                    if (SocketEvent.sharedinstance.socket == null && Network.isNetworkAvailable(DriverSearch.this)) {
                        SocketEvent.sharedinstance.socketConnectAfterDisconnect();
                    }


//                    if (emitcount == 1 && SocketEvent.sharedinstance.socket != null) {
//                        SocketEvent.sharedinstance.socket.emit("addCust", getCustomerRoom(), new Ack() {
//
//                            @Override
//                            public void call(Object... args) {
//                                if (args[0] != null && !args[0].toString().equals("0")) {
//
//}
//                            }
//                        });
//
//                    }
//                    emitcount++;
                    SocketEvent.sharedinstance.getDriverLocByEmit();
                    getJobDataforroom();


                    if (BookingHistory.sharedinstance.getTo() != null) {
                        if (BookingHistory.sharedinstance.getDstate() != null && BookingHistory.sharedinstance.getDstate().equals("Rejected")) {
                            RejectNoDriverFound();
                            handler.removeCallbacksAndMessages(null);
                            return;
                        } else if (BookingHistory.sharedinstance.getDstate() != null && BookingHistory.sharedinstance.getDstate().equals("POB")) {
                            POBDriverEmitReRoute();
                            if (searchlottieLayout != null) {
                                searchlottieLayout.setVisibility(View.GONE);
                            }
                            loadDriverDataBottomSheet();
                            handler.removeCallbacksAndMessages(null);

                        } else if (BookingHistory.sharedinstance.getJstate().equals("allocated") || BookingHistory.sharedinstance.getJstate().equals("Accepted")) {
                            sendRequest();
                            if (searchlottieLayout != null) {
                                searchlottieLayout.setVisibility(View.GONE);
                            }
                            loadDriverDataBottomSheet();
                            handler.removeCallbacksAndMessages(null);

                        } else if (BookingHistory.sharedinstance.getJstate().equals("JobDone")) {
                            charge();
                            Intent intent = new Intent(DriverSearch.this, RatingScreen.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            SharedPreferences setPrefrence = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
                            editor = setPrefrence.edit();
                            editor.putString("jobid", "");
                            editor.putString("drvcallsign", "");
                            editor.putString("officeId", "");
                            editor.putString("drvImageIp", "");
                            editor.putString("jobRef", "");
                            editor.apply();
                            officeId = "";
                            drvCallsign = "";
                            JOBID = "";
                            handler.removeCallbacksAndMessages(null);
                        } else {
                            if (searchlottieLayout != null) {
                                searchlottieLayout.setVisibility(View.GONE);
                            }                            //loadDriverDataBottomSheet();
                            handler.removeCallbacksAndMessages(null);
                        }
                    }

                }
            }, 2000);

        }
    }

    @Override
    public void recieveMsgOnNetwork(String message) {
        if (message.equals("Connected") && status == 1) {
            checkInternetDisconnect();
            snackbarOnInternet();
        } else if (message.equals("Disonnected")) {
            status = 1;
            snackbarOnNoInternet();

        }
    }

    private void snackbarOnInternet() {
        Snackbar snackbar = Snackbar
                .make(parentLayout, "Connected!", Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.GREEN);
        snackbar.show();
    }

    private void snackbarOnNoInternet() {
        Snackbar snackbar = Snackbar
                .make(parentLayout, "Check your internet connection!", Snackbar.LENGTH_INDEFINITE);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.RED);
        snackbar.show();
    }

    public void AfterDriverLockout() {
        sendJobToDriver();
        // getJobData();
        final Handler hndler = new Handler();
        hndler.postDelayed(new Runnable() {
            @Override
            public void run() {
                counter++;
                if (Driver.sharedInstance.getDrvUID() != null) {
                    searchDriver();
                    hndler.removeCallbacksAndMessages(null);
                    return;
                } else {
                    if(counter == secondemit)
                    {
                        sendJobToallDriver();
                    }
                    else if (counter == 60) {
                        NoDriverFound();
                        showHomeNav();
                        hndler.removeCallbacksAndMessages(null);
                        return;
                    }
                }

                hndler.postDelayed(this, 1000);
            }
        }, 1000);


    }


    public void RejectNoDriverFound() {

        infoCL.setVisibility(View.VISIBLE);
        Toast.makeText(activity, "No Driver Found", Toast.LENGTH_SHORT).show();
        BottomSheetLL.setVisibility(View.GONE);

        Glide.with(getApplicationContext())
                .load(R.drawable.profile)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                        roundedBitmapDrawable.setCircular(true);
                        circularImageView.setImageDrawable(roundedBitmapDrawable);
                    }
                });
        Glide.with(getApplicationContext())
                .load(R.drawable.ic_car)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                        roundedBitmapDrawable.setCircular(true);
                        vehicleImageView.setImageDrawable(roundedBitmapDrawable);
                    }
                });
        txtDriverName.setText("Loading");
        txtDriverRegNumber.setText("Loading");
        //txtDriverCarModel.setText("Loading");
        //txtDriverCarColor.setText("Loading");
        txtDriverContact.setText("Loading");
        CustTo.setText("Loading");
        CustFrom.setText("Loading");
        CustFare.setText(String.valueOf("Loading"));
        //date.setText("Loading");
        //time.setText("Loading");
        //  bookedvehicle.setText(BookingHistory.sharedinstance.getVehicletype());
        driverSMS.setVisibility(View.GONE);
        driverCallIB.setVisibility(View.GONE);

        bookedvehicle.setText("Loading");
        CommentLayout.setVisibility(View.GONE);
        FlightLayout.setVisibility(View.GONE);

        mMap.clear();

//		originMarkers.clear();
//		drvCarMarker.remove();
//		custLatLngMarker.remove();
//		destinationMarkers.clear();
        getLocationOfOrigin();
        searchlottieLayout.setVisibility(View.VISIBLE);

        if (driverlatlng != null) {
            if (directionhandler != null) {
                directionhandler.removeCallbacksAndMessages(null);
            }
        }
        SharedPreferences setPrefrence = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
        editor = setPrefrence.edit();
        editor.putString("jobid", "");
        editor.putString("drvcallsign", "");
        editor.putString("officeId", "");
        editor.putString("drvImageIp", "");
        editor.putString("jobRef", "");
        editor.apply();

        setPrefrence = getSharedPreferences(Rating.prefratingname, Context.MODE_PRIVATE);
        editor = setPrefrence.edit();
        editor.putString(Rating.prefdriverid, "");

        editor.putString(Rating.img_ip, "");
        editor.apply();

        officeId = "";
        drvCallsign = "";
        JOBID = "";
        jobidSahredPrefence = "";
        Driver.sharedInstance = new Driver();
        DriverLoc.sharedInstance = new DriverLoc();
        flag = 0;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.Theme_AppCompat_Light_Dialog);
        alertDialogBuilder.setTitle("Info");
        alertDialogBuilder.setIcon(R.drawable.ic_error_black_24dp);
        alertDialogBuilder.setMessage("Driver Temporarily Busy");
        alertDialogBuilder.setPositiveButton("Call Us",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        if (isPermissionGranted()) {
                            getOfficeNumberbyEmit(FIXED_OFFICE_KEY);
                        }

                    }
                });

        alertDialogBuilder.setNegativeButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                AfterDriverLockout();

                dialog.dismiss();


            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);

    }


    public void ClearBottomSheet() {
        View bottomSheetView = View.inflate(activity, R.layout.bottomsheetdriver, null);

//		TextView txtDriverName = bottomSheetView.findViewById(R.id.drvName);
//		TextView txtDriverRegNumber =bottomSheetView.findViewById(R.id.drvRegNumber);
//		TextView txtDriverCarModel = bottomSheetView.findViewById(R.id.drvCarModel);
//		TextView txtDriverCarColor = bottomSheetView.findViewById(R.id.drvVColor);
//		TextView txtDriverContact = bottomSheetView.findViewById(R.id.drvcontactnum);
//		ImageButton driverSMS =bottomSheetView.findViewById(R.id.driverMessage);
//		ImageButton driverCallIB = bottomSheetView.findViewById(R.id.driverCallIB);
//		TextView CustTo = bottomSheetView.findViewById(R.id.to);
//		TextView CustFrom = bottomSheetView.findViewById(R.id.from);
//		TextView CustFare = bottomSheetView.findViewById(R.id.fare);
//		TextView comment = bottomSheetView.findViewById(R.id.comment);
//		TextView bookedvehicle = bottomSheetView.findViewById(R.id.selectedcar);
//		TextView flightno =bottomSheetView.findViewById(R.id.flightno);
//		TextView date = bottomSheetView.findViewById(R.id.date);
//		TextView time = bottomSheetView.findViewById(R.id.time);
//		LinearLayout CommentLayout = bottomSheetView.findViewById(R.id.commentLayout);
//		LinearLayout FlightLayout = bottomSheetView.findViewById(R.id.flightLayout);


//		txtDriverName.setText("Loading");
//		txtDriverRegNumber.setText("Loading");
//		txtDriverCarModel.setText("Loading");
//		txtDriverCarColor.setText("Loading");
//		txtDriverContact.setText("Loading");
//		CustTo.setText("Loading");
//		CustFrom.setText("Loading");
//		CustFare.setText(String.valueOf("Loading"));
//		date.setText("Loading");
//		time.setText("Loading");
//		//  bookedvehicle.setText(BookingHistory.sharedinstance.getVehicletype());
//		driverSMS.setVisibility(View.GONE);
//		driverCallIB.setVisibility(View.GONE);
//
//			bookedvehicle.setText("Loading");
//			CommentLayout.setVisibility(View.GONE);
//			FlightLayout.setVisibility(View.GONE);


    }

    public void CheckRejectEmit() {

        rejectEmitHandler = new Handler();
        rejectEmitHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (checkDRVreject) {

                    checkDRVreject = false;
                    RejectNoDriverFound();
                }
                rejectEmitHandler.postDelayed(this, 1000);
            }
        }, 1000);
    }



    @Override
    public void onDirectionFinderFail(String error_message) {
        progressDialog.dismiss();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog);
        alertDialogBuilder.setTitle("Opps! an error eccurred");
        alertDialogBuilder.setIcon(R.drawable.ic_error_black_24dp);
        alertDialogBuilder.setMessage(error_message);
        alertDialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

    }


    public static void charge()
    {


        SharedPreferences preferences = activity.getSharedPreferences("creditCardCheck", MODE_PRIVATE);
        String getCardId= preferences.getString("cardId", "");
        String checkCard = preferences.getString("checkCard","");
        String custId= preferences.getString("custId","");

        if(checkCard.equals("yes"))
        {
//            SharedPreferences pref = activity.getSharedPreferences("customerData", MODE_PRIVATE);
//            String id= pref.getString("customer_Id", "");
            Double fare=BookingHistory.sharedinstance.getFare();
            String amount = String.valueOf(Math.round(fare*100));

            Stripe.apiKey = "sk_test_0XDT5RHd7IXtuxJ0vhYGRGsh";
            final Map<String,Object> chargeParam = new HashMap<>();
            chargeParam.put("amount",amount);
            chargeParam.put("currency","gbp");
            chargeParam.put("description",jobRef);
            chargeParam.put("customer",custId);
            chargeParam.put("receipt_email","musharrafslash@gmail.com");
            chargeParam.put("source",getCardId);

            new Thread(){
                @Override
                public void run() {
                    super.run();
                    try {
                        Charge charge = Charge.create(chargeParam);
                        //charge.capture();
                    } catch (AuthenticationException e) {
                        e.printStackTrace();
                    } catch (InvalidRequestException e) {
                        e.printStackTrace();
                    } catch (APIConnectionException e) {
                        e.printStackTrace();
                    } catch (CardException e) {
                        e.printStackTrace();
                    } catch (APIException e) {
                        e.printStackTrace();
                    }

                }
            }.start();





        }
    }


    @Override
    public void recieveonpob() {
        POBDriverEmitReRoute();
    }

    @Override
    public void recieveondriverlatlng() {

        if( valueAnimator==null){
            return;
        }
        if (driverlatlng != null) {

            if(DriverLoc.sharedInstance.getLat()!=null && DriverLoc.sharedInstance.getLong()!=null) {
                float results[]= new  float[1];
                Location.distanceBetween( driverlatlng.latitude,driverlatlng.longitude,DriverLoc.sharedInstance.getLat(),DriverLoc.sharedInstance.getLong(), results);
                Double meters = (double)results[0];

                speed=(meters/5)*2.23694;
                //  if(speed>5) {
                if(  ReRouteCount>=3 && checkPOBemit==true){
                    ReRouteCount=0;
                    try {
                        lat = DriverLoc.sharedInstance.getLat();
                        lng = DriverLoc.sharedInstance.getLong();
                        try{
                            for(i=2;BookingHistory.sharedinstance.getFromtovia().get(i).getAddress()!=null && i<9;i++){}
                        }
                        catch(Exception e){}
                        driverLatLng = lat + "," + lng;
                        new DirectionFinder(this, driverLatLng, BookingHistory.sharedinstance.getFromtovia().subList(2, i), BookingHistory.sharedinstance.getTo(), true).execute();

                    }
                    catch(Exception e){}
                }
                else{
                    ReRouteCount++;
                }
                //     }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        car_speed.setText(String.valueOf(Math.round(speed))+" Mph");

                    }
                });


                driverlatlng = new LatLng(DriverLoc.sharedInstance.getLat(), DriverLoc.sharedInstance.getLong());
            }
            else{
                return;
            }
//                            if (driverlatlng!=null){
//                                newlist.add(driverlatlng);
//                            }else {
//                                marker.setPosition(newPos);
//                            }


            newlist.add(driverlatlng);

            if (index < newlist.size() + 1) {

                start = newlist.get(index);
                end = newlist.get(next);

                index++;
                next = index + 1;


            }
        }
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                v = valueAnimator.getAnimatedFraction();
                try     {
                    lng = v * end.longitude + (1 - v)
                            * start.longitude;
                    lat = v * end.latitude + (1 - v)
                            * start.latitude;}
                catch(Exception e){
                    return;
                }
                newPos = new LatLng(lat, lng);
                if (start.latitude != end.latitude) {
                    try{	drvCarMarker.setPosition(newPos);}
                    catch(Exception e){
                        return;
                    }
                    drvCarMarker.setAnchor(0.5f, 0.5f);
                    if(tiltcheck){
                        if(speed>5 && newPos!=end){
                            CameraPosition cp = new CameraPosition.Builder()
                                    .target(driverlatlng)
                                    .tilt(90)
                                    .bearing(getBearing(end,newPos))
                                    .zoom(20)
                                    .build();
                            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cp);
                            mMap.moveCamera(cameraUpdate);}
                        else if(newPos==end){
                            CameraPosition cp = new CameraPosition.Builder()
                                    .target(driverlatlng)
                                    .tilt(90)
                                    .bearing(0)
                                    .zoom(20)
                                    .build();
                            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cp);
                            mMap.moveCamera(cameraUpdate);

                        }
                        drvCarMarker.setRotation(0);


                    }
                    else{
                        drvCarMarker.setRotation(getBearing(end, newPos));
                    }}

            }

        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {




                valueAnimator.start();


            }
        });



    }
}