package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import buzybeez.com.buzybeezcabcrystalpalace.DataHandler.SQLiteCustomerProfile;
import buzybeez.com.buzybeezcabcrystalpalace.FireBase.CustomerVerfication;
import buzybeez.com.buzybeezcabcrystalpalace.R;

public class AccountInfo extends AppCompatActivity {
    LinearLayout namePhoneEmailLayout;
    TextView name_textview, noProfileInfoMsg, email_textview, phone_textview;
    Button updateButton;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        namePhoneEmailLayout = findViewById(R.id.namePhoneEmailLayout);


        downloadImage();

        name_textview = (TextView) findViewById(R.id.name);
        noProfileInfoMsg = (TextView) findViewById(R.id.noProfileInfoMsg);
        email_textview = (TextView) findViewById(R.id.email);
        phone_textview = (TextView) findViewById(R.id.phone);
        updateButton = (Button) findViewById(R.id.update);

        updateButton.setVisibility(View.GONE);
        noProfileInfoMsg.setVisibility(View.GONE);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccountInfo.this, CustomerVerfication.class);
                startActivity(intent);
            }
        });

        checknameEmail();

    }

    @Override
    protected void onResume() {
        super.onResume();
        checknameEmail();
    }

    public void checknameEmail() {
        SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);
        String name = preferences.getString("cust_name", "");
        String email = preferences.getString("cust_email", "");
        String phone = preferences.getString("cust_phone", "");

        if (name.equals("") && email.equals("") && phone.equals("")) {
            updateButton.setVisibility(View.VISIBLE);
            noProfileInfoMsg.setVisibility(View.VISIBLE);
            namePhoneEmailLayout.setVisibility(View.GONE);

        } else {
            namePhoneEmailLayout.setVisibility(View.VISIBLE);
            updateButton.setVisibility(View.GONE);
            noProfileInfoMsg.setVisibility(View.GONE);
            name_textview.setText(name);
            email_textview.setText(email);
            phone_textview.setText(phone);
        }
    }

    public void downloadImage() {

        ImageView imageView = (ImageView) findViewById(R.id.profile);
        SQLiteCustomerProfile sqLiteCustomerProfile = new SQLiteCustomerProfile(getApplicationContext());


        if (sqLiteCustomerProfile.getprofile() != null) {
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(sqLiteCustomerProfile.getprofile(), 0, sqLiteCustomerProfile.getprofile().length));

        } else {

            imageView.setImageResource(R.drawable.profile);
        }


    }
}
