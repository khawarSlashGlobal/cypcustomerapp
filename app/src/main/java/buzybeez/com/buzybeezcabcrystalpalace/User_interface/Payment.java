package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Customer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import buzybeez.com.buzybeezcabcrystalpalace.FireBase.CustomerVerfication;
import buzybeez.com.buzybeezcabcrystalpalace.R;

public class Payment extends AppCompatActivity {
    TextView txtCash,txtGooglePay,txtAddPaymentMethod;
    String json;
    Gson gson;
    String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Payment");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initialize();
        getDataFromEmail();


//        try {
//            JSONObject jsonObject =new JSONObject(json);
//            jsonObject.getString("email");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


    }

    public void getDataFromEmail()
    {
        Stripe.apiKey = "sk_test_0XDT5RHd7IXtuxJ0vhYGRGsh";
        gson = new GsonBuilder().setPrettyPrinting().create();
        final Map<String, Object> options = new HashMap<>();
        options.put("email", "tests@gmail.com");




        new Thread(){
            @Override
            public void run() {

                try {
                    List<Customer>  customers = Customer.list(options).getData();

//                    String id= customers.get(0).getId();

                    json = gson.toJson(customers);

                    Log.d( "selectAllCard: ", json);



                    //Log.d( "email: ",custId);

                } catch (AuthenticationException e) {
                    e.printStackTrace();
                } catch (InvalidRequestException e) {
                    e.printStackTrace();
                } catch (APIConnectionException e) {
                    e.printStackTrace();
                } catch (CardException e) {
                    e.printStackTrace();
                } catch (APIException e) {
                    e.printStackTrace();
                }








            }
        }.start();





    }
    public void initialize()
    {
        txtCash =findViewById(R.id.txt_cash);
        txtGooglePay=findViewById(R.id.txt_google_pay);
        txtAddPaymentMethod=findViewById(R.id.addMethod);
    }

    public void cashOnClick(View view) {
        Intent intent = new Intent(Payment.this,CashActivity.class);
        startActivity(intent);
    }

    public void googlePayOnClick(View view) {
        Intent intent = new Intent(Payment.this,GooglePay.class);
        startActivity(intent);
    }

    public void paymentMethodOnClick(View view) {

        SharedPreferences pref = getSharedPreferences("", MODE_PRIVATE);
        String email = pref.getString("cust_email", "");
        if(!email.equals(""))
        {

            Intent intent = new Intent(Payment.this,PaymentMethodActivity.class);
            startActivity(intent);
//            SharedPreferences sharedPreferences = getSharedPreferences("Check Customer", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putString("verify","" );
//            editor.apply();
        }
        else {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Payment.this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            alertDialogBuilder.setTitle(Payment.this.getString(R.string.app_name));
            alertDialogBuilder.setMessage("Please Verify Your Account");
            alertDialogBuilder.setPositiveButton("Verify", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent(Payment.this, CustomerVerfication.class);
                    startActivity(intent);
                    dialog.cancel();
                }
            });
            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });
            alertDialogBuilder.show();





        }



    }
}
