package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import buzybeez.com.buzybeezcabcrystalpalace.Models.Booking;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CustomerConfig;
import buzybeez.com.buzybeezcabcrystalpalace.Models.DriverLoc;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.socket.client.Ack;

import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.FIXED_OFFICE_KEY;

public class DetailedBookingHistory extends BaseActivity implements OnMapReadyCallback {
    Booking b;
    private ImageButton myLocationButton;
    //map variables
    private GoogleMap mMap = null;
    PolylineOptions polylineOptions;
    //map routes variable
    LatLngBounds bound;
    //  ProgressDialog progressDialog1;
    public static double totalDuration = 0;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    public static double totalDistance = 0;
    DriverLoc d;
    SupportMapFragment mapFragment;
    LatLngBounds.Builder builder;
    String driverPhotoURL,vehiclePhotoURL;
    ConstraintLayout DriverCL,MapCL;
    public List<FromToVia> fromtoviaList = new ArrayList<>();
    public   vdViasRecyclerViewAdapter ViasAdapter;
    RecyclerView Vias;
    TextView dateTV,timeTV,statusTV,originTV,destinationTV,viasTV,viaslabel,fareTV,distanceTV,durationTV,dNameTV,dColorTV,dModelTV,dRegNumTV,dContactTV;
    String date,time,status,origin,destination,vias,fare,distance,duration,dName,dColor,dModel,dRegNum,dContact;
    ImageView driver_profile_image,vehicle_profile_image;
    int i;
    public static int flag=1;
    RatingBar ratingBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
// progressDialog1 = new ProgressDialog(this, R.style.MyDialogTheme);
//        progressDialog1.setTitle("Please Wait");
//        progressDialog1.setCancelable(false);
//        progressDialog1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog1.setMessage("Loading ...");
//progressDialog1.show();
        initilizingIds();


        //    targetlocationonClick();


        settingData();
        mapFragment.getMapAsync(this);
        String s = b.getDrvrcallsign();
        String [] str = s.split("@");
        if(str.length>1){
            getDriverLocByEmit(str[0],str[1]);}
        //   try {

        //new DirectionFinder(this, b.getfrom(),b.getFromtovia().subList(2,i) , b.getTo(), flag).execute();
        //  } catch (UnsupportedEncodingException e) {
        //        e.printStackTrace();
        //   }
    }

    public void initilizingIds(){
        ratingBar=findViewById(R.id.ratingBar);
        ratingBar.setEnabled(false);
        MapCL=findViewById(R.id.MapCL);
        DriverCL = findViewById(R.id.DriverCL);
        driver_profile_image = findViewById(R.id.imgDrvPic);
        vehicle_profile_image=findViewById(R.id.imgvehiclePic);
        viaslabel = findViewById(R.id.viasLabel);
        dateTV = (TextView)findViewById(R.id.tvDate);
        timeTV = (TextView)findViewById(R.id.tvTime);
        statusTV = (TextView)findViewById(R.id.tvStatus);
        originTV = (TextView)findViewById(R.id.to);
        destinationTV = (TextView)findViewById(R.id.from);
        viasTV = (TextView)findViewById(R.id.tvVias);
        fareTV = (TextView)findViewById(R.id.tvFare);
        distanceTV = (TextView)findViewById(R.id.tvDistance);
        durationTV = (TextView)findViewById(R.id.tvDuration);
        dColorTV = (TextView)findViewById(R.id.drvRegNumber);
        dModelTV = (TextView)findViewById(R.id.tvModel);
        dRegNumTV = (TextView)findViewById(R.id.tvRegNumber);
        dNameTV = (TextView)findViewById(R.id.drvName);
        dContactTV = (TextView)findViewById(R.id.drvcontactnum);
        mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.map);
        Vias=findViewById(R.id.Vias);

    }


    public void settingData(){
        b = Booking.getInstance();

        dateTV.setText(b.getDate());
        timeTV.setText(b.getTime());
        statusTV.setText(b.getCstate());
        originTV.setText(b.getfrom());
        destinationTV.setText(b.getTo());
        viasTV = (TextView)findViewById(R.id.tvVias);
        fareTV.setText((b.getFare().toString()));
        distanceTV.setText("");
        durationTV.setText("");
        Vias.setLayoutManager(new LinearLayoutManager(this));
        for (int i = 2; i <= b.getFromtovia().size() - 1; i++) {
            if (b.getFromtovia().get(i).getAddress() != null) {
                fromtoviaList.add(b.getFromtovia().get(i));
            }
        }
        ViasAdapter = new vdViasRecyclerViewAdapter(this, fromtoviaList);

        Vias.setAdapter(ViasAdapter);
        ViasAdapter.notifyDataSetChanged();




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void getDriverLocByEmit(final String drvCallsign, final String officeId){

        try {
            //setup PhotoURL
            final      String ip = CustomerConfig.sharedinstance.getImageIP();
            driverPhotoURL =  ip  + "/"+ officeId + "/"
                    + drvCallsign + "/DriverSnapshot.png";

            JSONArray query = new JSONArray();

            query.put("DriverReg");
            JSONObject filter = new JSONObject();
            filter.put("call_sign", drvCallsign);
            filter.put("office_id", officeId);
            query.put(filter);


            SocketEvent.sharedinstance.socket.emit("getdata", query, new Ack() {
                @Override
                public void call(Object... args) {
                    if (args[0] != null && !args[0].toString().equals("0")) {
                        try {
                            final JSONObject jsonObject = new JSONObject(args[0].toString());
                            //    final JsonNode jsonNode = JSONParse.convertJsonFormat(jsonObject);
                            //      ObjectMapper mapper = new ObjectMapper();
                            //    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            //    d = mapper.readValue(new TreeTraversingParser(jsonNode), DriverLoc.class);
                            JSONObject jsonob = new JSONObject();
                            JSONArray jsonarray = new JSONArray();
                            jsonob.put("driveruid", drvCallsign+"@"+officeId);
                            jsonarray.put("Ratings");
                            jsonarray.put(jsonob);
                            if (SocketEvent.sharedinstance.socket==null)
                            {
                                SocketEvent.sharedinstance.initializeSocket();
                            }
                            else if(!SocketEvent.sharedinstance.socket.connected())
                            {
                                SocketEvent.sharedinstance.socketConnectAfterDisconnect();
                            }
                            Log.i("prerating:", "ok");
                            SocketEvent.sharedinstance.socket.emit("getdata", jsonarray, new Ack() {
                                @Override
                                public void call(final Object... args) {

                                    //      getRatingvalue=0f;
                                    final    JSONObject data = (JSONObject) args[0];
                                    DriverCL.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            //    if(d!=null){
                                            DriverCL.setVisibility(View.VISIBLE);
                                            //     dColorTV.setText(b.d+", "+d.getVmake());
                                            //   dModelTV.setText(d.getVmake());
                                            //    dRegNumTV.setText("Reg #: "+jsonNode.get(""));
                                            try {
                                                dNameTV.setText(jsonObject.getString("drv_Name"));
                                                dContactTV.setText("Phone: " +jsonObject.getString("mob_tele_num"));
                                                ratingBar.setRating(   Float.parseFloat(data.getString("rating")));

                                                vehiclePhotoURL= ip  + "/" + officeId + "/" + jsonObject.getString("DefaultVehicle") + "/VehicleSnapshot.png";
                                                dRegNumTV.setText("Vehicle# "+jsonObject.getString("DefaultVehicle"));
                                            }
                                            catch(Exception e){}
                                            Glide.with(getApplicationContext())
                                                    .load(driverPhotoURL)
                                                    .asBitmap()
                                                    .into(new SimpleTarget<Bitmap>() {
                                                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                                        @Override
                                                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                                            RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                                            roundedBitmapDrawable.setCircular(true);
                                                            driver_profile_image.setImageDrawable(roundedBitmapDrawable);
                                                        }
                                                    });
                                            Glide.with(getApplicationContext())
                                                    .load(vehiclePhotoURL)
                                                    .asBitmap()
                                                    .into(new SimpleTarget<Bitmap>() {
                                                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                                        @Override
                                                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                                            RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                                            roundedBitmapDrawable.setCircular(true);
                                                            vehicle_profile_image.setImageDrawable(roundedBitmapDrawable);
                                                        }
                                                    });



                                            // }
                                        }
                                    });

                                }
                            });





                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public void onDirectionFinderStart() {
//
//    }
//
//    @Override
//    public void onDirectionFinderSuccess(List<Route> routes) {
//
//
//        polylinePaths = new ArrayList<>();
//        originMarkers = new ArrayList<>();
//        destinationMarkers = new ArrayList<>();
//        Polyline p1=null;
//
//        for (final Route route : routes) {
//            //   mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation.get(0), 16));
//
//            //calculate total duration & time
//
//
//
//            //setting the origin pointer
//            mMap.addMarker(new MarkerOptions()
//                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.originmarker))
//                    .anchor(0.5f, 0.5f)
//                    .flat(true)
//                    .title("ORIGIN")
//                    .visible(true)
//                    .snippet(route.startAddress.get(0))
//                    .position(route.startLocation.get(0)));
//
//            //setting waypoints marker
//            if (fromtoviaList.size() != 0) {
//                for (int j = 1; j <= fromtoviaList.size(); j++) {
//                  mMap.addMarker(new MarkerOptions()
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.waypoint_pointer))
//                            .anchor(0.5f, 0.5f)
//                            .flat(true)
//                            .title(route.startAddress.get(j))
//                            .position(route.startLocation.get(j)));
//                }
//            }
//
//            //setting the dest pointer
//         mMap.addMarker(new MarkerOptions()
//                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.destinationmarker))
//                    .snippet(route.endAddress.get(fromtoviaList.size()))
//                    .anchor(0.5f, 0.5f)
//                    .title("DESTINATION")
//                    .flat(true)
//                    .position(route.endLocation.get(fromtoviaList.size())));
//
//            //Setting plyline
//            polylineOptions = new PolylineOptions().
//                    geodesic(true).
//                    color(context.getResources().getColor(R.color.application_color_accent)).
//                    width(10);
//
//
//            for (int i = 0; i < route.points.size(); i++)
//                polylineOptions.add(route.points.get(i));
//
//            final Polyline p = mMap.addPolyline(polylineOptions);
//            polylinePaths.add(p);
//            //moveToBounds(p);
//
//
//
//
//
//
//
//            p1=p;
//
//        }
//
//
//        moveToBounds(p1);
//       // progressDialog1.dismiss();
//        if(!((b.getDrvrcallsign().equals("")) || (b.getDrvrcallsign().equals(null) ))){
//
//        }
//
//
//
//        String str = "";
//        int i = 0 ;
//        List<FromToVia> ftv1 = b.getFromtovia();
//        if (!(ftv1.get(2).getAddress()== null)) {
//            viaslabel.setVisibility(View.VISIBLE);
//            viasTV.setVisibility(View.VISIBLE);
//            for (FromToVia ftv : ftv1) {
//                if(!(ftv.getAddress()==null)){
//
//                    if ((ftv.getAddress().toString().equals(null)) == false){
//                        i++;
//                        if(i>2){
//                            str = str + ftv.getAddress() + "\n";}
//                    }
//                }
//                viasTV.setText(str);
//            }
//        } else {
//            viaslabel.setVisibility(View.GONE);
//            viasTV.setVisibility(View.GONE);
//        }
//    }
//    private void moveToBounds(Polyline p) {
//        try {
//            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//            for (int i = 0; i < p.getPoints().si5ze(); i++) {
//                builder.include(p.getPoints().get(i));
//            }
//
//            LatLngBounds bounds = builder.build();
//            int padding = 200; // offset from edges of the map in pixels
//
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//            mMap.moveCamera(cu);
//            bound = bounds;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            return;
//        }
//    }
//    @Override
//    public void onDirectionFinderFail(String error_message) {
////progressDialog1.dismiss();
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        LatLng originlatlng = new LatLng(b.getFromtovia().get(0).getLat(),b.getFromtovia().get(0).getLon());
        LatLng deslatlng = new LatLng(b.getFromtovia().get(1).getLat(),b.getFromtovia().get(1).getLon());

        try{
            for(i=2; b.getFromtovia().get(i).getAddress()!=null && i<9; i++){}
        }
        catch(Exception e){}
        builder = new LatLngBounds.Builder();
        mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.originmarker))
                .anchor(0.5f, 0.5f)
                .flat(true)
                .title("ORIGIN")
                .visible(true)
                .position(originlatlng));
        builder.include(originlatlng);

        for(i--;i>1;i--){
            LatLng ftvlatlng = new LatLng(b.getFromtovia().get(i).getLat(),b.getFromtovia().get(i).getLon());
            builder.include(ftvlatlng);
            //setting waypoints marker


            mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.waypoint_pointer))
                    .anchor(0.5f, 0.5f)
                    .flat(true)
                    .title("WAYPOINT")
                    .position(ftvlatlng));

        }
        builder.include(deslatlng);

        //setting the dest pointer
        mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.destinationmarker))

                .anchor(0.5f, 0.5f)
                .title("DESTINATION")
                .flat(true)
                .position(deslatlng));
        LatLngBounds bounds = builder.build();
        //     int padding = 200; // offset from edges of the map in pixels
        int yppi = getResources().getDisplayMetrics().heightPixels/Math.round(getResources().getDisplayMetrics().ydpi);
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = yppi*225;
        int padding = 200; // offset from edges of the map 12% of screen


        bound = bounds;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,width, height, padding);
        mMap.moveCamera(cu);

        mMap.getUiSettings().setScrollGesturesEnabled(false);
    }
    double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.CEILING);
        return bd.doubleValue();





    }
//    public void targetlocationonClick() {
//
//        myLocationButton = (ImageButton) findViewById(R.id.targetlocation);
//
//        myLocationButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (bound != null) {
//                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bound, 100));
//                }
//            }
//        });

    //  }
}
