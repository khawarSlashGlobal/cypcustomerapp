package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import buzybeez.com.buzybeezcabcrystalpalace.R;

public class Settings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        TextView account_textview = (TextView) findViewById(R.id.account_info);

        account_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AccountInfo.class);
                startActivity(intent);
            }
        });

        TextView addHome = (TextView) findViewById(R.id.addHome);
        addHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences preferences = getSharedPreferences("home_address", MODE_PRIVATE);
                String home_Location = preferences.getString("name", "");


                if (home_Location.equals("")) {
                    Intent intent = new Intent(Settings.this, LocationPickerActivity.class);
                    intent.putExtra("action","home");
                    startActivity(intent);
                } else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Settings.this, R.style.Theme_AppCompat_Light_Dialog);
                    alertDialogBuilder.setTitle("Home Location");
                    String message = home_Location + "<br><br>" + "<b>" + "Do You Want to Change it ?" + "</b>";
                    alertDialogBuilder.setMessage(Html.fromHtml(message));
                    alertDialogBuilder.setPositiveButton("yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    Intent intent = new Intent(Settings.this, LocationPickerActivity.class);
                                    intent.putExtra("action","home");
                                    startActivity(intent);
                                }
                            });

                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }


            }
        });

        final TextView addOffice = (TextView) findViewById(R.id.addOffice);
        addOffice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getSharedPreferences("office_address", MODE_PRIVATE);
                String office_Location = preferences.getString("office", "");


                if (office_Location.equals("")) {
                    Intent intent = new Intent(Settings.this, LocationPickerActivity.class);
                    intent.putExtra("action","office");
                    startActivity(intent);
                } else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Settings.this, R.style.Theme_AppCompat_Light_Dialog);
                    alertDialogBuilder.setTitle("Office Location");
                    String message = office_Location + "<br><br>" + "<b>" + "Do You Want to Change it ?" + "</b>";
                    alertDialogBuilder.setMessage(Html.fromHtml(message));
                    alertDialogBuilder.setPositiveButton("yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    Intent intent = new Intent(Settings.this, LocationPickerActivity.class);
                                    intent.putExtra("action","office");
                                    startActivity(intent);
                                }
                            });

                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }


            }

        });
    }
}
