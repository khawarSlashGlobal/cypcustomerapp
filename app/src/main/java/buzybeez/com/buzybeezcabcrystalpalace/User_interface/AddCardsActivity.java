package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.braintreepayments.cardform.view.CardForm;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Customer;
import com.stripe.model.CustomerCollection;
import com.stripe.model.ExternalAccountCollection;
import com.stripe.model.Token;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

public class AddCardsActivity extends BaseActivity implements OnCountryPickerListener, ConnectionListener {
    CardForm cardForm;
    ImageView flagImage;
    Button btnNext, buttonScan;
    EditText editTextCountry;
    CountryPicker countryPicker;
    Gson gson;
    String json;
    String email = "";
    String custId;

    List<Customer> customers;
    String custEmail;
    private static final int REQUEST_AUTOTEST = 200;
    ProgressDialog progressDialog;
    boolean addCard = true;

//    Customer customer;
//    Customer custId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cards);
        initialize();
        countryNamePicker();

        SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);
        custEmail = preferences.getString("cust_email", "");
        //custEmail = preferences.getString("cust_phone", "");
        if
        (Network.isNetworkAvailable(AddCardsActivity.this)) {
            getDataFromEmail(custEmail);
        }

        snackbarForConnectivityStatus();
    }


    public void initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Card Form");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        flagImage = findViewById(R.id.imageFlag);

        editTextCountry = findViewById(R.id.editTextCountry2);
        btnNext = findViewById(R.id.btnNext);
        cardForm = findViewById(R.id.card_form);
        buttonScan = findViewById(R.id.buttonScan);

        cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .postalCodeRequired(true)
                .mobileNumberRequired(false)
                .setup(AddCardsActivity.this);
        cardForm.getCvvEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        countryPicker = new CountryPicker.Builder().with(this).listener(this).build();


    }

    @Override
    public void onSelectCountry(Country country) {
        editTextCountry.setText(country.getName());
        flagImage.setImageResource(country.getFlag());

    }

    private void countryNamePicker() {
        editTextCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countryPicker.showDialog(getSupportFragmentManager());
            }
        });
    }

    //get data only login email
    public void getDataFromEmail(final String customerEmail) {


        Stripe.apiKey = "sk_test_0XDT5RHd7IXtuxJ0vhYGRGsh";
        gson = new GsonBuilder().setPrettyPrinting().create();
        final Map<String, Object> options = new HashMap<>();
        options.put("email", customerEmail);


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    customers = Customer.list(options).getData();
                    Log.d("custNumb",customers.toString());

                } catch (AuthenticationException e) {
                    e.printStackTrace();
                } catch (InvalidRequestException e) {
                    e.printStackTrace();
                } catch (APIConnectionException e) {
                    e.printStackTrace();
                } catch (CardException e) {
                    e.printStackTrace();
                } catch (APIException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (customers.size() > 0) {
                            email = customers.get(0).getEmail();
                            custId = customers.get(0).getId();

                        }

                    }
                });

            }
        })
                .start();

    }



    //click event of add button
    public void onAddCard(View view) {

        if(cardForm.isValid())
        {
            progressDialog = new ProgressDialog(AddCardsActivity.this);
            progressDialog.setTitle("Add new Card");
            progressDialog.setMessage("Inserting card please wait...");
            progressDialog.show();
            if
            (Network.isNetworkAvailable(AddCardsActivity.this)) {



                if (email.equals(custEmail)) {

                    // Toast.makeText(getApplicationContext(), "registered", Toast.LENGTH_LONG).show();

                    SharedPreferences preferences = getSharedPreferences("Check Customer", MODE_PRIVATE);
                    String verify = preferences.getString("verify", "");

                    if (verify.equals("yes")) {

                        addCard(custId);
                        //progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddCardsActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                        alertDialogBuilder.setTitle(AddCardsActivity.this.getString(R.string.app_name));

                        alertDialogBuilder.setMessage("Your previous card is available first include your cards into app then add new card");
                        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                                finish();
                            }
                        });
                        alertDialogBuilder.show();
                        //Toast.makeText(getApplicationContext(),"You are already registered but not a login plx login to you account first ",Toast.LENGTH_LONG).show();

                    }


                } else {

                    //Toast.makeText(getApplicationContext(), "not registered", Toast.LENGTH_LONG).show();
                    createCustomer();
                    //progressDialog.dismiss();
                }

            }
            else {
                progressDialog.dismiss();
            }
        }
        else {
            Toast.makeText(getApplicationContext(),"Please enter complete and valid information",Toast.LENGTH_LONG).show();

        }





    }

    //add card into stripe
    public void addCard(final String id) {
        SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);
        String name = preferences.getString("cust_name", "");
        final Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Stripe.apiKey = "sk_test_0XDT5RHd7IXtuxJ0vhYGRGsh";
        final Map<String, Object> source = new HashMap<>();
        final Map<String, Object> cardParam = new HashMap<>();
        cardParam.put("name",name);
        cardParam.put("number", cardForm.getCardNumber());
        cardParam.put("exp_month", cardForm.getExpirationMonth());
        cardParam.put("exp_year", cardForm.getExpirationYear());
        cardParam.put("cvc", cardForm.getCvv());
        cardParam.put("address_zip", cardForm.getPostalCode());
        cardParam.put("address_country", editTextCountry.getText().toString());


//                cardParam.put("country_code",cardForm.getCountryCode());
//               cardParam.put("mobile_number",cardForm.getMobileNumber());
        //cardParam.put("country name",editTextCountry.getText().toString());
        final Map<String, Object> tokenPattern = new HashMap<>();
        tokenPattern.put("card", cardParam);

        new Thread() {
            @Override
            public void run() {
                try {

                    Token token = Token.create(tokenPattern);

                    if (token == null) {
                        progressDialog.dismiss();
                    }

                    Boolean cardIsNotExist = true;
                    Customer customers = Customer.retrieve(id);
                    ExternalAccountCollection allCardDetail = customers.getSources();
                    json = gson.toJson(allCardDetail);

                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        JSONArray userArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < userArray.length(); i++) {
                            JSONObject cardDetail = userArray.getJSONObject(i);

                            String fingerprint = cardDetail.getString("fingerprint");

                            if (fingerprint.equals(token.getCard().getFingerprint())) {
                                cardIsNotExist = false;
                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    if (cardIsNotExist) {
                        source.put("source", token.getId());

                        customers.getSources().create(source);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                finish();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Card is already exist", Toast.LENGTH_LONG).show();
                            }
                        });


                    }

                } catch (AuthenticationException e) {
                    e.printStackTrace();
                } catch (InvalidRequestException e) {
                    e.printStackTrace();
                } catch (APIConnectionException e) {
                    e.printStackTrace();
                } catch (CardException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddCardsActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                            alertDialogBuilder.setTitle(AddCardsActivity.this.getString(R.string.app_name));

                            alertDialogBuilder.setMessage("your card is invalid please enter a valid card detail");
                            alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                    //finish();
                                }
                            });
                            alertDialogBuilder.show();
                        }
                    });

                } catch (APIException e) {
                    e.printStackTrace();
                }

            }
        }.start();


    }

    //create customer on stripe
    public void createCustomer() {


        SharedPreferences preferences = getSharedPreferences("", MODE_PRIVATE);
        String name = preferences.getString("cust_name", "");
        String email = preferences.getString("cust_email", "");
        String phone = preferences.getString("cust_phone", "");



        final Map<String, Object> address = new HashMap<>();
        address.put("line1", editTextCountry.getText().toString());


        final Map<String, Object> shipping = new HashMap<>();
        shipping.put("address", address);
        shipping.put("name", name);
        shipping.put("phone", phone);


        final Map<String, Object> customerParam = new HashMap<>();


        customerParam.put("email", email);
        customerParam.put("description",phone);
        customerParam.put("shipping", shipping);

        //customerParam.put("phone","1");


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final Customer customer = Customer.create(customerParam);
                    String id = customer.getId();
                    addCard(id);
                } catch (AuthenticationException e) {
                    e.printStackTrace();
                } catch (InvalidRequestException e) {
                    e.printStackTrace();
                } catch (APIConnectionException e) {
                    e.printStackTrace();
                } catch (CardException e) {
                    e.printStackTrace();
                } catch (APIException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void onScanButton(View view) {

        Stripe.apiKey = "sk_test_0XDT5RHd7IXtuxJ0vhYGRGsh";

        Map<String, Object> addressParams = new HashMap<String, Object>();
        addressParams.put("line1", "1234 Main Street");
        addressParams.put("city", "San Francisco");
        addressParams.put("state", "CA");
        addressParams.put("postal_code", "94111");
        addressParams.put("country", "US");

        Map<String, Object> billingParams = new HashMap<String, Object>();
        billingParams.put("name", "Jenny Rosen");
        billingParams.put("address", addressParams);

        Map<String, Object> cardholderParams = new HashMap<>();
        cardholderParams.put("name", "Jenny Rosen");
        cardholderParams.put("email", "jenny.rosen@example.com");
        cardholderParams.put("phone_number", "+18008675309");
        cardholderParams.put("status", "active");
        cardholderParams.put("type", "individual");
        cardholderParams.put("billing", billingParams);











//        progressDialog = new ProgressDialog(AddCardsActvity.this);
//        progressDialog.setTitle("Add new Card");
//        progressDialog.setMessage("Inserting card please wait...");
//        progressDialog.show();
//        Intent intent = new Intent(this, CardIOActivity.class);
//        intent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true);
//        intent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false);
//        intent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false);
//
//
//        startActivityForResult(intent, REQUEST_AUTOTEST);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_AUTOTEST) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {

                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }
            }
            else {
                resultDisplayStr = "Scan was canceled.";
            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
            //editTextCardNumber.setText(resultDisplayStr);
            cardForm.getCardEditText().setText(resultDisplayStr);
        }
    }



//
//    private void storeCardInformation(String cardNumber, Integer expMonth, Integer expYear, String cvc, String type, String addressZip, String country) {
//        String count = "1";
//        SharedPreferences preferences = getSharedPreferences("counterName", MODE_PRIVATE);
//        String number = preferences.getString("card_Number", "");
//
//        if (number.equals("")) {
//            SharedPreferences sharedPreferences = getSharedPreferences("counterName", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putString("card_Number", count);
//            editor.apply();
//        } else {
//            int num = Integer.parseInt(number);
//            num = num + 1;
//            count = String.valueOf(num);
//            SharedPreferences sharedPreferences = getSharedPreferences("counterName", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putString("card_Number", count);
//            editor.apply();
//        }
//
//        SharedPreferences setPrefrence = getSharedPreferences("card" + count, Context.MODE_PRIVATE);
//
//        List<CreditCardInfo> creditCardInfo = new ArrayList<>();
//
//        String sharedPrefCardNumber = cardNumber;
//
//        String sharedPrefMonth = String.valueOf(expMonth);
//        String sharedPrefYear = String.valueOf(expYear);
//        String sharedPrefCvv = cvc;
//        String sharedPrefType = type;
//        String sharedPrefPostCode = addressZip;
//        String sharedPrefCountry = country;
//
//
//        creditCardInfo.add(new CreditCardInfo(sharedPrefCardNumber, sharedPrefMonth, sharedPrefYear, sharedPrefCvv, sharedPrefType, sharedPrefPostCode, sharedPrefCountry,count));
//        Gson gson = new Gson();
//        String json = gson.toJson(creditCardInfo);
//        SharedPreferences.Editor edit = setPrefrence.edit();
//        edit.putString("Set", json);
//        edit.commit();
////        edit.putString("card_Number", sharedPrefCardNumber);
////        edit.putString("month",sharedPrefMonth);
////        edit.putString("year",sharedPrefYear);
////        edit.putString("cvv",sharedPrefCvv);
////        edit.putString("type",sharedPrefType);
////        edit.putString("post_code",sharedPrefPostCode);
////        edit.putString("country",sharedPrefCountry);
//        //edit.apply();
//        Intent intent = new Intent(AddCardsActvity.this,PaymentMethodActivity.class);
//        startActivity(intent);
//        finish();
//    }


}
