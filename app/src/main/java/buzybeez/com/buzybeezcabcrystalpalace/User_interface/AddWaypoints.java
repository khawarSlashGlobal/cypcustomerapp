package buzybeez.com.buzybeezcabcrystalpalace.User_interface;


import android.content.Intent;
import android.content.SharedPreferences;

import android.location.Address;
import android.location.Geocoder;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AutoCompleteTextView;

import android.widget.Button;
import android.widget.ImageView;


import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.R;

import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.dropoffloc;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.flag;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.fromtoviaList;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.pickuploc;

public class AddWaypoints extends BaseActivity implements ViasRecyclerViewAdapter.ItemClickListener, ConnectionListener {

    public static ViasRecyclerViewAdapter adapter;
    FloatingActionButton addwaypoint, homebtn, officebtn,mapsbtn;
    RecyclerView recyclerView;
    AutoCompleteTextView pickup1, destination1;
    ImageView cancelPickupLocation1, cancelDropLocation1, swapicon1;
    Button nextbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_waypoints);

        // set up the RecyclerView
        nextbtn =findViewById(R.id.buttonNext);
        recyclerView = findViewById(R.id.waypointsRecylerView);
        addwaypoint = findViewById(R.id.addWayPoint);
        homebtn = findViewById(R.id.homebtn);
        officebtn = findViewById(R.id.officebtn);
        mapsbtn = findViewById(R.id.viasFromMapBTN);
        pickup1 = findViewById(R.id.select_location1);
        destination1 = findViewById(R.id.select_location2);
        cancelPickupLocation1 = findViewById(R.id.cancel_pickup1);
        cancelDropLocation1 = findViewById(R.id.cancel_drop1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ViasRecyclerViewAdapter(this, fromtoviaList);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        enable_home_office_button();
        cancelPickupLocation1.setVisibility(View.GONE);
        cancelDropLocation1.setVisibility(View.GONE);
        swapicon1=findViewById(R.id.swap_icon2);
        swapicon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String swaptemp;
                boolean isairporttemp;
                isairporttemp=isAirportAtOri;
                isAirportAtOri= isAirportAtDest;
                isAirportAtDest= isairporttemp;
                swaptemp= pickup1.getText().toString();
                pickup1.setText( destination1.getText().toString());
                pickuploc.setText( destination1.getText().toString());
                destination1.setText(swaptemp);
                dropoffloc.setText(swaptemp);
                if(pickup1.getText().toString().equals(""))
                {
                    cancelPickupLocation1.setVisibility(View.GONE);
                }
                else
                {
                    extractAddressFromOrigin(pickup1.getText().toString(),AddWaypoints.this);

                }
                if(destination1.getText().toString().equals(""))
                {
                    cancelDropLocation1.setVisibility(View.GONE);
                }   else
                {
                    extractAddressFromDestination(destination1.getText().toString(),AddWaypoints.this);

                }

            }
        });
        //  pickup1.setText(pickuploc.getText());
        // destination1.setText(dropoffloc.getText());
        snackbarForConnectivityStatus();
        setpickuploc();
        setdestinationloc();
        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                btnclicktobookingscreen(pickuploc,dropoffloc,AddWaypoints.this);
            }
        });
        addwaypoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//
//                AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
//                        .setTypeFilter(Place.TYPE_COUNTRY)
//                        .setCountry("UK")
//                        .build();
//
//
//                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//                Intent intent;
//
//                //restrict user to search places only in UK
//
//                try {
//                    intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
//                            .setFilter(autocompleteFilter).build(AddWaypoints.this);
//
////                    intent = builder.build((Activity) OriginAndDestination.this);
//
//                    startActivityForResult(intent, 1);
//
//                } catch (GooglePlayServicesRepairableException e) {
//                    e.printStackTrace();
//                } catch (GooglePlayServicesNotAvailableException e) {
//                    e.printStackTrace();
//                }
                Intent intent = new Intent(AddWaypoints.this, AutocompletePlacePickerActivity.class);
                intent.putExtra("action", "waypoints");
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        fromtoviaList.remove(position);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!pickuploc.getText().equals("")) {
            pickup1.setText(pickuploc.getText());
            extractAddressFromOrigin(String.valueOf(pickuploc.getText().toString().trim()),this);
        }

        if (!dropoffloc.getText().equals("")) {
            destination1.setText(dropoffloc.getText());
            extractAddressFromDestination(String.valueOf(dropoffloc.getText().toString().trim()),this);
        }

        if (pickup1.getText().equals("")) {
            cancelPickupLocation1.setVisibility(View.GONE);
        }

        if (destination1.getText().equals("")) {
            cancelDropLocation1.setVisibility(View.GONE);
        }
    }

    public void setpickuploc() {
        //When user enter location cancel button will appear in pickup location
        pickup1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (pickup1.getText().equals("") || pickup1.getText().toString().length()<1) {
                    cancelPickupLocation1.setVisibility(View.GONE);
                } else {
                    cancelPickupLocation1.setVisibility(View.VISIBLE);
                }
            }
        });


        cancelPickupLocation1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickup1.setText("");
                pickuploc.setText("");
                cancelPickupLocation1.setVisibility(View.GONE);
            }
        });
        pickup1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
//                AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
//                        .setTypeFilter(Place.TYPE_COUNTRY)
//                        .setCountry("UK")
//                        .build();
//
//
//                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//                Intent intent;
//
//                //restrict user to search places only in UK
//
//                try {
//                    intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
//                            .setFilter(autocompleteFilter).build(AddWaypoints.this);
//
////                    intent = builder.build((Activity) OriginAndDestination.this);
//
//                    startActivityForResult(intent, 2);
//
//                } catch (GooglePlayServicesRepairableException e) {
//                    e.printStackTrace();
//                } catch (GooglePlayServicesNotAvailableException e) {
//                    e.printStackTrace();
//                }
                flag = true;
                Intent intent = new Intent(AddWaypoints.this, AutocompletePlacePickerActivity.class);
                intent.putExtra("action", "pickup");
                startActivityForResult(intent, 2);  }
        });
    }

    public void setdestinationloc() {

        //When user enter location cancel button will appear in pickup location
        destination1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (destination1.getText().equals("") || destination1.getText().toString().length()<1) {
                    cancelDropLocation1.setVisibility(View.GONE );
                } else {
                    cancelDropLocation1.setVisibility(View.VISIBLE);
                }

            }


        });


        cancelDropLocation1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                destination1.setText("");
                dropoffloc.setText("");
                cancelDropLocation1.setVisibility(View.GONE);
            }
        });

        destination1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
//                AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
//                        .setTypeFilter(Place.TYPE_COUNTRY)
//                        .setCountry("UK")
//                        .build();
//
//
//                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//                Intent intent;
//
//                //restrict user to search places only in UK
//
//                try {
//                    intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
//                            .setFilter(autocompleteFilter).build(AddWaypoints.this);
//
////                    intent = builder.build((Activity) OriginAndDestination.this);
//
//                    startActivityForResult(intent, 3);
//
//                } catch (GooglePlayServicesRepairableException e) {
//                    e.printStackTrace();
//                } catch (GooglePlayServicesNotAvailableException e) {
//                    e.printStackTrace();
                //      }
                flag = false;
                Intent intent = new Intent(AddWaypoints.this, AutocompletePlacePickerActivity.class);
                intent.putExtra("action", "dropoffloc");
                startActivityForResult(intent, 3); }
        });
    }


//    public FromToVia createFromToViaObj(Intent data) {
//        Place place = PlacePicker.getPlace(data, this);
//        String winfo = "";
//        String wpAddress = String.format(String.valueOf(place.getAddress()));
//        Double wlat = (place.getLatLng()).latitude;
//        Double wlon = (place.getLatLng()).longitude;
//        Geocoder coder = new Geocoder(this);
//        List<Address> address;
//        LatLng p1 = null;
//        Address location = null;
//        try {
//            // May throw an IOException
//            address = coder.getFromLocationName(wpAddress, 5);
//
//            location = address.get(0);
//            String wpPostalCode = location.getPostalCode();
//            //p1 = new LatLng(location.getLatitude(), location.getLongitude() );
//
//        } catch (IOException ex) {
//
//            ex.printStackTrace();
//        }
//        FromToVia ftv = new FromToVia(winfo, wpAddress, wlat, wlon, wpAddress);
//        return ftv;
//    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == 1) {

            if (resultCode == RESULT_OK) {
                //  fromtoviaList.add(createFromToViaObj(data));
                // Notify the adapter that an item inserted
                adapter.notifyDataSetChanged();
                int pos = fromtoviaList.size() - 1;
                recyclerView.scrollToPosition(pos);


            }
        }

//        if (requestCode == 2) {
//
//            if (resultCode == RESULT_OK) {
//
//                Place place = PlacePicker.getPlace(data, this);
//                String oriAdd = String.format(String.valueOf(place.getAddress()));
//                pickup1.setText(oriAdd);
//                pickuploc.setText(oriAdd);
//
//
//            }
//        }
//        if (requestCode == 3) {
//
//            if (resultCode == RESULT_OK) {
//
//                Place place = PlacePicker.getPlace(data, this);
//                String destAdd = String.format(String.valueOf(place.getAddress()));
//                destination1.setText(destAdd);
//                dropoffloc.setText(destAdd);
//
//
//            }
        //      }
    }


    public void HomeClicked(View view) {
        SharedPreferences preference = getSharedPreferences("home_address", MODE_PRIVATE);

        FromToVia ftv = new FromToVia(
                "", preference.getString("name", ""),
                Double.parseDouble(preference.getString("homelat", "")),
                Double.parseDouble(preference.getString("homelng", "")),
                preference.getString("homepostcode", "")
        );


        fromtoviaList.add(ftv);
        // Notify the adapter that an item inserted
        adapter.notifyDataSetChanged();
        int pos = fromtoviaList.size() - 1;
        recyclerView.scrollToPosition(pos);
    }

    public void OfficeClick(View view) {
        SharedPreferences preferences = getSharedPreferences("office_address", MODE_PRIVATE);
        String office_Location = preferences.getString("office", "");


        FromToVia ftv = new FromToVia(
                "", preferences.getString("office", ""),
                Double.parseDouble(preferences.getString("officelat", "")),
                Double.parseDouble(preferences.getString("officelng", "")),
                preferences.getString("officepostcode", "")
        );


        fromtoviaList.add(ftv);
        // Notify the adapter that an item inserted
        adapter.notifyDataSetChanged();
        int pos = fromtoviaList.size() - 1;
        recyclerView.scrollToPosition(pos);
    }

    public void enable_home_office_button() {

        SharedPreferences preferences = getSharedPreferences("office_address", MODE_PRIVATE);
        String office_Location = preferences.getString("office", "");

        SharedPreferences preference = getSharedPreferences("home_address", MODE_PRIVATE);
        String home_Location = preference.getString("name", "");


        if (home_Location.equals("")) {
            homebtn.setVisibility(View.GONE);
        }
        if (office_Location.equals("")) {
            officebtn.setVisibility(View.GONE);
        }

    }
    public void mapsClick(View view) {

        Intent intent = new Intent(this, LocationPickerActivity.class);
        intent.putExtra("action","vias");
        startActivity(intent);
    }
}