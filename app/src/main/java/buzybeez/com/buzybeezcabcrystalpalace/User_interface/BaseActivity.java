package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.safetynet.SafetyNet;

import org.joda.time.DateTimeZone;
import org.joda.time.Instant;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import buzybeez.com.buzybeezcabcrystalpalace.DataCenter.DataHolder;
import buzybeez.com.buzybeezcabcrystalpalace.DataHandler.SQLiteDatabaseHandler;
import buzybeez.com.buzybeezcabcrystalpalace.Email.GMailSender;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.OnNetworkConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Booking;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CfgCustApp;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CustomerConfig;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CustomerReg;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FareListMapping;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;
import buzybeez.com.buzybeezcabcrystalpalace.Models.OfficeDetail;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Rating;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.AlarmBroadcastReceiver;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Connect;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.GPStracker;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.NetworkStateChangeReceiver;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.socket.client.Ack;

import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.APP_EMAIL;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.APP_PASSWORD;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.FIXED_OFFICE_KEY;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.SHARE_SUBJECT;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.BookingScreen.totalDistance;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.BookingScreen.totalDuration;


/**
 * Created by hv on 1/23/18.
 */

public class BaseActivity extends AppCompatActivity implements OnNetworkConnectionListener {
    private static final int REQUEST_PERMISSION_CALL = 111;
    public static final int REQUEST_ALLPERMISSIONS_CODE = 0;
    public static boolean checkPOBemit;
    public static boolean checkDRVreject;
    public static double cyplat;
    public static double cyplon;
    public static double wcplat;
    public static double wcplon;
    public static double nbtlat;
    public static double nbtlon;
    public static double surlat;
    public static double surlon;
    public static double nbalat;
    public static double nbalon;
    public static double xyzlat;
    public static double xyzlon;
    //for booking
    public static Booking booking = new Booking();
    public static SQLiteDatabaseHandler db;
    public static boolean stop = false;
    public static String officeId, drvCallsign = "";
    public static Context context;
    static Bitmap bitmap;
    public GoogleApiClient googleApiClient;
    public double latitude, longitude;
    public Location mLocation;
    public String deviceID = null;
    public View parentLayout;
    public String pathOfImage;
    //variables for permmisionsCheck
    int MyVersion = Build.VERSION.SDK_INT;
    TelephonyManager mngr;
    LocationManager locationManager;
    String carSymbol = null;
    URL url;
    Uri bmpUri;
    File imgFile;
    NetworkStateChangeReceiver networkStateChangeReceiver;
    ArrayList<String> ids;
    String jobid;
    int pos;
    public static boolean isAirportAtOri = false;
    public static boolean isAirportAtDest = false;
    SharedPreferences.Editor editor;
    //for addresslocate
    private GPStracker gpsTracker;
    private int status = 0;
    public static Map<String, List<Double>> fareList;
    public static List<Double> valuesOfFixedFare;
    public static ProgressDialog progressDialog;
    public static List<FromToVia> fromtoviaList = new ArrayList<>();
    public static String mDespostcode = null;
    public static String mDesOutcode = null;
    public static double deslat;
    public static double deslon;
    public static String mDesaddress = null;
    public static GoogleMap mMap;
    public static String mpostcode = null;
    public static String mOutcode = null;
    public static double originlat;
    public static double originlon;
    public static boolean ratesfilled=false;
    public static GMailSender sender=null ;
    public void getOfficeNumberbyEmit(String officeName) {
        try {

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this)) {
                JSONArray query = new JSONArray();
                query.put("OfficeName");


                JSONObject filter = new JSONObject();


                filter.put("abb", officeName);

                query.put(filter);

                SocketEvent.sharedinstance.socket.emit("getdata", query,
                        new Ack() {
                            @Override
                            public void call(Object... args) {
                                if (args[0] != null && !args[0].toString().equals("0")) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(args[0].toString());
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(jsonObject);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        OfficeDetail.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), OfficeDetail.class);
                                        Officecall_action();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
            }
            else
            {
                if (SocketEvent.sharedinstance.socket==null)
                {
                    SocketEvent.sharedinstance.initializeSocket();
                }
                else if(!SocketEvent.sharedinstance.socket.connected())
                {
                    SocketEvent.sharedinstance.socketConnectAfterDisconnect();
                }
            }
//				autoDismissDialogue("Error", "No Internet");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void Officecall_action() {
        try {

            String num = OfficeDetail.sharedinstance.getOfficeNumber();
            Uri number = Uri.parse("tel:" + num);
            Intent callIntent = new Intent(Intent.ACTION_CALL, number);
            if (android.support.v4.app.ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(callIntent);
        } catch (android.content.ActivityNotFoundException ex) {
        }

    }
    public boolean isPermissionGranted() {

        if (ContextCompat.checkSelfPermission(getBaseContext(),
                android.Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.CALL_PHONE)) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE},
                        REQUEST_PERMISSION_CALL);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE},
                        REQUEST_PERMISSION_CALL);
            }
        }
        return true;
    }
    public void ThreadSendMail(final String body){
        new Thread(new Runnable() {
            public void run() {        ///Toast.makeText(getApplicationContext(), "Connect", Toast.LENGTH_LONG).show();
                //sender.addAttachment(filename);
                try {
                    Log.e("sending email use mail",getSharedPrefEmail());
                    //  sender.addAttachment(Environment.getExternalStorageDirectory().getPath() + "/sdcard/mysdfile.txt");
                    sender.sendMail(SHARE_SUBJECT+" Notification", body, CfgCustApp.sharedinstance.getoffemail(),
                            getSharedPrefEmail());
                }
                catch(Exception e){}
            }}).start();
    }
    public void getCfgCustApp(Context con) {
        if (Network.isNetworkAvailable(getApplicationContext())) {
            if (SocketEvent.sharedinstance.socket == null) {
                SocketEvent.sharedinstance.initializeSocket();
            }
            final Timer timer = new Timer();
            timer.schedule(new TimerTask() {

                @Override
                public void run() {
                    if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(getApplicationContext())) {
                        timer.cancel();
                        JSONArray query = new JSONArray();
                        JSONObject filter = new JSONObject();
                        try {
                            filter.put("officename", FIXED_OFFICE_KEY);
                            query.put("CfgCustApp");
                            query.put(filter);
                            SocketEvent.sharedinstance.socket.emit("getdata", query, new Ack() {
                                @Override
                                public void call(final Object... args) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(args[0].toString());
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(jsonObject);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        CfgCustApp.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), CfgCustApp.class);
                                        int i = 0;
                                        if(sender==null){
                                            sender = new GMailSender(CfgCustApp.sharedinstance.getoffemail(),CfgCustApp.sharedinstance.getoffemailpwd());
                                        } }catch (JSONException ex) {
                                        ex.printStackTrace();
                                    } catch (JsonParseException e) {
                                        e.printStackTrace();
                                    } catch (JsonMappingException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }

            }, 100, 1000);
        } else {

            Toast.makeText(con, "Internet not Connected", Toast.LENGTH_SHORT).show();

        }
    }
    private void plotToPlace(final TextView dropoffloc,final TextView pickuploc,final Context con) {
        //progressDialog.show();
        try {
            JSONArray data = new JSONArray();
            data.put("AirportFixedPrices");

            JSONObject plotToPlaceData = new JSONObject();
            plotToPlaceData.put("FromPlot", DataHolder.getInstance().originData.get(1));
            plotToPlaceData.put("ToPlace", dropoffloc.getText().toString());
            plotToPlaceData.put("AccRef", "CASH");
            plotToPlaceData.put("PlotToPlaceChk", 1);

            data.put(plotToPlaceData);

            SocketEvent.sharedinstance.socket.emit("getmultiplerecords", data, new Ack() {
                @Override
                public void call(Object... args) {

                    try {
                        JSONArray jsonArray = new JSONArray(args[0].toString());

                        fareList = new HashMap<>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject plotsData = jsonArray.getJSONObject(i);
                            double accVal = plotsData.getDouble("AccVal");
                            String vehicleType = plotsData.getString("VehicleType");
                            double drvVal = plotsData.getDouble("DrvVal");

                            valuesOfFixedFare = new ArrayList<>();
                            valuesOfFixedFare.add(accVal);
                            valuesOfFixedFare.add(drvVal);

                            if (vehicleType.equals("Saloon")) {

                                fareList.put("S", valuesOfFixedFare);
                                //journeyFare.setText(String.format(" %.2f", valuesOfFixedFare.get(0)));

                            } else if (vehicleType.equals("Estate")) {

                                fareList.put("E", valuesOfFixedFare);

                            } else if (vehicleType.equals("Executive")) {

                                fareList.put("X", valuesOfFixedFare);

                            } else if (vehicleType.equals("8 Passenger")) {

                                fareList.put("8", valuesOfFixedFare);

                            } else if (vehicleType.equals("6 Passenger")) {

                                fareList.put("6", valuesOfFixedFare);
                            }
                        }


                        // intentToBookingScreen();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    if(progressDialog.isShowing()){
//                        progressDialog.cancel();}
//                    Intent intent = new Intent();
//                    intent.putExtra("pickupValue", pickuploc.getText().toString());
//                    intent.putExtra("dropoffValue", dropoffloc.getText().toString());
//                    intent.setClass(OriginAndDestination.this, BookingScreen.class);
//                    startActivity(intent);
                    intentToBookingScreen(pickuploc,dropoffloc,con);}

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void intentToBookingScreen(TextView pickuploc, TextView dropoffloc, Context con) {
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {

//            }
//        }, 0);

//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
        if (progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        //   handler.removeCallbacks(this);
        Intent intent = new Intent();
        intent.putExtra("pickupValue", pickuploc.getText().toString());
        intent.putExtra("dropoffValue", dropoffloc.getText().toString());
        intent.setClass(con, BookingScreen.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        //  }
//        }, 3000);

    }
    private void placeToPlot(final TextView pickuploc,final TextView dropoffloc,final Context con) {
        //progressDialog.show();
        try {
            JSONArray data = new JSONArray();
            data.put("AirportFixedPrices");

            JSONObject placeToPlotData = new JSONObject();
            placeToPlotData.put("FromPlace", pickuploc.getText().toString());
            placeToPlotData.put("ToPlot", DataHolder.getInstance().destinationData.get(1));
            placeToPlotData.put("AccRef", "CASH");
            placeToPlotData.put("PlaceToPlotChk", 1);

            data.put(placeToPlotData);

            SocketEvent.sharedinstance.socket.emit("getmultipledata", data, new Ack() {
                @Override
                public void call(Object... args) {
                    try {

                        JSONArray jsonArray = new JSONArray(args[0].toString());

                        fareList = new HashMap<>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject plotsData = jsonArray.getJSONObject(i);
                            double accVal = plotsData.getDouble("AccVal");
                            String vehicleType = plotsData.getString("VehicleType");
                            double drvVal = plotsData.getDouble("DrvVal");

                            valuesOfFixedFare = new ArrayList<>();

                            valuesOfFixedFare.add(accVal);
                            valuesOfFixedFare.add(drvVal);

                            if (vehicleType.equals("Saloon")) {

                                fareList.put("S", valuesOfFixedFare);
                                // journeyFare.setText(String.format(" %.2f", valuesOfFixedFare.get(0)));

                            } else if (vehicleType.equals("Estate")) {

                                fareList.put("E", valuesOfFixedFare);

                            } else if (vehicleType.equals("Executive")) {

                                fareList.put("X", valuesOfFixedFare);

                            } else if (vehicleType.equals("8 Passenger")) {

                                fareList.put("8", valuesOfFixedFare);

                            } else if (vehicleType.equals("6 Passenger")) {

                                fareList.put("6", valuesOfFixedFare);
                            }

                        }
                        //intentToBookingScreen();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    intentToBookingScreen(pickuploc,dropoffloc,con);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void extractAddressFromDestination(String address, Context con) {
        Geocoder mGeocoder = new Geocoder(con, Locale.getDefault());
        DataHolder.getInstance().destinationData.clear();
        DataHolder.getInstance().destinationDataLatLng.clear();
        List<Address> addresses = null;
        try {
            addresses = mGeocoder.getFromLocationName(address, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            addresses.get(0).getLocality();
            if (addresses.get(0).getPostalCode() == null) {
                address = String.valueOf(address);
                String[] tempOutCode = address.split(",");
                String[] splitoutCode = tempOutCode[0].split(" ");
                mDespostcode = tempOutCode[0];

                if (splitoutCode.length == 2) {
                    mDesOutcode = splitoutCode[1];
                } else {
                    mDesOutcode = "NPC";
                }

                latitude = addresses.get(0).getLatitude();
                deslat = latitude;

                longitude = addresses.get(0).getLongitude();
                deslon = longitude;

                DataHolder.getInstance().destinationData.add(0, address);
                DataHolder.getInstance().destinationData.add(1, mDesOutcode);
                if (mDespostcode.equals("") || mDespostcode == null) {
                    DataHolder.getInstance().destinationData.add(2, mDesOutcode);
                } else {
                    DataHolder.getInstance().destinationData.add(2, mDespostcode);
                }


                DataHolder.getInstance().destinationDataLatLng.add(0, deslat);
                DataHolder.getInstance().destinationDataLatLng.add(1, deslon);

            } else {
                String postalCode = addresses.get(0).getPostalCode();
                mDesaddress = String.valueOf(address);
                String[] retrivepostCode = postalCode.split(" ");
                mDesOutcode = retrivepostCode[0];
                // mDespostcode = retrivepostCode[1];
                mDespostcode = postalCode;

                latitude = addresses.get(0).getLatitude();
                deslat = latitude;

                longitude = addresses.get(0).getLongitude();
                deslon = longitude;

                DataHolder.getInstance().destinationData.add(0, address);
                DataHolder.getInstance().destinationData.add(1, mDesOutcode);
                DataHolder.getInstance().destinationData.add(2, mDespostcode);

                DataHolder.getInstance().destinationDataLatLng.add(0, deslat);
                DataHolder.getInstance().destinationDataLatLng.add(1, deslon);

            }
            if (latitude != 0.0 && longitude != 0.0) {
                LatLng lng = new LatLng(latitude, longitude);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(lng);
                mMap.moveCamera(cameraUpdate);
            }
        }
    }
    public void extractAddressFromOrigin(String address, Context con) {

        Geocoder mGeocoder = new Geocoder(con, Locale.getDefault());
        DataHolder.getInstance().originData.clear();
        DataHolder.getInstance().originDataLatLng.clear();
        List<Address> addresses = null;
        try {
            addresses = mGeocoder.getFromLocationName(address, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            addresses.get(0).getLocality();
            if (addresses.get(0).getPostalCode() == null) {
                address = String.valueOf(address);
                String[] tempOutCode = address.split(",");
                String[] splitoutCode = tempOutCode[0].split(" ");
                mpostcode = tempOutCode[0];
                if (splitoutCode.length == 2) {
                    mOutcode = splitoutCode[1];
                } else {
                    mOutcode = "NPC";
                }

                latitude = addresses.get(0).getLatitude();
                originlat = latitude;

                longitude = addresses.get(0).getLongitude();
                originlon = longitude;

                DataHolder.getInstance().originData.add(0, address);
                DataHolder.getInstance().originData.add(1, mOutcode);
                DataHolder.getInstance().originData.add(2, mpostcode);

                DataHolder.getInstance().originDataLatLng.add(0, originlat);
                DataHolder.getInstance().originDataLatLng.add(1, originlon);

            } else {
                String postalCode = addresses.get(0).getPostalCode();
                address = String.valueOf(address);
                String[] retrivepostCode = postalCode.split(" ");
                mOutcode = retrivepostCode[0];
//                mpostcode = retrivepostCode[1];
                mpostcode = postalCode;

                latitude = addresses.get(0).getLatitude();
                originlat = latitude;

                longitude = addresses.get(0).getLongitude();
                originlon = longitude;

                DataHolder.getInstance().originData.add(0, address);
                DataHolder.getInstance().originData.add(1, mOutcode);
                //DataHolder.getInstance().originData.add(2, mpostcode);
                if (mpostcode.equals("") || mpostcode == null) {
                    DataHolder.getInstance().originData.add(2, mOutcode);
                } else {
                    DataHolder.getInstance().originData.add(2, mpostcode);
                }


                DataHolder.getInstance().originDataLatLng.add(0, originlat);
                DataHolder.getInstance().originDataLatLng.add(1, originlon);

            }
            if (latitude != 0.0 && longitude != 0.0) {

            } else {

            }
        }
    }
    public void btnclicktobookingscreen(TextView pickuploc,TextView dropoffloc,Context con){
//		progressDialog = new ProgressDialog(con, R.style.MyDialogTheme);
        progressDialog.setTitle("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("finding routes ...");

        //intentToBookingScreen();
        BookingScreen.totalDuration = 0;
        BookingScreen.totalDistance = 0;


        if (TextUtils.isEmpty(pickuploc.getText().toString())) {


//                    OriginAndDestination.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
            new AlertDialog.Builder(con, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                    .setIcon(R.drawable.ic_error_black_24dp)
                    .setTitle("Empty Field")
                    .setMessage("Please enter pickup location")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).create().show();
            return;
            //}
//                    });

        } else if (TextUtils.isEmpty(dropoffloc.getText().toString())) {
//                    OriginAndDestination.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
            new AlertDialog.Builder(con, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                    .setIcon(R.drawable.ic_error_black_24dp)
                    .setTitle("Empty Field")
                    .setMessage("Please enter destination location")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).create().show();
            return;
//                        }
//                    });
        }
        else if (pickuploc.getText().toString().equals(dropoffloc.getText().toString())) {
            Toast.makeText(con, "Both addresses can't same", Toast.LENGTH_SHORT).show();
            return;
        } else if (Network.isNetworkAvailable(getApplicationContext())
                && pickuploc.getText().toString() != null &&
                dropoffloc.getText().toString() != null) {
            if (SocketEvent.sharedinstance.socket == null && Network.isNetworkAvailable(con)) {
                SocketEvent.sharedinstance.socketConnectAfterDisconnect();
                //  initializeApplication();
//				getCfgCustApp(this);
//				extractAddressFromOrigin(pickuploc.getText().toString(),con);
//				extractAddressFromDestination(dropoffloc.getText().toString(),con);
                //    while (SocketEvent.sharedinstance.socket == null){}
                RatesAccMileage();
                //	while (!ratesfilled && DataHolder.getInstance().originData.get(0)!=""){}
            }
            else
            {
                RatesAccMileage();
            }


            progressDialog.show();
            if (isAirportAtOri && isAirportAtDest) {
                placeToPlot(pickuploc,dropoffloc,con);
                //intentToBookingScreen(pickuploc,dropoffloc,con);

                //Toast.makeText(OriginAndDestination.this, "both ,so  placeToPlot", Toast.LENGTH_SHORT).show();
            } else if (isAirportAtOri && fromtoviaList.size()==0) {
                placeToPlot(pickuploc,dropoffloc,con);
                //intentToBookingScreen(pickuploc,dropoffloc,con);

                //Toast.makeText(OriginAndDestination.this, "placeToPlot", Toast.LENGTH_SHORT).show();
            } else if (isAirportAtDest && fromtoviaList.size()==0) {
                plotToPlace(dropoffloc,pickuploc,con);
                //intentToBookingScreen(pickuploc,dropoffloc,con);

                //Toast.makeText(OriginAndDestination.this, "plotToPlace", Toast.LENGTH_SHORT).show();
            } else {
                progressDialog.show();
                intentToBookingScreen(pickuploc,dropoffloc,con);
                //Toast.makeText(OriginAndDestination.this, "simple" , Toast.LENGTH_SHORT).show();

            }

        } else {
            snackbarForConnectivityStatus();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();

    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        context = this;


        parentLayout = findViewById(android.R.id.content);
        db = new SQLiteDatabaseHandler(this);


        getSharedPrefenceIDdrv();
    }

    //this method store LatLong and pass on Mapready
    public void storeLatLong() {
        try {
            gpsTracker = new GPStracker(this);
            mLocation = gpsTracker.getLocation();

            latitude = mLocation.getLatitude();
            longitude = mLocation.getLongitude();

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

    }


    //this method run to access all permissions for app
    public void initializeApplication() {


        if (!Network.sharedInstance.isNetworkAvailable(getApplicationContext())) {

            alertOk("Network problem", "Network not available.Connect your Network.");
        }

        new Connect(BaseActivity.this);

        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {

            if (checkLocationPermission() && checkPhoneStatePermission() && checkWriteStoragePermission() && checkReadStoragePermission()) {

                initializeGAPIClient();
                mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            } else {
                requestAllPermissions();
            }
        } else {
            mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            initializeGAPIClient();
        }

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion <= Build.VERSION_CODES.JELLY_BEAN) {
            // Do something for lollipop and above versions
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            locationManager.sendExtraCommand(LocationManager.GPS_PROVIDER, "delete_aiding_data", null);
            Bundle bundle1 = new Bundle();
            locationManager.sendExtraCommand("gps", "force_xtra_injection", bundle1);
            locationManager.sendExtraCommand("gps", "force_time_injection", bundle1);
        } else {
            // do something for phones running an SDK before lollipop
        }

    }

    public boolean checkLocationPermission() {
        int result = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
//            storeLatLong();
            return true;

        } else {
            return false;
        }
    }

    private boolean checkPhoneStatePermission() {
        int result = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;

        }
    }

    private boolean checkWriteStoragePermission() {
        int result = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStoragePermission() {
        int result = ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void initializeGAPIClient() {
        try {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addApi(SafetyNet.API)
                    .addApi(ActivityRecognition.API)
                    .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                    .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                    .build();

            googleApiClient.connect();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    private void requestAllPermissions() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.READ_PHONE_STATE) ||
                ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            //Toast.makeText(context,"GPS permission allows us to access location data. Please allow in App Settings for additional functionality.",Toast.LENGTH_LONG).show();
            //turnGPSOnFromSettings();
            ActivityCompat.requestPermissions(BaseActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_ALLPERMISSIONS_CODE);

        } else {

            ActivityCompat.requestPermissions(BaseActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_ALLPERMISSIONS_CODE);
        }
    }

    public void alertOk(final String title, final String message) {

        if (!((Activity) BaseActivity.this).isFinishing()) {

            try {

                BaseActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new android.app.AlertDialog.Builder(BaseActivity.this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                                .setIcon(R.drawable.ic_audiotrack_light)
                                .setTitle(title)
                                .setMessage(message)
                                .setCancelable(false)
                                .setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).create().show();
                    }
                });

            } catch (Exception e) {
                e.getMessage();
            }

        }
    }

    public void nearstOfficeLocation() {
        if (Network.isNetworkAvailable(getApplicationContext())) {
            if (SocketEvent.sharedinstance.socket == null) {
                SocketEvent.sharedinstance.initializeSocket();
            }
            final Timer timer = new Timer();
            timer.schedule(new TimerTask() {

                @Override
                public void run() {
                    if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(getApplicationContext())) {
                        timer.cancel();
                        JSONArray query;
                        query = new JSONArray();
                        JSONObject filter = new JSONObject();
                        try {
                            filter.put("officename", "CustomerApp");

                            query.put("CfgCustApp");
                            query.put(filter);

                            SocketEvent.sharedinstance.socket.emit("getdata", query, new Ack() {
                                @Override
                                public void call(final Object... args) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(args[0].toString());

                                        JsonNode jsonNode = JSONParse.convertJsonFormat(jsonObject);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        CustomerConfig.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), CustomerConfig.class);

                                        JSONArray dataString = (JSONArray) jsonObject.get("officePosition");
                                        JSONObject officeposition = (JSONObject) dataString.get(0);

//1.CYP
                                        JSONArray cypCordinate = officeposition.getJSONArray(FIXED_OFFICE_KEY);
                                        cyplat = (double) cypCordinate.get(0);
                                        cyplon = (double) cypCordinate.get(1);
                                        CustomerConfig.sharedinstance.setNearestOfficeName(FIXED_OFFICE_KEY);
                                        CustomerConfig.sharedinstance.setNearestOfficeLat(cyplat);
                                        CustomerConfig.sharedinstance.setNearestOfficeLon(cyplon);

                                        DataHolder.getInstance().nearestOfficeName.add(0, FIXED_OFFICE_KEY);
                                        DataHolder.getInstance().nearestOfficeLatLng.add(0, cyplat);
                                        DataHolder.getInstance().nearestOfficeLatLng.add(1, cyplon);


                                        //CustomerConfig.setNearestOfficeNumber("123");
//
//
////2. WCP
//                                        JSONArray wcpCordinate = officeposition.getJSONArray("WCP");
//                                        wcplat = (double) wcpCordinate.get(0);
//                                        wcplon = (double) wcpCordinate.get(1);
//                                        CustomerConfig.sharedinstance.setNearestOfficeName("WCP");
//                                        CustomerConfig.sharedinstance.setNearestOfficeLat(wcplat);
//                                        CustomerConfig.sharedinstance.setNearestOfficeLon(wcplon);
//
//                                        //CustomerConfig.setNearestOfficeNumber("234");
//
//                                        DataHolder.getInstance().nearestOfficeName.add(1, "WCP");
//                                        DataHolder.getInstance().nearestOfficeLatLng.add(2, wcplat);
//                                        DataHolder.getInstance().nearestOfficeLatLng.add(3, wcplon);
//
////3. NBT
//
//                                        JSONArray nbtCordinate = officeposition.getJSONArray("NBT");
//                                        nbtlat = (double) nbtCordinate.get(0);
//                                        nbtlon = (double) nbtCordinate.get(1);
//                                        CustomerConfig.sharedinstance.setNearestOfficeName("NBT");
//                                        CustomerConfig.sharedinstance.setNearestOfficeLat(nbtlat);
//                                        CustomerConfig.sharedinstance.setNearestOfficeLon(nbtlon);
//
//                                        // CustomerConfig.setNearestOfficeNumber("345");
//
//                                        DataHolder.getInstance().nearestOfficeName.add(2, "NBT");
//                                        DataHolder.getInstance().nearestOfficeLatLng.add(4, nbtlat);
//                                        DataHolder.getInstance().nearestOfficeLatLng.add(5, nbtlon);
//
////4. SUR
//                                        JSONArray surCordinate = officeposition.getJSONArray("SUR");
//                                        surlat = (double) surCordinate.get(0);
//                                        surlon = (double) surCordinate.get(1);
//                                        CustomerConfig.sharedinstance.setNearestOfficeName("SUR");
//                                        CustomerConfig.sharedinstance.setNearestOfficeLat(surlat);
//                                        CustomerConfig.sharedinstance.setNearestOfficeLon(surlon);
//
//
//                                        DataHolder.getInstance().nearestOfficeName.add(3, "SUR");
//                                        DataHolder.getInstance().nearestOfficeLatLng.add(6, surlat);
//                                        DataHolder.getInstance().nearestOfficeLatLng.add(7, surlon);
//
//                                        //CustomerConfig.setNearestOfficeNumber("456");
//
////5. NBA
//                                        JSONArray nbaCordinate = officeposition.getJSONArray("NBA");
//                                        nbalat = (double) nbaCordinate.get(0);
//                                        nbalon = (double) nbaCordinate.get(1);
//                                        CustomerConfig.sharedinstance.setNearestOfficeName("NBA");
//                                        CustomerConfig.sharedinstance.setNearestOfficeLat(nbalat);
//                                        CustomerConfig.sharedinstance.setNearestOfficeLon(nbalon);
//
//                                        //CustomerConfig.setNearestOfficeNumber("567");
//
//                                        DataHolder.getInstance().nearestOfficeName.add(4, "NBA");
//                                        DataHolder.getInstance().nearestOfficeLatLng.add(8, nbalat);
//                                        DataHolder.getInstance().nearestOfficeLatLng.add(9, nbalon);
//
////6. XYZ
//                                        JSONArray xyzCordinate = officeposition.getJSONArray("XYZ");
//                                        xyzlat = (double) xyzCordinate.get(0);
//                                        xyzlon = (double) xyzCordinate.get(1);
//                                        CustomerConfig.sharedinstance.setNearestOfficeName("XYZ");
//                                        CustomerConfig.sharedinstance.setNearestOfficeLat(xyzlat);
//                                        CustomerConfig.sharedinstance.setNearestOfficeLon(xyzlon);
//
//                                        DataHolder.getInstance().nearestOfficeName.add(5, "XYZ");
//                                        DataHolder.getInstance().nearestOfficeLatLng.add(10, xyzlat);
//                                        DataHolder.getInstance().nearestOfficeLatLng.add(11, xyzlon);
//                                        //CustomerConfig.setNearestOfficeNumber("678");

                                    } catch (JSONException ex) {
                                        ex.printStackTrace();
                                    } catch (JsonParseException e) {
                                        e.printStackTrace();
                                    } catch (JsonMappingException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }

            }, 100, 1000);
        } else {

            Toast.makeText(BaseActivity.this, "Internet not Connected", Toast.LENGTH_SHORT).show();

        }
    }


    public void RatesAccMileage() {

        JSONArray query = new JSONArray();
        JSONObject filter = new JSONObject();

        query.put("RatesAccMileage");

        try {
            filter.put("OfficeID", FIXED_OFFICE_KEY);
            filter.put("RateName", "CASH");
            query.put(filter);

            SocketEvent.sharedinstance.socket.emit("getmultipledata", query, new Ack() {

                @Override
                public void call(final Object... args) {

                    if (args[0] != null && !args[0].toString().equals("0")) {

                        try {
                            db.deleteFareData();
                            JSONArray jsonArray = new JSONArray(args[0].toString());
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject job = jsonArray.getJSONObject(i);


                                JsonNode jsonNode = JSONParse.convertJsonFormat(job);
                                ObjectMapper mapper = new ObjectMapper();
                                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

                                FareListMapping.sharedInstanceFareList = mapper.readValue(new TreeTraversingParser(jsonNode), FareListMapping.class);

                                db.addFareDataToSQLite(FareListMapping.sharedInstanceFareList);

                            }
                            ratesfilled=true;
                        } catch (Exception e) {
                            ratesfilled=true;
                            e.getMessage();
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //get current generic date
    public String getDate(double myTimestamp) {
        Date time = new Date((long) myTimestamp * 1000);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String date = sdf.format(time);

        return date;
    }

    //get current generic time
    public final double getBST() {
        return (System.currentTimeMillis() + DateTimeZone.forID("Europe/London").getOffset(new Instant())) / 1000L;
    }

    public String gettime(double myTimestamp) {
        Date time = new Date((long) myTimestamp * 1000);

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String date = sdf.format(time);

        return date;
    }

    private String getDateTimeToHexa() {
        Calendar mCalendar = Calendar.getInstance();
        TimeZone gmtTime = TimeZone.getTimeZone(TimeZone.getDefault().getDisplayName());
        mCalendar.setTimeZone(gmtTime);
        final Date date = mCalendar.getTime();
        return Long.toHexString(date.getTime() / 1000);
    }

    @TargetApi(Build.VERSION_CODES.N)
    public String getJobRef(String hexPhone) throws NumberFormatException, UnsupportedEncodingException {

        //String ref = "" + DataHolder.getInstance().namesOfNearOffice.get(0) + "GA-" + hexPhone + "@";
        String ref = "" + FIXED_OFFICE_KEY + "LA-" + hexPhone + "@";

        ref += getDateTimeToHexa();
        return ref;
    }

    public String getDeviceID() {
        try {
            mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            deviceID = mngr.getDeviceId();


        } catch (Exception e) {
            alertOk("Driver Exception", e.getMessage());
        }

        CustomerReg.sharedinstance.setCust_uid(deviceID);

        return deviceID;
    }

    public void autoDismissDialogue(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.alertinfo);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        final AlertDialog closedialog = builder.create();

        closedialog.show();

        final Timer timer2 = new Timer();
        timer2.schedule(new TimerTask() {
            public void run() {
                closedialog.dismiss();
                timer2.cancel(); //this will cancel the timer of the system
            }
        }, 3000); // the timer will count 5 seconds....
    }

    public String getSharedPrefPhone() {

        SharedPreferences getPrefrence = getSharedPreferences("", Context.MODE_PRIVATE);
        String getSharedPrefPhone = getPrefrence.getString("cust_phone", "");
        return getSharedPrefPhone;
    }

    public String getSharedPrefEmail() {

        SharedPreferences getPrefrence = getSharedPreferences("", Context.MODE_PRIVATE);
        String getSharedPrefEmail = getPrefrence.getString("cust_email", "");
        return getSharedPrefEmail;
    }

    public String getSharedPrefName() {

        SharedPreferences getPrefrence = getSharedPreferences("", Context.MODE_PRIVATE);
        String getSharedPrefName = getPrefrence.getString("cust_name", "");
        return getSharedPrefName;
    }

    public void setNumberSharedPreference(String phone, String email, String name) {

        //this is for store value in sharePreferences

        String sharedPrefPhone = String.valueOf(phone);
        String sharedPrefEmail = email;
        String sharedPrefName = name;
        SharedPreferences setPrefrence = getSharedPreferences("", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = setPrefrence.edit();
        edit.putString("cust_phone", sharedPrefPhone);
        edit.putString("cust_email", sharedPrefEmail);
        edit.putString("cust_name", sharedPrefName);

        edit.apply();

    }

    //generic function for internet connect and disconnect status

    public void snackbarForConnectivityStatus() {
        try {
            IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
            networkStateChangeReceiver = new NetworkStateChangeReceiver();

            this.registerReceiver(networkStateChangeReceiver, intentFilter);

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    SharedPreferences setPrefrence;
    @Override
    public void recieveMsgOnNetwork(String message) {
        if (message.equals("Connected") && status == 1) {

            setPrefrence = getSharedPreferences(Rating.prefratingname, Context.MODE_PRIVATE);
            if (!setPrefrence.getString(Rating.prefdriverid, "").equals("") && !setPrefrence.getString(Rating.prefdriverfare, "").equals("")) {
                Intent intent = new Intent(getApplicationContext(), RatingScreen.class);
                startActivity(intent);
            }

            snackbarOnInternet();
        } else if (message.equals("Disonnected")) {
            status = 1;
            snackbarOnNoInternet();
        }
    }

    private void snackbarOnInternet() {

        Snackbar snackbar = Snackbar
                .make(parentLayout, "Connected!", Snackbar.LENGTH_LONG);

//        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.GREEN);
        snackbar.show();
    }

    private void snackbarOnNoInternet() {
        Snackbar snackbar = Snackbar
                .make(parentLayout, "Check your internet connection!", Snackbar.LENGTH_INDEFINITE);

//        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.RED);
        snackbar.show();
    }

    @Override
    protected void onDestroy() {
        if (networkStateChangeReceiver != null) {
            this.unregisterReceiver(networkStateChangeReceiver);
        }
        super.onDestroy();


    }

    public void getSharedPrefenceIDdrv() {


        SharedPreferences preferences = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
        officeId = preferences.getString("officeId", "");
        drvCallsign = preferences.getString("drvcallsign", "");

    }

//	@RequiresApi(api = Build.VERSION_CODES.KITKAT)
//	public void detectJobDone() {
//
//
//
//		try {
//			ids = db.getids("Booked");
//			jobid = Objects.requireNonNull(getIntent().getExtras()).getString("jobid");
//			pos = Objects.requireNonNull(getIntent().getExtras()).getInt("pos");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		boolean retval = (ids.contains(jobid));
//
//
//		if (retval) {
//			if (jobid != null && pos >= 0) {
//				removeData(pos);
//			}
//			//inserting completed booking in SQLite
//			db.insertBookingForHistory(DataHolder.getInstance().bookingdate,
//					DataHolder.getInstance().bookingtime, DataHolder.getInstance().origin,
//					DataHolder.getInstance().destination, DataHolder.getInstance().cusfare,
//					"Completed", DataHolder.getInstance().jobID, DataHolder.getInstance().jobReference, DataHolder.getInstance().unixdateNtime);
//		}
//		db.insertBookingForHistory(DataHolder.getInstance().bookingdate,
//				DataHolder.getInstance().bookingtime, DataHolder.getInstance().origin,
//				DataHolder.getInstance().destination, DataHolder.getInstance().cusfare,
//				"Completed", DataHolder.getInstance().jobID, DataHolder.getInstance().jobReference, DataHolder.getInstance().unixdateNtime);
//
////		SharedPreferences setPrefrence = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
////		editor = setPrefrence.edit();
////		editor.putString("jobid", "");
////		editor.putString("drvcallsign", "");
////		editor.putString("officeId", "");
////		editor.putString("drvImageIp", "");
////		editor.putString("jobRef", "");
////		editor.apply();
////		officeId = "";
////		drvCallsign = "";
//
//
//
//		finish();
//		DataHolder.getInstance().clearAllDataHolders();
//		Intent intent = new Intent(this, RatingScreen.class);
//		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		this.startActivity(intent);
//	}


    public void dropbreadCrumb(String category, String action, String label) {

        String breadCrumb = String.format("%s|%s|%s", category, action, label);

        Crashlytics.log(breadCrumb);

    }
//    public void getDozeMode(){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            BroadcastReceiver  receiver = new BroadcastReceiver() {
//                @RequiresApi(api = Build.VERSION_CODES.M) @Override public void onReceive(Context context, Intent intent) {
//                    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//
//                    if (pm.isDeviceIdleMode()) {
//                        // the device is now in doze mode
//                        Log.d("","Doze Enable");
//                    } else {
//                        // the device just woke up from doze mode
//                        Log.d("","Doze False");
//                    }
//                }
//            };
//
//            this.registerReceiver(receiver, new IntentFilter(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED));
//        }
//    }

    double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.CEILING);
        return bd.doubleValue();
    }
    public void startAlarmBroadcastReceiver(Context context, long delay) {
        Intent _intent = new Intent(context, AlarmBroadcastReceiver.class);
        //  Bundle extras= new Bundle();
//		extras.putString("futurebooking","1");
//		extras.putString("vehType",DataHolder.getInstance().vehType);
//		extras.putString("originLng",String.valueOf(DataHolder.getInstance().originLng));
//		extras.putString("originLat",String.valueOf(DataHolder.getInstance().originLat));
//		extras.putString("origin", DataHolder.getInstance().origin);
//		extras.putString("destination",DataHolder.getInstance().destination);
//		extras.putString("_fromOutcode", DataHolder.getInstance()._fromOutcode);
//		extras.putString("jobID",DataHolder.getInstance().jobID);
//		extras.putString("jobReference",DataHolder.getInstance().jobReference );
//		extras.putString("telephone",   DataHolder.getInstance().telephone );
//		extras.putString("jobinoffice", CfgCustApp.sharedinstance.getJobInOffice());
//		extras.putString("drvImageIp", CustomerConfig.sharedinstance.getImageIP());
//		_intent.putExtras(extras);

        SharedPreferences sharedPreferences = getSharedPreferences("localnoti", Context.MODE_PRIVATE);
        SharedPreferences.Editor extras = sharedPreferences.edit();
        extras.putString("futurebooking","0");
        extras.putString("vehType",DataHolder.getInstance().vehType);
        extras.putString("originLng",String.valueOf(DataHolder.getInstance().originLng));
        extras.putString("originLat",String.valueOf(DataHolder.getInstance().originLat));
        extras.putString("origin", DataHolder.getInstance().origin);
        extras.putString("destination",DataHolder.getInstance().destination);
        extras.putString("_fromOutcode", DataHolder.getInstance()._fromOutcode);
        extras.putString("jobID",DataHolder.getInstance().jobID);
        extras.putString("jobReference",DataHolder.getInstance().jobReference );
        extras.putString("telephone",   DataHolder.getInstance().telephone );
        extras.putString("jobinoffice", CfgCustApp.sharedinstance.getJobInOffice());
        extras.putString("drvImageIp", CustomerConfig.sharedinstance.getImageIP());
        extras.apply();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, _intent, 0);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        // Remove any previous pending intent.
        alarmManager.cancel(pendingIntent);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delay, pendingIntent);
    }
}

