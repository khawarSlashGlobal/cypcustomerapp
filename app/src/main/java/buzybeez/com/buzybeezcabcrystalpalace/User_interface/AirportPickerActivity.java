package buzybeez.com.buzybeezcabcrystalpalace.User_interface;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import buzybeez.com.buzybeezcabcrystalpalace.DataHandler.SQLiteAirportList;
import buzybeez.com.buzybeezcabcrystalpalace.Helpers.AirportSelectionAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Airports;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.GPStracker;

import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.dropoffloc;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.pickuploc;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.dropoffloc;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.isAirportAtDest;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.isAirportAtOri;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.pickuploc;

public class AirportPickerActivity extends BaseActivity implements
        ConnectionListener,
        GoogleMap.OnCameraIdleListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener ,GoogleMap.OnCameraMoveListener {

    public static double lat;
    public static double lng;
    public static boolean flag;
    GPStracker gpStracker;
    LatLng latLng;
    private ImageButton myLocationButton;
    private  RecyclerView airportRecyclerView;
    public RecyclerView.Adapter mAdapterAirports;
    GoogleMap mMap;
    ArrayList<Marker> airportsMarkers = new ArrayList<>();
    ImageView imgCancelSearch;
    AutoCompleteTextView tvSearchAirports;
    String selectedAirport = "";
    // int switchScreens = 0;
    TextView selectionMsg,originOption,destionationOption;
    ConstraintLayout selectionMsgCL;
    ArrayList<Airports> airportDataList = new ArrayList<>();
    SQLiteAirportList airportDbReff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airport_picker);
        airportDbReff = new SQLiteAirportList(this);
        airportDataList = airportDbReff.getAllAirports();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        try {
            gpStracker = new GPStracker(this);
            if (getLocationMode(this) == 1) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            } else {
                snackbarForConnectivityStatus();
                screenObjects_id();
                setUpAirportSelectionMsg();
                MylocationBtnClick();

            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        setupRecyclerView();
    }

    private void setupAirportmarkers(ArrayList<Airports> airportDataList) {

        for (Airports a:airportDataList
        ) {
            airportsMarkers.add(mMap.addMarker(new MarkerOptions().position( new LatLng(a.getLatitude(),a.getLongitude())).flat(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.airport)).title(a.getPlace()).snippet(a.getPostcode())));
        }
    }

    public void screenObjects_id() {

        imgCancelSearch = findViewById(R.id.cancel_search);
        tvSearchAirports = findViewById(R.id.searchAirports);
        airportRecyclerView =(RecyclerView) findViewById(R.id.airportRecyclerView);
        tvSearchAirports.clearFocus();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                setupAirportmarkers(airportDataList);
                mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        moveToBounds(airportDataList);
                    }
                });
                mMap.setPadding(0,0,0,500);
            }
        });
    }

    private void setUpAirportSelectionMsg() {
        selectionMsgCL = findViewById(R.id.selectionMsgCL);
        selectionMsgCL.setVisibility(View.GONE);
        selectionMsg = findViewById(R.id.aName);
        originOption = findViewById(R.id.OriginBTN);
        originOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                isAirportAtOri = true;
                Intent returnIntent = new Intent();
                returnIntent.putExtra("action","pickup");
                setResult(Activity.RESULT_OK,returnIntent);
                pickuploc.setText(selectedAirport);
                finish();
            }
        });
        destionationOption = findViewById(R.id.DesitinationBTN);
        destionationOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                switchScreens = 2;
//                Intent intent = new Intent(AirportPickerActivity.this, OriginAndDestination.class);
//                intent.putExtra("airportDest", selectedAirport);
//                intent.putExtra("flag", switchScreens);
//                startActivity(intent);
//                finish();
//                selection = 2;

                isAirportAtDest = true;
                Intent returnIntent = new Intent();
                returnIntent.putExtra("action","dropoffloc");
                setResult(Activity.RESULT_OK,returnIntent);
                dropoffloc.setText(selectedAirport);
                finish();
            }
        });
    }

    public int getLocationMode(Context context) throws Settings.SettingNotFoundException {
        return Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
    }

    public void animateCameraLatLng(LatLng latLng) {
        //storeLatLong();
        if (latLng.latitude != 0.0) {
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15.0f).build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            mMap.animateCamera(cameraUpdate);
        }
    }
    public void updateSelection(Airports selectedAirportData) {
        selectedAirport = selectedAirportData.getName();
        selectedAirportData.setSelected(true);
        for (Airports a : airportDataList) {
            if (a.getPostcode().equals(selectedAirportData.getPostcode())) {
            } else {
                a.setSelected(false);
            }
        }
        selectionMsgCL.setVisibility(View.VISIBLE);
        selectionMsg.setText("Set "+selectedAirportData.getName().toUpperCase()+ " as ?");
        mAdapterAirports.notifyDataSetChanged();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView() {
        airportRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mAdapterAirports = new AirportSelectionAdapter(airportDataList, new AirportSelectionAdapter.OnAirportItemClickListener() {
            @Override
            public void onAirportItemClick(Airports selectedAirportData, View v,int position) {
                updateSelection(selectedAirportData);
                onViewSnapped( selectedAirportData,position,v);
                airportRecyclerView.scrollToPosition(position);
                mAdapterAirports.notifyDataSetChanged();
            }
        });
        airportRecyclerView.setAdapter(mAdapterAirports);
        mAdapterAirports.notifyDataSetChanged();
        final SnapHelper snapHelper = new LinearSnapHelper() {
            int  selectedPosition = 0;
            @Override
            public View findSnapView(RecyclerView.LayoutManager layoutManager) {
                View view = super.findSnapView(layoutManager);
                if (view != null) {
                    final int newPosition = layoutManager.getPosition(view);
                    if (newPosition != selectedPosition && airportRecyclerView.getScrollState() == RecyclerView.SCROLL_STATE_IDLE) {
                        onViewSnapped( airportDataList.get(newPosition),newPosition,view);
                        updateSelection( airportDataList.get(newPosition));
                        selectedPosition = newPosition;
                    }
                }
                return view;
            }
        };
        snapHelper.attachToRecyclerView(airportRecyclerView);
        airportRecyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {
                selectionMsgCL.setVisibility(View.GONE);
                return false;
            }
        });
    }

    private void onViewSnapped(Airports snapedAirportData,int snapedIndex,View view) {
        animateCameraLatLng(new LatLng(snapedAirportData.getLatitude(),snapedAirportData.getLongitude()));
        airportsMarkers.get(snapedIndex).setTitle(snapedAirportData.getPlace());
        airportsMarkers.get(snapedIndex).setSnippet(snapedAirportData.getPostcode());
        airportsMarkers.get(snapedIndex).showInfoWindow();
    }

    //    setting the camera bounds to fit route in single screen
    private void moveToBounds(ArrayList<Airports> airports) {

        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (int i = 0; i < airports.size(); i++) {
                builder.include(new LatLng(airports.get(i).getLatitude(),airports.get(i).getLongitude()));
            }
            LatLngBounds bounds = builder.build();
            int padding = 40; // offset from edges of the map in pixels
            if (bounds != null) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
            }
            setUpAirportSelectionMsg();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public void MylocationBtnClick() {
        myLocationButton = (ImageButton) findViewById(R.id.targetlocation);
        gpStracker = new GPStracker(AirportPickerActivity.this);
        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectionMsgCL.getVisibility()==View.VISIBLE){
                    selectionMsgCL.setVisibility(View.GONE);
                }
                moveToBounds(airportDataList);
            }
        });

    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(AirportPickerActivity.this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            break;

                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            try {
                                status.startResolutionForResult(AirportPickerActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                            }
                            break;

                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Toast.makeText(AirportPickerActivity.this, "Setting change not allowed", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


    }

    public void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ALLPERMISSIONS_CODE);

        } else {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ALLPERMISSIONS_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case REQUEST_ALLPERMISSIONS_CODE: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    EnableGPSAutoMatically();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCameraIdle() {
        selectionMsgCL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCameraMove() {
        selectionMsgCL.setVisibility(View.GONE);
    }

    public class CenterZoomLayoutManagerAirport extends LinearLayoutManager {

        private final float mShrinkAmount = 0.3f;
        private final float mShrinkDistance = 0.2f;

        public CenterZoomLayoutManagerAirport(Context context) {
            super(context);
        }

        public CenterZoomLayoutManagerAirport(Context context, int orientation, boolean reverseLayout) {
            super(context, orientation, reverseLayout);
        }

        @Override
        public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
            int orientation = getOrientation();
            if (orientation == VERTICAL) {
                int scrolled = super.scrollVerticallyBy(dy, recycler, state);
                float midpoint = getHeight() / 2.f;
                float d0 = 0.f;
                float d1 = mShrinkDistance * midpoint;
                float s0 = 1.f;
                float s1 = 1.f - mShrinkAmount;
                for (int i = 0; i < getChildCount(); i++) {
                    View child = getChildAt(i);
                    float childMidpoint =
                            (getDecoratedBottom(child) + getDecoratedTop(child)) / 2.f;
                    float d = Math.min(d1, Math.abs(midpoint - childMidpoint));
                    float scale = s0 + (s1 - s0) * (d - d0) / (d1 - d0);
                    child.setScaleX(scale);
                    child.setScaleY(scale);
                }
                return scrolled;
            } else {
                return 0;
            }
        }

        @Override
        public int scrollHorizontallyBy(int dx, RecyclerView.Recycler recycler, RecyclerView.State state) {
            int orientation = getOrientation();
            if (orientation == HORIZONTAL) {
                int scrolled = super.scrollHorizontallyBy(dx, recycler, state);

                float midpoint = getWidth() / 2.f;
                float d0 = 0.f;
                float d1 = mShrinkDistance * midpoint;
                float s0 = 1.f;
                float s1 = 1.f - mShrinkAmount;
                for (int i = 0; i < getChildCount(); i++) {
                    View child = getChildAt(i);
                    float childMidpoint =
                            (getDecoratedRight(child) + getDecoratedLeft(child)) / 2.f;
                    float d = Math.min(d1, Math.abs(midpoint - childMidpoint));
                    float scale = s0 + (s1 - s0) * (d - d0) / (d1 - d0);
                    child.setScaleX(scale+0.0f);
                    child.setScaleY(scale+0.0f);
                }
                return scrolled;
            } else {
                return 0;
            }

        }
    }
}