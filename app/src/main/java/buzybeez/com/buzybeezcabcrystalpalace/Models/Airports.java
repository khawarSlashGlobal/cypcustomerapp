package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Airports {

    //Airports singleton
    public static Airports sharedinstance = new Airports();
    public static Airports getInstance() {
        return sharedinstance;
    }

    //this "isSelected" field is only for recyclerview, it does not map to database.
    @JsonIgnoreProperties
    private boolean isSelected;
    public boolean isSelected() {
        return isSelected;
    }
    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    @JsonProperty("_id")
    private String Id;

    @JsonProperty("Country")
    private String Country;

    @JsonProperty("OfficeId")
    private String OfficeId;

    @JsonProperty("Name")
    private String Name;

    @JsonProperty("ShortName")
    private String ShortName;

    @JsonProperty("Plot")
    private String Plot;

    @JsonProperty("Place")
    private String Place;

    @JsonProperty("Postcode")
    private String Postcode;

    @JsonProperty("Latitude")
    private double Latitude;

    @JsonProperty("Longitude")
    private double Longitude;

    public Airports(String id, String country, String officeId, String name, String shortName, String plot, String place, String postcode, double latitude, double longitude) {
        Id = id;
        Country = country;
        OfficeId = officeId;
        Name = name;
        ShortName = shortName;
        Plot = plot;
        Place = place;
        Postcode = postcode;
        Latitude = latitude;
        Longitude = longitude;
    }

    public Airports() {
    }


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getOfficeId() {
        return OfficeId;
    }

    public void setOfficeId(String officeId) {
        OfficeId = officeId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getShortName() {
        return ShortName;
    }

    public void setShortName(String shortName) {
        ShortName = shortName;
    }

    public String getPlot() {
        return Plot;
    }

    public void setPlot(String plot) {
        Plot = plot;
    }

    public String getPlace() {
        return Place;
    }

    public void setPlace(String place) {
        Place = place;
    }

    public String getPostcode() {
        return Postcode;
    }

    public void setPostcode(String postcode) {
        Postcode = postcode;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }
}
//              Airports
//              "_id" : ObjectId("5a9e469fa7847484fd45a30b"),
//              "Country" : "UK",
//              "OfficeId" : "NBA",
//              "Name" : "LONDON HEATHROW AIRPORT TERMINAL 1 TW6 1AP",
//              "ShortName" : "LHR1",
//              "Plot" : "TW6",
//              "Place" : "LONDON HEATHROW AIRPORT TERMINAL 1",
//              "Postcode" : "TW6 1AP",
//              "Latitude" : 51.479392,
//              "Longitude" : -0.4544963