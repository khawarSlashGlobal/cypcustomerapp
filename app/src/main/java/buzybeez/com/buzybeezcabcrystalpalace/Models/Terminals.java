package buzybeez.com.buzybeezcabcrystalpalace.Models;

/**
 * Created by visibility on 30/11/2016.
 */

public class Terminals {
    String postcode;
    int lhr1, lhr4, lgw, ltn, std, lcy;

    public Terminals(String postcode, int lhr1, int lhr4, int lgw, int ltn, int std, int lcy) {
        this.postcode = postcode;
        this.lhr1 = lhr1;
        this.lhr4 = lhr4;
        this.lgw = lgw;
        this.ltn = ltn;
        this.std = std;
        this.lcy = lcy;
    }

    public Terminals() {

    }

    public void setPostCode(String postcode) {
        this.postcode = postcode;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public int getLhr1() {
        return this.lhr1;
    }

    public void setLhr1(int lhr) {
        this.lhr1 = lhr;
    }

    public int getLhr4() {
        return this.lhr4;
    }

    public void setLhr4(int lhr) {
        this.lhr4 = lhr;
    }

    public int getLgw() {
        return this.lgw;
    }

    public void setLgw(int lgw) {
        this.lgw = lgw;
    }

    public int getLtn() {
        return this.ltn;
    }

    public void setLtn(int ltn) {
        this.ltn = ltn;
    }

    public int getStd() {
        return this.std;
    }

    public void setStd(int std) {
        this.std = std;
    }

    public int getLcy() {
        return this.lcy;
    }

    public void setLcy(int lcy) {
        this.lcy = lcy;
    }


}
