package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hv on 5/10/15 DriverApp3.
 */
public class Favorite {

    @JsonProperty("Oinfo")
    private Object Oinfo;
    @JsonProperty("Dinfo")
    private Object Dinfo;
    @JsonProperty("Oaddress")
    private Object Oaddress;
    @JsonProperty("Daddress")
    private Object Daddress;
    @JsonProperty("olat")
    private double Olat;
    @JsonProperty("olon")
    private double Olon;
    @JsonProperty("dlat")
    private double Dlat;
    @JsonProperty("dlon")
    private double Dlon;

    /**
     * @return The info
     */
    @JsonProperty("Oinfo")
    public Object getOriginInfo() {
        return Oinfo;
    }

    /**
     * @param info The info
     */
    @JsonProperty("Oinfo")
    public void setOriginInfo(Object info) {
        this.Oinfo = info;
    }

    @JsonProperty("Dinfo")
    public Object getDestInfo() {
        return Dinfo;
    }

    /**
     * @param info The info
     */
    @JsonProperty("Dinfo")
    public void setDestInfo(Object info) {
        this.Dinfo = info;
    }

    /**
     * @return The address
     */
    @JsonProperty("Oaddress")
    public Object getOriginAddress() {
        return Oaddress;
    }

    /**
     * @param address The address
     */
    @JsonProperty("Oaddress")
    public void setOriginAddress(Object address) {
        this.Oaddress = address;
    }

    @JsonProperty("Daddress")
    public Object getDestAddress() {
        return Daddress;
    }

    /**
     * @param address The address
     */
    @JsonProperty("Daddress")
    public void setDestAddress(Object address) {
        this.Daddress = address;
    }

    /**
     * @return The lat
     */
    @JsonProperty("olat")
    public double getOriginLat() {
        return Olat;
    }

    /**
     * @param lat The lat
     */
    @JsonProperty("olat")
    public void setOriginLat(double lat) {
        this.Olat = lat;
    }

    @JsonProperty("dlat")
    public double getDestLat() {
        return Dlat;
    }

    /**
     * @param lat The lat
     */
    @JsonProperty("dlat")
    public void setDestLat(double lat) {
        this.Dlat = lat;
    }

    /**
     * @return The lon
     */
    @JsonProperty("olon")
    public double getOriginLon() {
        return Olon;
    }

    /**
     * @param lon The lon
     */
    @JsonProperty("olon")
    public void setOriginLon(double lon) {
        this.Olon = lon;
    }

    @JsonProperty("dlon")
    public double getDestLon() {
        return Dlon;
    }

    /**
     * @param lon The lon
     */
    @JsonProperty("dlon")
    public void setDestLon(double lon) {
        this.Dlon = lon;
    }


}