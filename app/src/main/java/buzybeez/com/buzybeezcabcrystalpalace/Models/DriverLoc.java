package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hv on 5/10/15 DriverApp3.
 */


@SuppressWarnings("ALL")
public class DriverLoc {

    public static DriverLoc sharedInstance = new DriverLoc();
    @JsonProperty("callsign")
    private String callsign;
    @JsonProperty("office_id")
    private String office_id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("vtype")
    private String vtype;
    @JsonProperty("reg")
    private String reg;
    @JsonProperty("vcolour")
    private String vcolour;
    @JsonProperty("vmodel")
    private String vmodel;
    @JsonProperty("vmake")
    private String vmake;
    /*@JsonProperty("socketid")
    private String socketid;*/
    @JsonProperty("isvirtual")
    private Boolean isvirtual;
    @JsonProperty("loc")
    private Loc loc;
    @JsonProperty("speed")
    private int speed;
    @JsonProperty("lng")
    private Double lng;
    @JsonProperty("lat")
    private Double lat;
    @JsonProperty("accuracy")
    private int accuracy;
    @JsonProperty("battery")
    private String battery;
    @JsonProperty("outcode")
    private String outcode;
    @JsonProperty("outcodetime")
    private Double outcodetime;
    @JsonProperty("timestamp")
    private Double timestamp;
    @JsonProperty("clrtimestamp")
    private Double clrtimestamp;
    @JsonProperty("from")
    private String from;
    @JsonProperty("to")
    private String to;
    @JsonProperty("state")
    private String state;
    @JsonProperty("lstate")
    private String lstate;
    @JsonProperty("jobid")
    private String jobid;
    @JsonProperty("priority")
    private int priority;
    @JsonProperty("telephone")
    private String telephone;
    @JsonProperty("_id")
    private String _id;


    public DriverLoc() {
    }

    /**
     * @return The callsign
     */
    @JsonProperty("callsign")
    public String getCallsign() {
        return callsign;
    }

    /**
     * @param callsign The callsign
     */
    @JsonProperty("callsign")
    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    /**
     * @return The office_id
     */
    @JsonProperty("office_id")
    public String getOfficeId() {
        return office_id;
    }

    /**
     * @param officeId The office_id
     */
    @JsonProperty("office_id")
    public void setOfficeId(String office_id) {
        this.office_id = office_id;
    }

    /**
     * @return The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The vtype
     */
    @JsonProperty("vtype")
    public String getVtype() {
        return vtype;
    }

    /**
     * @param vtype The vtype
     */
    @JsonProperty("vtype")
    public void setVtype(String vtype) {
        this.vtype = vtype;
    }

    /**
     * @return The reg
     */
    @JsonProperty("reg")
    public String getReg() {
        return reg;
    }

    /**
     * @param reg The reg
     */
    @JsonProperty("reg")
    public void setReg(String reg) {
        this.reg = reg;
    }

    /**
     * @return The vcolour
     */
    @JsonProperty("vcolour")
    public String getVcolour() {
        return vcolour;
    }

    /**
     * @param vcolour The vcolour
     */
    @JsonProperty("vcolour")
    public void setVcolour(String vcolour) {
        this.vcolour = vcolour;
    }

    /**
     * @return The vmodel
     */
    @JsonProperty("vmodel")
    public String getVmodel() {
        return vmodel;
    }

    /**
     * @param vmodel The vmodel
     */
    @JsonProperty("vmodel")
    public void setVmodel(String vmodel) {
        this.vmodel = vmodel;
    }

    /**
     * @return The vmake
     */
    @JsonProperty("vmake")
    public String getVmake() {
        return vmake;
    }

    /**id
     * @return The socketid
     */
    /*@JsonProperty("socketid")
    public String getSocketid() {
        return socketid;
    }

    *//**
     * @param socketid The socketid
     *//*
    @JsonProperty("socketid")
    public void setSocketid(String socketid) {
        this.socketid = socketid;
    }*/

    /**
     * @param vmake The vmake
     */
    @JsonProperty("vmake")
    public void setVmake(String vmake) {
        this.vmake = vmake;
    }

    /**
     * @return The isvirtual
     */
    @JsonProperty("isvirtual")
    public Boolean getIsvirtual() {
        return isvirtual;
    }

    /**
     * @param isvirtual The isvirtual
     */
    @JsonProperty("isvirtual")
    public void setIsvirtual(Boolean isvirtual) {
        this.isvirtual = isvirtual;
    }

    /**
     * @return The loc
     */
    @JsonProperty("loc")
    public Loc getLoc() {
        return loc;
    }

    /**
     * @param loc The loc
     */
    @JsonProperty("loc")
    public void setLoc(Loc loc) {
        this.loc = loc;
    }

    /**
     * @return The speed
     */
    @JsonProperty("speed")
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed The speed
     */
    @JsonProperty("speed")
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @return The long
     */
    @JsonProperty("long")
    public Double getLong() {
        return lng;
    }

    /**
     * @param long The long
     */
    @JsonProperty("long")
    public void setLong(Double lng) {
        this.lng = lng;
    }

    /**
     * @return The lat
     */
    @JsonProperty("lat")
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    @JsonProperty("lat")
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return The accuracy
     */
    @JsonProperty("accuracy")
    public int getAccuracy() {
        return accuracy;
    }

    /**
     * @param accuracy The accuracy
     */
    @JsonProperty("accuracy")
    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    /**
     * @return The battery
     */
    @JsonProperty("battery")
    public String getBattery() {
        return battery;
    }

    /**
     * @param battery The battery
     */
    @JsonProperty("battery")
    public void setBattery(String battery) {
        this.battery = battery;
    }

    /**
     * @return The outcode
     */
    @JsonProperty("outcode")
    public String getOutcode() {
        return outcode;
    }

    /**
     * @param outcode The outcode
     */
    @JsonProperty("outcode")
    public void setOutcode(String outcode) {
        this.outcode = outcode;
    }

    /**
     * @return The outcodetime
     */
    @JsonProperty("outcodetime")
    public Double getOutcodetime() {
        return outcodetime;
    }

    /**
     * @param outcodetime The outcodetime
     */
    @JsonProperty("outcodetime")
    public void setOutcodetime(Double outcodetime) {
        this.outcodetime = outcodetime;
    }

    /**
     * @return The timestamp
     */
    @JsonProperty("timestamp")
    public Double getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp The timestamp
     */
    @JsonProperty("timestamp")
    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return The clrtimestamp
     */
    @JsonProperty("clrtimestamp")
    public Double getClrtimestamp() {
        return clrtimestamp;
    }

    /**
     * @param clrtimestamp The clrtimestamp
     */
    @JsonProperty("clrtimestamp")
    public void setClrtimestamp(Double clrtimestamp) {
        this.clrtimestamp = clrtimestamp;
    }

    /**
     * @return The from
     */
    @JsonProperty("from")
    public String getfrom() {
        return from;
    }

    /**
     * @param from The from
     */
    @JsonProperty("from")
    public void setfrom(String from) {
        this.from = from;
    }

    /**
     * @return The to
     */
    @JsonProperty("to")
    public String getto() {
        return to;
    }

    /**
     * @param to The to
     */
    @JsonProperty("to")
    public void setto(String to) {
        this.to = to;
    }

    /**
     * @return The state
     */
    @JsonProperty("state")
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The lstate
     */
    @JsonProperty("lstate")
    public String getLstate() {
        return lstate;
    }

    /**
     * @param lstate The lstate
     */
    @JsonProperty("lstate")
    public void setLstate(String lstate) {
        this.lstate = lstate;
    }

    /**
     * @return The jobid
     */
    @JsonProperty("jobid")
    public String getJobid() {
        return jobid;
    }

    /**
     * @param jobid The jobid
     */
    @JsonProperty("jobid")
    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    /**
     * @return The priority
     */
    @JsonProperty("priority")
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority The priority
     */
    @JsonProperty("priority")
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return The Id
     */

    @JsonProperty("_id")
    public String getId() {
        return _id;
    }


    /**
     * @param Id The _id
     */

    @JsonProperty("_id")
    public void setId(String Id) {
        this._id = Id;
    }


    /**
     * @return The telephone
     */
    @JsonProperty("telephone")
    public String gettelephone() {
        return telephone;
    }

    /**
     * @param telephone The telephone
     */
    @JsonProperty("telephone")
    public void settelephone(String telephone) {
        this.telephone = telephone;
    }

}

