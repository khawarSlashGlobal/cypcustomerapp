package buzybeez.com.buzybeezcabcrystalpalace.Models;

/**
 * Created by SGI on 31/10/2016.
 */

public class home {

    //String Account_Name;
    String home;
    double lng, lat;


    public home(String home, double lat, double lng) {
        this.home = home;
        this.lat = lat;
        this.lng = lng;
    }

    public home() {
    }

    public String gethome() {
        return home;
    }

    public void sethome(String home) {
        this.home = home;
    }

    public double getlat() {
        return lat;
    }

    public void setlat(double lat) {
        this.lat = lat;
    }

    public double getlng() {
        return lng;
    }

    public void setlng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "HomeModel{" +
                ", home='" + home + '\'' +
                ", lat=" + lat +
                ", long=" + lng +
                '}';
    }
}
