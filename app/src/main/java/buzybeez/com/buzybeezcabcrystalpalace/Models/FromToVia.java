package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hv on 5/10/15 DriverApp3.
 */
public class FromToVia {

    @JsonProperty("info")
    private String info;
    @JsonProperty("address")
    private String address;
    @JsonProperty("lat")
    private double lat;
    @JsonProperty("lon")
    private double lon;
    @JsonProperty("postcode")
    private String postcode;

    public FromToVia(String info, String address, double lat, double lon, String postcode) {
        this.info = info;
        this.address = address;
        this.lat = lat;
        this.lon = lon;
      //  this.postcode = postcode;
        setPostcode(postcode);
    }

    public FromToVia() {
    }

    /**
     * @return The info
     */
    @JsonProperty("info")
    public Object getInfo() {
        return info;
    }

    /**
     * @param info The info
     */
    @JsonProperty("info")
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * @return The address
     */
    @JsonProperty("address")
    public Object getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return The lat
     */
    @JsonProperty("lat")
    public double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    @JsonProperty("lat")
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * @return The lon
     */
    @JsonProperty("lon")
    public double getLon() {
        return lon;
    }

    /**
     * @param lon The lon
     */
    @JsonProperty("lon")
    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
     * @return The postcode
     */
    @JsonProperty("postcode")
    public Object getPostcode() {
        return postcode;
    }

    /**
     * @param postcode The postcode
     */
    @JsonProperty("postcode")
    public void setPostcode(String postcode) {
        if (postcode == null) {
            this.postcode = "NPC";
        } else if (postcode.equals("NPC")) {
            this.postcode = null;

        } else if (postcode.equals("null")) {
            this.postcode = "NPC";

        } else {
            this.postcode = postcode;
        }
    }


}