package buzybeez.com.buzybeezcabcrystalpalace.Models;

/**
 * Created by SGI on 31/10/2016.
 */

public class VehicleModel {

    //String Account_Name;
    private String Vehicle_Name;
    private int unit;
    private double cost;


    public VehicleModel(String vehicle_Name, int unit, double cost) {

        Vehicle_Name = vehicle_Name;
        this.unit = unit;
        this.cost = cost;
    }

    public VehicleModel() {
    }

    public String getVehicle_Name() {
        return Vehicle_Name;
    }

    public void setVehicle_Name(String vehicle_Name) {
        Vehicle_Name = vehicle_Name;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "VehicleModel{" +
                ", Vehicle_Name='" + Vehicle_Name + '\'' +
                ", unit=" + unit +
                ", cost=" + cost +
                '}';
    }
}
