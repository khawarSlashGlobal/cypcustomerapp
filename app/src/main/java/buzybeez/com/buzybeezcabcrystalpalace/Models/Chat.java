package buzybeez.com.buzybeezcabcrystalpalace.Models;

public class Chat {

    private String message;
    private String dateTime;
    private boolean status;



    public Chat() {
    }

    public Chat(String message, String dateTime, boolean status) {
        this.message = message;
        this.dateTime = dateTime;
        this.status=status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}

