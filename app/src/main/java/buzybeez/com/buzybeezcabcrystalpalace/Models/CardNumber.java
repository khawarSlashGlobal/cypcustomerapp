package buzybeez.com.buzybeezcabcrystalpalace.Models;

public class CardNumber {
    String card;
    String count;

    public CardNumber() {
    }

    public CardNumber(String card, String count) {
        this.card = card;
        this.count = count;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
