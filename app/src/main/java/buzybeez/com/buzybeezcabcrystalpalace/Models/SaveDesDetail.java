package buzybeez.com.buzybeezcabcrystalpalace.Models;

public class SaveDesDetail {

	private String address;
	private double lat;
	private double lon;
	private String postcode;
	private String outcode;

	public SaveDesDetail(String address, double lat, double lon, String postcode, String outcode) {
		this.address = address;
		this.lat = lat;
		this.lon = lon;
		this.postcode = postcode;
		this.outcode = outcode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getOutcode() {
		return outcode;
	}

	public void setOutcode(String outcode) {
		this.outcode = outcode;
	}
}
