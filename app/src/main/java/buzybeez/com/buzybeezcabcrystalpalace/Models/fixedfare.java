package buzybeez.com.buzybeezcabcrystalpalace.Models;

/**
 * Created by visibility on 30/11/2016.
 */

public class fixedfare {
    String postcode;
    int lhr, lgw, ltn, std, lcy;

    public fixedfare(String postcode, int lhr, int lgw, int ltn, int std, int lcy) {
        this.postcode = postcode;
        this.lhr = lhr;
        this.lgw = lgw;
        this.ltn = ltn;
        this.std = std;
        this.lcy = lcy;
    }

    public fixedfare() {

    }

    public void setPostCode(String postcode) {
        this.postcode = postcode;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public int getLhr() {
        return this.lhr;
    }

    public void setLhr(int lhr) {
        this.lhr = lhr;
    }

    public int getLgw() {
        return this.lgw;
    }

    public void setLgw(int lgw) {
        this.lgw = lgw;
    }

    public int getLtn() {
        return this.ltn;
    }

    public void setLtn(int ltn) {
        this.ltn = ltn;
    }

    public int getStd() {
        return this.std;
    }

    public void setStd(int std) {
        this.std = std;
    }

    public int getLcy() {
        return this.lcy;
    }

    public void setLcy(int lcy) {
        this.lcy = lcy;
    }


}
