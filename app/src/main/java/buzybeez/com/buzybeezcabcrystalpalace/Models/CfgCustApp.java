package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Map;


public class CfgCustApp {

    public static int notiid=0;
    //CfgCustApp singleton
    public static CfgCustApp sharedinstance = new CfgCustApp();
    public static CfgCustApp getInstance() {
        return sharedinstance;
    }

    @JsonProperty("_id")
    private String _id;

    @JsonProperty("officename")
    private String officename;

    @JsonProperty("imageIP")
    private String imageIP;

    @JsonProperty("jobInOffice")
    private String jobInOffice;

    @JsonProperty("OfficePosition")
    // private List<OfficePosition> officePosition = new ArrayList<>();
    private ArrayList<Double> OfficePosition = new ArrayList();

    @JsonProperty("googleServiceKey")
    private String googleServiceKey;

    @JsonProperty("googleDirectionKey")
    private String googleDirectionKey;

    @JsonProperty("googlePlaceKey")
    private String googlePlaceKey;

    @JsonProperty("Legacyserverkey")
    private String Legacyserverkey;

    @JsonProperty("phoneNumber")
    private String phoneNumber;

    @JsonProperty("iOS_Version")
    private String iOS_Version;

    @JsonProperty("Android_Version")
    private String Android_Version;

    @JsonProperty("airport_min_price")
    private double airport_min_price;

    @JsonProperty("airport_pickup_ext")
    private double airport_pickup_ext;

    @JsonProperty("all_vehicle_fare")
    private ArrayList<Map<String,Double>> all_vehicle_fare = new ArrayList();

    @JsonProperty("milesUnit")
    private ArrayList<Double> milesUnit = new ArrayList();

    @JsonProperty("costUnit")
    private ArrayList<Double> costUnit = new ArrayList();

    @JsonProperty("offemailpwd")
    private String offemailpwd ;

    @JsonProperty("offemail")
    private String offemail ;

    @JsonProperty("SmtpHost")
    private String SmtpHost ;


    @JsonProperty("SmtpPort")
    private int SmtpPort ;



    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getOfficename() {
        return officename;
    }

    public void setOfficename(String officename) {
        this.officename = officename;
    }

    public String getImageIP() {
        return imageIP;
    }

    public void setImageIP(String imageIP) {
        this.imageIP = imageIP;
    }

    public String getJobInOffice() {
        return jobInOffice;
    }

    public void setJobInOffice(String jobInOffice) {
        this.jobInOffice = jobInOffice;
    }

    public String getGoogleServiceKey() {
        return googleServiceKey;
    }

    public void setGoogleServiceKey(String googleServiceKey) {
        this.googleServiceKey = googleServiceKey;
    }

    public String getGoogleDirectionKey() {
        return googleDirectionKey;
    }

    public void setGoogleDirectionKey(String googleDirectionKey) {
        this.googleDirectionKey = googleDirectionKey;
    }

    public String getGooglePlaceKey() {
        return googlePlaceKey;
    }

    public void setGooglePlaceKey(String googlePlaceKey) {
        this.googlePlaceKey = googlePlaceKey;
    }

    public String getLegacyserverkey() {
        return Legacyserverkey;
    }

    public void setLegacyserverkey(String legacyserverkey) {
        Legacyserverkey = legacyserverkey;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getiOS_Version() {
        return iOS_Version;
    }

    public void setiOS_Version(String iOS_Version) {
        this.iOS_Version = iOS_Version;
    }

    public String getoffemailpwd() {
        return offemailpwd;
    }

    public void setoffemailpwd(String Offemailpwd) {
        offemailpwd = Offemailpwd;
    }

    public String getoffemail() {
        return offemail;
    }

    public void setoffemail(String Offemail) {
        offemail = Offemail;
    }

    public String getSmtpHost() {
        return SmtpHost;
    }

    public void setSmtpHost(String SmtpHost) {
        this.SmtpHost = SmtpHost;
    }

    public int getSmtpPort() {
        return SmtpPort;
    }

    public void setSmtpPort(int SmtpPort) {
        this.SmtpPort = SmtpPort;
    }


    public double getAirport_min_price() {
        return airport_min_price;
    }

    public void setAirport_min_price(double airport_min_price) {
        this.airport_min_price = airport_min_price;
    }

    public double getAirport_pickup_ext() {
        return airport_pickup_ext;
    }

    public void setAirport_pickup_ext(double airport_pickup_ext) {
        this.airport_pickup_ext = airport_pickup_ext;
    }

    public ArrayList<Map<String, Double>> getAll_vehicle_fare() {
        return all_vehicle_fare;
    }

    public void setAll_vehicle_fare(ArrayList<Map<String, Double>> all_vehicle_fare) {
        this.all_vehicle_fare = all_vehicle_fare;
    }

    public ArrayList<Double> getMilesUnit() {
        return milesUnit;
    }


    public ArrayList<Double> getCostUnit() {
        return costUnit;
    }




    public ArrayList<Double> getOfficePosition() {
        return OfficePosition;
    }

    public void setOfficePosition(ArrayList<Double> officePosition) {
        OfficePosition = officePosition;
    }

    public void setMilesUnit(ArrayList<Double> milesUnit) {
        this.milesUnit = milesUnit;
    }

    public void setCostUnit(ArrayList<Double> costUnit) {
        this.costUnit = costUnit;
    }
}
/*SAMPLE DATA
 *//* 1 *//*
{
        "_id" : ObjectId("5a2be72895cae0ccda2f93c6"),
        "officename" : "CustomerApp",
        "imageIP" : "88.208.208.184",
        "jobInOffice" : "XYZ",
        "OfficePosition" : [
        {
        "CYP" : [
        51.41798,
        -0.07307
        ],
        "WCP" : [
        51.3813,
        -0.24526
        ],
        "NBT" : [
        51.41245,
        -0.28409
        ],
        "SUR" : [
        51.3922,
        -0.30329
        ],
        "NBA" : [
        51.648589999,
        -0.17336
        ],
        "XYZ" : [
        56.129918,
        -3.792925
        ],
        "YYY" : [
        56.129918,
        -3.792925
        ],
        "ZZZ" : [
        51.41798,
        -0.07307
        ]
        }
        ],
        "googleServiceKey" : "AIzaSyDZnBTjfDwq78ddXExMtImxnRrsBCdmv8M",
        "googleDirectionKey" : "AIzaSyDuoFHe7NQ5U6GZ1SSu7XcckrQ9Bi8_at0",
        "googlePlaceKey" : "AIzaSyBo-l39pRyrZ6V-y6nkmPhw8em4rRAocpk",
        "Legacyserverkey" : "AIzaSyA54iNszUlssiVJLeYNX0eCtYcM8Bs9HlQ",
        "phoneNumber" : "02083903000",
        "iOS_Version" : "2.2",
        "Android_Version" : "",
        "airport_min_price" : 20.0,
        "airport_pickup_ext" : 5,
        "all_vehicle_fare" : [
        {
        "E" : 25,
        "6" : 50,
        "8" : 100,
        "X" : 25
        }
        ],
        "milesUnit" : [
        12,
        10,
        999
        ],
        "costUnit" : [
        1.7,
        1.4,
        1
        ]
        }*/