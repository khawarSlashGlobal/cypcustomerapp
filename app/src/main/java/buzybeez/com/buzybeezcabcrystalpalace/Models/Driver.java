package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by SGI on 03/12/2016.
 */
public class Driver {
    public static Driver sharedInstance = new Driver();

    private String callsign, jobid;
    private double lat;
    private double lng;
    private String drvUID;
    private String officeID;
    private Marker marker = null;
    private String driverPCO;

    public String getDrvUID() {
        return drvUID;
    }

    public void setDrvUID(String drvUID) {
        this.drvUID = drvUID;
    }

    public String getOfficeID() {
        return officeID;
    }

    public void setOfficeID(String officeID) {
        this.officeID = officeID;
    }

    public String getDriverPCO() {
        return driverPCO;
    }

    public void setDriverPCO(String driverPCO) {
        this.driverPCO = driverPCO;
    }

    public String getCallsign() {
        return this.callsign;
    }

    public void setcallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getJobid() {
        return this.jobid;
    }

    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    /**
     * @return The lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * @return The lon
     */
    public double getLng() {
        return lng;
    }

    /**
     * @param lng The lon
     */
    public void setLng(double lng) {
        this.lng = lng;
    }


    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }


}