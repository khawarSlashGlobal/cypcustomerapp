package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hv on 3/12/18.
 */

public class CustomerConfig {


    public static CustomerConfig sharedinstance = new CustomerConfig();
    public String nearestOfficeName;
    public double nearestOfficeLat;
    public double nearestOfficeLon;
    public String nearestOfficeNumber;
    @JsonProperty("_id")
    private String _id;
    @JsonProperty("officename")
    private String officename;
    @JsonProperty("imageIP")
    private String imageIP;
    @JsonProperty("jobInOffice")
    private String jobInOffice;

    private CustomerConfig() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getOfficename() {
        return officename;
    }

    public void setOfficename(String officename) {
        this.officename = officename;
    }

    public String getImageIP() {
        return imageIP;
    }

    public void setImageIP(String imageIP) {
        this.imageIP = imageIP;
    }

    public String getJobInOffice() {
        return jobInOffice;
    }

    public void setJobInOffice(String jobInOffice) {
        this.jobInOffice = jobInOffice;
    }

    public String getNearestOfficeName() {
        return nearestOfficeName;
    }

    public void setNearestOfficeName(String nearestOfficeName) {
        this.nearestOfficeName = nearestOfficeName;
    }

    public double getNearestOfficeLat() {
        return nearestOfficeLat;
    }

    public void setNearestOfficeLat(double nearestOfficeLat) {
        this.nearestOfficeLat = nearestOfficeLat;
    }

    public double getNearestOfficeLon() {
        return nearestOfficeLon;
    }

    public void setNearestOfficeLon(double nearestOfficeLon) {
        this.nearestOfficeLon = nearestOfficeLon;
    }

    public String getNearestOfficeNumber() {
        return nearestOfficeNumber;
    }

    public void setNearestOfficeNumber(String nearestOfficeNumber) {
        this.nearestOfficeNumber = nearestOfficeNumber;
    }
}
