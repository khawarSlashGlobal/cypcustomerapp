package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hv on 5/10/15 DriverApp3.
 */
@SuppressWarnings("ALL")


@JsonInclude(JsonInclude.Include.ALWAYS)


public class Booking {

    public static Booking sharedinstance = new Booking();
    @JsonProperty("logd")
    public Object logd;
    //private int bookingID;
    @JsonProperty("_id")
    private String Id;
    @JsonProperty("account")
    private String account;
    @JsonProperty("accuser")
    private String accuser;
    @JsonProperty("changed")
    private Boolean changed;
    @JsonProperty("flag")
    private Integer flag;
    @JsonProperty("hold")
    private Boolean hold;
    @JsonProperty("date")
    private String date;
    @JsonProperty("time")
    private String time;
    @JsonProperty("datentime")
    private double datentime;
    @JsonProperty("custname")
    private String custname;
    @JsonProperty("userid")
    private String userid;
    @JsonProperty("driverrate")
    private String driverrate;
    @JsonProperty("timetodespatch")
    private double timetodespatch;
    @JsonProperty("tag")
    private String tag;
    @JsonProperty("fare")
    private Double fare;
    @JsonProperty("oldfare")
    private Double oldfare;
    @JsonProperty("drvfare")
    private Double drvfare;
    @JsonProperty("olddrvfare")
    private Double olddrvfare;
    @JsonProperty("from")
    private String from;
    @JsonProperty("from_info")
    private String from_info;
    @JsonProperty("to")
    private String to;
    @JsonProperty("from_outcode")
    private String from_outcode;
    @JsonProperty("to_outcode")
    private String to_outcode;
    @JsonProperty("to_info")
    private String to_info;
    @JsonProperty("jobtype")
    private String jobtype;
    @JsonProperty("fromtovia")
    private List<FromToVia> fromtovia = new ArrayList<>();
    @JsonProperty("vehicletype")
    private String vehicletype;
    @JsonProperty("jobref")
    private String jobref;
    //	@JsonProperty("fromtoviaList")
//	private List<FromToVia> fromtovia = new ArrayList<>();
    @JsonProperty("jobmileage")
    private Double jobmileage;
    @JsonProperty("leadtime")
    private Double leadtime;
    @JsonProperty("Officenearby")
    private String office;
    @JsonProperty("orderno")
    private String orderno;
    @JsonProperty("bookedby")
    private String bookedby;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty("creditcard")
    private String creditcard;
    @JsonProperty("drvrcallsign")
    private String drvrcallsign;
    @JsonProperty("drvrname")
    private String drvrname;
    @JsonProperty("drvrreqcallsign")
    private String drvrreqcallsign;
    @JsonProperty("drvreqdname")
    private String drvreqdname;
    @JsonProperty("flightno")
    private String flightno;
    @JsonProperty("telephone")
    private String telephone;
    @JsonProperty("jstate")
    private String jstate;
    @JsonProperty("mstate")
    private String mstate;
    @JsonProperty("dstate")
    private String dstate;
    @JsonProperty("cstate")
    private String cstate;
    @JsonProperty("isdirty")
    private Boolean isdirty;
    @JsonProperty("numofvia")
    private Integer numofvia;
    @JsonProperty("pin")
    private String pin;
    @JsonProperty("despatchtime")
    private double despatchtime;
    @JsonProperty("callerid")
    private String callerid;
    @JsonProperty("logc")
    private Object[][] logc;

    public static Booking getInstance() {
        return sharedinstance;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @JsonProperty("fromtovia")
    public List<FromToVia> getFromtovia() {
        return fromtovia;
    }

    /**
     * @param fromtovia The fromtovia
     */
    @JsonProperty("fromtovia")
    public void setFromtovia(List<FromToVia> fromtovia) {
        this.fromtovia = fromtovia;
    }

    /*@JsonProperty("jobtype")
    private String jobType;
    public String getJobType(){return jobType;}
    public void setJobType(String s){jobtype}*/

    //public static Booking sharedInstance = new Booking();

    /**
     * @return The Id
     */

    /**
     * @return The account
     */
    @JsonProperty("account")
    public String getAccount() {
        return account;
    }

    /**
     * @param account The account
     */
    @JsonProperty("account")
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return The accuser
     */

    @JsonProperty("accuser")
    public String getAccuser() {
        return accuser;
    }

    /**
     * @param accuser The accuser
     */

    @JsonProperty("accuser")
    public void setAccuser(String accuser) {
        this.accuser = accuser;
    }

    /**
     * @return The changed
     */

    @JsonProperty("changed")
    public Boolean getChanged() {
        return changed;
    }

    /**
     * @param changed The changed
     */
    @JsonProperty("changed")
    public void setChanged(Boolean changed) {
        this.changed = changed;
    }

    /**
     * @return The flag
     */
    @JsonProperty("flag")
    public Integer getFlag() {
        return flag;
    }

    /**
     * @param flag The flag
     */
    @JsonProperty("flag")
    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    /**
     * @return The hold
     */
    @JsonProperty("hold")
    public Boolean getHold() {
        return hold;
    }

    /**
     * @param hold The hold
     */
    @JsonProperty("hold")
    public void setHold(Boolean hold) {
        this.hold = hold;
    }

    /**
     * @return The date
     */
    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return The time
     */
    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    @JsonProperty("time")
    public void setTime(String time) {
        this.time = time;

    }

    /**
     * @return The datentime
     */
    @JsonProperty("datentime")
    public double getDatentime() {
        return datentime;
    }

    /**
     * @param datentime The datentime
     */
    @JsonProperty("datentime")
    public void setDatentime(long datentime) {
        this.datentime = datentime;
    }

    /**
     * @return The custname
     */
    @JsonProperty("custname")
    public String getCustname() {
        return custname;
    }

    /**
     * @param custname The custname
     */
    @JsonProperty("custname")
    public void setCustname(String custname) {
        this.custname = custname;
    }

    /**
     * @return The userid
     */
    @JsonProperty("userid")
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid The userid
     */
    @JsonProperty("userid")
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return The driverrate
     */
    @JsonProperty("driverrate")
    public String getDriverrate() {
        return driverrate;
    }

    /**
     * @param driverrate The driverrate
     */
    @JsonProperty("driverrate")
    public void setDriverrate(String driverrate) {
        this.driverrate = driverrate;
    }

    /**
     * @return The timetodespatch
     */
    @JsonProperty("timetodespatch")
    public double getTimetodespatch() {
        return timetodespatch;
    }

    /**
     * @param timetodespatch The timetodespatch
     */
    @JsonProperty("timetodespatch")
    public void setTimetodespatch(double timetodespatch) {
        this.timetodespatch = timetodespatch;
    }

    /**
     * @return The tag
     */
    @JsonProperty("tag")
    public String getTag() {
        return tag;
    }

    /**
     * @param tag The tag
     */
    @JsonProperty("tag")
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * @return The fare
     */
    @JsonProperty("fare")
    public Double getFare() {
        return fare;
    }

    /**
     * @param fare The fare
     */
    @JsonProperty("fare")
    public void setFare(Double fare) {
        this.fare = fare;
    }

    @JsonProperty("oldfare")
    public Double getOldFare() {
        return oldfare;
    }

    /**
     * @param fare The fare
     */
    @JsonProperty("oldfare")
    public void setOldFare(Double oldfare) {
        this.oldfare = oldfare;
    }

    /**
     * @return The drvfare
     */
    @JsonProperty("drvfare")
    public Double getDrvfare() {
        return drvfare;
    }

    /**
     * @param drvfare The drvfare
     */
    @JsonProperty("drvfare")
    public void setDrvfare(Double drvfare) {
        this.drvfare = drvfare;
    }

    @JsonProperty("olddrvfare")
    public Double getOldDrvfare() {
        return olddrvfare;
    }

    /**
     * @param drvfare The drvfare
     */
    @JsonProperty("olddrvfare")
    public void setOldDrvfare(Double olddrvfare) {
        this.olddrvfare = olddrvfare;
    }

    /**
     * @return The from
     */
    @JsonProperty("from_info")
    public String getfrom_info() {
        return from_info;
    }

    /**
     * @param from The from
     */
    @JsonProperty("from_info")
    public void setfrom_info(String from) {
        this.from_info = from;
    }

    @JsonProperty("from")
    public String getfrom() {
        return from;
    }

    @JsonProperty("from")
    public void setfrom(String from) {
        this.from = from;
    }

    /**
     * @return The to
     */


    @JsonProperty("to")
    public String getTo() {
        return to;
    }

    /**
     * @param to The to
     */
    @JsonProperty("to")
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return The fromOutcode
     */
    @JsonProperty("from_outcode")
    public String getFromOutcode() {
        return from_outcode;
    }

    /**
     * @param fromOutcode The from_outcode
     */
    @JsonProperty("from_outcode")
    public void setFrom_outcode(String fromOutcode) {
        if (fromOutcode.equals("null")) {
            this.from_outcode = "NPC";

        } else {
            this.from_outcode = fromOutcode;
        }
    }

    /**
     * @return The toOutcode
     */
    @JsonProperty("to_outcode")
    public String getto_outcode() {
        return to_outcode;
    }

    /**
     * @param toOutcode The to_outcode
     */
    @JsonProperty("to_outcode")
    public void setto_outcode(String to_outcode) {
        if (to_outcode.equals("null")) {
            this.to_outcode = "NPC";

        } else {
            this.to_outcode = to_outcode;
        }
    }

    /**
     * @return The fromInfo
     */
 /*   @JsonProperty("from_info")
    public String getFrom_info() {
        return from_info;
    }

    *//**
     * @param fromInfo The from_info
     *//*
    @JsonProperty("from_info")
    public void setFrom_info(String fromInfo) {
        this.from_info = fromInfo;
    }
*/

    /**
     * @return The toInfo
     */
    @JsonProperty("to_info")
    public String getto_info() {
        return to_info;
    }

    /**
     * @param toInfo The to_info
     */
    @JsonProperty("to_info")
    public void setto_info(String toInfo) {
        this.to_info = toInfo;
    }

    /**
     * @return The jobtype
     */
    @JsonProperty("jobtype")
    public String getJobtype() {
        return jobtype;
    }

    /**
     * @param jobtype The jobtype
     */
    @JsonProperty("jobtype")
    public void setJobtype(String jobtype) {
        this.jobtype = jobtype;
    }

//	/**
//	 * @return The fromtoviaList
//	 */
//	@JsonProperty("fromtoviaList")
//	public List<FromToVia> getFromtovia() {
//		return fromtovia;
//	}
//
//	/**
//	 * @param fromtovia The fromtoviaList
//	 */
//	@JsonProperty("fromtoviaList")
//	public void setFromtovia(List<FromToVia> fromtovia) {
//		this.fromtovia = fromtovia;
//	}

    /**
     * @return The vehicletype
     */
    @JsonProperty("vehicletype")
    public String getVehicletype() {
        return vehicletype;
    }

    /**
     * @param vehicletype The vehicletype
     */
    @JsonProperty("vehicletype")
    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    /**
     * @return The jobref
     */
    @JsonProperty("jobref")
    public String getJobref() {
        return jobref;
    }

    /**
     * @param jobref The jobref
     */
    @JsonProperty("jobref")
    public void setJobref(String jobref) {
        this.jobref = jobref;
    }

    /**
     * @return The jobmileage
     */
    @JsonProperty("jobmileage")
    public Double getJobmileage() {
        return jobmileage;
    }

    /**
     * @param jobmileage The jobmileage
     */
    @JsonProperty("jobmileage")
    public void setJobmileage(Double jobmileage) {
        this.jobmileage = jobmileage;
    }

    /**
     * @return The leadtime
     */
    @JsonProperty("leadtime")
    public Double getLeadtime() {
        return leadtime;
    }

    /**
     * @param leadtime The leadtime
     */
    @JsonProperty("leadtime")
    public void setLeadtime(Double leadtime) {
        this.leadtime = leadtime;
    }

    /**
     * @return The Officenearby
     */
    @JsonProperty("Officenearby")
    public String getOffice() {
        return office;
    }

    /**
     * @param office The Officenearby
     */
    @JsonProperty("Officenearby")
    public void setOffice(String office) {
        this.office = office;
    }

    /**
     * @return The orderno
     */
    @JsonProperty("orderno")
    public String getOrderno() {
        return orderno;
    }

    /**
     * @param orderno The orderno
     */
    @JsonProperty("orderno")
    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    /**
     * @return The bookedby
     */
    @JsonProperty("bookedby")
    public String getBookedby() {
        return bookedby;
    }

    /**
     * @param bookedby The bookedby
     */
    @JsonProperty("bookedby")
    public void setBookedby(String bookedby) {
        this.bookedby = bookedby;
    }

    /**
     * @return The comment
     */
    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return The creditcard
     */
    @JsonProperty("creditcard")
    public String getCreditcard() {
        return creditcard;
    }

    /**
     * @param creditcard The creditcard
     */
    @JsonProperty("creditcard")
    public void setCreditcard(String creditcard) {
        this.creditcard = creditcard;
    }

    /**
     * @return The drvrcallsign
     */
    @JsonProperty("drvrcallsign")
    public String getDrvrcallsign() {
        return drvrcallsign;
    }

    /**
     * @param drvrcallsign The drvrcallsign
     */
    @JsonProperty("drvrcallsign")
    public void setDrvrcallsign(String drvrcallsign) {
        this.drvrcallsign = drvrcallsign;
    }

    /**
     * @return The drvrname
     */
    @JsonProperty("drvrname")
    public String getDrvrname() {
        return drvrname;
    }

    /**
     * @param drvrname The drvrname
     */
    @JsonProperty("drvrname")
    public void setDrvrname(String drvrname) {
        this.drvrname = drvrname;
    }

    /**
     * @return The drvrreqcallsign
     */
    @JsonProperty("drvrreqcallsign")
    public String getDrvrreqcallsign() {
        return drvrreqcallsign;
    }

    /**
     * @param drvrreqcallsign The drvrreqcallsign
     */
    @JsonProperty("drvrreqcallsign")
    public void setDrvrreqcallsign(String drvrreqcallsign) {
        this.drvrreqcallsign = drvrreqcallsign;
    }

    /**
     * @return The drvreqdname
     */
    @JsonProperty("drvreqdname")
    public String getDrvreqdname() {
        return drvreqdname;
    }

    /**
     * @param drvreqdname The drvreqdname
     */
    @JsonProperty("drvreqdname")
    public void setDrvreqdname(String drvreqdname) {
        this.drvreqdname = drvreqdname;
    }

    /**
     * @return The flightno
     */
    @JsonProperty("flightno")
    public String getFlightno() {
        return flightno;
    }

    /**
     * @param flightno The flightno
     */
    @JsonProperty("flightno")
    public void setFlightno(String flightno) {
        this.flightno = flightno;
    }

    /**
     * @return The telephone
     */
    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone The telephone
     */
    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return The jstate
     */
    @JsonProperty("jstate")
    public String getJstate() {
        return jstate;
    }

    /**
     * @param jstate The jstate
     */
    @JsonProperty("jstate")
    public void setJstate(String jstate) {
        this.jstate = jstate;
    }

    /**
     * @return The mstate
     */
    @JsonProperty("mstate")
    public String getMstate() {
        return mstate;
    }

    /**
     * @param mstate The mstate
     */
    @JsonProperty("mstate")
    public void setMstate(String mstate) {
        this.mstate = mstate;
    }

    /**
     * @return The dstate
     */
    @JsonProperty("dstate")
    public String getDstate() {
        return dstate;
    }

    /**
     * @param dstate The dstate
     */
    @JsonProperty("dstate")
    public void setDstate(String dstate) {
        this.dstate = dstate;
    }

    /**
     * @return The cstate
     */
    @JsonProperty("cstate")
    public String getCstate() {
        return cstate;
    }

    /**
     * @param cstate The cstate
     */
    @JsonProperty("cstate")
    public void setCstate(String cstate) {
        this.cstate = cstate;
    }

    /**
     * @return The isdirty
     */
    @JsonProperty("isdirty")
    public Boolean getIsdirty() {
        return isdirty;
    }

    /**
     * @param isdirty The isdirty
     */
    @JsonProperty("isdirty")
    public void setIsdirty(Boolean isdirty) {
        this.isdirty = isdirty;
    }

    /**
     * @return The numofvia
     */
    @JsonProperty("numofvia")
    public Integer getNumofvia() {
        return numofvia;
    }

    /**
     * @param numofvia The numofvia
     */
    @JsonProperty("numofvia")
    public void setNumofvia(Integer numofvia) {
        this.numofvia = numofvia;
    }

    /*
     * @return
     * The logd
     */

    /*@JsonProperty("logd")
    public List<Logd> getLogd() {
        return logd;
    }

*//*
     *
     * @param logd
     * The logd
     *//*

    @JsonProperty("logd")
    public void setLogd(List<Logd> logd) {
        this.logd = logd;
    }*/

    @JsonProperty("logd")
    public Object getLogd() {
        return logd;
    }

    @JsonProperty("logd")
    public void setLogd(Object logd) {
        this.logd = logd;

    }

    @JsonProperty("pin")
    public String getPin() {
        return pin;
    }

    @JsonProperty("pin")
    public void setPin(String pin) {
        this.pin = pin;
    }

  /*  @JsonProperty("olddrvfare")
    public void setOlddrvfare(double olddrvfare) {
        this.olddrvfare = olddrvfare;
    }

    @JsonProperty("olddrvfare")
    public double getOlddrvfare() {
        return olddrvfare;
    }
*/
    /*@JsonProperty("oldfare")
    public void setOldfare(double oldfare) {
        this.oldfare = oldfare;
    }

    @JsonProperty("oldfare")
    public double getOldfare() {
        return oldfare;
    }*/

    @JsonProperty("despatchtime")
    public double getDespatchtime() {
        return despatchtime;
    }

    @JsonProperty("despatchtime")
    public void setDespatchtime(double despatchtime) {
        this.despatchtime = despatchtime;
    }

    @JsonProperty("callerid")
    public void setcallerid(String callerId) {
        this.callerid = callerId;
    }

    @JsonProperty("callerid")
    public String getcallerid() {
        return callerid;
    }

    @JsonProperty("logc")
    public Object[][] getLogc() {
        return logc;
    }

    @JsonProperty("logc")
    public void setLogc(Object[][] logc) {
        this.logc = logc;
    }
}
