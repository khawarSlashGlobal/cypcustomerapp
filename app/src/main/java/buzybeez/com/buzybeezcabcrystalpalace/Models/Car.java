package buzybeez.com.buzybeezcabcrystalpalace.Models;


/*
 * Created by skh
 * this class is not from database
 * this is only used in carSelectionAdapter Class
 * */
public class Car {
    String carName;
    String carPassengerCapacity;
    String carLuggageCapacity;
    Integer carImage;
    boolean isSleceted;

    public Car(String carName, String carPassengerCapacity, String carLuggageCapacity, Integer carImage, boolean isSleceted) {
        this.carName = carName;
        this.carPassengerCapacity = carPassengerCapacity;
        this.carLuggageCapacity = carLuggageCapacity;
        this.carImage = carImage;
        this.isSleceted = isSleceted;
    }

    public boolean isSleceted() {
        return isSleceted;
    }

    public void setSleceted(boolean sleceted) {
        isSleceted = sleceted;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarPassengerCapacity() {
        return carPassengerCapacity;
    }

    public void setCarPassengerCapacity(String carPassengerCapacity) {
        this.carPassengerCapacity = carPassengerCapacity;
    }

    public String getCarLuggageCapacity() {
        return carLuggageCapacity;
    }

    public void setCarLuggageCapacity(String carLuggageCapacity) {
        this.carLuggageCapacity = carLuggageCapacity;
    }

    public Integer getCarImage() {
        return carImage;
    }

    public void setCarImage(Integer carImage) {
        this.carImage = carImage;
    }
}
