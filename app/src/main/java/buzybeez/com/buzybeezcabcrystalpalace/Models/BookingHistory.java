package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class BookingHistory {
    public static BookingHistory sharedinstance = new BookingHistory();
    private int bookingID;
    @JsonProperty("_id")
    private String Id;
    @JsonProperty("account")
    private String account;
    @JsonProperty("accuser")
    private String accuser;
    @JsonProperty("changed")
    private Boolean changed;
    @JsonProperty("flag")
    private Integer flag;
    @JsonProperty("hold")
    private Boolean hold;
    @JsonProperty("date")
    private String date;
    @JsonProperty("time")
    private String time;
    @JsonProperty("datentime")
    private long datentime;
    @JsonProperty("custname")
    private String custname;
    @JsonProperty("userid")
    private String userid;
    @JsonProperty("driverrate")
    private String driverrate;
    @JsonProperty("timetodespatch")
    private String timetodespatch;
    @JsonProperty("tag")
    private String tag;
    @JsonProperty("fare")
    private Double fare;
    @JsonProperty("oldfare")
    private Double oldfare;
    @JsonProperty("drvfare")
    private Double drvfare;
    @JsonProperty("olddrvfare")
    private Double olddrvfare;
    @JsonProperty("from")
    private String from;
    @JsonProperty("to")
    private String to;
    @JsonProperty("from_outcode")
    private String fromOutcode;
    @JsonProperty("to_outcode")
    private String toOutcode;
    @JsonProperty("from_info")
    private String fromInfo;
    @JsonProperty("to_info")
    private String toInfo;
    @JsonProperty("jobtype")
    private String jobtype;
    @JsonProperty("fromtovia")
    private List<FromToVia> fromtovia = new ArrayList<>();
    @JsonProperty("vehicletype")
    private String vehicletype;
    @JsonProperty("jobref")
    private String jobref;
    @JsonProperty("jobmileage")
    private Double jobmileage;
    @JsonProperty("leadtime")
    private Double leadtime;
    @JsonProperty("office")
    private String office;
    @JsonProperty("orderno")
    private String orderno;
    @JsonProperty("bookedby")
    private String bookedby;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty("creditcard")
    private String creditcard;
    @JsonProperty("drvrcallsign")
    private String drvrcallsign;
    @JsonProperty("drvrname")
    private String drvrname;
    @JsonProperty("drvrreqcallsign")
    private String drvrreqcallsign;
    @JsonProperty("drvreqdname")
    private String drvreqdname;
    @JsonProperty("flightno")
    private String flightno;
    @JsonProperty("telephone")
    private String telephone;
    @JsonProperty("jstate")
    private String jstate;
    @JsonProperty("mstate")
    private String mstate;
    @JsonProperty("dstate")
    private String dstate;
    @JsonProperty("cstate")
    private String cstate;
    @JsonProperty("isdirty")
    private Boolean isdirty;
    @JsonProperty("numofvia")
    private Integer numofvia;
    @JsonProperty("logd")
    private List<Logd> logd = new ArrayList<>();
    @JsonProperty("logc")
    private Object[] logc;
    private String bookingStatus;

    public BookingHistory() {
    }

    public BookingHistory(String date, String time, String origin,
                          String dest, double fare, String bookingStatus, String jobId, String jobRef, long dateNtime, int bookingId) {
        this.date = date;
        this.time = time;
        this.from = origin;
        this.to = dest;
        this.fare = fare;
        this.bookingStatus = bookingStatus;
        this.Id = jobId;
        this.jobref = jobRef;
        this.datentime = dateNtime;
        this.bookingID = bookingId;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public Object[] getLogc() {
        return logc;
    }

    public void setLogc(Object[][] logc) {
        this.logc = logc;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    /*@JsonProperty("logc")
    private ArrayList<Object> logc = new ArrayList<>();*/

 /*   @JsonProperty("logc")
    private Object[][] logc;*/


    /*@JsonProperty("jobtype")
    private String jobType;
    public String getJobType(){return jobType;}
    public void setJobType(String s){jobtype}*/

    /**
     * @return The Id
     */
    @JsonProperty("_id")
    public String getId() {
        return Id;
    }

    /**
     * @param Id The _id
     */
    @JsonProperty("_id")
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     * @return The account
     */
    @JsonProperty("account")
    public String getAccount() {
        return account;
    }

    /**
     *
     * @param account
     * The account
     */
    /*@JsonProperty("account")
    public void setAccount(String account) {
        this.account = account;
    }*/

    /**
     *
     * @return
     * The accuser
     */

    /*@JsonProperty("accuser")
    public String getAccuser() {
        return accuser;
    }*/

    /**
     *
     * @param accuser
     * The accuser
     */

    /*@JsonProperty("accuser")
    public void setAccuser(String accuser) {
        this.accuser = accuser;
    }*/

    /**
     * @return The changed
     */

    @JsonProperty("changed")
    public Boolean getChanged() {
        return changed;
    }

    /**
     * @param changed The changed
     */
    @JsonProperty("changed")
    public void setChanged(Boolean changed) {
        this.changed = changed;
    }

    /**
     * @return The flag
     */
    @JsonProperty("flag")
    public Integer getFlag() {
        return flag;
    }

    /**
     * @param flag The flag
     */
    @JsonProperty("flag")
    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    /**
     * @return The hold
     */
    @JsonProperty("hold")
    public Boolean getHold() {
        return hold;
    }

    /**
     * @param hold The hold
     */
    @JsonProperty("hold")
    public void setHold(Boolean hold) {
        this.hold = hold;
    }

    /**
     * @return The date
     */
    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return The time
     */
    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    @JsonProperty("time")
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return The datentime
     */
    @JsonProperty("datentime")
    public long getDatentime() {
        return datentime;
    }

    /**
     * @param datentime The datentime
     */
    @JsonProperty("datentime")
    public void setDatentime(long datentime) {
        this.datentime = datentime;
    }

    /**
     * @return The custname
     */
    @JsonProperty("custname")
    public String getCustname() {
        return custname;
    }

    /**
     * @param custname The custname
     */
    @JsonProperty("custname")
    public void setCustname(String custname) {
        this.custname = custname;
    }

    /**
     * @return The userid
     */
    @JsonProperty("userid")
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid The userid
     */
    @JsonProperty("userid")
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return The driverrate
     */
    @JsonProperty("driverrate")
    public String getDriverrate() {
        return driverrate;
    }

    /**
     * @param driverrate The driverrate
     */
    @JsonProperty("driverrate")
    public void setDriverrate(String driverrate) {
        this.driverrate = driverrate;
    }

    /**
     * @return The timetodespatch
     */
    @JsonProperty("timetodespatch")
    public String getTimetodespatch() {
        return timetodespatch;
    }

    /**
     * @param timetodespatch The timetodespatch
     */
    @JsonProperty("timetodespatch")
    public void setTimetodespatch(String timetodespatch) {
        this.timetodespatch = timetodespatch;
    }

    /**
     * @return The tag
     */
    @JsonProperty("tag")
    public String getTag() {
        return tag;
    }

    /**
     * @param tag The tag
     */
    @JsonProperty("tag")
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * @return The fare
     */
    @JsonProperty("fare")
    public Double getFare() {
        return fare;
    }

    /**
     * @param fare The fare
     */
    @JsonProperty("fare")
    public void setFare(Double fare) {
        this.fare = fare;
    }

    @JsonProperty("oldfare")
    public Double getOldFare() {
        return oldfare;
    }


    @JsonProperty("oldfare")
    public void setOldFare(Double oldfare) {
        this.oldfare = oldfare;
    }

    /**
     * @return The drvfare
     */
    @JsonProperty("drvfare")
    public Double getDrvfare() {
        return drvfare;
    }

    /**
     * @param drvfare The drvfare
     */
    @JsonProperty("drvfare")
    public void setDrvfare(Double drvfare) {
        this.drvfare = drvfare;
    }

    @JsonProperty("olddrvfare")
    public Double getOldDrvfare() {
        return olddrvfare;
    }

    @JsonProperty("olddrvfare")
    public void setOldDrvfare(Double olddrvfare) {
        this.olddrvfare = olddrvfare;
    }

    /**
     * @return The from
     */
    @JsonProperty("from")
    public String getFrom() {
        return from;
    }

    /**
     * @param from The from
     */
    @JsonProperty("from")
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return The to
     */
    @JsonProperty("to")
    public String getTo() {
        return to;
    }

    /**
     * @param to The to
     */
    @JsonProperty("to")
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return The fromOutcode
     */
    @JsonProperty("from_outcode")
    public String getFromOutcode() {
        return fromOutcode;
    }

    /**
     * @param fromOutcode The from_outcode
     */
    @JsonProperty("from_outcode")
    public void setFromOutcode(String fromOutcode) {
        if (fromOutcode.isEmpty())
            this.fromOutcode = "NPC";
        else
            this.fromOutcode = fromOutcode;
    }

    /**
     * @return The toOutcode
     */
    @JsonProperty("to_outcode")
    public String getToOutcode() {
        return toOutcode;
    }

    /**
     * @param toOutcode The to_outcode
     */
    @JsonProperty("to_outcode")
    public void setToOutcode(String toOutcode) {
        if (toOutcode.isEmpty())
            this.toOutcode = "NPC";
        else
            this.toOutcode = toOutcode;
    }

    /**
     * @return The fromInfo
     */
    @JsonProperty("from_info")
    public String getFromInfo() {
        return fromInfo;
    }

    /**
     * @param fromInfo The from_info
     */
    @JsonProperty("from_info")
    public void setFromInfo(String fromInfo) {
        this.fromInfo = fromInfo;
    }

    /**
     * @return The toInfo
     */
    @JsonProperty("to_info")
    public String getToInfo() {
        return toInfo;
    }

    /**
     * @param toInfo The to_info
     */
    @JsonProperty("to_info")
    public void setToInfo(String toInfo) {
        this.toInfo = toInfo;
    }

    /**
     * @return The jobtype
     */
    @JsonProperty("jobtype")
    public String getJobtype() {
        return jobtype;
    }

    /**
     * @param jobtype The jobtype
     */
    @JsonProperty("jobtype")
    public void setJobtype(String jobtype) {
        this.jobtype = jobtype;
    }

    /**
     * @return The fromtovia
     */
    @JsonProperty("fromtovia")
    public List<FromToVia> getFromtovia() {
        return fromtovia;
    }

    /**
     * @param fromtovia The fromtovia
     */
    @JsonProperty("fromtovia")
    public void setFromtovia(List<FromToVia> fromtovia) {
        this.fromtovia = fromtovia;
    }

    /**
     * @return The vehicletype
     */
    @JsonProperty("vehicletype")
    public String getVehicletype() {
        return vehicletype;
    }

    /**
     * @param vehicletype The vehicletype
     */
    @JsonProperty("vehicletype")
    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    /**
     * @return The jobref
     */
    @JsonProperty("jobref")
    public String getJobref() {
        return jobref;
    }

    /**
     * @param jobref The jobref
     */
    @JsonProperty("jobref")
    public void setJobref(String jobref) {
        this.jobref = jobref;
    }

    /**
     * @return The jobmileage
     */
    @JsonProperty("jobmileage")
    public Double getJobmileage() {
        return jobmileage;
    }

    /**
     * @param jobmileage The jobmileage
     */
    @JsonProperty("jobmileage")
    public void setJobmileage(Double jobmileage) {
        this.jobmileage = jobmileage;
    }

    /**
     * @return The leadtime
     */
    @JsonProperty("leadtime")
    public Double getLeadtime() {
        return leadtime;
    }

    /**
     * @param leadtime The leadtime
     */
    @JsonProperty("leadtime")
    public void setLeadtime(Double leadtime) {
        this.leadtime = leadtime;
    }

    /**
     * @return The office
     */
    @JsonProperty("office")
    public String getOffice() {
        return office;
    }

    /**
     * @param office The office
     */
    @JsonProperty("office")
    public void setOffice(String office) {
        this.office = office;
    }

    /**
     * @return The orderno
     */
    @JsonProperty("orderno")
    public String getOrderno() {
        return orderno;
    }

    /**
     * @param orderno The orderno
     */
    @JsonProperty("orderno")
    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    /**
     * @return The bookedby
     */
    @JsonProperty("bookedby")
    public String getBookedby() {
        return bookedby;
    }

    /**
     * @param bookedby The bookedby
     */
    @JsonProperty("bookedby")
    public void setBookedby(String bookedby) {
        this.bookedby = bookedby;
    }

    /**
     * @return The comment
     */
    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return The creditcard
     */
    @JsonProperty("creditcard")
    public String getCreditcard() {
        return creditcard;
    }

    /**
     * @param creditcard The creditcard
     */
    @JsonProperty("creditcard")
    public void setCreditcard(String creditcard) {
        this.creditcard = creditcard;
    }

    /**
     * @return The drvrcallsign
     */
    @JsonProperty("drvrcallsign")
    public String getDrvrcallsign() {
        return drvrcallsign;
    }

    /**
     * @param drvrcallsign The drvrcallsign
     */
    @JsonProperty("drvrcallsign")
    public void setDrvrcallsign(String drvrcallsign) {
        this.drvrcallsign = drvrcallsign;
    }

    /**
     * @return The drvrname
     */
    @JsonProperty("drvrname")
    public String getDrvrname() {
        return drvrname;
    }

    /**
     * @param drvrname The drvrname
     */
    @JsonProperty("drvrname")
    public void setDrvrname(String drvrname) {
        this.drvrname = drvrname;
    }

    /**
     * @return The drvrreqcallsign
     */
    @JsonProperty("drvrreqcallsign")
    public String getDrvrreqcallsign() {
        return drvrreqcallsign;
    }

    /**
     * @param drvrreqcallsign The drvrreqcallsign
     */
    @JsonProperty("drvrreqcallsign")
    public void setDrvrreqcallsign(String drvrreqcallsign) {
        this.drvrreqcallsign = drvrreqcallsign;
    }

    /**
     * @return The drvreqdname
     */
    @JsonProperty("drvreqdname")
    public String getDrvreqdname() {
        return drvreqdname;
    }

    /**
     * @param drvreqdname The drvreqdname
     */
    @JsonProperty("drvreqdname")
    public void setDrvreqdname(String drvreqdname) {
        this.drvreqdname = drvreqdname;
    }

    /**
     * @return The flightno
     */
    @JsonProperty("flightno")
    public String getFlightno() {
        return flightno;
    }

    /**
     * @param flightno The flightno
     */
    @JsonProperty("flightno")
    public void setFlightno(String flightno) {
        this.flightno = flightno;
    }

    /**
     * @return The telephone
     */
    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone The telephone
     */
    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return The jstate
     */
    @JsonProperty("jstate")
    public String getJstate() {
        return jstate;
    }

    /**
     * @param jstate The jstate
     */
    @JsonProperty("jstate")
    public void setJstate(String jstate) {
        this.jstate = jstate;
    }

    /**
     * @return The mstate
     */
    @JsonProperty("mstate")
    public String getMstate() {
        return mstate;
    }

    /**
     * @param mstate The mstate
     */
    @JsonProperty("mstate")
    public void setMstate(String mstate) {
        this.mstate = mstate;
    }

    /**
     * @return The dstate
     */
    @JsonProperty("dstate")
    public String getDstate() {
        return dstate;
    }

    /**
     * @param dstate The dstate
     */
    @JsonProperty("dstate")
    public void setDstate(String dstate) {
        this.dstate = dstate;
    }

    /**
     * @return The cstate
     */
    @JsonProperty("cstate")
    public String getCstate() {
        return cstate;
    }

    /**
     * @param cstate The cstate
     */
    @JsonProperty("cstate")
    public void setCstate(String cstate) {
        this.cstate = cstate;
    }

    /**
     * @return The isdirty
     */
    @JsonProperty("isdirty")
    public Boolean getIsdirty() {
        return isdirty;
    }

    /**
     * @param isdirty The isdirty
     */
    @JsonProperty("isdirty")
    public void setIsdirty(Boolean isdirty) {
        this.isdirty = isdirty;
    }

    /**
     * @return The numofvia
     */
    @JsonProperty("numofvia")
    public Integer getNumofvia() {
        return numofvia;
    }

    /**
     * @param numofvia The numofvia
     */
    @JsonProperty("numofvia")
    public void setNumofvia(Integer numofvia) {
        this.numofvia = numofvia;
    }

    /*
     * @return
     * The logd
     */

    @JsonProperty("logd")
    public List<Logd> getLogd() {
        return logd;
    }

    /*
     *
     * @param logd
     * The logd
     */

    @JsonProperty("logd")
    public void setLogd(List<Logd> logd) {
        this.logd = logd;
    }

}
