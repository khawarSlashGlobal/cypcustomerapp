package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hv on 4/5/18.
 */

public class CustomerReg {

    public static CustomerReg sharedinstance = new CustomerReg();
    @JsonProperty("_id")
    private String _id;
    @JsonProperty("cust_uid")
    private String cust_uid;
    @JsonProperty("blacklist")
    private boolean blacklist;
    @JsonProperty("cust_email")
    private String cust_email;
    @JsonProperty("cust_name")
    private String cust_name;
    @JsonProperty("cust_phone")
    private String cust_phone;

    public CustomerReg() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCust_uid() {
        return cust_uid;
    }

    public void setCust_uid(String cust_uid) {
        this.cust_uid = cust_uid;
    }

    public boolean isBlacklist() {
        return blacklist;
    }

    public void setBlacklist(boolean blacklist) {
        this.blacklist = blacklist;
    }

    public String getCust_email() {
        return cust_email;
    }

    public void setCust_email(String cust_email) {
        this.cust_email = cust_email;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCust_phone() {
        return cust_phone;
    }

    public void setCust_phone(String cust_phone) {
        this.cust_phone = cust_phone;
    }
}
