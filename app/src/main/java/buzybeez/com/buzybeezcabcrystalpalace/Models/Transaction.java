package buzybeez.com.buzybeezcabcrystalpalace.Models;

public class Transaction {

    String cardId;
    String amount;
    String currency;
    String date;
    String time;

    public Transaction() {
    }

    public Transaction(String cardId, String amount, String currency, String date, String time) {
        this.cardId = cardId;
        this.amount = amount;
        this.currency = currency;
        this.date = date;
        this.time = time;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDateTime() {
        return date;
    }

    public void setDateTime(String dateTime) {
        this.date = dateTime;
    }
}
