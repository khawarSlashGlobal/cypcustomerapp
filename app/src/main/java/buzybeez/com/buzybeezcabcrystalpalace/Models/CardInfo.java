package buzybeez.com.buzybeezcabcrystalpalace.Models;

public class CardInfo {

    String brand ;
    String country;
    String expMonth;
    String expYear ;
    String cvcCheck ;
    String last4;
    String id ;
    String custId;
    String fingerprint;


    public CardInfo() {
    }

    public CardInfo(String brand, String country, String expMonth, String expYear, String cvcCheck, String last4, String id, String custId, String fingerprint) {
        this.brand = brand;
        this.country = country;
        this.expMonth = expMonth;
        this.expYear = expYear;
        this.cvcCheck = cvcCheck;
        this.last4 = last4;
        this.id = id;
        this.custId=custId;
        this.fingerprint=fingerprint;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }

    public String getCvcCheck() {
        return cvcCheck;
    }

    public void setCvcCheck(String cvcCheck) {
        this.cvcCheck = cvcCheck;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }


    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }
}
