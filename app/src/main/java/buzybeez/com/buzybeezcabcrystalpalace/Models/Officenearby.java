package buzybeez.com.buzybeezcabcrystalpalace.Models;

/**
 * Created by SGI on 31/10/2016.
 */

public class Officenearby {

    //String Account_Name;
    String offices, number;
    double lng, lat;


    public Officenearby(String offices, double lat, double lng) {
        this.offices = offices;
        this.lat = lat;
        this.lng = lng;
    }

    public Officenearby() {
    }

    public String getoffices() {
        return offices;
    }

    public void setoffices(String offices) {
        this.offices = offices;
    }

    public double getlat() {
        return lat;
    }

    public void setlat(double lat) {
        this.lat = lat;
    }

    public double getlng() {
        return lng;
    }

    public void setlng(double lng) {
        this.lng = lng;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "OfficeModel{" +
                ", offices='" + offices + '\'' +
                ", lat=" + lat +
                ", long=" + lng +
                ", number=" + number +
                '}';
    }
}
