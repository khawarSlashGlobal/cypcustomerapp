package buzybeez.com.buzybeezcabcrystalpalace.Models;

import java.io.Serializable;

public class User implements Serializable {

    String image;

    public User() {
    }


    public User(String image) {
        this.image = image;

    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
