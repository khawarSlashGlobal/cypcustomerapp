package buzybeez.com.buzybeezcabcrystalpalace.Models;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class ListCountry {

    private Map<String, String> languagesMap = new TreeMap<String, String>();

    public ListCountry() {
        initLanguageMap();
    }

    public static void main(String[] args) {

        ListCountry obj = new ListCountry();
        obj.getListOfCountries();

    }

    public ArrayList<String> getListOfCountries(Locale locale) {
        String local = " ";
        ArrayList<String> loc = new ArrayList<>();
        String[] locales = Locale.getISOCountries();

        for (String countryCode : locales) {
            Locale obj = new Locale("", countryCode);
            System.out.println("Country Code = " + obj.getCountry()
                    + ", Country Name = " + obj.getDisplayCountry(locale));
            local = "Country Code = " + obj.getCountry() +
                    ", Country Name = " + obj.getDisplayCountry(obj);
            if (obj.getCountry().toLowerCase().equals("uk")
                    || obj.getCountry().toLowerCase().equals("gb")
                    || obj.getCountry().toLowerCase().equals("us")
                    || obj.getCountry().toLowerCase().equals("pk")) {
                loc.add(local);
            }
        }
        return loc;
    }

    public List<Locale> getListOfCountries() {

        String[] countries = Locale.getISOCountries();
        ArrayList<Locale> list = new ArrayList<Locale>();
        int supportedLocale = 0, nonSupportedLocale = 0;
        Locale obj = null;

        for (String countryCode : countries) {

            obj = null;
            if (languagesMap.get(countryCode) == null) {

                obj = new Locale("", countryCode);
                nonSupportedLocale++;

            } else {

                //create a Locale with own country's languages
                obj = new Locale(languagesMap.get(countryCode), countryCode);
                supportedLocale++;
            }

            System.out.println("Country Code = " + obj.getCountry()
                    + ", Country Name = " + obj.getDisplayCountry(obj)
                    + ", Languages = " + obj.getDisplayLanguage());
            list.add(obj);
        }

        System.out.println("nonSupportedLocale : " + nonSupportedLocale);
        System.out.println("supportedLocale : " + supportedLocale);
        return list;
    }

    // create Map with country code and languages
    public void initLanguageMap() {

        Locale[] locales = Locale.getAvailableLocales();

        for (Locale obj : locales) {

            if ((obj.getDisplayCountry() != null) && (!"".equals(obj.getDisplayCountry()))) {
                languagesMap.put(obj.getCountry(), obj.getLanguage());
            }

        }

    }

}
