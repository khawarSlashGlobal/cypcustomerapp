package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Logd {

    @JsonProperty("state")
    private String state;
    @JsonProperty("timestamp")
    private Double timestamp;
    @JsonProperty("uniqueid")
    private String uniqueid;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("speed")
    private Double speed;
    @JsonProperty("accuracy")
    private Double accuracy;

    /**
     * @return The state
     */
    @JsonProperty("state")
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The timestamp
     */
    @JsonProperty("timestamp")
    public Double getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp The timestamp
     */
    @JsonProperty("timestamp")
    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return The uniqueid
     */
    @JsonProperty("uniqueid")
    public String getUniqueid() {
        return uniqueid;
    }

    /**
     * @param uniqueid The uniqueid
     */
    @JsonProperty("uniqueid")
    public void setUniqueid(String uniqueid) {
        this.uniqueid = uniqueid;
    }

    /**
     * @return The longitude
     */
    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The longitude
     */
    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return The latitude
     */
    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude The latitude
     */
    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return The speed
     */
    @JsonProperty("speed")
    public Double getSpeed() {
        return speed;
    }

    /**
     * @param speed The speed
     */
    @JsonProperty("speed")
    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    /**
     * @return The accuracy
     */
    @JsonProperty("accuracy")
    public Double getAccuracy() {
        return accuracy;
    }

    /**
     * @param accuracy The accuracy
     */
    @JsonProperty("accuracy")
    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

}
/*
import java.util.ArrayList;
import java.util.List;

public class Logd {

    @JsonProperty("logd")
    private List<Logd_> logd = new ArrayList<Logd_>();


    */
/**
 * @return The logd
 * @param logd The logd
 *//*

    @JsonProperty("logd")
    public List<Logd_> getLogd() {
        return logd;
    }

    */
/**
 * @param logd The logd
 *//*

    @JsonProperty("logd")
    public void setLogd(List<Logd_> logd) {
        this.logd = logd;
    }
}*/
