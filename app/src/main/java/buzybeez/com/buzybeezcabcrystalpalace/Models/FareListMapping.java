package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hv on 3/12/18.
 */

public class FareListMapping {

    public static FareListMapping sharedInstanceFareList = new FareListMapping();
    @JsonProperty("Acc_Unit")
    public List<String> acc_Unit = new ArrayList<String>();
    @JsonProperty("_id")
    private String _id;

    @JsonProperty("RateName")
    private String rateName;

    @JsonProperty("OfficeID")
    private String officeID;

    @JsonProperty("VehicleType")
    private String vehicleType;

    @JsonProperty("vehsymbol")
    private String vehSymbol;

    @JsonProperty("Acc_Round")
    private String acc_Round;

    @JsonProperty("Acc_RTypePriceRb")
    private String acc_RTypePriceRb;

    @JsonProperty("Acc_ToNearest")
    private String acc_ToNearest;

    @JsonProperty("Acc_VATRate")
    private String acc_VatRate;

    @JsonProperty("Acc_MinPrice")
    private String acc_MinPrice;

    @JsonProperty("Acc_ExtDrop")
    private String acc_ExtDrop;
    @JsonProperty("Acc_cost")
    private List<String> acc_Cost = new ArrayList<String>();
    @JsonProperty("Drv_Unit")
    private List<String> drv_Unit = new ArrayList<String>();
    @JsonProperty("Drv_cost")
    private List<String> drv_Cost = new ArrayList<String>();
    @JsonProperty("Drv_Round")
    private String drv_Round;
    @JsonProperty("Drv_RTypePriceRb")
    private String drv_RTypePriceRb;
    @JsonProperty("Drv_ToNearest")
    private String drv_ToNearest;
    @JsonProperty("Drv_VATRate")
    private String drv_VATRate;
    @JsonProperty("Drv_MinPrice")
    private String drv_MinPrice;
    @JsonProperty("Drv_ExtDrop")
    private String drv_ExtDrop;
    @JsonProperty("Drv_perc")
    private String drv_Perc;
    @JsonProperty("status")
    private boolean status;

    public FareListMapping() {
    }

    public List<String> getAcc_Unit() {
        return acc_Unit;
    }

    public void setAcc_Unit(List<String> acc_Unit) {
        this.acc_Unit = acc_Unit;
    }

    public List<String> getAcc_Cost() {
        return acc_Cost;
    }

    public void setAcc_Cost(List<String> acc_Cost) {
        this.acc_Cost = acc_Cost;
    }

    public List<String> getDrv_Unit() {
        return drv_Unit;
    }

    public void setDrv_Unit(List<String> drv_Unit) {
        this.drv_Unit = drv_Unit;
    }

    public List<String> getDrv_Cost() {
        return drv_Cost;
    }

    public void setDrv_Cost(List<String> drv_Cost) {
        this.drv_Cost = drv_Cost;
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getRateName() {
        return rateName;
    }

    public void setRateName(String rateName) {
        this.rateName = rateName;
    }

    public String getOfficeID() {
        return officeID;
    }

    public void setOfficeID(String officeID) {
        this.officeID = officeID;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehSymbol() {
        return vehSymbol;
    }

    public void setVehSymbol(String vehSymbol) {
        this.vehSymbol = vehSymbol;
    }

    public String getAcc_Round() {
        return acc_Round;
    }

    public void setAcc_Round(String acc_Round) {
        this.acc_Round = acc_Round;
    }

    public String getAcc_RTypePriceRb() {
        return acc_RTypePriceRb;
    }

    public void setAcc_RTypePriceRb(String acc_RTypePriceRb) {
        this.acc_RTypePriceRb = acc_RTypePriceRb;
    }

    public String getAcc_ToNearest() {
        return acc_ToNearest;
    }

    public void setAcc_ToNearest(String acc_ToNearest) {
        this.acc_ToNearest = acc_ToNearest;
    }

    public String getAcc_VatRate() {
        return acc_VatRate;
    }

    public void setAcc_VatRate(String acc_VatRate) {
        this.acc_VatRate = acc_VatRate;
    }

    public String getAcc_MinPrice() {
        return acc_MinPrice;
    }

    public void setAcc_MinPrice(String acc_MinPrice) {
        this.acc_MinPrice = acc_MinPrice;
    }

    public String getAcc_ExtDrop() {
        return acc_ExtDrop;
    }

    public void setAcc_ExtDrop(String acc_ExtDrop) {
        this.acc_ExtDrop = acc_ExtDrop;
    }


    public String getDrv_Round() {
        return drv_Round;
    }

    public void setDrv_Round(String drv_Round) {
        this.drv_Round = drv_Round;
    }

    public String getDrv_RTypePriceRb() {
        return drv_RTypePriceRb;
    }

    public void setDrv_RTypePriceRb(String drv_RTypePriceRb) {
        this.drv_RTypePriceRb = drv_RTypePriceRb;
    }

    public String getDrv_ToNearest() {
        return drv_ToNearest;
    }

    public void setDrv_ToNearest(String drv_ToNearest) {
        this.drv_ToNearest = drv_ToNearest;
    }

    public String getDrv_VATRate() {
        return drv_VATRate;
    }

    public void setDrv_VATRate(String drv_VATRate) {
        this.drv_VATRate = drv_VATRate;
    }

    public String getDrv_MinPrice() {
        return drv_MinPrice;
    }

    public void setDrv_MinPrice(String drv_MinPrice) {
        this.drv_MinPrice = drv_MinPrice;
    }

    public String getDrv_ExtDrop() {
        return drv_ExtDrop;
    }

    public void setDrv_ExtDrop(String drv_ExtDrop) {
        this.drv_ExtDrop = drv_ExtDrop;
    }


    public String getDrv_Perc() {
        return drv_Perc;
    }

    public void setDrv_Perc(String drv_Perc) {
        this.drv_Perc = drv_Perc;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


}
