package buzybeez.com.buzybeezcabcrystalpalace.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hv on 5/10/15 DriverApp3.
 */
public class FavorP {

    @JsonProperty("Oinfo")
    private Object Oinfo;
    @JsonProperty("Oaddress")
    private Object Oaddress;
    @JsonProperty("olat")
    private double Olat;
    @JsonProperty("olon")
    private double Olon;

    /**
     * @return The info
     */
    @JsonProperty("Oinfo")
    public Object getOriginInfo() {
        return Oinfo;
    }

    /**
     * @param info The info
     */
    @JsonProperty("Oinfo")
    public void setOriginInfo(Object info) {
        this.Oinfo = info;
    }

    /**
     *
     * @param info
     * The info
     */

    /**
     * @return The address
     */
    @JsonProperty("Oaddress")
    public Object getOriginAddress() {
        return Oaddress;
    }

    /**
     * @param address The address
     */
    @JsonProperty("Oaddress")
    public void setOriginAddress(Object address) {
        this.Oaddress = address;
    }

    /**
     *
     * @param address
     * The address
     */

    /**
     * @return The lat
     */
    @JsonProperty("olat")
    public double getOriginLat() {
        return Olat;
    }

    /**
     * @param lat The lat
     */
    @JsonProperty("olat")
    public void setOriginLat(double lat) {
        this.Olat = lat;
    }


    @JsonProperty("olon")
    public double getOriginLon() {
        return Olon;
    }

    /**
     * @param lon The lon
     */
    @JsonProperty("olon")
    public void setOriginLon(double lon) {
        this.Olon = lon;
    }

    /**
     *
     * @param lon
     * The lon
     */


}