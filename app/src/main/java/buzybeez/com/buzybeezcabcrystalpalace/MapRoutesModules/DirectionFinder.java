package buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import buzybeez.com.buzybeezcabcrystalpalace.Models.CfgCustApp;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FromToVia;

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */
public class DirectionFinder {
    private static final String DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/json?";
   // private static final String GOOGLE_API_KEY = "AIzaSyA-tV6onCgvn03jfWd00JzRFTsPFOemjXo";
   private static final String GOOGLE_API_KEY = CfgCustApp.sharedinstance.getGoogleDirectionKey();

    private static boolean flag;
    //new ArrayList<String>();
    Route route;
    private int flag2 = 0;
    private DirectionFinderListener listener;
    private TimeToDespatcInterface ref;
    private String origin;
    private String destination;
    private List<FromToVia> fromtoviaList = new ArrayList<>();


    public DirectionFinder(Context context, String origin, List<FromToVia> fromtoviaList, String destination, boolean flag) {
        this.listener = (DirectionFinderListener) context;
        this.origin = origin;
        this.destination = destination;
        this.fromtoviaList = fromtoviaList;
        this.flag = flag;
        this.ref = (TimeToDespatcInterface) context;
        route = new Route();
    }

    public DirectionFinder(Context context, String origin, String destination, int flag2) {
        this.listener = (DirectionFinderListener) context;
        this.origin = origin;
        this.destination = destination;
        this.flag2 = flag2;
        route = new Route();
    }

    public void execute() throws UnsupportedEncodingException {
        if (flag2 == 1) {
            listener.onDirectionFinderStart();
            new DownloadRawData().execute(createUrl());
            return;
        }
        if (flag == true) {
            listener.onDirectionFinderStart();
        } else {
            ref.start();
        }
        new DownloadRawData().execute(createUrl());
    }

    public String getWaypointString() throws UnsupportedEncodingException {
        String wPoints = "";
        for (FromToVia ftv : fromtoviaList) {

            wPoints = wPoints + URLEncoder.encode((String) ftv.getAddress(), "utf-8") + "|";
        }

        if (wPoints != null && wPoints.length() > 0 && wPoints.charAt(wPoints.length() - 1) == '|') {
            wPoints = wPoints.substring(0, wPoints.length() - 1);
        }
        wPoints = "&waypoints=" + wPoints;
        //  wPoints = "&via:"+wPoints;

        return wPoints;
    }

    private String createUrl() throws UnsupportedEncodingException {


        String urlOrigin = URLEncoder.encode(origin, "utf-8");
        String urlDestination = URLEncoder.encode(destination, "utf-8");
        String url;


        if (fromtoviaList.size() == 0) {
            url = DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&avoid=tolls|highways|ferries&mode=driving&alternatives=true&key=" + GOOGLE_API_KEY;

        } else {
            //this is how should be the url must be with waypoints
            // https://maps.googleapis.com/maps/api/directions/json?origin=Boston,MA&destination=Concord,MA&waypoints=Charlestown,MA|Lexington,MA&key=AIzaSyA-tV6onCgvn03jfWd00JzRFTsPFOemjXo
            String wp = getWaypointString();
            url = DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + wp + "&avoid=tolls|highways|ferries&mode=driving&alternatives=true&key=" + GOOGLE_API_KEY;
        }

        return url;
    }
    private void parseJSon(String data) throws JSONException {
        if (data == null)
            return;

       if((isApiExpired(data))==true){
           return;
       }
        List<Distance> distance1 = new ArrayList<>();
        List<Duration> duration1 = new ArrayList<>();
        List<String> endAddress1 = new ArrayList<>();
        List<LatLng> endLocation1 = new ArrayList<>();
        List<String> startAddress1 = new ArrayList<>();
        List<LatLng> startLocation1 = new ArrayList<>();

        List<Route> routes = new ArrayList<Route>();
        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonRoutes = jsonData.getJSONArray("routes");
      //  for (int i = 0; i < jsonRoutes.length(); i++) {
             int   i = getShortestRouteIndex( data);
            JSONObject jsonRoute = jsonRoutes.getJSONObject(i);
            JSONObject overview_polylineJson = jsonRoute.getJSONObject("overview_polyline");
            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");

            for (int j = 0; j <= fromtoviaList.size(); j++) {
                JSONObject jsonLeg = jsonLegs.getJSONObject(j);
                JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
                JSONObject jsonDuration = jsonLeg.getJSONObject("duration");
                JSONObject jsonEndLocation = jsonLeg.getJSONObject("end_location");
                JSONObject jsonStartLocation = jsonLeg.getJSONObject("start_location");

                distance1.add(new Distance(jsonDistance.getString("text"), jsonDistance.getInt("value")));
                duration1.add(new Duration(jsonDuration.getString("text"), jsonDuration.getInt("value")));
                endAddress1.add(jsonLeg.getString("end_address"));
                startAddress1.add(jsonLeg.getString("start_address"));
                startLocation1.add(new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng")));
                endLocation1.add(new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng")));
            }
            route.distance = distance1;
            route.duration = duration1;
            route.endAddress = endAddress1;
            route.endLocation = endLocation1;
            route.startAddress = startAddress1;
            route.startLocation = startLocation1;
            route.points = decodePolyLine(overview_polylineJson.getString("points"));
            routes.add(route);

        if (flag2 == 1) {
            listener.onDirectionFinderSuccess(routes);
            return;
        }
        if (flag == true) {
            listener.onDirectionFinderSuccess(routes);
        } else {

            Double totalDuration = 0.0;
            for (Duration d : route.duration) {
                totalDuration = totalDuration + d.value;
            }
            //converting sec to min, 1 sec = 0.0166667 min
            totalDuration = totalDuration * 0.0166667;
            totalDuration = round(totalDuration, 0);
            ref.finish(totalDuration + " min");

        }

    }
    private boolean isApiExpired(String data) throws JSONException {
        JSONObject jsonData = new JSONObject(data);
        String apiStatus = jsonData.getString("status");
        boolean isapiExpired = false;
        switch (apiStatus){
            case "OVER_QUERY_LIMIT":
                String error_message = jsonData.getString("error_message");
                listener.onDirectionFinderFail(error_message);
                isapiExpired = true;
                break;
            case "OK":
                isapiExpired = false;
                break;
        }
        return isapiExpired;
    }
    private int getShortestRouteIndex(String data) throws JSONException {

        ArrayList<Double> allRoutesDistancesList = new ArrayList<>();

        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonRoutes = jsonData.getJSONArray("routes");
        for (int i = 0; i < jsonRoutes.length(); i++) {
            List<Distance> routeDistances = new ArrayList<>();
            JSONObject jsonRoute = jsonRoutes.getJSONObject(i);
            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");
            for (int j = 0; j <= fromtoviaList.size(); j++) {
                JSONObject jsonLeg = jsonLegs.getJSONObject(j);
                JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
                routeDistances.add(new Distance(jsonDistance.getString("text"), jsonDistance.getInt("value")));
                    }

            Double totalDistance = 0.0;
            for (Distance d : routeDistances) {
                totalDistance = totalDistance + d.value;
            }
            //converting sec to min, 1 sec = 0.0166667 min
            totalDistance = totalDistance * 0.0166667;
            totalDistance = round(totalDistance, 0);
            allRoutesDistancesList.add(totalDistance);
        }
        if(allRoutesDistancesList.size()==0){return  0 ;}else {
        return  allRoutesDistancesList.indexOf(Collections.min(allRoutesDistancesList));}

    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.CEILING);
        return bd.doubleValue();
    }

    private List<LatLng> decodePolyLine(final String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<LatLng>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(
                    lat / 100000d, lng / 100000d
            ));
        }
        return decoded;
    }

    private class DownloadRawData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String link = params[0];
            try {
                URL url = new URL(link);
                InputStream is = url.openConnection().getInputStream();
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                parseJSon(res);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
