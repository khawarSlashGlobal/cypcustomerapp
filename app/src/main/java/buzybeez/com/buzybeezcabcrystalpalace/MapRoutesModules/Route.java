package buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */
public class Route {
    public List<Distance> distance;
    public List<Duration> duration;
    public List<String> endAddress;
    public List<LatLng> endLocation;
    public List<String> startAddress;
    public List<LatLng> startLocation;
    public List<LatLng> points;
}
