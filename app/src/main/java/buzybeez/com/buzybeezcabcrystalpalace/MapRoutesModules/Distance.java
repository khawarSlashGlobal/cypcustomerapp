package buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules;

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */


public class Distance {

/*
    distance indicates the total distance covered by each leg, as a field with the following elements:

    value = indicates the distance in meters
    text =  contains a human-readable representation of the distance, displayed in units as used at the origin
    (or as overridden within the units parameter in the request).
    (For example, miles and feet will be used for any origin within the United States.)
    Note that regardless of what unit system is displayed as text, the distance.value field always
    contains a value expressed in meters.
*/


    public static String mMilesDistanceFormated;
    public String text;
    public String mMilesDistance;
    public String mMiles;
    public int value;

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
        String CurrentString = text;
        String[] separated = CurrentString.split(" ");

        if (separated[1].equals("m")) {
            float miles = (float) (Float.parseFloat(separated[0]) * 0.000621371);
            mMilesDistance = String.valueOf(miles);
            mMilesDistanceFormated = String.format("%.3f", miles);

        } else if (separated[1].equals("miles")) {
            float miles = (float) (Float.parseFloat(separated[0]));
            mMilesDistance = String.valueOf(miles);
            mMilesDistanceFormated = String.format("%.2f", miles);

        } else {
            mMiles = separated[0];
            if (mMiles.contains(",")) {
                mMiles = mMiles.replace(",", "");
            }
            Float milesINfloat = Float.parseFloat(mMiles);
            mMilesDistance = String.valueOf(milesINfloat * 0.621371);
            //trim decimal mile values
            Float trimFloat = Float.parseFloat(mMilesDistance);
            mMilesDistanceFormated = String.format("%.2f", trimFloat);

        }

    }
}
