package buzybeez.com.buzybeezcabcrystalpalace.MapRoutesModules;

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */
public class Duration {

/*
    duration indicates the total duration of each leg, as a field with the following elements:

    value = indicates the duration in seconds.
    text  = contains a human-readable representation of the duration.
*/


    public String text;
    public int value;

    public Duration(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
