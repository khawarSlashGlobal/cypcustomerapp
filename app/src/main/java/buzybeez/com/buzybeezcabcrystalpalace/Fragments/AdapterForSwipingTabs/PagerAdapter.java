package buzybeez.com.buzybeezcabcrystalpalace.Fragments.AdapterForSwipingTabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import buzybeez.com.buzybeezcabcrystalpalace.Fragments.TabsForHistory.BookedJobs;
import buzybeez.com.buzybeezcabcrystalpalace.Fragments.TabsForHistory.CanceledJobs;
import buzybeez.com.buzybeezcabcrystalpalace.Fragments.TabsForHistory.CompletedJobs;

public class PagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                BookedJobs tab1 = new BookedJobs();
                return tab1;
            case 1:
                CompletedJobs tab2 = new CompletedJobs();
                return tab2;
            case 2:
                CanceledJobs tab3 = new CanceledJobs();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
