package buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;


import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Card;
import com.stripe.model.Customer;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import buzybeez.com.buzybeezcabcrystalpalace.Models.CardInfo;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CardNumber;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.ChargesDetailActivty;

import static android.content.Context.MODE_PRIVATE;

public class CardAdapter extends BaseAdapter {
    private Activity activity;
    List<CardInfo> cardDetail;


    TextView cardDetails;
    private SparseArray<Pattern> mCCPatterns = null;

    public CardAdapter(Activity activity, List<CardInfo> cardNumber, TextView cardDetails) {
        this.activity = activity;
        this.cardDetail = cardNumber;
        this.cardDetails = cardDetails;
    }



    @Override
    public int getCount() {
        return cardDetail.size();
    }

    @Override
    public Object getItem(int position) {
        return cardDetail.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void validateCardNumber(String cardNumber)
    {
        final int mDefaultDrawableResId = R.drawable.cio_ic_visa;
        int mCurrentDrawableResId = 0;
        Drawable mCurrentDrawable;






    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MessageViewHolders holders = new MessageViewHolders();
        LayoutInflater messageInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        final CardInfo cardNum  = cardDetail.get(position);
        //String get=cardNum.substring(cardNum.length()-4);
        convertView = messageInflater.inflate(R.layout.custom_card_list, null);
        holders.cardNumber=convertView.findViewById(R.id.textViewCard);
        holders.cardImage=convertView.findViewById(R.id.cardImage);
        holders.selectCard=convertView.findViewById(R.id.simpleSwitch);

        holders.deleteImage=convertView.findViewById(R.id.imageButtonDelete);
        final String card = cardNum.getLast4();
        final String cardId =cardNum.getId();
        final String custId = cardNum.getCustId();
        final String brand = cardNum.getBrand();
        final String fingerprints = cardNum.getFingerprint();



        SharedPreferences preferences = activity.getSharedPreferences("creditCardCheck", MODE_PRIVATE);
        final String getCardId= preferences.getString("cardId", "");
        if(getCardId.equals(cardId))
        {

            holders.selectCard.setChecked(true);

        }




        //String get=card.substring(card.length()-4);

        holders.cardNumber.setText("...."+card);

        if(brand.equals("Visa"))
        {
            holders.cardImage.setImageResource(R.drawable.cio_ic_visa);
        }
        else if(brand.equals("MasterCard"))
        {
            holders.cardImage.setImageResource(R.drawable.mastercard);
        }

        else if(brand.equals("American Express"))
        {
            holders.cardImage.setImageResource(R.drawable.amex);
        }
        else {
            holders.cardImage.setImageResource(R.drawable.cio_ic_visa);
        }


        holders.cardNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(activity, ChargesDetailActivty.class);
                intent.putExtra("cust_id",custId);
                intent.putExtra("card_id",cardId);
                intent.putExtra("number",card);
                intent.putExtra("brand",brand);
                intent.putExtra("fingerprint",fingerprints);
                activity.startActivity(intent);

            }
        });



//        mCCPatterns = new SparseArray<>();
//        mCCPatterns.put(R.drawable.cio_ic_visa, Pattern.compile(
//                "Visa credit card"));
//        mCCPatterns.put(R.drawable.amex, Pattern.compile("Unknown credit card"));
//        mCCPatterns.put(R.drawable.mastercard, Pattern.compile(
//                        "Mastercard credit card"));
//
//        int mDrawableResId = 0;
//        for (int i = 0; i < mCCPatterns.size(); i++) {
//            int key = mCCPatterns.keyAt(i);
//            // get the object by the key.
//            Pattern p = mCCPatterns.get(key);
//            Matcher m = p.matcher(brand);
//            if (m.find()) {
//                holders.cardImage.setImageResource(key);
//                break;
//            }
//        }

        holders.selectCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(holders.selectCard.isChecked())
                {
                    SharedPreferences sharedPreferences = activity.getSharedPreferences("creditCardCheck", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("checkCard","yes" );
                    editor.putString("cardId",cardId);
                    editor.putString("custId",custId);
                    editor.putString("cardNumber",card);
                    editor.putString("brand",brand);
                    editor.apply();
                    notifyDataSetChanged();
                }
                else {

                    SharedPreferences preferences = activity.getSharedPreferences("creditCardCheck", MODE_PRIVATE);
                    final String getCardId= preferences.getString("cardId", "");

                    if(getCardId.equals(cardId))
                    {

                        SharedPreferences sharedPreferences = activity.getSharedPreferences("creditCardCheck", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("checkCard","" );
                        editor.putString("cardId","");
                        editor.putString("custId","");
                        editor.putString("cardNumber","");
                        editor.putString("brand","");
                        editor.apply();

                    }


                }


            }
        });






        holders.deleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                alertDialogBuilder.setTitle("Delete Card");
                alertDialogBuilder.setMessage("Are you sure want to delete?");

                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        final ProgressDialog progressDialog = new ProgressDialog(activity);
                        progressDialog.setTitle("Delete");
                        progressDialog.setMessage("Deleting please wait...");
                        progressDialog.show();


                        SharedPreferences preferences = activity.getSharedPreferences("creditCardCheck", MODE_PRIVATE);
                        final String idCard = preferences.getString("cardId","");

                        if(idCard.equals(cardId))
                        {
                            SharedPreferences sharedPreferences = activity.getSharedPreferences("creditCardCheck", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("checkCard","" );
                            editor.putString("cardId","");
                            editor.putString("custId","");
                            editor.putString("cardNumber","");
                            editor.putString("brand","");
                            editor.apply();
                        }


                        Stripe.apiKey = "sk_test_0XDT5RHd7IXtuxJ0vhYGRGsh";
                        new Thread() {
                            @Override
                            public void run() {
                                Customer customer = null;
                                try {

                                    customer = Customer.retrieve(custId);
                                    Card card = (Card) customer.getSources().retrieve(cardId);
                                    card.delete();



                                } catch (AuthenticationException e) {
                                    e.printStackTrace();
                                } catch (InvalidRequestException e) {
                                    e.printStackTrace();
                                } catch (APIConnectionException e) {
                                    e.printStackTrace();
                                } catch (CardException e) {
                                    e.printStackTrace();
                                } catch (APIException e) {
                                    e.printStackTrace();
                                }
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cardDetail.remove(position);

                                        notifyDataSetChanged();


                                        if(cardDetail.isEmpty())
                                        {
                                            cardDetails.setText("No Card Found");
                                        }
                                        progressDialog.dismiss();
                                    }
                                });

                            }}.start();






                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
                alertDialogBuilder.show();



            }
        });



        return convertView;
    }
    class MessageViewHolders {
        TextView cardNumber;
        ImageView cardImage,deleteImage;
        Switch selectCard;
    }
}

