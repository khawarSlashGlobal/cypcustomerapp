package buzybeez.com.buzybeezcabcrystalpalace.Fragments.TabsForHistory;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;

import buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants;
import buzybeez.com.buzybeezcabcrystalpalace.DataHandler.SQLiteDatabaseHandler;
import buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview.BookedAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview.CanceledAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Booking;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.DetailedBookingHistory;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.JSONParse;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.socket.client.Ack;

import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.FIXED_OFFICE_KEY;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.canceledBookingHistoryOfUser;


/**
 * A simple {@link Fragment} subclass.
 */
public class CanceledJobs extends Fragment {
    //New RV Code

    public static RecyclerView mRecyclerViewCEN;
    public static ImageView imgEmptyDocumentCEN;
    private SQLiteDatabaseHandler dbCEN;
    private RecyclerView.Adapter mAdapterCEN;
    ProgressBar progressBarBOK;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbCEN = new SQLiteDatabaseHandler(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        // try {
        view = inflater.inflate(R.layout.fragment_cancelled_jobs, container, false);
        mRecyclerViewCEN = view.findViewById(R.id.listCanceled);
        imgEmptyDocumentCEN = view.findViewById(R.id.emptyDoc);
        imgEmptyDocumentCEN.setImageResource(R.drawable.emptydoc);
        imgEmptyDocumentCEN.setVisibility(View.GONE);
        progressBarBOK = view.findViewById(R.id.progressBarBOK2);
        mAdapterCEN = new CanceledAdapter(canceledBookingHistoryOfUser, new BookedAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Booking booking, View v) {
                // Toast.makeText(getActivity(), "ViewDetails clicked", Toast.LENGTH_SHORT).show();
                Booking.sharedinstance = booking;
                String driverCallSign = booking.getDrvrcallsign();
                Intent intent1  = new Intent(getActivity(), DetailedBookingHistory.class);
                intent1.putExtra("driverCallSign",driverCallSign );
                startActivity(intent1);
            }
        });


//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(canceledBookingHistoryOfUser.isEmpty()) {
            getCancelledBookingHistory();
        }
        else{ progressBarBOK.setVisibility(View.GONE);
            mRecyclerViewCEN.setAdapter(mAdapterCEN);
            mRecyclerViewCEN.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapterCEN.notifyDataSetChanged();
        }
    }

    public void getCancelledBookingHistory() { try{if
    (!Network.isNetworkAvailable(this.getContext())) {return;}}
    catch(Exception e){return;}
        //fetch phone number
        if (SocketEvent.sharedinstance.socket==null)
        {
            SocketEvent.sharedinstance.initializeSocket();
        }
        else if(!SocketEvent.sharedinstance.socket.connected())
        {
            SocketEvent.sharedinstance.socketConnectAfterDisconnect();
        }
        SharedPreferences preferences = this.getActivity().getSharedPreferences("", this.getActivity().MODE_PRIVATE);
        final String cust_phone1 = preferences.getString("cust_phone", "");
        if (cust_phone1.equals("")) {

        } else {
            //if phone number is avalible, connect socket if not connected
            if (SocketEvent.sharedinstance.socket == null) {
                SocketEvent.sharedinstance.initializeSocket();
            }
//            final Timer timer = new Timer();
//            timer.schedule(new TimerTask() {
//                @Override
//                public void run() {

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this.getContext())) {

//                        timer.cancel();
                JSONArray query = new JSONArray();
                JSONObject filter = new JSONObject();
                query.put("Booking");
                try {
                    //filter.put("telephone", cust_phone);
                    filter.put("office", FIXED_OFFICE_KEY);
                    filter .put("telephone" , cust_phone1);
                    filter  .put("bookedby" , "CustomerOnline");
                    filter.put("jobref", new JSONObject().put("$regex", "LA|LI"));
                    filter   .put("cstate", new JSONObject()
                            .put("$in" , new JSONArray().put("cancelled")));
                    query.put(filter);

                    SocketEvent.sharedinstance.socket.emit("getagentpayments", query, new Ack() {

                        @Override
                        public void call(final Object... args) {
                            //   bookedBookingHistoryOfUser.clear();
                            if (args[0] != null && !args[0].toString().equals("0")) {
                                try {

                                    JSONArray jsonArray = new JSONArray(args[0].toString());
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject booking = jsonArray.getJSONObject(i);
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(booking);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        // Booking.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
                                        // Booking b = Booking.sharedinstance;
                                        Booking b =mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
                                        //setup datarraylists for each recycler view
                                        if (b.getCstate().equals("cancelled")) {
                                            canceledBookingHistoryOfUser.add(b);
                                        }
                                    }
                                    // mAdapterCEN.notifyDataSetChanged();

//                                    getActivity().runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            progressBarBOK.setVisibility(View.GONE);
//
//                                          mRecyclerViewCEN.setAdapter(mAdapterCEN);
//                                            mRecyclerViewCEN.setLayoutManager(new LinearLayoutManager(getActivity()));
//                                            mAdapterCEN.notifyDataSetChanged();
//
//                                        }
//                                    });

                                } catch (Exception e) {
                                    try {  getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            progressBarBOK.setVisibility(View.GONE);


                                        }

                                    });}
                                    catch(Exception ee){}
                                    e.getMessage();
                                }

                            }
                            getbookingclosedCancelledBookingHistory();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            //   }

            // }, 100, 1000);

        }

    }
    public void getbookingclosedCancelledBookingHistory() { try{if
    (!Network.isNetworkAvailable(this.getContext())) {return;}}
    catch(Exception e){return;}
        //fetch phone number
        if (SocketEvent.sharedinstance.socket==null)
        {
            SocketEvent.sharedinstance.initializeSocket();
        }
        else if(!SocketEvent.sharedinstance.socket.connected())
        {
            SocketEvent.sharedinstance.socketConnectAfterDisconnect();
        }
        SharedPreferences preferences = this.getActivity().getSharedPreferences("", this.getActivity().MODE_PRIVATE);
        final String cust_phone1 = preferences.getString("cust_phone", "");
        if (cust_phone1.equals("")) {

        } else {
            //if phone number is avalible, connect socket if not connected
            if (SocketEvent.sharedinstance.socket == null) {
                SocketEvent.sharedinstance.initializeSocket();
            }
//            final Timer timer = new Timer();
//            timer.schedule(new TimerTask() {
//                @Override
//                public void run() {

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this.getContext())) {

//                        timer.cancel();
                JSONArray query = new JSONArray();
                JSONObject filter = new JSONObject();
                query.put("BookingClosed");
                try {
                    //filter.put("telephone", cust_phone);
                    long time = Network.sharedInstance.getBST() - 2592000; // for previous 30 days
                    filter.put("office", FIXED_OFFICE_KEY);
                    filter.put("telephone" , cust_phone1);
                    filter.put("jobref", new JSONObject().put("$regex", "LA|LI"));
                    filter .put("bookedby" , "CustomerOnline");
                    filter .put("cstate", new JSONObject()
                            .put("$in" , new JSONArray().put("cancelled")));
                    filter   .put("datentime" , new JSONObject().put("$gte",time));


                    query.put(filter);

                    SocketEvent.sharedinstance.socket.emit("getagentpayments", query, new Ack() {

                        @Override
                        public void call(final Object... args) {
                            //   bookedBookingHistoryOfUser.clear();
                            if (args[0] != null && !args[0].toString().equals("0")) {
                                try {

                                    JSONArray jsonArray = new JSONArray(args[0].toString());
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject booking = jsonArray.getJSONObject(i);
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(booking);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        // Booking.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
                                        // Booking b = Booking.sharedinstance;
                                        Booking b =mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
                                        //setup datarraylists for each recycler view
                                        if (b.getCstate().equals("cancelled")) {
                                            canceledBookingHistoryOfUser.add(b);

                                        }
                                    }
                                    sortDataDecendingOrder();
                                } catch (Exception e) {
                                    try {  getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            progressBarBOK.setVisibility(View.GONE);


                                        }

                                    });}
                                    catch(Exception ee){}
                                    e.getMessage();
                                }

                            }

                            try{
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mAdapterCEN.notifyDataSetChanged();
                                        progressBarBOK.setVisibility(View.GONE);

                                        mRecyclerViewCEN.setAdapter(mAdapterCEN);
                                        mRecyclerViewCEN.setLayoutManager(new LinearLayoutManager(getActivity()));
                                        mAdapterCEN.notifyDataSetChanged();

                                    }
                                });}
                            catch (Exception e){}

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            //   }

            // }, 100, 1000);

        }

    }
    public void sortDataDecendingOrder(){
        Collections.sort(canceledBookingHistoryOfUser, new Comparator<Booking>() {
            @Override
            public int compare(Booking o1, Booking o2) {
                int two = (int) o2.getDatentime();
                int one = (int) o1.getDatentime();
                return two-one;
            }
        });
    }

}


