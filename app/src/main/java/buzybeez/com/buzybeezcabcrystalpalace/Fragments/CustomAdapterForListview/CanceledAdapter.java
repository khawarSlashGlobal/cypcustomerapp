package buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import buzybeez.com.buzybeezcabcrystalpalace.Models.Booking;
import buzybeez.com.buzybeezcabcrystalpalace.R;

public class CanceledAdapter extends RecyclerView.Adapter<CanceledAdapter.MyViewHolder> {

    private final BookedAdapter.OnItemClickListener listener;
    ArrayList<Booking> dataBooked;

    public CanceledAdapter(ArrayList<Booking> data
            , BookedAdapter.OnItemClickListener listener) {
        this.listener = listener;
        this.dataBooked = data;
    }

    @Override
    public CanceledAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.booked_custom_list_yellow, parent, false);
        CanceledAdapter.MyViewHolder holder1 = new CanceledAdapter.MyViewHolder(view1);
        return holder1;
    }

    @Override
    public void onBindViewHolder(CanceledAdapter.MyViewHolder myViewHolder, final int position) {


        myViewHolder.itemView.setTag(position);
        Booking bookingHistoryItem = dataBooked.get(position);
        myViewHolder.bind(bookingHistoryItem, listener);
        myViewHolder.date.setText(bookingHistoryItem.getDate());
        myViewHolder.time.setText(bookingHistoryItem.getTime());
        myViewHolder.origin.setText(bookingHistoryItem.getfrom());
        myViewHolder.dest.setText(bookingHistoryItem.getTo());
        myViewHolder.fare.setText("£ " + bookingHistoryItem.getFare());
        myViewHolder.status.setText(bookingHistoryItem.getCstate());
        myViewHolder.status.setTextColor(Color.RED);
        myViewHolder.driverSearchBtn.setVisibility(View.GONE);
        myViewHolder.Cancelbtn.setVisibility(View.GONE);
        myViewHolder.editbooking.setVisibility(View.GONE);
        //this is for dropdown vias
     /*   String str = "";
        List<FromToVia> ftv1 = bookingHistoryItem.getFromtovia();
        if ((ftv1.size() == 0) == false) {
            myViewHolder.viasLable.setVisibility(View.VISIBLE);
            myViewHolder.readMoreLessBtn.setVisibility(View.VISIBLE);
            myViewHolder.vias.setVisibility(View.VISIBLE);
            for (FromToVia ftv : ftv1) {
                if(ftv.getAddress()==null){}else {
                if ((ftv.getAddress().toString().equals(null)) == false){
                    str = str + ftv.getAddress() + "\n";
                }
            }}
        } else {
            myViewHolder.viasLable.setVisibility(View.GONE);
            myViewHolder.readMoreLessBtn.setVisibility(View.GONE);
            myViewHolder.vias.setVisibility(View.GONE);
        }

        myViewHolder.vias.setText(str);*/
    }

    @Override
    public int getItemCount() {
        return dataBooked.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Booking item, View v);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        //  class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView date;
        public TextView time;
        public TextView origin;
        public TextView dest;
        public TextView fare;
        public TextView status;
        public TextView readMoreLessBtn;
        public TextView driverSearchBtn;
        public TextView vias;
        public TextView viasLable;
        public TextView viewDetails;
        public  TextView Cancelbtn,editbooking;
        public MyViewHolder(View itemView) {
            super(itemView);
            // itemView.setOnClickListener(this);
            date = itemView.findViewById(R.id.tvDate);
            time = itemView.findViewById(R.id.tvTime);
            origin = itemView.findViewById(R.id.to);
            dest = itemView.findViewById(R.id.from);
            fare = itemView.findViewById(R.id.tvFare);
            status = itemView.findViewById(R.id.tvStatus);
            vias = itemView.findViewById(R.id.tvVias);
            viasLable = itemView.findViewById(R.id.viasLabel);
            viewDetails = itemView.findViewById(R.id.viewDetails);
            Cancelbtn= itemView.findViewById(R.id.CancelBookingbtn);
            editbooking=  itemView.findViewById(R.id.editbooking);
            vias.setSingleLine(true);
            readMoreLessBtn = itemView.findViewById(R.id.moreLessBtn);
            readMoreLessBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String str = readMoreLessBtn.getText().toString().trim();
                    if (str.equals("View More")) {
                        vias.setSingleLine(false);
                        readMoreLessBtn.setText("View Less");
                    } else {
                        vias.setSingleLine(true);
                        readMoreLessBtn.setText("View More");
                    }
                }
            });
            driverSearchBtn = itemView.findViewById(R.id.searchDriverBtn);
        }
        public void bind(final Booking item, final BookedAdapter.OnItemClickListener listener) {

            viewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(item, view);
                }
            });



        }

    }


}

