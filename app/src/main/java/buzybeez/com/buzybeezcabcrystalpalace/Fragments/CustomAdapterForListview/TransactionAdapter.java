package buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import buzybeez.com.buzybeezcabcrystalpalace.Models.Transaction;
import buzybeez.com.buzybeezcabcrystalpalace.R;

public class TransactionAdapter extends BaseAdapter {

    private Activity activity;
    List<Transaction> transactionsInfo;

    public TransactionAdapter(Activity activity, List<Transaction> transactionsInfo) {
        this.activity = activity;
        this.transactionsInfo = transactionsInfo;
    }

    @Override
    public int getCount() {
        return transactionsInfo.size();
    }

    @Override
    public Object getItem(int i) {
        return transactionsInfo.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        final Transaction transaction = transactionsInfo.get(i);

        final TransactionViewHolders transactionViewHolders = new TransactionViewHolders();
        LayoutInflater messageInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        view = messageInflater.inflate(R.layout.custom_payment_list,null);
        transactionViewHolders.textAmount = view.findViewById(R.id.textViewAmount);
        transactionViewHolders.textCurrency= view.findViewById(R.id.textViewCurrency);
        transactionViewHolders.dateTime = view.findViewById(R.id.textViewDateTime);

        String textAmount = transaction.getAmount();
        String textCurrency = transaction.getCurrency();
        String dateTime = transaction.getDateTime();
        String time = transaction.getTime();

        transactionViewHolders.textAmount.setText(textAmount);
        transactionViewHolders.textCurrency.setText(textCurrency);
        transactionViewHolders.dateTime.setText(dateTime);
      //  transactionViewHolders.textTime.setText(time);



        return view;
    }


    class TransactionViewHolders {
        TextView textAmount;
        TextView textCurrency;
        TextView dateTime;
        //TextView textTime;

    }

}
