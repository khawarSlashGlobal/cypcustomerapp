package buzybeez.com.buzybeezcabcrystalpalace.Fragments.TabsForHistory;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;

import buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants;
import buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview.BookedAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview.CompletedAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Booking;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.DetailedBookingHistory;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.JSONParse;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.socket.client.Ack;

import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.FIXED_OFFICE_KEY;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.completedBookingHistoryOfUser;


/**
 * A simple {@link Fragment} subclass.
 */
public class CompletedJobs extends Fragment {

    public static  ImageView imgEmptyDocumentCOM;
    ProgressBar progressBarBOK;
    //New RV Code
    public static RecyclerView mRecyclerViewCOM;
    private RecyclerView.Adapter mAdapterCOM;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        // try {
        view = inflater.inflate(R.layout.fragment_completed_jobs, container, false);
        mRecyclerViewCOM = view.findViewById(R.id.listCompleted);
        imgEmptyDocumentCOM = view.findViewById(R.id.emptyDoc);
        imgEmptyDocumentCOM.setImageResource(R.drawable.emptydoc);
        imgEmptyDocumentCOM.setVisibility(View.GONE);
        progressBarBOK = view.findViewById(R.id.progressBarBOK3);
        mAdapterCOM = new CompletedAdapter(completedBookingHistoryOfUser, new BookedAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Booking booking, View v) {
                // Toast.makeText(getActivity(), "ViewDetails clicked", Toast.LENGTH_SHORT).show();
                Booking.sharedinstance = booking;
                String driverCallSign = booking.getDrvrcallsign();
                Intent intent1  = new Intent(getActivity(), DetailedBookingHistory.class);
                intent1.putExtra("driverCallSign",driverCallSign );
                startActivity(intent1);

            }
        });


//        }catch (Exception e){
//            e.printStackTrace();
//        }
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        if(completedBookingHistoryOfUser.isEmpty()) {
            getCompletedBookingHistory();
        }
        else{
            progressBarBOK.setVisibility(View.GONE);
            mRecyclerViewCOM.setAdapter(mAdapterCOM);
            mRecyclerViewCOM.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapterCOM.notifyDataSetChanged();}
    }
    public void getCompletedBookingHistory() { try{if
    (!Network.isNetworkAvailable(this.getContext())) {return;}}
    catch(Exception e){return;}
        //fetch phone number
        if (SocketEvent.sharedinstance.socket==null)
        {
            SocketEvent.sharedinstance.initializeSocket();
        }
        else if(!SocketEvent.sharedinstance.socket.connected())
        {
            SocketEvent.sharedinstance.socketConnectAfterDisconnect();
        }
        SharedPreferences preferences = this.getActivity().getSharedPreferences("", this.getActivity().MODE_PRIVATE);
        final String cust_phone1 = preferences.getString("cust_phone", "");
        if (cust_phone1.equals("")) {

        } else {
            //if phone number is avalible, connect socket if not connected
            if (SocketEvent.sharedinstance.socket == null) {
                SocketEvent.sharedinstance.initializeSocket();
            }
//            final Timer timer = new Timer();
//            timer.schedule(new TimerTask() {
//                @Override
//                public void run() {

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this.getContext())) {

//                        timer.cancel();
                JSONArray query = new JSONArray();
                JSONObject filter = new JSONObject();
                query.put("Booking");
                try {
                    //filter.put("telephone", cust_phone);
                    filter.put("office", FIXED_OFFICE_KEY);

                    filter  .put("telephone", cust_phone1);
                    filter.put("jobref", new JSONObject().put("$regex", "LA|LI"));
                    filter.put("bookedby", "CustomerOnline");
                    //    filter   .put("cstate", new JSONObject()
                    //                  .put("$in" , new JSONArray().put("despatched")));
                    filter   .put("jstate", new JSONObject()
                            .put("$in" , new JSONArray().put("JobDone")));
                    filter   .put("flag",1 );
                    query.put(filter);

                    SocketEvent.sharedinstance.socket.emit("getagentpayments", query, new Ack() {

                        @Override
                        public void call(final Object... args) {
                            //   bookedBookingHistoryOfUser.clear();
                            if (args[0] != null && !args[0].toString().equals("0")) {
                                try {
                                    JSONArray jsonArray = new JSONArray(args[0].toString());
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject booking = jsonArray.getJSONObject(i);
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(booking);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        // Booking.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
                                        // Booking b = Booking.sharedinstance;
                                        Booking b =mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
                                        //setup datarraylists for each recycler view
                                        if (b.getCstate().equals("despatched")) {
                                            completedBookingHistoryOfUser.add(b);
                                        }
                                    }

//                                   mAdapterCOM.notifyDataSetChanged();
//                                    getActivity().runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            progressBarBOK.setVisibility(View.GONE);
//                                           mRecyclerViewCOM.setAdapter(mAdapterCOM);
//                                          mRecyclerViewCOM.setLayoutManager(new LinearLayoutManager(getActivity()));
//                                           mAdapterCOM.notifyDataSetChanged();
//                                        }
//                                    });

                                } catch (Exception e) {

                                }

                            }
                            getbookingclosedCompletedBookingHistory();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            //   }

            // }, 100, 1000);

        }

    }
    public void getbookingclosedCompletedBookingHistory() { try{if
    (!Network.isNetworkAvailable(this.getContext())) {return;}}
    catch(Exception e){return;}
        //fetch phone number
        if (SocketEvent.sharedinstance.socket==null)
        {
            SocketEvent.sharedinstance.initializeSocket();
        }
        else if(!SocketEvent.sharedinstance.socket.connected())
        {
            SocketEvent.sharedinstance.socketConnectAfterDisconnect();
        }
        SharedPreferences preferences = this.getActivity().getSharedPreferences("", this.getActivity().MODE_PRIVATE);
        final String cust_phone1 = preferences.getString("cust_phone", "");
        if (cust_phone1.equals("")) {

        } else {
            //if phone number is avalible, connect socket if not connected
            if (SocketEvent.sharedinstance.socket == null) {
                SocketEvent.sharedinstance.initializeSocket();
            }
//            final Timer timer = new Timer();
//            timer.schedule(new TimerTask() {
//                @Override
//                public void run() {

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this.getContext())) {

//                        timer.cancel();
                JSONArray query = new JSONArray();
                JSONObject filter = new JSONObject();
                query.put("BookingClosed");
                try {
                    //filter.put("telephone", cust_phone);
                    filter.put("office", FIXED_OFFICE_KEY);
                    long time = Network.sharedInstance.getBST() - 12960000; // for previous 150 days
                    filter.put("jobref", new JSONObject().put("$regex", "LA|LI"));
                    filter  .put("telephone", cust_phone1);
                    filter .put("bookedby", "CustomerOnline");
//                    filter   .put("cstate", new JSONObject()
//                                    .put("$in" , new JSONArray().put("despatched")));
                    filter.put("jstate", new JSONObject()
                            .put("$in" , new JSONArray().put("JobDone")));
                    filter .put("datentime", new JSONObject().put("$gte", time));
                    filter.put("flag",1);
                    query.put(filter);

                    SocketEvent.sharedinstance.socket.emit("getagentpayments", query, new Ack() {

                        @Override
                        public void call(final Object... args) {
                            //   bookedBookingHistoryOfUser.clear();
                            if (args[0] != null && !args[0].toString().equals("0")) {
                                try {
                                    JSONArray jsonArray = new JSONArray(args[0].toString());
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject booking = jsonArray.getJSONObject(i);
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(booking);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        // Booking.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
                                        // Booking b = Booking.sharedinstance;
                                        Booking b =mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
                                        //setup datarraylists for each recycler view
                                        if (b.getCstate().equals("despatched")) {
                                            completedBookingHistoryOfUser.add(b);
                                        }
                                    }
                                    sortDataDecendingOrder();

                                } catch (Exception e) {
                                    try {   getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            progressBarBOK.setVisibility(View.GONE);


                                        }

                                    }); }
                                    catch(Exception ee){}
                                    e.getMessage();
                                }

                            }

                            try{
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mAdapterCOM.notifyDataSetChanged();
                                        progressBarBOK.setVisibility(View.GONE);
                                        mRecyclerViewCOM.setAdapter(mAdapterCOM);
                                        mRecyclerViewCOM.setLayoutManager(new LinearLayoutManager(getActivity()));
                                        mAdapterCOM.notifyDataSetChanged();
                                    }
                                });}
                            catch (Exception e){}

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            //   }

            // }, 100, 1000);

        }

    }
    public void sortDataDecendingOrder(){
        Collections.sort(completedBookingHistoryOfUser, new Comparator<Booking>() {
            @Override
            public int compare(Booking o1, Booking o2) {
                int two = (int) o2.getDatentime();
                int one = (int) o1.getDatentime();
                return two-one;
            }
        });
    }

}
