package buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import buzybeez.com.buzybeezcabcrystalpalace.Models.Chat;
import buzybeez.com.buzybeezcabcrystalpalace.R;

public class ChatAdapter extends BaseAdapter {
private Activity activity;
    List<Chat> mChat;
    boolean custId;

    public ChatAdapter(Activity activity, List<Chat> mChat, boolean custId) {
        this.activity = activity;
        this.mChat = mChat;
        this.custId=custId;
    }

    @Override
    public int getCount() {
        return mChat.size();
    }

    @Override
    public Object getItem(int position) {
        return mChat.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageViewHolder holder = new MessageViewHolder();
        LayoutInflater messageInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        Chat chat = mChat.get(position);
       // if(mChat.get(position).getSender().equals(custId))
        if(chat.isStatus())
        {
            convertView = messageInflater.inflate(R.layout.chat_item_left, null);
           holder.now=convertView.findViewById(R.id.now);
            holder.show_message=convertView.findViewById(R.id.show_message);
            holder.show_message.setText(chat.getMessage());
            holder.now.setText(chat.getDateTime());
        }
        else {
            convertView = messageInflater.inflate(R.layout.chat_item_right, null);
            holder.now=convertView.findViewById(R.id.now);
            holder.show_message=convertView.findViewById(R.id.show_message);
            holder.show_message.setText(chat.getMessage());
            holder.now.setText(chat.getDateTime());
        }



        return convertView;

    }
}

class MessageViewHolder {
  TextView show_message,now;
}
