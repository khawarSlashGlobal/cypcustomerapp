package buzybeez.com.buzybeezcabcrystalpalace.Fragments.TabsForHistory;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;

import buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants;
import buzybeez.com.buzybeezcabcrystalpalace.DataCenter.DataHolder;
import buzybeez.com.buzybeezcabcrystalpalace.Fragments.CustomAdapterForListview.BookedAdapter;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Booking;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.BaseActivity;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.DetailedBookingHistory;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.DriverSearch;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.EditBookingActivity;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.JSONParse;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.socket.client.Ack;

import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.FIXED_OFFICE_KEY;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.bookedBookingHistoryOfUser;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination.canceledBookingHistoryOfUser;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookedJobs extends Fragment {
    public static Intent intent;
    String jobID = "";
    String jState = "";
    double timetodespatch = 0.0;
    long currentTime = 0;
    double compareTime = 0.0;
    Object[] logc = new Object[]{};
    ImageView imgEmptyDocumentBOK;
    ProgressBar progressBarBOK;
    String bookingDate = "";
    String bookingTime = "";
    String bookingOrigin = "";
    String bookingDest = "";
    double fare = 0.0;
    String jobRef = "";
    long bookingDatenTime = 0;
    String drvCallSign = "";
    String vehType = "";
    String frmOutCode = "";
    String phone = "";
    ImageView imgEmptyDocument;
    //New RV Code
    private RecyclerView mRecyclerViewBOK;
    private RecyclerView.Adapter mAdapter1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        //  try {
        view = inflater.inflate(R.layout.fragment_booked_jobs, container, false);
        mRecyclerViewBOK = view.findViewById(R.id.listBooked);

        imgEmptyDocumentBOK = view.findViewById(R.id.emptyDoc);
        imgEmptyDocumentBOK.setImageResource(R.drawable.emptydoc);
        imgEmptyDocumentBOK.setVisibility(View.GONE);
        progressBarBOK = view.findViewById(R.id.progressBarBOK);
        // progressBarBOK.setVisibility(View.GONE);
        mAdapter1 = new BookedAdapter(bookedBookingHistoryOfUser, new BookedAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Booking booking, View v) {


                switch (v.getId()) {
                    case R.id.viewDetails:
                        // Toast.makeText(getActivity(), "ViewDetails clicked", Toast.LENGTH_SHORT).show();
                        Booking.sharedinstance = booking;
                        String driverCallSign = booking.getDrvrcallsign();
                        Intent intent1  = new Intent(getActivity(), DetailedBookingHistory.class);
                        intent1.putExtra("driverCallSign",driverCallSign );
                        startActivity(intent1);

                        break;
                    case R.id.editbooking:
                        Booking.sharedinstance = booking;
                        Intent intent  = new Intent(getActivity(), EditBookingActivity.class );
                        startActivity(intent);
                        break;
                    case R.id.searchDriverBtn:
                        fare = booking.getFare();
                        jobID = booking.getId();
                        jState = booking.getJstate();
                        timetodespatch =booking.getDatentime();
                        currentTime = Network.sharedInstance.getBST();
                        Booking.sharedinstance = booking;
                        DataHolder.getInstance().jobReference = booking.getJobref();
                        DataHolder.getInstance().vehType =booking.getVehicletype();
                        DataHolder.getInstance().originLng = booking.getFromtovia().get(0).getLon();
                        DataHolder.getInstance().originLat = booking.getFromtovia().get(0).getLat();
                        DataHolder.getInstance().jobID = booking.getId();
                        DataHolder.getInstance().origin = booking.getfrom();
                        DataHolder.getInstance().destination = booking.getTo();
                        DataHolder.getInstance()._fromOutcode = booking.getFromOutcode();
                        compareTime =  timetodespatch - currentTime;
                        logc = booking.getLogc();
                        Toast.makeText(getActivity(), "searchDriverBtn clicked"+jobID, Toast.LENGTH_SHORT).show();
                        jState = booking.getJstate();
                        if (compareTime < 900) {
                            if (jState.equals("allocated")
                                    || jState.equals("unallocated")
                                    || jState.equals("Accepted")) {
                                intent = new Intent(getActivity(), DriverSearch.class);
                                intent.putExtra("jobid", jobID);
                                startActivity(intent);
                            }
                        }
                        else {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Light_Dialog);
                                    alertDialogBuilder.setTitle("Booking Info");
                                    String message = "<br>" + "<b>" + "Wait! This is Upcoming Job" + "</b>";
                                    alertDialogBuilder.setMessage(Html.fromHtml(message));
                                    alertDialogBuilder.setPositiveButton("Ok Wait!",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface arg0, int arg1) {
                                                    arg0.dismiss();
                                                }
                                            });
                                    alertDialogBuilder.setNegativeButton("Cancel Booking", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            cancelBooking();
                                            bookedBookingHistoryOfUser.clear();
                                            canceledBookingHistoryOfUser.clear();
                                            getBookedBookingHistory();
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();
                                }
                            });
                        }
                        break;
                    case R.id.CancelBookingbtn:

                        fare = booking.getFare();
                        jobID = booking.getId();
                        jState = booking.getJstate();
                        timetodespatch =booking.getTimetodespatch();
                        currentTime = Network.sharedInstance.getBST();
                        Booking.sharedinstance = booking;
                        DataHolder.getInstance().jobReference = booking.getJobref();
                        DataHolder.getInstance().vehType =booking.getVehicletype();
                        DataHolder.getInstance().originLng = booking.getFromtovia().get(0).getLon();
                        DataHolder.getInstance().originLat = booking.getFromtovia().get(0).getLat();
                        DataHolder.getInstance().jobID = booking.getId();
                        DataHolder.getInstance().origin = booking.getfrom();
                        DataHolder.getInstance().destination = booking.getTo();
                        DataHolder.getInstance()._fromOutcode = booking.getFromOutcode();
                        //   compareTime =  timetodespatch - currentTime;
                        logc = booking.getLogc();
                        //  Toast.makeText(getActivity(), "searchDriverBtn clicked"+jobID, Toast.LENGTH_SHORT).show();
                        jState = booking.getJstate();
//                            if (compareTime < 900) {
//                                if (jState.equals("allocated")
//                                        || jState.equals("unallocated")
//                                        || jState.equals("Accepted")) {
//                                    intent = new Intent(getActivity(), DriverSearch.class);
//                                    intent.putExtra("jobid", jobID);
//                                    startActivity(intent);
//                                }
//                            }
//                            else {
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Light_Dialog);
//                                        alertDialogBuilder.setTitle("Booking Info");
//                                        String message = "<br>" + "<b>" + "Wait! This is Upcoming Job" + "</b>";
//                                        alertDialogBuilder.setMessage(Html.fromHtml(message));
//                                        alertDialogBuilder.setPositiveButton("Ok Wait!",
//                                                new DialogInterface.OnClickListener() {
//                                                    @Override
//                                                    public void onClick(DialogInterface arg0, int arg1) {
//                                                        arg0.dismiss();
//                                                    }
//                                                });
//                                        alertDialogBuilder.setNegativeButton("Cancel Booking", new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
                        cancelBooking();
                        bookedBookingHistoryOfUser.clear();
                        canceledBookingHistoryOfUser.clear();
                        getBookedBookingHistory();

                        // ((BookingHistoryActivity)getActivity()).finish();

//                                                dialog.dismiss();
//                                            }
//                                        });
//                                        AlertDialog alertDialog = alertDialogBuilder.create();
//                                        alertDialog.show();
                        //    }
                        //  });
                        //  }

                        break;
                    default:
                        Toast.makeText(getActivity(), "Any one clicked", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });


//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(bookedBookingHistoryOfUser.isEmpty()) {

            getBookedBookingHistory();
        }
        else{
            progressBarBOK.setVisibility(View.GONE);
            mRecyclerViewBOK.setAdapter(mAdapter1);
            mRecyclerViewBOK.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter1.notifyDataSetChanged();}

    }
    public void getBookedBookingHistory() { try{if
    (!Network.isNetworkAvailable(this.getContext())) {return;}}
    catch(Exception e){return;}
        //fetch phone number
        if (SocketEvent.sharedinstance.socket==null)
        {
            SocketEvent.sharedinstance.initializeSocket();
        }
        else if(!SocketEvent.sharedinstance.socket.connected())
        {
            SocketEvent.sharedinstance.socketConnectAfterDisconnect();
        }
        SharedPreferences preferences = this.getActivity().getSharedPreferences("", this.getActivity().MODE_PRIVATE);
        final String cust_phone1 = preferences.getString("cust_phone", "");
        if (cust_phone1.equals("")) {

        } else {
            //if phone number is avalible, connect socket if not connected
            if (SocketEvent.sharedinstance.socket == null) {
                SocketEvent.sharedinstance.initializeSocket();
            }
//            final Timer timer = new Timer();
//            timer.schedule(new TimerTask() {
//                @Override
//                public void run() {

            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(this.getContext())) {

//                        timer.cancel();
                JSONArray query = new JSONArray();
                JSONObject filter = new JSONObject();
                query.put("Booking");
                try {
                    long time = Network.sharedInstance.getBST() - 172800;
                    //filter.put("telephone", cust_phone);
                    filter.put("office", FIXED_OFFICE_KEY);
                    filter.put("telephone", cust_phone1);
                    filter.put("jobref", new JSONObject().put("$regex", "LA|LI"));
                    filter.put("flag", 0);
                    filter.put("datentime", new JSONObject().put("$gte", time));
                    filter.put("bookedby" , "CustomerOnline");


                    filter.put("cstate", new JSONObject()
                            .put("$in" , new JSONArray().put("booked").put("despatched").put("reverted")));
                    filter.put("jstate", new JSONObject()
                            .put("$nin" , new JSONArray().put("JobDone")));
                    query.put(filter);

                    SocketEvent.sharedinstance.socket.emit("getagentpayments", query, new Ack() {

                        @Override
                        public void call(final Object... args) {
                            //   bookedBookingHistoryOfUser.clear();
                            if (args[0] != null && !args[0].toString().equals("0")) {
                                try {

                                    JSONArray jsonArray = new JSONArray(args[0].toString());
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject booking = jsonArray.getJSONObject(i);
                                        JsonNode jsonNode = JSONParse.convertJsonFormat(booking);
                                        ObjectMapper mapper = new ObjectMapper();
                                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                        // Booking.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
                                        // Booking b = Booking.sharedinstance;
                                        Booking b =mapper.readValue(new TreeTraversingParser(jsonNode), Booking.class);
                                        //setup datarraylists for each recycler view
                                        if (b.getCstate().equals("booked")) {
                                            bookedBookingHistoryOfUser.add(b);
                                        }
                                    }
                                    sortDataDecendingOrder();


                                } catch (Exception e) {
                                    try {     getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            progressBarBOK.setVisibility(View.GONE);


                                        }
                                    });}
                                    catch (Exception ee){}
                                    e.getMessage();
                                }

                            }

                            try{
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mAdapter1.notifyDataSetChanged();
                                        progressBarBOK.setVisibility(View.GONE);

                                        mRecyclerViewBOK.setAdapter(mAdapter1);
                                        mRecyclerViewBOK.setLayoutManager(new LinearLayoutManager(getActivity()));
                                        mAdapter1.notifyDataSetChanged();
                                    }
                                });
                            }
                            catch(Exception e){}
                        }

                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            //   }

            // }, 100, 1000);

        }

    }
    public void cancelBooking() {
        BaseActivity baseActivity = new BaseActivity();
        try {
            if (SocketEvent.sharedinstance.socket.connected() && Network.isNetworkAvailable(getActivity())) {
                JSONArray table = new JSONArray();
                table.put("Booking");

                JSONArray mainLogC = new JSONArray();
                JSONArray logcData = new JSONArray();
                JSONObject filter = new JSONObject();
                filter.put("_id", jobID);
                table.put(filter);
                if (logc.length == 0) {
                    mainLogC = DataHolder.getInstance().jsonArrayLogC;
                    logcData.put(Network.sharedInstance.getBST());
                    logcData.put("Customer cancelled job");
                    logcData.put(getSharedPrefEmail());
                    logcData.put(getSharedPrefName());
                    mainLogC.put(logcData);

                } else {

                    for (int i = 0; i < logc.length; i++) {

                        Object[] obj = (Object[]) logc[i];

                        JSONArray previousLogC = new JSONArray();

                        for (int j = 0; j < obj.length; j++) {

                            Object var = obj[j];

                            previousLogC.put(var);
                        }
                        mainLogC.put(previousLogC);
                    }
                    logcData = new JSONArray();

                    logcData.put(Network.sharedInstance.getBST());
                    logcData.put("Customer cancelled job");
                    logcData.put(getSharedPrefEmail());
                    logcData.put(getSharedPrefName());
                    mainLogC.put(logcData);
                    JSONObject parameters = new JSONObject();
                    parameters.put("jstate", "allocated");
                    parameters.put("cstate", "cancelled");
                    parameters.put("dstate", "");
                    parameters.put("flag", 1);
                    parameters.put("fare", 0);
                    parameters.put("drvfare", 0);
                    parameters.put("drvfare", 0);
                    parameters.put("oldfare", fare);
                    parameters.put("olddrvfare", fare);
                    parameters.put("logc", mainLogC);
                    table.put(parameters);
                    SocketEvent.sharedinstance.socket.emit("updatedata", table, new Ack() {
                        @Override
                        public void call(Object... args) {
                        }
                    });
                }
            } else
                Toast.makeText(baseActivity, "No Internet", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getSharedPrefEmail() {
        SharedPreferences getPrefrence = getActivity().getSharedPreferences("", Context.MODE_PRIVATE);
        String getSharedPrefEmail = getPrefrence.getString("cust_email", "");
        return getSharedPrefEmail;
    }

    public String getSharedPrefName() {

        SharedPreferences getPrefrence = getActivity().getSharedPreferences("", Context.MODE_PRIVATE);
        String getSharedPrefName = getPrefrence.getString("cust_name", "");
        return getSharedPrefName;
    }

    public void sortDataDecendingOrder(){
        Collections.sort(bookedBookingHistoryOfUser, new Comparator<Booking>() {
            @Override
            public int compare(Booking o1, Booking o2) {
                int two = (int) o2.getDatentime();
                int one = (int) o1.getDatentime();
                return two-one;
            }
        });
    }

}
