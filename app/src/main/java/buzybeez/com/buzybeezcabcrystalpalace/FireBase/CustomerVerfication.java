package buzybeez.com.buzybeezcabcrystalpalace.FireBase;

import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rilixtech.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import buzybeez.com.buzybeezcabcrystalpalace.DataCenter.DataHolder;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Models.CustomerReg;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.BaseActivity;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.JSONParse;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;
import io.socket.client.Ack;

public class CustomerVerfication extends BaseActivity implements ConnectionListener {
    String android_id;
    private static final String TAG = "PhoneAuth";
    boolean deviceidcheck=false;
    private EditText cus_Name;
    private EditText cus_Number;
    private EditText cus_email;
    private EditText cus_verifyCode;
    private Button submitDetails;
    private Button confirmCodeBtn;
    boolean b = false;

    private TextView status_verify;

    private String phoneVerificationId;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            verificationCallbacks;
    private PhoneAuthProvider.ForceResendingToken resendToken;

    private FirebaseAuth fbAuth;
    CountryCodePicker ccp;
    String phoneNumber;
    String cName;
    String cEmail;

    SharedPreferences preferences;
    public static final String mypreference = "mypref";
    public static final String Name = "nameKey";
    public static final String Email = "emailKey";
    public static final String Number = "numKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_verfication);

        snackbarForConnectivityStatus();

        customerverifyScreenID();

        fbAuth = FirebaseAuth.getInstance();

       /* if (!getSharedPrefPhone().equals("")) {
            sendDetails();
        }
*/

    }
    /*private void sendDetails(){
        Intent in = getIntent();
        Intent intent = new Intent(this, DriverSearch.class);
        // intent = getIntent();
        String recieveQuery = in.getStringExtra("query");
        intent.putExtra("query", recieveQuery);
        startActivity(intent);
    }*/

    public void customerverifyScreenID(){
        ccp = findViewById(R.id.ccp);

        cus_Name = findViewById(R.id.cusNameId);
        cus_Number = findViewById(R.id.phoneId);
        cus_email = findViewById(R.id.emailId);

        submitDetails = findViewById(R.id.submitId);

        status_verify = findViewById(R.id.verifystatus);
        status_verify.setVisibility(View.GONE);
        cus_verifyCode = findViewById(R.id.codeId);
        cus_verifyCode.setVisibility(View.GONE);
        confirmCodeBtn = findViewById(R.id.codeConfirmBtnId);
        confirmCodeBtn.setVisibility(View.GONE);
    }

    public boolean athenticateFields(){

        String strUserName = cus_Name.getText().toString();
        String strNumber = cus_Number.getText().toString();
        String strEmail = cus_email.getText().toString();

        if(TextUtils.isEmpty(strUserName)) {
            cus_Name.setError("please enter name");
            return false;
        }
        if (strUserName.length()>20){
            Toast.makeText(this, "Invalid Name ", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(TextUtils.isEmpty(strNumber)) {
            cus_Number.setError("please enter number");
            return false;
        }

        if(TextUtils.isEmpty(strEmail)) {
            cus_email.setError("please enter email");
            return false;
        }

        Pattern emailPattern = Pattern.compile( "^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");
        Matcher emailMatcher = emailPattern.matcher(cus_email.getText().toString().trim());
        if (!emailMatcher.matches()) {
            Toast.makeText(this, " Invalid Email ", Toast.LENGTH_SHORT).show();
            //show your message if not matches with email pattern
            return false;
        }
        return true;
    }
    public void checksendCode(final View view)
    {
        if (athenticateFields() == true) {


            phoneNumber = ccp.getSelectedCountryCodeWithPlus() + "" + cus_Number.getText().toString();
            cName = cus_Name.getText().toString();
            cEmail = cus_email.getText().toString();}
        else {
            return;
        }

        android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        final String token = FirebaseInstanceId.getInstance().getToken();

        if (!token.equals("")) {

            DataHolder.getInstance().fcmToken = token;

        }


        JSONArray query = new JSONArray();
        JSONObject filter = new JSONObject();

        query.put("CustomerReg");


//        if (preferences.contains(Number)) {
//                try {
//                    filter.put("cust_phone",preferences.getString(Number, ""));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }else{
//
//            try {
//
//                filter.put("cust_phone",CustomerReg.sharedinstance.getCust_phone());
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
        try{
            filter.put("cust_phone", phoneNumber);
            query.put(filter);

            SocketEvent.sharedinstance.socket.emit("getdata", query, new Ack() {

                @Override
                public void call(final Object... args) {

                    if (args[0] != null && !args[0].toString().equals("0")) {

                        try {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(args[0].toString());
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            JsonNode jsonNode = JSONParse.convertJsonFormat(jsonObject);
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

                            CustomerReg.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), CustomerReg.class);

                            if(CustomerReg.sharedinstance.getCust_uid().equals(android_id))
                            {
                                setNumberSharedPreference(phoneNumber, cEmail, cName);

                                CustomerReg.sharedinstance.setCust_name(cName);

                                CustomerReg.sharedinstance.setCust_phone(phoneNumber);

                                CustomerReg.sharedinstance.setCust_email(cEmail);
                                finish();
                            }else{

                                try {  runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        deviceidcheck= true;
                                        sendCode(view);


                                    }

                                });}
                                catch(Exception ee){}
                            }
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }else{

                        try {  runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                sendCode(view);


                            }

                        });}
                        catch(Exception ee){}
                    }
                }
            });}
        catch(Exception e){}

    }
    public void sendCode(View view) {







        setUpVerificatonCallbacks();

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                30,              // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                this,     // Activity (for callback binding)
                verificationCallbacks);

        status_verify.setVisibility(View.VISIBLE);
        cus_verifyCode.setVisibility(View.VISIBLE);
        confirmCodeBtn.setVisibility(View.VISIBLE);
        // submitDetails.setEnabled(false);

//            setNumberSharedPreference(phoneNumber, cEmail, cName);
//
//
//           CustomerReg.sharedinstance.setCust_name(cName);
//
//           CustomerReg.sharedinstance.setCust_phone(phoneNumber);
//
//           CustomerReg.sharedinstance.setCust_email(cEmail);


    }



    private void setUpVerificatonCallbacks() {

        verificationCallbacks =
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                    @Override
                    public void onVerificationCompleted(
                            PhoneAuthCredential credential) {
                        // Toast.makeText(CustomerVerfication.this, "Code send", Toast.LENGTH_SHORT).show();


                        signInWithPhoneAuthCredential(credential);

                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {

                        if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            // Invalid request
                            //confirmCodeBtn.setEnabled(false);
                            //Toast.makeText(CustomerVerfication.this, "Wrong Code", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "Invalid credential: "
                                    + e.getLocalizedMessage());

                            Toast.makeText(CustomerVerfication.this, "Please enter Valid Number", Toast.LENGTH_SHORT).show();
                        } else if (e instanceof FirebaseTooManyRequestsException) {
                            // SMS quota exceeded
                            Log.d(TAG, "SMS Quota exceeded.");
                            Toast.makeText(CustomerVerfication.this, "SMS Quota exceeded.", Toast.LENGTH_SHORT).show();

                        }






                    }

                    @Override
                    public void onCodeAutoRetrievalTimeOut(String s) {

                        super.onCodeAutoRetrievalTimeOut(s);
                        submitDetails.setText("Resend");
                    }



                    @Override
                    public void onCodeSent(String verificationId,
                                           PhoneAuthProvider.ForceResendingToken token) {

                        Toast.makeText(CustomerVerfication.this, "code send", Toast.LENGTH_SHORT).show();

                        phoneVerificationId = verificationId;
                        resendToken = token;

                        Log.d("",""+phoneVerificationId);

                       /* verifyButton.setEnabled(true);
                        sendButton.setEnabled(false);
                        resendButton.setEnabled(true);*/
                    }
                };
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        fbAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {


                            status_verify.setText("Code Verified....!");
                            cus_verifyCode.setVisibility(View.INVISIBLE);
                            confirmCodeBtn.setVisibility(View.VISIBLE);
                            submitDetails.setVisibility(View.GONE);
                            FirebaseUser user = task.getResult().getUser();

                            setNumberSharedPreference(phoneNumber, cEmail, cName);
                            CustomerReg.sharedinstance.setCust_name(cName);
                            CustomerReg.sharedinstance.setCust_phone(phoneNumber);
                            CustomerReg.sharedinstance.setCust_email(cEmail);
                            try {
                                customerVerificationData();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            fbAuth.signOut();
                            finish();



                        } else {
                            if (task.getException() instanceof
                                    FirebaseAuthInvalidCredentialsException) {

                                // The verification code entered was invalid
                                Toast.makeText(CustomerVerfication.this, "Please enter a valid code", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                });
    }


    public boolean verifyCode(View view) {


        try {

            String code = cus_verifyCode.getText().toString();

            if (TextUtils.isEmpty(code)) {
                cus_verifyCode.setError("please Enter Code");
                return false;

            } else {

                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(phoneVerificationId, code);
                //signInWithPhoneAuthCredential(credential);
                fbAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

//                            Intent i = new Intent(getApplicationContext(), BookingScreen.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                            startActivity(i);
                            finish();
                            Toast.makeText(getApplicationContext(), "Code Verify", Toast.LENGTH_SHORT).show();
                            try {
                                setNumberSharedPreference(phoneNumber, cEmail, cName);

                                CustomerReg.sharedinstance.setCust_name(cName);

                                CustomerReg.sharedinstance.setCust_phone(phoneNumber);

                                CustomerReg.sharedinstance.setCust_email(cEmail);


                                if (SocketEvent.sharedinstance.socket == null) {
                                    SocketEvent.sharedinstance.initializeSocket();
                                    if(deviceidcheck==true){
                                        JSONArray   jsonarray=new JSONArray();
                                        try{

                                            jsonarray.put("CustomerReg");
                                            JSONObject jsonob = new JSONObject();
                                            jsonob.put("cust_phone", phoneNumber);
                                            jsonarray.put(jsonob);
                                            jsonob = new JSONObject();
                                            jsonob.put("cust_uid", android_id);
                                            jsonob.put("fcm_token",FirebaseInstanceId.getInstance().getToken());
                                            jsonarray.put(jsonob);
                                            Log.e("updateuid",jsonarray.toString());
                                        }catch(Exception e){}
                                        SocketEvent.sharedinstance.socket.emit("updatedata", jsonarray, new Ack() {
                                            @Override
                                            public void call(Object... args) {

                                            }
                                        });
                                    }
                                    else {
                                        customerVerificationData();
                                    }
                                }
                                else { if(deviceidcheck==true){
                                    JSONArray   jsonarray=new JSONArray();
                                    try{

                                        jsonarray.put("CustomerReg");
                                        JSONObject jsonob = new JSONObject();
                                        jsonob.put("cust_phone", phoneNumber);
                                        jsonarray.put(jsonob);
                                        jsonob = new JSONObject();
                                        jsonob.put("cust_uid", android_id);
                                        jsonob.put("fcm_token",FirebaseInstanceId.getInstance().getToken());
                                        jsonarray.put(jsonob);
                                        Log.e("updateuid",jsonarray.toString());
                                    }catch(Exception e){}
                                    SocketEvent.sharedinstance.socket.emit("updatedata", jsonarray, new Ack() {
                                        @Override
                                        public void call(Object... args) {

                                        }
                                    });
                                }
                                else {
                                    customerVerificationData();
                                }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(getApplicationContext(), "Verification Failed, Invalid credentials", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });


            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Wrong Code", Toast.LENGTH_SHORT).show();
        }
        return false;

    }




    //sending customer verification data in data base and check it is allready have or not

    public void customerVerificationData() throws JSONException {
        android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        final String token = FirebaseInstanceId.getInstance().getToken();

        if (!token.equals("")) {

            DataHolder.getInstance().fcmToken = token;

        }


        JSONArray query = new JSONArray();
        JSONObject filter = new JSONObject();

        query.put("CustomerReg");


//        if (preferences.contains(Number)) {
//                try {
//                    filter.put("cust_phone",preferences.getString(Number, ""));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }else{
//
//            try {
//
//                filter.put("cust_phone",CustomerReg.sharedinstance.getCust_phone());
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
        filter.put("cust_phone", getSharedPrefPhone());
        query.put(filter);

        SocketEvent.sharedinstance.socket.emit("getdata", query, new Ack() {

            @Override
            public void call(final Object... args) {

                if (args[0] != null && !args[0].toString().equals("0")) {

                    try {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(args[0].toString());
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                        JsonNode jsonNode = JSONParse.convertJsonFormat(jsonObject);
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

                        CustomerReg.sharedinstance = mapper.readValue(new TreeTraversingParser(jsonNode), CustomerReg.class);

                    } catch (Exception e) {
                        e.getMessage();
                    }
                } else {

                    JSONArray query = new JSONArray();
                    query.put("CustomerReg");
                    JSONObject values = new JSONObject();

                    try {
                        values.put("_id", CustomerReg.sharedinstance.get_id());
                        values.put("cust_uid",android_id );
                        values.put("blacklist", "false");
                        values.put("cust_email", CustomerReg.sharedinstance.getCust_email());
                        values.put("cust_name", CustomerReg.sharedinstance.getCust_name());
                        values.put("cust_phone", CustomerReg.sharedinstance.getCust_phone());
                        values.put("fcm_token", token);
                        query.put(values);

                        SocketEvent.sharedinstance.socket.emit("createdata", query, new Ack() {
                            @Override
                            public void call(Object... args) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }

//    private void resendVerificationCode(String phoneNumber,
//                                        PhoneAuthProvider.ForceResendingToken token) {
//        PhoneAuthProvider.getInstance().verifyPhoneNumber(
//                phoneNumber,        // Phone number to verify
//                60,                 // Timeout duration
//                TimeUnit.SECONDS,   // Unit of timeout
//                this,               // Activity (for callback binding)
//                verificationCallbacks,         // OnVerificationStateChangedCallbacks
//                token);             // ForceResendingToken from callbacks
//    }



}