package buzybeez.com.buzybeezcabcrystalpalace.FireBase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import buzybeez.com.buzybeezcabcrystalpalace.Models.CfgCustApp;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Driver;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.DriverSearch;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.OriginAndDestination;


/**
 * Created by SGI on 17/02/2019.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if( remoteMessage.getData()!=null)
        {
            for(String key :remoteMessage.getData().keySet()) {
                if(key.equals("state") && remoteMessage.getData().get(key).equals("despatch")) {
                    SharedPreferences preferences = getSharedPreferences("DriverState", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor= preferences.edit();
                    editor.putString("jobid", remoteMessage.getData().get("_id"));
                    String Driveruid=remoteMessage.getData().get("drvrcallsign");
                    String[] split = Driveruid.split("@");
                    editor.putString("drvcallsign",split[0] );
                    editor.putString("officeId", split[1]);
                    //editor.putString("drvImageIp", "");
                    //editor.putString("jobRef", bundle.getString("jobRef"));

                    Driver.sharedInstance.setDrvUID(Driveruid);

                    //editor.putString("drvImageIp", "");
                    //editor.putString("jobRef", bundle.getString("jobRef"));
                    editor.apply();
                    Intent intent = new Intent(this, DriverSearch.class);
                    startActivity(intent);
                }
                if(key.equals("state") && remoteMessage.getData().get(key).equals("cancelled")) {
                    Intent intent = new Intent(this, OriginAndDestination.class);
                    //   PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

                    startActivity(intent);

                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "1")
                            .setContentTitle(remoteMessage.getNotification().getTitle())
                            .setContentText(remoteMessage.getNotification().getBody())
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                            .setStyle(new NotificationCompat.BigTextStyle())
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                            .setSmallIcon(R.drawable.surbitonappicon)
                            .setAutoCancel(true);

                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        NotificationChannel mChannel = new NotificationChannel("1", "1", importance);
// Create a notification and set the notification channel.
                        notificationManager.createNotificationChannel(mChannel);
                    }

                    notificationManager.notify(CfgCustApp.notiid, notificationBuilder.build());
                    CfgCustApp.notiid++;
                }}}
        //   Intent intent = new Intent(this, DriverSearch.class);
        //   PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "1")
//                .setContentTitle("Myown")
//                .setContentText(remoteMessage.getNotification().getBody())
//                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                .setStyle(new NotificationCompat.BigTextStyle())
//                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//                .setSmallIcon(R.drawable.surbitonappicon)
//                .setAutoCancel(true)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            int importance = NotificationManager.IMPORTANCE_HIGH;
//            NotificationChannel mChannel = new NotificationChannel("1", "1", importance);
//// Create a notification and set the notification channel.
//            notificationManager.createNotificationChannel(mChannel);
//        }
//
//        notificationManager.notify(CfgCustApp.notiid, notificationBuilder.build());
//        CfgCustApp.notiid++;
    }


}


