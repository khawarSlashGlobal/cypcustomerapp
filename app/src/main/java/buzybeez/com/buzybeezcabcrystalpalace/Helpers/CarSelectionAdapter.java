package buzybeez.com.buzybeezcabcrystalpalace.Helpers;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import buzybeez.com.buzybeezcabcrystalpalace.Models.Car;
import buzybeez.com.buzybeezcabcrystalpalace.R;


public class CarSelectionAdapter extends RecyclerView.Adapter<CarSelectionAdapter.MyViewHolder> {

    private final OnItemClickListener listener;


    ArrayList<Car> carlist;


    public CarSelectionAdapter(ArrayList<Car> carlist,
                               OnItemClickListener listener) {
        this.listener = listener;
        this.carlist = carlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_list, parent, false);
        MyViewHolder holder1 = new MyViewHolder(view1);


        return holder1;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, final int position) {
        myViewHolder.itemView.setTag(position);
        Car carDataItem = carlist.get(position);
        myViewHolder.bind(carDataItem, listener);
        myViewHolder.carNameTV.setText(carDataItem.getCarName());
        myViewHolder.carPassengerCapacityTV.setText(carDataItem.getCarPassengerCapacity());
        myViewHolder.carLuggageCapacityTV.setText(carDataItem.getCarLuggageCapacity());
        myViewHolder.carImageIV.setImageResource(carDataItem.getCarImage());
        if (carDataItem.isSleceted()) {
            myViewHolder.selectionIndicatorIV.setVisibility(View.VISIBLE);
        } else {
            myViewHolder.selectionIndicatorIV.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return carlist.size();
    }


    /*skh: This OnItemClickListener interface has one method
     * onItemClick(Car selectedCarData, View v) which is implemented
     * in the BookingScreen.java class as inner class in the
     * constructor of CarSelectionAdapter*/
    public interface OnItemClickListener {
        void onItemClick(Car selectedCarData, View v);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout cl;
        public ImageView carImageIV, selectionIndicatorIV;
        public TextView carNameTV, carPassengerCapacityTV, carLuggageCapacityTV;


        public MyViewHolder(View itemView) {
            super(itemView);
            // itemView.setOnClickListener(this);


            carImageIV = (ImageView) itemView.findViewById(R.id.icon);
            selectionIndicatorIV = itemView.findViewById(R.id.selectionIndicator);
            carNameTV = (TextView) itemView.findViewById(R.id.item);
            carPassengerCapacityTV = (TextView) itemView.findViewById(R.id.passengerID);
            carLuggageCapacityTV = (TextView) itemView.findViewById(R.id.suitcaseID);
            cl = (ConstraintLayout) itemView.findViewById(R.id.cl);

        }
        /*skh: This method sets the  OnClickListener on cl and
        calls the method onItemClick(Car selectedCarData, View v);*/
        public void bind(final Car selectedCarData, final OnItemClickListener listener) {
            cl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(selectedCarData, v);
                }
            });


        }

    }


}

