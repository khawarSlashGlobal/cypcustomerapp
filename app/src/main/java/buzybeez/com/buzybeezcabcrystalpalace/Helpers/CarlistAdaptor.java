package buzybeez.com.buzybeezcabcrystalpalace.Helpers;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import buzybeez.com.buzybeezcabcrystalpalace.R;

/*skh:
* This is not used  */

public class CarlistAdaptor extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] itemname;
    private final Integer[] imgid;

    String[] totalPassengers = {
            "4 Passenger",
            "4 Passenger",
            "5 Passenger",
            "3 Passenger",
            "8 Passenger",

    };

    String[] totalSuitcases = {
            "1 Suitcase",
            "2 Suitcase",
            "3 Suitcase",
            "1 Suitcase",
            "4 Suitcase",

    };


    public CarlistAdaptor(Activity context, String[] itemname, Integer[] imgid) {
        super(context, R.layout.car_list, itemname);
        // TODO Auto-generated constructor stub

        this.context = context;
        this.itemname = itemname;
        this.imgid = imgid;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.car_list, null, true);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.item);
        TextView passenger = (TextView) rowView.findViewById(R.id.passengerID);
        TextView suitcase = (TextView) rowView.findViewById(R.id.suitcaseID);

        imageView.setImageResource(imgid[position]);
        txtTitle.setText(itemname[position]);
        passenger.setText(totalPassengers[position]);
        suitcase.setText(totalSuitcases[position]);


        return rowView;

    }

    ;
}
