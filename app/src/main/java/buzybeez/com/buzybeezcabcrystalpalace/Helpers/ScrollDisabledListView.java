package buzybeez.com.buzybeezcabcrystalpalace.Helpers;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ListView;


/*SKH:
* This is ScrollDisabledListView extends the ListView and disable the scroll.
* This <buzybeez.com.buzybeezcabcrystalpalace.Helpers.ScrollDisabledListView
* is used in the activity_autocomplete_place_picker.xml
*/

public class ScrollDisabledListView extends ListView {

    public ScrollDisabledListView(Context context) {
        super(context);
    }
    public ScrollDisabledListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public ScrollDisabledListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int heightMeasureSpec_custom = MeasureSpec.makeMeasureSpec(
                Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec_custom);
        ViewGroup.LayoutParams params = getLayoutParams();
        params.height = getMeasuredHeight();
    }
}