package buzybeez.com.buzybeezcabcrystalpalace.Helpers;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import buzybeez.com.buzybeezcabcrystalpalace.Models.Airports;
import buzybeez.com.buzybeezcabcrystalpalace.R;


public class AirportSelectionAdapter extends RecyclerView.Adapter<AirportSelectionAdapter.MyViewHolder> {
    private final OnAirportItemClickListener listener;
    ArrayList<Airports> airportlist;

    public AirportSelectionAdapter(ArrayList<Airports> carlist, OnAirportItemClickListener listener) {
        this.listener = listener;
        this.airportlist = carlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.airport_item, parent, false);
        MyViewHolder holder1 = new MyViewHolder(view1);
        return holder1;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, final int position) {
        //              Airports
//              "_id" : ObjectId("5a9e469fa7847484fd45a30b"),
//              "Country" : "UK",
//              "OfficeId" : "NBA",
//              "Name" : "LONDON HEATHROW AIRPORT TERMINAL 1 TW6 1AP",
//              "ShortName" : "LHR1",
//              "Plot" : "TW6",
//              "Place" : "LONDON HEATHROW AIRPORT TERMINAL 1",
//              "Postcode" : "TW6 1AP",
//              "Latitude" : 51.479392,
//              "Longitude" : -0.4544963
//
        Airports DataItem = airportlist.get(position);

        myViewHolder.itemView.setTag(position);
        Airports airportDataitem = airportlist.get(position);
        myViewHolder.bind(airportDataitem, listener,position);
        myViewHolder.airportName.setText(airportDataitem.getPlace());
        myViewHolder.airportPostcode.setText(airportDataitem.getPostcode());
        if(airportDataitem.getPlace().contains("HEATHROW")){
            myViewHolder.airportImage.setImageResource(R.drawable.heathrow);
        }else if (airportDataitem.getPlace().contains("STANSTED")){
            myViewHolder.airportImage.setImageResource(R.drawable.stansted);
        }else if (airportDataitem.getPlace().contains("LUTON")){
            myViewHolder.airportImage.setImageResource(R.drawable.luton);
        }else if (airportDataitem.getPlace().contains("GATWICK")){
            myViewHolder.airportImage.setImageResource(R.drawable.gatwick);
        }else if (airportDataitem.getPlace().contains("LONDON CITY AIRPORT")){
            myViewHolder.airportImage.setImageResource(R.drawable.londoncityairport);
        }else {
            myViewHolder.airportImage.setImageResource(R.drawable.airport_art);
        }
        if(DataItem.isSelected()){
            myViewHolder.selectionIndicatorIV.setVisibility(View.VISIBLE);
        }else {
            myViewHolder.selectionIndicatorIV.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return airportlist.size();
    }

    public interface OnAirportItemClickListener {
        void onAirportItemClick(Airports selectedAirportData, View v, int position);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout cl ;
        public TextView airportName;
        public ImageView airportImage,selectionIndicatorIV;
        public TextView airportPostcode;
        public TextView  airportSelection;

        public MyViewHolder(View itemView) {
            super(itemView);
            // itemView.setOnClickListener(this);
            airportImage = (ImageView) itemView.findViewById(R.id.AirportImage);
            airportName = itemView.findViewById(R.id.AirportName);
            airportPostcode = (TextView) itemView.findViewById(R.id.AirportPostcode);
            airportSelection = (TextView) itemView.findViewById(R.id.selectAirport);
                selectionIndicatorIV=(ImageView) itemView.findViewById(R.id.SlectionIndicatr);
            cl = (ConstraintLayout) itemView.findViewById(R.id.cl);
        }

        public void bind(final Airports selectedAirportData, final OnAirportItemClickListener listener,final int position) {

            cl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onAirportItemClick(selectedAirportData, v,position);
                }
            });

        }

    }


}

