package buzybeez.com.buzybeezcabcrystalpalace.Utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import buzybeez.com.buzybeezcabcrystalpalace.Models.CfgCustApp;
import buzybeez.com.buzybeezcabcrystalpalace.R;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.SplashScreen;

public class AlarmBroadcastReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
       Bundle Extras= intent.getExtras();
        Intent intent1 = new Intent(context, SplashScreen.class);
//        Bundle extras= new Bundle();
//        extras.putString( "futurebooking",  Extras.getString("futurebooking"));
//        extras.putString( "vehType",  Extras.getString("vehType"));
//        extras.putString( "originLng",  Extras.getString( "originLng"));
//        extras.putString(  "originLat", Extras.getString("originLat"));
//        extras.putString( "origin",  Extras.getString("origin"));
//        extras.putString( "destination",  Extras.getString("destination"));
//        extras.putString(  "_fromOutcode", Extras.getString( "_fromOutcode"));
//        extras.putString( "jobID",  Extras.getString("jobID"));
//        extras.putString(  "jobReference", Extras.getString("jobReference"));
//        extras.putString( "telephone",  Extras.getString("telephone"));
//        extras.putString( "jobinoffice",  Extras.getString("jobinoffice"));
//        extras.putString( "drvImageIp",  Extras.getString("drvImageIp"));
//
//intent1.putExtras(extras);
        SharedPreferences sharedPreferences = context.getSharedPreferences("localnoti", Context.MODE_PRIVATE);
        SharedPreferences.Editor extras = sharedPreferences.edit();
        extras.putString("futurebooking","1");
        extras.apply();
        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent1,PendingIntent.FLAG_IMMUTABLE);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, "1")
                .setContentTitle("Booking time arrived")
                .setContentText("Your Booking is Waiting for you!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.drawable.surbitonappicon)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("1", "1", importance);
// Create a notification and set the notification channel.
            notificationManager.createNotificationChannel(mChannel);
        }

        notificationManager.notify(CfgCustApp.notiid, notificationBuilder.build());

    }

}
