package buzybeez.com.buzybeezcabcrystalpalace.Utils;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by hv on 1/24/18.
 */

public class Network extends Application {

    public static Network sharedInstance = new Network();

    private Network() {
    }

    static public boolean isNetworkAvailable(Context c) {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();


    }

    public long getBST() {

        int offset = TimeZone.getDefault().getRawOffset() + TimeZone.getDefault().getDSTSavings();
        long now = System.currentTimeMillis() + offset;
        // return (System.currentTimeMillis() + DateTimeZone.forID("Europe/London").getOffset(new Instant())) / 1000L;

        return now / 1000L;
    }


    public int getnumberofMonth(Double startDate, Double endDate) {

        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(getDate(startDate));
        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(getDate(endDate));

        int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
        int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);

        return diffMonth;

    }

    public Date getDate(Double myTimestamp) {

        Double myDouble = myTimestamp;
        long myLong = System.currentTimeMillis() + ((long) (myDouble * 1000));
        Date itemDate = new Date(myLong);

        return itemDate;
    }

    public String getDateformat(double myTimestamp) {
        Date time = new Date((long) myTimestamp * 1000);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String date = sdf.format(time);

        return date;
    }


}
