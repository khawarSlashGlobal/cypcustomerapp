package buzybeez.com.buzybeezcabcrystalpalace.Utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Ack;

public class ratingavg {
    /**
     * Created by HJ on 21/02/2019.
     * takes user and database rating of driver and gives average of them
     */
    String temprating;
    JSONArray jsonarray;
    JSONObject jsonob;
    JSONObject data;

    public ratingavg(final String driveruid, final Float newrating) {

        try {
            jsonob = new JSONObject();
            jsonarray = new JSONArray();
            jsonob.put("driveruid", driveruid);
            jsonarray.put("Ratings");
            jsonarray.put(jsonob);
            if (SocketEvent.sharedinstance.socket==null)
            {
                SocketEvent.sharedinstance.initializeSocket();
            }
            else if(!SocketEvent.sharedinstance.socket.connected())
            {
                SocketEvent.sharedinstance.socketConnectAfterDisconnect();
            }
            Log.i("prerating:", "ok");
            SocketEvent.sharedinstance.socket.emit("getdata", jsonarray, new Ack() {
                @Override
                public void call(final Object... args) {
                    try {
                        data = (JSONObject) args[0];
                        Log.i("rating:", data.getString("rating"));
                        temprating = String.format("%.1f", (Double.parseDouble(data.getString("rating")) + newrating) / 2);
                        Log.i("temprating:", temprating);
                        jsonarray = new JSONArray();
                        jsonarray.put("Ratings");
                        jsonob = new JSONObject();
                        jsonob.put("driveruid", driveruid);
                        jsonarray.put(jsonob);
                        jsonob = new JSONObject();
                        jsonob.put("rating", temprating);
                        Log.e("error_ob", "error");
                        jsonarray.put(jsonob);

                        SocketEvent.sharedinstance.socket.emit("updatedata", jsonarray, new Ack() {
                            @Override
                            public void call(Object... args) {
                                Log.i("rating_upload:", args[0].toString());
                            }
                        });
                    } catch (JSONException e) {
                        Log.e("emit_error: ", e.getMessage());
                    }

                }
            });
        } catch (JSONException e) {
            Log.e("JSON_error:", e.getMessage());
        }
    }
}