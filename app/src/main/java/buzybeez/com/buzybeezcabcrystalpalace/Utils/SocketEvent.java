package buzybeez.com.buzybeezcabcrystalpalace.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import buzybeez.com.buzybeezcabcrystalpalace.DataCenter.DataHolder;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.Constants;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.poblistener;
import buzybeez.com.buzybeezcabcrystalpalace.Models.BookingHistory;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Chat;
import buzybeez.com.buzybeezcabcrystalpalace.Models.Driver;
import buzybeez.com.buzybeezcabcrystalpalace.Models.DriverLoc;
import buzybeez.com.buzybeezcabcrystalpalace.Models.LoadMsg;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.BaseActivity;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.ChatActivity;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.DriverSearch;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.JSONParse;
import buzybeez.com.buzybeezcabcrystalpalace.User_interface.RatingScreen;
import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.PORT_TO_CONNECT;
import static buzybeez.com.buzybeezcabcrystalpalace.Constants.AppConstants.URL_TO_CONNECT;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.BookingScreen.jobRef;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.DriverSearch.JOBID;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.DriverSearch.directionhandler;
import static buzybeez.com.buzybeezcabcrystalpalace.User_interface.DriverSearch.rejectEmitHandler;


/**
 * Created by hv on 1/24/18.
 */

public class SocketEvent extends BaseActivity {

    public static poblistener poblisten;
    public static SocketEvent sharedinstance = new SocketEvent();
    private final Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... objects) {

        }
    };
    private final Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... objects) {

        }
    };
    private final Emitter.Listener onReconnectAttempt = new Emitter.Listener() {
        @Override
        public void call(Object... objects) {
        }
    };
    private final Emitter.Listener onReconnecting = new Emitter.Listener() {
        @Override
        public void call(Object... objects) {
        }
    };
    private final Emitter.Listener onReconnectError = new Emitter.Listener() {
        @Override
        public void call(Object... objects) {
        }
    };
    private final Emitter.Listener onReconnect = new Emitter.Listener() {
        @Override
        public void call(Object... objects) {
        }
    };
    private final Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... objects) {
        }
    };
    private final Emitter.Listener onReconnectFailed = new Emitter.Listener() {
        @Override
        public void call(Object... objects) {
        }
    };
    public int internetStatus = 0;
    public Socket socket;
    public IO.Options io;
    // These Variable Use for Emit
    ArrayList<String> ids;
    String jobid;
    int pos;
    SharedPreferences.Editor editor;
    Context context;
    SharedPreferences preferences;
    String ip = null;
    Handler handler;
    public static String msg = null;
    public static int isActivityOpen = 0;
    public static int b = 0;

    public static int counter = 0;


    private Emitter.Listener onJobRecieved = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            dropbreadCrumb("Event", "Socket reconnected", "Socket Event");

            //rejectEmitHandler.removeCallbacksAndMessages(null);

//			if (!args[0].toString().equals("0")) {
//				Driver driver = Driver.sharedInstance;
//
//				String data = args[0].toString();
//				String[] record = data.split("\\|");
//				String drvUID = record[0];
//				Driver.sharedInstance.setDrvUID(drvUID);
//				String[] rec = drvUID.split("@");
//				String callSign = rec[0];
//				driver.setcallsign(callSign);
//				String office = rec[1];
//				driver.setOfficeID(office);
//				getDriverLocByEmit();
//			}
        }
    };
    private Emitter.Listener onJobDone = new Emitter.Listener() {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void call(Object... args) {
            dropbreadCrumb("Event", "Socket reconnected", "Socket Event");

            detectJobDone();
            if (directionhandler != null) {
                directionhandler.removeCallbacksAndMessages(null);
            }
            if(rejectEmitHandler != null)
            {
                rejectEmitHandler.removeCallbacksAndMessages(null);
            }
        }
    };

    private Emitter.Listener drvoffered = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            dropbreadCrumb("Event", "Socket reconnected", "Socket Event");
            if (!args[0].toString().equals("0")) {
                Driver driver = Driver.sharedInstance;

                String data = args[0].toString();
                String[] record = data.split("\\|");
                String drvUID = record[0];
                Driver.sharedInstance.setDrvUID(drvUID);
                String[] rec = drvUID.split("@");
                String callSign = rec[0];
                driver.setcallsign(callSign);
                String office = rec[1];
                driver.setOfficeID(office);
                getDriverLocByEmit();
            }
        }
    };

    private Emitter.Listener onPOB = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            dropbreadCrumb("Event", "Socket reconnected", "Socket Event");
            //checkPOBemit = true;
            poblisten.recieveonpob();
        }
    };

    private Emitter.Listener drvreject = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            checkDRVreject = true;

        }
    };

    private Emitter.Listener driverLocation = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            dropbreadCrumb("Event", "Socket reconnected", "Socket Event");

            if (!args[0].toString().equals("0")) {
                final DriverLoc driverLoc = DriverLoc.sharedInstance;

                String data = args[0].toString();
                final String[] locationData = data.split("\\|");

                String drvlatlong = locationData[2];

                final String[] splitlatlong = drvlatlong.split(",");

                double lat = Double.parseDouble(splitlatlong[0]);
                double lng = Double.parseDouble(splitlatlong[1]);
                driverLoc.setLat(lat);
                driverLoc.setLong(lng);
                try{
                    Thread.sleep(3000);}
                catch(Exception e){}
                poblisten.recieveondriverlatlng();

            }

        }
    };


    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        //String msgs;
        @Override
        public void call(final Object... args) {


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String data = args[0].toString();
                    String moreData[] = data.split("\\|");
                    msg = moreData[2];
                    //first condition when chat activity is not running and if b when activity is stop so will notcreate counter badge

                    if (isActivityOpen == 0) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        Date date = new Date();
                        String now = formatter.format(date);
                        LoadMsg.msg.add(new Chat(msg, now, true));
                        counter++;
                        if (b != 1) {
                            DriverSearch.getCounter();
                        }

                        DriverSearch.getAct();
                    }

                    //second when activity is running
                    else {
                        ChatActivity.getDatas(msg);
                    }

                }
            });
        }


    };

    public void sendMessage(String msg) {
        if (!SocketEvent.sharedinstance.socket.connected()) {

            SocketEvent.sharedinstance.socketConnectAfterDisconnect();
        }

        String id = DriverLoc.sharedInstance.getCallsign() + "@" + DriverLoc.sharedInstance.getOfficeId();
        if (id != "" && id != null) {


            if (jobRef.contains("@")) {
                String room = jobRef;
                String[] array = room.split("@");
                String[] rooms = array[0].split("-");
                String reqRoom = rooms[1];
                String eventName = Constants.DAcceptMessageCustomer;
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(reqRoom);
                jsonArray.put(id);
                jsonArray.put(eventName);
                jsonArray.put(msg);
                jsonArray.put("");
                SocketEvent.sharedinstance.socket.emit(Constants.intraDC, jsonArray);
            }

        }
    }




    public SocketEvent() {

        context = this;

    }

    public void getDriverLocByEmit() {
        try {
            dropbreadCrumb("Event", "Socket reconnected", "Socket Event");

            JSONArray query = new JSONArray();
            query.put("Driverloc");
            JSONObject filter = new JSONObject();

            if (!drvCallsign.equals("") && !officeId.equals("")) {
                filter.put("callsign", drvCallsign);
                filter.put("office_id", officeId);
            } else {
                filter.put("callsign", Driver.sharedInstance.getCallsign());
                filter.put("office_id", Driver.sharedInstance.getOfficeID());
            }
            query.put(filter);


            SocketEvent.sharedinstance.socket.emit("getdata", query, new Ack() {
                @Override
                public void call(Object... args) {
                    if (args[0] != null && !args[0].toString().equals("0")) {

                        try {

                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            JsonNode jsonNode = JSONParse.convertJsonFormat(jsonObject);
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            DriverLoc.sharedInstance = mapper.readValue(new TreeTraversingParser(jsonNode), DriverLoc.class);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnectSocket() {
        socket.disconnect();
    }

    public Socket initializeSocket() {

        dropbreadCrumb("Event", "Socket reconnected", "Socket Event");

        io = new IO.Options();
        io.forceNew = false;
        io.multiplex = false;
        io.reconnection = true;
        io.reconnectionAttempts = -1;
        io.reconnectionDelay = 5;

        try {
            GetIP object = new GetIP();

            try {
                //This Url for Testing Remove this When u live
                //ip = object.execute("http://www.buzybeezuk.com/config.html").get();

                //ip = object.execute("http://www.buzybeezuk.com/test-config.html").get();
                ip = object.execute(URL_TO_CONNECT).get();


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            if (ip.contains("\n"))
                ip = ip.replace("\n", "");

            //socket = IO.socket("http://" + ip + ":6666", io);
            socket = IO.socket("http://" + ip + PORT_TO_CONNECT, io);

            if (!drvCallsign.equals("")) {

                socket.io().reconnection();
            } else {
                if (socket.connected() == false) {
                    connectSocket();
                }
            }


        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


        return socket;
    }

    public void connectSocket() {

        dropbreadCrumb("Event", "Socket reconnected", "Socket Event");


        //socket.connect();
//		if (socket!=null && !socket.connected()) {
        socket.connect();

        socket.on(Socket.EVENT_CONNECT, onConnect);
        socket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        socket.on(Socket.EVENT_RECONNECT_ATTEMPT, onReconnectAttempt);
        socket.on(Socket.EVENT_RECONNECTING, onReconnecting);
        socket.on(Socket.EVENT_RECONNECT_ERROR, onReconnectError);
        socket.on(Socket.EVENT_RECONNECT, onReconnect);
        socket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        socket.on(Socket.EVENT_RECONNECT_FAILED, onReconnectFailed);

        //listner emits
        socket.on(Constants.recieveJob, onJobRecieved);
        socket.on(Constants.jobDone, onJobDone);
        socket.on(Constants.driverPosition, driverLocation);
        socket.on(Constants.POB, onPOB);
        socket.on(Constants.DriverOffer, drvoffered);
        socket.on(Constants.DriverReject, drvreject);
        socket.on(Constants.CAcceptMessageD, onNewMessage);
        socket.on(Constants.Drivercancelcustjob,drvreject);

//		if (Network.isNetworkAvailable(context)){
        //socket.connect();
        //}

    }

    public void socketConnectAfterDisconnect() {

        io = new IO.Options();

        io.forceNew = false;
        io.multiplex = false;
        io.reconnection = true;
        io.reconnectionAttempts = -1;
        io.reconnectionDelay = 5;

        try {
            if (ip == null) {

                GetIP object = new GetIP();
                try {
                    //This Url for Testing Remove this When u live
                    //ip = object.execute("http://www.buzybeezuk.com/config.html").get();

                    //for local office
                    ip = object.execute(URL_TO_CONNECT).get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                if (ip != null && ip.contains("\n"))
                    ip = ip.replace("\n", "");


                //port 4444 is live office
                //port 6666 is test office

                socket = IO.socket("http://" + ip + PORT_TO_CONNECT, io);
                if (socket.connected() == false) {
                    connectSocket();
                }

            } else {

                socket = IO.socket("http://" + ip + PORT_TO_CONNECT, io);
                if (socket.connected() == false) {
                    connectSocket();

                }


            }
//			if (socket.connected() == false) {
//				connectSocket();
//
//			}

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void detectJobDone() {
        DriverSearch.charge();
        preferences = BaseActivity.context.getSharedPreferences("DriverState", Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString("jobid", "");
        editor.putString("drvcallsign", "");
        editor.putString("officeId", "");
        editor.putString("drvImageIp", "");
        editor.putString("jobRef", "");
        editor.apply();
        officeId = "";
        drvCallsign = "";
        JOBID = "";

        try {

            ids = db.getids("Booked");
            jobid = Objects.requireNonNull(getIntent().getExtras()).getString("jobid");
            pos = Objects.requireNonNull(getIntent().getExtras()).getInt("pos");
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean retval = (ids.contains(jobid));


        if (retval) {
            if (jobid != null && pos >= 0) {
                //removeData(pos);
            }
            //inserting completed booking in SQLite
            db.insertBookingForHistory(BookingHistory.sharedinstance.getDate(),
                    BookingHistory.sharedinstance.getTime(), BookingHistory.sharedinstance.getTo(),
                    BookingHistory.sharedinstance.getFrom(), BookingHistory.sharedinstance.getFare(),
                    "Completed", BookingHistory.sharedinstance.getId(), BookingHistory.sharedinstance.getJobref(), BookingHistory.sharedinstance.getDatentime());
        } else {
            db.insertBookingForHistory(BookingHistory.sharedinstance.getDate(),
                    BookingHistory.sharedinstance.getTime(), BookingHistory.sharedinstance.getTo(),
                    BookingHistory.sharedinstance.getFrom(), BookingHistory.sharedinstance.getFare(),
                    "Completed", BookingHistory.sharedinstance.getId(), BookingHistory.sharedinstance.getJobref(), BookingHistory.sharedinstance.getDatentime());
        }


        finish();
        DataHolder.getInstance().clearAllDataHolders();
        Intent intent = new Intent(BaseActivity.context, RatingScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BaseActivity.context.startActivity(intent);
        Driver.sharedInstance=new Driver();
        DriverLoc.sharedInstance= new DriverLoc();
    }


}
