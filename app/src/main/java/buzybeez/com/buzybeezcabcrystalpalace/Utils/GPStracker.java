package buzybeez.com.buzybeezcabcrystalpalace.Utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by hv on 1/25/18.
 */

public class GPStracker extends Service implements LocationListener {
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    private final Context mContext;
    // Declaring a Location Manager
    public LocationManager locationManager;
    public LocationListener locationListener;
    public String address, state;
    // flag for GPS status
    boolean isGPSEnabled = false;
    // flag for network status
    boolean isNetworkEnabled = false;
    // flag for GPS status
    boolean canGetLocation = false;
    public Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    boolean setUpListner = false;

    //skh
    private  LocationProviderStatusListner listener;
    public interface LocationProviderStatusListner {
        void onDeviceLocationChanged(Location location);
        void onLocProviderDisabled(String provider) ;
        void onLocProviderEnabled(String provider);
        void onProviderStatusChanged(String provider, int status, Bundle extras);
    }


    public GPStracker(Context context) {
        this.mContext = context;
        getLocation();
    }

    public GPStracker(Context context,LocationProviderStatusListner listener) {
        this.listener = listener;
        this.mContext = context;
        setUpListner = true;
        getLocation();
    }



    @SuppressLint("MissingPermission")
    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);


            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {

                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            if (location != null) {

                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }

            Log.e("latitude", "latitude--" + latitude);

            try {
                Log.e("latitude", "inside latitude--" + latitude);
                addresses = geocoder.getFromLocation(latitude, longitude, 1);


                if (addresses != null && addresses.size() > 0) {
                    address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    //locationTxt.setText(address + " " + city + " " + country);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }


    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPStracker.this);
        }
    }

    /**
     * Function to get latitude
     */
    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        //Toast.makeText(mContext, "Loc Changed", Toast.LENGTH_SHORT).show();
        if(setUpListner)listener.onDeviceLocationChanged(location);
    }

    @Override
    public void onProviderDisabled(String provider) {
        if(setUpListner)listener.onLocProviderDisabled(provider);
        //Toast.makeText(mContext, "P Disable", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderEnabled(String provider) {
        if(setUpListner)listener.onLocProviderEnabled(provider);
        //Toast.makeText(mContext, "P Enable", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        if(setUpListner)listener.onProviderStatusChanged(provider,status,extras);
        //Toast.makeText(mContext, "P Status changed", Toast.LENGTH_SHORT).show();


    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


//    private final Context context;
//
//    boolean isGPSEnabled = false;
//    boolean isNetworkEnabled = false;
//    boolean canGetlocation = false;
//
//    Location location;
//    public double latitude, longitude;
//    protected LocationManager locationManager;
//
//    public GPStracker(Context context){
//        this.context = context;
//    }
//
//
//    //create a Getlocation method
//    public Location getLocation(){
//
//        try{
//
//            locationManager = (LocationManager)context.getSystemService(LOCATION_SERVICE);
//            isGPSEnabled = locationManager.isProviderEnabled(locationManager.GPS_PROVIDER);
//            isNetworkEnabled = locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER);
//
//            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
//            || ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)  == PackageManager.PERMISSION_GRANTED){
//
//                if (isGPSEnabled){
//                    if (location == null){
//                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000 ,10,this);
//                        if (locationManager != null){
//                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                            locationManager.removeUpdates(this);
//
//                        }
//                    }
//                }
//                //if location not found from gps then it will found from network
//                if (location == null){
//                    if (isNetworkEnabled){
//                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,1000,10,this);
//                            if (locationManager != null){
//                                location =locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//                                locationManager.removeUpdates(this);
//                            }
//
//                    }
//                }
//
//            }
//
//
//
//        }catch (Exception ex){
//
//        }
//
//        return location;
//    }
//
//
//
////following are the default method of gps if we implement locationlistener
//    public void onLocationChanged(Location location){
//        if (location != null)
//        {
//            Log.i("SuperMap", "Location changed : Lat: " + location.getLatitude() + " Lng: " + location.getLongitude());
//            latitude = location.getLatitude();
//            longitude = location.getLongitude();
//            Log.i("latitude,longitude", ""+latitude+","+longitude);
//            locationManager.removeUpdates(this);
//
//        }
//
//    }
//
//
//    public void onStatusChanged(String provider, int status, Bundle extras){
//
//    }
//
//
//    public void onProviderEnabled(String provider){
//
//
//    }
//
//
//    public  void onProviderDisabled(String Provider){
//
//    }
//
//
//    public IBinder onBind(Intent args){
//
//        return null;
//
//    }
}


