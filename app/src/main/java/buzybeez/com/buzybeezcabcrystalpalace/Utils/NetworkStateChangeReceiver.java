package buzybeez.com.buzybeezcabcrystalpalace.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;


import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.ListenerGPS;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by SGI on 2/19/2018.
 */

public class NetworkStateChangeReceiver extends BroadcastReceiver {
    public static final String NETWORK_AVAILABLE_ACTION = "buzybeez.com.buzybeezcabsurbiton.NetworkAvailable";
    public static final String IS_NETWORK_AVAILABLE = "isNetworkAvailable";
    LocationManager locationManager;
    boolean gpsStatus;
    private ConnectionListener ref;
    private WifiManager wifiManager;
    private ListenerGPS listenerGPS;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {

//            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//            TelephonyManager mTelephonyManager = (TelephonyManager)
//                    context.getSystemService(Context.TELEPHONY_SERVICE);
//            int networkType = mTelephonyManager.getNetworkType();
//
//            final NetworkInfo mobileInfo =
//                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
//
//            wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//
//            if (wifiManager.isWifiEnabled()) {
//                wifiManager.setWifiEnabled(false);
//                showSettingAlert(context, "Enable Mobile Data", "Go to settings to settings to enable mobile data");
//            }
//            String get = getNetworkClass(context);
//            if (get.equals("4G")) {
//                dataOn3G( "3G", context);
//            }
//            else
//                dataOn3G("3G" ,context);


            Intent networkStateIntent = new Intent(NETWORK_AVAILABLE_ACTION);
            networkStateIntent.putExtra(IS_NETWORK_AVAILABLE, isConnectedToInternet(context));

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }


    private boolean isConnectedToInternet(Context context) {
        try {
            ref = (ConnectionListener) context;
            if (context != null && (Network.isNetworkAvailable(context))) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                ref.recieveMsgOnNetwork("Connected");
                if (!SocketEvent.sharedinstance.socket.connected()) {

                    SocketEvent.sharedinstance.socketConnectAfterDisconnect();
                }
                return networkInfo != null && networkInfo.isConnected();
            } else {
                ref.recieveMsgOnNetwork("Disonnected");
                SocketEvent.sharedinstance.disconnectSocket();
                return false;
            }

        } catch (NullPointerException e) {
            Log.e(NetworkStateChangeReceiver.class.getName(), e.getMessage());
            return false;
        }
    }

    public String getNetworkClass(Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = mTelephonyManager.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "4G";
            default:
                return "Unknown";

        }
    }

    public String dataOn3G(String a, Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = mTelephonyManager.getNetworkType();
        switch (a) {
            case "3G":
                networkType = TelephonyManager.NETWORK_TYPE_UMTS;

                networkType = TelephonyManager.NETWORK_TYPE_EVDO_0;
                networkType = TelephonyManager.NETWORK_TYPE_EVDO_A;
                networkType = TelephonyManager.NETWORK_TYPE_HSDPA;
                networkType = TelephonyManager.NETWORK_TYPE_HSUPA;
                networkType = TelephonyManager.NETWORK_TYPE_HSPA;
                networkType = TelephonyManager.NETWORK_TYPE_EVDO_B;
                networkType = TelephonyManager.NETWORK_TYPE_EHRPD;
                networkType = TelephonyManager.NETWORK_TYPE_HSPAP;


//            default:
//                return "unknown";

        }
        a = String.valueOf(networkType);
        return a;
    }


}


