package buzybeez.com.buzybeezcabcrystalpalace.Interfaces;

public interface WSCallerVersionListener {
    public void onGetResponse(boolean isUpdateAvailable);
}
