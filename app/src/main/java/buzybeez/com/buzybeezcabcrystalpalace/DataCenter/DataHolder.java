package buzybeez.com.buzybeezcabcrystalpalace.DataCenter;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hv on 3/31/18.
 */

public class DataHolder {

    public static DataHolder ourInstance = new DataHolder();
    public String flightholder = "";
    public String takechildSeat = "";
    public String bookingdate = "";
    public String bookingtime = "";
    public long unixdateNtime = 0;
    public double cusfare;
    public String additionalInstruction = "";
    public String fcmToken = "";
    public long timetodespatch = 0;
    public String jobID = "";
    public String jobReference = "";
    public String origin = "";
    public String destination = "";
    public String _fromOutcode = "";
    public String vehType = "";
    public double originLat = 0.0;
    public double originLng = 0.0;
    public String telephone = "";
    public Object[] objectLogC = new Object[]{};
    public JSONArray jsonArrayLogC = new JSONArray();
    public ArrayList<String> originData = new ArrayList<>();
    public ArrayList<String> destinationData = new ArrayList<>();
    public ArrayList<Double> originDataLatLng = new ArrayList<>();
    public ArrayList<Double> destinationDataLatLng = new ArrayList<>();
    public ArrayList<Double> nearestOfficeLatLng = new ArrayList<>();
    public ArrayList<String> nearestOfficeName = new ArrayList<>();
    public Map<String, Double> nearestOfficeData = new HashMap<>();
    public ArrayList<String> namesOfNearOffice = new ArrayList<>();


    private DataHolder() {
    }

    public static DataHolder getInstance() {
        return ourInstance;
    }

    //clear all variables
    public void clearAllDataHolders() {
        //clearing arrays
        originData.clear();
        destinationData.clear();
        originDataLatLng.clear();
        destinationDataLatLng.clear();
        nearestOfficeLatLng.clear();
        nearestOfficeName.clear();
        nearestOfficeData.clear();
        namesOfNearOffice.clear();

        flightholder = "";
        takechildSeat = "";
        bookingdate = "";
        bookingtime = "";
        unixdateNtime = 0;
        cusfare = 0.0;
        additionalInstruction = "";
        fcmToken = "";
        timetodespatch = 0;
        jobID = "";
        jobReference = "";
        origin = "";
        destination = "";
        _fromOutcode = "";
        vehType = "";
        originLat = 0.0;
        originLng = 0.0;
        telephone = "";
        objectLogC = new Object[]{};
        jsonArrayLogC = new JSONArray();
    }


}
