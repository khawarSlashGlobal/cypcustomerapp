package buzybeez.com.buzybeezcabcrystalpalace.DataHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import buzybeez.com.buzybeezcabcrystalpalace.Models.Airports;

/*created by skh
* This class SQLiteAirportList extends the SQLiteOpenHelper
* it has
* */


public class SQLiteAirportList extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "AirportDatabse";
    private static final int DB_VER = 1;
    private final String TABLE_NAME = "Airport";
    private final String COLUMN_ID = "id";
    private final String COLUMN_NAME = "ProfileImage";

    private String Id = "Id";

    private String Country = "Country";

    private String OfficeId= "OfficeId";

    private String Name = "Name";

    private String ShortName = "ShortName";

    private String Plot = "Plot";

    private String Place = "Place";

    private String Postcode = "Postcode";

    private String Latitude = "Latitude";

    private String Longitude = "Longitude";


    public SQLiteAirportList(Context context) {
        super(context, DATABASE_NAME, null, DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS Airport(id TEXT PRIMARY KEY ," +
                "Country TEXT " +
                ",OfficeId TEXT," +
                "Name TEXT," +
                "ShortName TEXT," +
                "Plot TEXT," +
                "Place TEXT," +
                " Postcode TEXT," +
                "Latitude  DOUBLE," +
                "Longitude DOUBLE)");

    }



    /*skh: This method takes an airport as parameter creates the
    getWritableDatabase object and add the airport to the
    "AirportDatabse" database then closes the getWritableDatabase object
    use this method to add an airport to database*/
    public void addAirportToDb(Airports a) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Id, (a.getId()).toString());
        values.put(Country,a.getCountry().toString());
        values.put(OfficeId, a.getOfficeId().toString());
        values.put(Name, a.getName().toString());
        values.put(ShortName, a.getShortName().toString());
        values.put(Plot, a.getPlot().toString());
        values.put(Place, a.getPlace().toString());
        values.put(Postcode, a.getPostcode().toString());
        values.put(Latitude,(double)a.getLatitude());
        values.put(Longitude,(double) a.getLongitude());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }


    /*skh: This method returns an ArrayList of type Airport
    * use this method to get all the airports stored in the database*/
    public ArrayList<Airports> getAllAirports() {
        ArrayList<Airports> AirportsList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //Cursor res = db.rawQuery("select * from " + TABLE_HISTORY  );
            String query = "SELECT * FROM " + TABLE_NAME ;

            Cursor res = db.rawQuery(query, null);

            while (res.moveToNext()) {
                Airports a = new Airports();
                a.setId(res.getString(0));
                a.setCountry(res.getString(1));
                a.setOfficeId(res.getString(2));
                a.setName(res.getString(3));
                a.setShortName(res.getString(4));
                a.setPlot(res.getString(5));
                a.setPlace(res.getString(6));
                a.setPostcode(res.getString(7));
                a.setLatitude(res.getDouble(8));
                a.setLongitude(res.getDouble(9));
                AirportsList.add(a);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return AirportsList;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


}
