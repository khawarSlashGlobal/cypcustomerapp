package buzybeez.com.buzybeezcabcrystalpalace.DataHandler;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import buzybeez.com.buzybeezcabcrystalpalace.Models.BookingHistory;
import buzybeez.com.buzybeezcabcrystalpalace.Models.FareListMapping;

/**
 * Created by hv on 3/12/18.
 */

public class SQLiteDatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "FareList";
    private static final String TABLE_NAME = "Fare";
    private static final String KEY_ID = "id";
    private static final String KEY_RATENAME = "RateName";
    private static final String KEY_OFFICEID = "OfficeID";
    private static final String KEY_VEHICLETYPE = "VehicleType";
    private static final String KEY_VEHSYMBOL = "vehsymbol";
    private static final String KEY_ACC_ROUND = "Acc_Round";
    private static final String KEY_ACC_RTYPEPRICERB = "Acc_RTypePriceRb";
    private static final String KEY_ACC_TONEAREST = "Acc_ToNearest";
    private static final String KEY_Acc_VATRATE = "Acc_VatRate";
    private static final String KEY_Acc_MINPRICE = "Acc_MinPrice";
    private static final String KEY_Acc_EXTDROP = "Acc_ExtDrop";
    private static final String KEY_ACC_UNIT = "Acc_Unit";
    private static final String KEY_ACC_COST = "Acc_cost";
    private static final String KEY_DRV_ROUND = "Drv_Round";
    private static final String KEY_DRV_RTYPEPRICERB = "Drv_RTypePriceRb";
    private static final String KEY_DRV_TONEAREST = "Drv_ToNearest";
    private static final String KEY_DRV_VATRATE = "Drv_VatRate";
    private static final String KEY_DRV_MINPRICE = "Drv_MinPrice";
    private static final String KEY_DRV_EXTDROP = "Drv_ExtDrop";
    private static final String KEY_DRV_UNIT = "Drv_Unit";
    private static final String KEY_DRV_COST = "Drv_Cost";
    private static final String KEY_DRV_PERC = "Drv_Perc";
    private static final String KEY_STATUS = "status";

    private static final String TABLE_HISTORY = "History";
    private static final String KEY_BOOKING_ID = "history_Id";
    private static final String KEY_BOOKING_DATE = "date";
    private static final String KEY_BOOKING_TIME = "time";
    private static final String KEY_BOOKING_ORIGIN = "origin";
    private static final String KEY_BOOKING_DESTINATION = "destination";
    private static final String KEY_BOOKING_VIAS = "vias";
    private static final String KEY_BOOKING_FARE = "fare";
    private static final String KEY_BOOKING_STATUS = "status";
    private static final String KEY_BOOKING_JOBID = "jobID";
    private static final String KEY_BOOKING_JOBREF = "jobRef";
    private static final String KEY_BOOKING_DATENTIME = "dateNtime";


    private static final String[] COLUMNS = {KEY_ID, KEY_RATENAME, KEY_OFFICEID, KEY_VEHICLETYPE, KEY_VEHSYMBOL,
            KEY_ACC_ROUND, KEY_ACC_RTYPEPRICERB, KEY_ACC_TONEAREST, KEY_Acc_VATRATE, KEY_Acc_MINPRICE, KEY_Acc_EXTDROP,
            KEY_ACC_UNIT, KEY_ACC_COST, KEY_DRV_ROUND, KEY_DRV_RTYPEPRICERB, KEY_DRV_TONEAREST, KEY_DRV_VATRATE,
            KEY_DRV_MINPRICE, KEY_DRV_EXTDROP, KEY_DRV_UNIT, KEY_DRV_COST, KEY_DRV_PERC, KEY_STATUS};

    public SQLiteDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATION_TABLE = "CREATE TABLE " + TABLE_NAME + " ( "
                + "id STRING PRIMARY KEY, " + "RateName TEXT, "
                + "OfficeID TEXT, " + "VehicleType TEXT, " + "vehsymbol TEXT, " + "Acc_Round TEXT, " +
                "Acc_RTypePriceRb TEXT, " + "Acc_ToNearest TEXT, " + "Acc_VatRate TEXT, " + "Acc_MinPrice TEXT, " +
                "Acc_ExtDrop TEXT, " + "Acc_Unit BLOB, " + "Acc_cost BLOB, " + "Drv_Round TEXT, " + "Drv_RTypePriceRb TEXT, " +
                "Drv_ToNearest TEXT, " + "Drv_VatRate TEXT, " + "Drv_MinPrice TEXT, " + "Drv_ExtDrop TEXT, " +
                "Drv_Unit BLOB, " + "Drv_Cost BLOB, " + "Drv_Perc TEXT, " + "status boolean )";

        //History Table
        String HISTORY_TABLE = "CREATE TABLE " + TABLE_HISTORY + " ( "
                + "history_Id INTEGER PRIMARY KEY AUTOINCREMENT, " + "date TEXT, " + "time TEXT, " +
                "origin TEXT, " + "destination TEXT, " + "fare DOUBLE, " + "status TEXT, "
                + "jobID TEXT, " + "jobRef TEXT, " + "dateNtime DOUBLE)";

        db.execSQL(CREATION_TABLE);
        db.execSQL(HISTORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP Table if EXISTS " + TABLE_NAME);
        this.onCreate(db);
    }

    public void addFareDataToSQLite(FareListMapping farelist) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, farelist.get_id());
        values.put(KEY_RATENAME, farelist.getRateName());
        values.put(KEY_OFFICEID, farelist.getOfficeID());
        values.put(KEY_VEHICLETYPE, farelist.getVehicleType());
        values.put(KEY_VEHSYMBOL, farelist.getVehSymbol());
        values.put(KEY_ACC_ROUND, farelist.getAcc_Round());
        values.put(KEY_ACC_RTYPEPRICERB, farelist.getAcc_RTypePriceRb());
        values.put(KEY_ACC_TONEAREST, farelist.getAcc_ToNearest());
        values.put(KEY_Acc_VATRATE, farelist.getAcc_VatRate());
        values.put(KEY_Acc_MINPRICE, farelist.getAcc_MinPrice());
        values.put(KEY_Acc_EXTDROP, farelist.getAcc_ExtDrop());
        values.put(KEY_ACC_UNIT, String.valueOf(farelist.getAcc_Unit()));
        values.put(KEY_ACC_COST, String.valueOf(farelist.getAcc_Cost()));
        values.put(KEY_DRV_ROUND, farelist.getDrv_Round());
        values.put(KEY_DRV_RTYPEPRICERB, farelist.getDrv_RTypePriceRb());
        values.put(KEY_DRV_TONEAREST, farelist.getDrv_ToNearest());
        values.put(KEY_DRV_VATRATE, farelist.getDrv_VATRate());
        values.put(KEY_DRV_MINPRICE, farelist.getDrv_MinPrice());
        values.put(KEY_DRV_EXTDROP, farelist.getDrv_ExtDrop());
        values.put(KEY_DRV_UNIT, String.valueOf(farelist.getDrv_Unit()));
        values.put(KEY_DRV_COST, String.valueOf(farelist.getDrv_Cost()));
        values.put(KEY_DRV_PERC, farelist.getDrv_Perc());
        values.put(KEY_STATUS, farelist.isStatus());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public void deleteFareData() {
        // Get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
    }

    //getting values in cursor and setting again in class farelistmapping
    @SuppressLint("NewApi")
    public Cursor getCarSymbol(String vehSymbol, FareListMapping fare) {

        Cursor cursor = null;

        try {

            SQLiteDatabase db = this.getReadableDatabase();
            String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_VEHSYMBOL + " = '" + vehSymbol + "'";
            //  cursor = db.rawQuery(query, new String[]{vehSymbol}, null);

            cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            fare.set_id(String.valueOf(cursor.getString(0)));
            fare.setRateName(String.valueOf(cursor.getString(1)));
            fare.setOfficeID(String.valueOf(cursor.getString(2)));
            fare.setVehicleType(String.valueOf(cursor.getString(3)));
            fare.setVehSymbol(String.valueOf(cursor.getString(4)));
            fare.setAcc_Round(String.valueOf(cursor.getString(5)));
            fare.setAcc_RTypePriceRb(String.valueOf(cursor.getString(6)));
            fare.setAcc_ToNearest(String.valueOf(cursor.getString(7)));
            fare.setAcc_VatRate(String.valueOf(cursor.getString(8)));
            fare.setAcc_MinPrice(String.valueOf(cursor.getString(9)));
            fare.setAcc_ExtDrop(String.valueOf(cursor.getString(10)));
            fare.setAcc_Unit(getarrayList(cursor, cursor.getColumnName(11)));
            fare.setAcc_Cost(getarrayList(cursor, cursor.getColumnName(12)));
            fare.setDrv_Round(String.valueOf(cursor.getString(13)));
            fare.setDrv_RTypePriceRb(String.valueOf(cursor.getString(14)));
            fare.setDrv_ToNearest(String.valueOf(cursor.getString(15)));
            fare.setDrv_VATRate(String.valueOf(cursor.getString(16)));
            fare.setDrv_MinPrice(String.valueOf(cursor.getString(17)));
            fare.setDrv_ExtDrop(String.valueOf(cursor.getString(18)));
            fare.setDrv_Unit(getarrayList(cursor, cursor.getColumnName(19)));
            fare.setDrv_Cost(getarrayList(cursor, cursor.getColumnName(20)));
            fare.setDrv_Perc(String.valueOf(cursor.getString(21)));
            boolean flag = cursor.getString(22).equals("1");
            fare.setStatus(flag);
            FareListMapping.sharedInstanceFareList = fare;

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cursor;
    }

    private ArrayList<String> getarrayList(Cursor cursor, String coloumName) {
        ArrayList<String> mylist = new ArrayList<>();

        if (cursor.getCount() > 0) {
            String accAccount = cursor.getString(cursor.getColumnIndex(coloumName));
            accAccount = accAccount.replaceAll("\\[", "").replaceAll("\\]", "");

            String[] array = accAccount.split(",");

            for (int i = 0; i < array.length; i++) {

                mylist.add(array[i]);
            }
        }
        return mylist;
    }

    public void addNearestOffice(String nearestOffice, double lat, double lon, String number) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("nstOffice", nearestOffice);
        value.put("nstOfficeLat", lat);
        value.put("nstOfficeLon", lon);
        value.put("mobileNumber", number);
        db.insert("nearestOffce", null, value);
    }

    public void insertBookingForHistory(String date, String time, String origin,
                                        String dest, double fare, String status, String jobID, String jobRef, long dateNtime) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_BOOKING_DATE, date);
            values.put(KEY_BOOKING_TIME, time);
            values.put(KEY_BOOKING_ORIGIN, origin);
            values.put(KEY_BOOKING_DESTINATION, dest);

            values.put(KEY_BOOKING_FARE, fare);
            values.put(KEY_BOOKING_STATUS, status);
            values.put(KEY_BOOKING_JOBID, jobID);
            values.put(KEY_BOOKING_JOBREF, jobRef);
            values.put(KEY_BOOKING_DATENTIME, dateNtime);

            db.insert(TABLE_HISTORY, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void insertBookingForHistoryWithVias(String date, String time, String origin,
//                                        String dest, double fare, String status, String jobID, String jobRef, long dateNtime,ArrayList<String> waypointAddress){
//
//        String vias="";
//       String strSeparator = "_^_";
//            for (int i = 0;i<waypointAddress.size(); i++) {
//                vias = vias+waypointAddress.get(i);
//                // Do not append comma at the end of last element
//                if(i<waypointAddress.size()-1){
//                    vias = vias+strSeparator;
//                }
//            }
//
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            ContentValues values = new ContentValues();
//            values.put(KEY_BOOKING_DATE, date);
//            values.put(KEY_BOOKING_TIME, time);
//            values.put(KEY_BOOKING_ORIGIN, origin);
//            values.put(KEY_BOOKING_DESTINATION, dest);
//            //KEY_BOOKING_VIAS
//            values.put(KEY_BOOKING_VIAS,vias);
//            values.put(KEY_BOOKING_FARE, fare);
//            values.put(KEY_BOOKING_STATUS, status);
//            values.put(KEY_BOOKING_JOBID, jobID);
//            values.put(KEY_BOOKING_JOBREF, jobRef);
//            values.put(KEY_BOOKING_DATENTIME, dateNtime);
//
//            db.insert(TABLE_HISTORY, null, values);
//        } catch(Exception e){
//            e.printStackTrace();
//        }
//    }


    public ArrayList<BookingHistory> getAllHistory(String status) {
        ArrayList<BookingHistory> bookingList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //Cursor res = db.rawQuery("select * from " + TABLE_HISTORY  );
            String query = "SELECT * FROM " + TABLE_HISTORY + " WHERE " + KEY_BOOKING_STATUS + " = '" + status + "'";

            Cursor res = db.rawQuery(query, null);

            while (res.moveToNext()) {
                int id = res.getInt(0);
                String date = res.getString(1);
                String time = res.getString(2);
                String origin = res.getString(3);
                String dest = res.getString(4);
                double fare = Double.parseDouble(res.getString(5));
                String stats = res.getString(6);
                String jobID = res.getString(7);
                String jobRef = res.getString(8);
                long dateNtime = res.getLong(9);


                BookingHistory bookingHistory = new BookingHistory(date, time, origin, dest, Math.round(fare), stats, jobID, jobRef, dateNtime, id);
                bookingList.add(bookingHistory);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookingList;
    }

    public void del(int id) {
        String[] srgs = new String[]{
                String.valueOf(id)
        };
        getWritableDatabase().delete(TABLE_HISTORY, "history_Id=?", srgs);

    }

    public void update(String date, String time, String origin,
                       String dest, double fare, String status, String jobID, String jobRef, double dateNtime) {


        String[] srgs = new String[]{
                String.valueOf(BookingHistory.sharedinstance.getBookingID())
        };


        ContentValues values = new ContentValues();
        values.put(KEY_BOOKING_DATE, date);
        values.put(KEY_BOOKING_TIME, time);
        values.put(KEY_BOOKING_ORIGIN, origin);
        values.put(KEY_BOOKING_DESTINATION, dest);
        values.put(KEY_BOOKING_FARE, fare);
        values.put(KEY_BOOKING_STATUS, status);
        values.put(KEY_BOOKING_JOBID, jobID);
        values.put(KEY_BOOKING_JOBREF, jobRef);
        values.put(KEY_BOOKING_DATENTIME, dateNtime);

        this.getWritableDatabase().update(TABLE_HISTORY, values, "history_Id=?", srgs);


    }

    public ArrayList<String> getids(String status) {
        ArrayList<String> bookingList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //Cursor res = db.rawQuery("select * from " + TABLE_HISTORY  );
            String query = "SELECT * FROM " + TABLE_HISTORY + " WHERE " + KEY_BOOKING_STATUS + " = '" + status + "'";

            Cursor res = db.rawQuery(query, null);

            while (res.moveToNext()) {


                String jobID = res.getString(7);

                bookingList.add(jobID);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookingList;
    }


}