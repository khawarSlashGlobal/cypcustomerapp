package buzybeez.com.buzybeezcabcrystalpalace.DataHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteCustomerProfile extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "mysql";
    private static final int DB_VER = 1;
    private final String TABLE_NAME = "Profile";
    private final String COLUMN_ID = "id";
    private final String COLUMN_NAME = "ProfileImage";


    public SQLiteCustomerProfile(Context context) {
        super(context, DATABASE_NAME, null, DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS Profile(id INTEGER PRIMARY KEY AUTOINCREMENT,ProfileImage BLOB)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public long addprofile(byte[] image) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("ProfileImage", image);
        return getWritableDatabase().insert("Profile", null, contentValues);

    }

    public byte[] getprofile() {
        byte[] name = null;

        String col = "ProfileImage";

        Cursor cursor = getReadableDatabase().query("Profile", new String[]{col}, null, null, null, null, null);

//           while(cursor.moveToNext()) {
//               int image_index = cursor.getColumnIndex("ProfileImage");
//                name =cursor.getBlob(image_index);
//
//          }
        try {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    int image_index = cursor.getColumnIndex("ProfileImage");
                    name = cursor.getBlob(image_index);
                }
            }
            assert cursor != null;
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return name;
    }


}
