package buzybeez.com.buzybeezcabcrystalpalace.InternetStatusBar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import buzybeez.com.buzybeezcabcrystalpalace.Interfaces.OnNetworkConnectionListener;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.Network;
import buzybeez.com.buzybeezcabcrystalpalace.Utils.SocketEvent;

/**
 * Created by hv on 4/24/18.
 */

public class NetworkStateChangeReceiver extends BroadcastReceiver {

    public static final String NETWORK_AVAILABLE_ACTION = "buzybeez.com.buzybeezcabsurbiton.NetworkAvailable";
    public static final String IS_NETWORK_AVAILABLE = "isNetworkAvailable";
    private OnNetworkConnectionListener ref;


    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Intent networkStateIntent = new Intent(NETWORK_AVAILABLE_ACTION);
            networkStateIntent.putExtra(IS_NETWORK_AVAILABLE, isConnectedToInternet(context));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    private boolean isConnectedToInternet(Context context) {
        try {
            ref = (OnNetworkConnectionListener) context;
            if (context != null && (Network.isNetworkAvailable(context))) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                ref.recieveMsgOnNetwork("Connected");

                if (!SocketEvent.sharedinstance.socket.connected()) {

                    SocketEvent.sharedinstance.socketConnectAfterDisconnect();
                }

                return networkInfo != null && networkInfo.isConnected();
            } else {
                ref.recieveMsgOnNetwork("Disonnected");
                SocketEvent.sharedinstance.disconnectSocket();
                return false;
            }
        } catch (NullPointerException e) {
            Log.e(NetworkStateChangeReceiver.class.getName(), e.getMessage());
            return false;
        }
    }
}
