package buzybeez.com.buzybeezcabcrystalpalace.Email;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.Security;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import android.app.AlertDialog.Builder;
import android.util.Log;

import buzybeez.com.buzybeezcabcrystalpalace.Models.CfgCustApp;
import buzybeez.com.buzybeezcabsurbiton.Email.JSSEProvider;

public class GMailSender extends javax.mail.Authenticator
{
    //   private String mailhost = "smtp.gmail.com";
    private String mailhost = CfgCustApp.sharedinstance.getSmtpHost();
    private String user;
    private String password;
    private Session session;
    private Multipart _multipart;

    static
    {
        Security.addProvider(new JSSEProvider());
    }

    public GMailSender(String user, String password)
    {
        this.user = user;
        this.password = password;

        Properties props = new Properties();
        props.put("mail.smtp.host", CfgCustApp.sharedinstance.getSmtpHost());
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", CfgCustApp.sharedinstance.getSmtpPort());

        session = Session.getDefaultInstance(props, this);
        _multipart = new MimeMultipart();
    }

    protected PasswordAuthentication getPasswordAuthentication()
    {
        return new PasswordAuthentication(user, password);
    }

    public synchronized void sendMail(String subject, String body, String sender, String recipients) throws Exception
    {
        MimeMessage mm = new MimeMessage(session);
        mm.setFrom(new InternetAddress(sender));
        mm.addRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
        mm.setSubject(subject);
        mm.setText(body);
        _multipart = new MimeMultipart();
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(body,"text/html");
        _multipart.addBodyPart(messageBodyPart);
        mm.setContent(_multipart);
        Transport.send(mm);


    }

    public void addAttachment(String filename) throws Exception
    {
        BodyPart messageBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(filename);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename);

        _multipart.addBodyPart(messageBodyPart);
    }

    public class ByteArrayDataSource implements DataSource
    {
        private byte[] data;
        private String type;

        public ByteArrayDataSource(byte[] data, String type)
        {
            super();
            this.data = data;
            this.type = type;
        }

        public ByteArrayDataSource(byte[] data)
        {
            super();
            this.data = data;
        }

        public void setType(String type)
        {
            this.type = type;
        }

        public String getContentType()
        {
            if (type == null)
                return "application/octet-stream";
            else
                return type;
        }

        public InputStream getInputStream() throws IOException
        {
            return new ByteArrayInputStream(data);
        }

        public String getName()
        {
            return "ByteArrayDataSource";
        }

        public OutputStream getOutputStream() throws IOException
        {
            throw new IOException("Not Supported");
        }
    }
}